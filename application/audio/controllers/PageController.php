<?php

/**
 * Description of PageController
 *
 * @author Aram
 */
class Audio_PageController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function viewAction() {
        Lingua_Auth::rejectUnlessAuth("USER");

        //loggined user lingua id
        $lingua_id = Lingua_Auth::getLinguaId();

        //getting current audio id
        $audio_id = $this->getParam('audio_id');

        //set the view counter
        //if current user has been view this video this function gonna to do nothing
        $result = Lingua_Query_Audio::audioSetView($audio_id, $lingua_id);
        if ($result) {
            Lingua_Query_Audio::audioCompileViews($audio_id);
        }
        // selecting audio by got id
        $AUDIO = new Lingua_Selected_Audio($audio_id);

        //if tha audio is not avaliable or isn't exists redirect to error page
        $AUDIO->rejectUnlessValid();

        //getting user unknow words with translations
        $this->view->words = Lingua_Query_User::userGetUnknownWordsByAudio($audio_id, $lingua_id);

        //audio object
        $this->view->AUDIO = $AUDIO->AUDIO;
        $text = nl2br($AUDIO->AUDIO->text1);

        //split string by spaces, ',' and . to get words count in the string
        $data = preg_split('/\s+|,|\.|\[|\]|\(|\)/', $text, null, PREG_SPLIT_NO_EMPTY);

        $this->view->word_count = count($data);

        $this->view->PAGE = $PAGE = new Page_Audiopage($AUDIO->AUDIO);
        $paid = $this->view->paid_status = Lingua_Query_User::userIsPaid($lingua_id);

        if (!$paid) {
            $this->view->credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
        }
        // set the view counter
        // if current user has been view this audio this function gonna to do nothing
        Lingua_Query_Audio::audioSetView($audio_id, $lingua_id);

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss
        Lingua_GlobalKeys::storeKey('audio_page', $key);
    }

    //--------------------------------------------------------------------------
    public function getUserUnknowWordsAction() {
        $audio_id = $this->getParam('audio_id');
        //getting user unknow words with translations
        $words = Lingua_Query_User::userGetUnknownWordsByAudio($audio_id, Lingua_Auth::getLinguaId());

        echo json_encode(array('data' => $words));
        exit;
    }

    //--------------------------------------------------------------------------
    public function audioExamineAction() {
        $this->_helper->layout->disableLayout();

        $audio_id = $this->view->audio_id = $this->getParam('audio_id');
        $page = $this->getParam('page');

        $AUDIO = new Lingua_Selected_Audio($audio_id);
        $AUDIO->rejectUnlessValid();

        $this->view->AUDIO = $AUDIO->AUDIO;

        $examiner = $this->view->examiner = new Page_Examiner($AUDIO->AUDIO, $page);
        $examiner->setExamine();
    }

    //--------------------------------------------------------------------------
    public function closeMessageAction() {
        $action_name = $this->getParam('action_name');

        $action = Lingua_Query_System_Actions::systemActionsGet($action_name);
        $action = new Lingua_Object($action);

        $action->dump();
        if ($action->system_actions_id)
            Lingua_Query_System_Messages::messageSetShown($action->system_actions_id, Lingua_Auth::getLinguaId());
        echo 'done';
        exit;
    }

    //--------------------------------------------------------------------------
    public function audioGetTranslationAction() {
        try {
            Lingua_Query_System_Actions::beginTransaction();
            $action_name = "get-text-translation";
            $account_state = Lingua_Auth::isVIPAccount();
            $lingua_id = Lingua_Auth::getLinguaId();

            if (!$account_state) {
                //if the payment message is not shown to user, show it!
                $action = Lingua_Query_System_Actions::systemActionsGet($action_name);
                $action = new Lingua_Object($action);

                $status = Lingua_Query_System_Messages::messageIsShow($action->system_actions_id, $lingua_id);
                if (!$status) {
                    $message = "Во Free статусе авторский перевод ограничен запасом звезд:
1 авторский перевод=1 звезда.<br /><br />
Узнайте как можно добыть звезды или получите Premium статус для полного доступа ко всем возможностям Langwee и снятием всех ограничений!";

                    Lingua_Query_System_Actions::commit();
                    echo json_encode(array('result' => 'fail', 'reason' => 'showMessage', 'message' => $message, 'action' => $action_name));
                    exit;
                }

                $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
                if ($credit <= $action->stars) {
                    echo json_encode(array('result' => 'fail', 'reason' => 'noCredit'));
                    Lingua_Query_System_Actions::commit();
                    exit;
                }
            }
            $audio_id = $this->getParam('audio_id');

            // select audio by got id
            $AUDIO = new Lingua_Selected_Audio($audio_id);

            $PAGE = new Page_Audiopage($AUDIO->AUDIO);
            $words = $PAGE->getTextTrgWords();

            echo json_encode(array('data' => $words));
            if (!$account_state) {
                Lingua_Query_User_Credit::userCreditWithdraw($lingua_id, $action->stars);
                Lingua_Query_User_Credit_History::creditHistorySet($action->system_actions_id, $lingua_id);
            }
            Lingua_Query_System_Actions::commit();
        } catch (Exception $e) {
            
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function wordAddAction() {
        //reject if user is not logged in
        Lingua_Auth::rejectUnlessAuth("USER");

        //is paid for standart account
        $account_state = Lingua_Auth::isVIPAccount();

        //loggned in lingua id
        $lingua_id = Lingua_Auth::getLinguaId();

        if (!$account_state) {
            //if the payment message is not shown to user, show it!
            $action = Lingua_Query_System_Actions::systemActionsGet('translate');
            $action = new Lingua_Object($action);

            $status = Lingua_Query_System_Messages::messageIsShow($action->system_actions_id, $lingua_id);
            if (!$status) {
                $message = "Во Free статусе количество новых слов, которые вы можете переводить и добавлять в словарь, ограничено запасом звезд:
<b>1 слово=1 звезда</b>.<br /><br />
Узнайте как можно добыть звезды или получите Premium статус для полного доступа ко всем возможностям Langwee и снятием всех ограничений!";
                echo json_encode(array('result' => 'fail', 'reason' => 'showMessage', 'message' => $message, 'action' => 'translate'));
                exit;
            }

            $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
            if ($credit <= $action->stars) {
                echo json_encode(array('result' => 'fail', 'reason' => 'noCredit'));
                exit;
            }
        }

        //source word english, france, german , italiy , etc.
        $word_src = $this->getParam('word');

        //audio id which belongs the word
        $audio_id = $this->getParam('audio_id');

        //word source language
        $word_src_lang = $this->getParam('audio_lang', 1);

        //word target language, it should take language id from DB!
        $word_trg_lang = $this->getParam('trg_lang', 6);

        //word context in the text
        $context = $this->getParam('context');
        $context = preg_replace('/\s+|\n/', ' ', $context);
        $context = trim($context);
        //get world translation
        $word_trg = Lingua_Query_User::userGetUnknowWordFromLocalDB($word_src);
        if (!$word_trg) {
            //google translate api helper object
            $obj = new Lingua_Helper_Google();

            $data = $obj->translate($word_src, $word_trg_lang, $word_src_lang);
            $word_trg = $data;
        }

        //get the audio id from which the word has taken
        $w_trg = preg_replace('/\s/', '', $word_trg);
        $w_src = preg_replace('/\s/', '', $word_src);

        $w_trg = strtolower($w_trg);
        $w_src = strtolower($w_src);

        if (empty($w_trg) || empty($w_src) || $w_trg == $w_src) {
            echo json_encode(array('result' => 'fail', 'reason' => 'noTrasnaltion'));
            exit;
        }
echo $audio_id;
        $word_id = Lingua_Query_User::userAddUnknowAudioWord($w_trg, $w_src, $word_src_lang, $word_trg_lang, Lingua_Auth::getLinguaId(), $audio_id, $context);
        echo json_encode(array('result' => 'success', 'word_src' => $word_src, 'word_trg' => $word_trg, 'audio_id' => $audio_id, 'user_word_id' => $word_id));

        if (!$account_state) {
            Lingua_Query_User_Credit::userCreditWithdraw($lingua_id, $action->stars);
            Lingua_Query_User_Credit_History::creditHistorySet($action->system_actions_id, $lingua_id);
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function wordTranslationViewAction() {
        $audio_id = $this->getParam('audio_id');

        $this->_helper->layout->disableLayout();
        $this->view->words = Lingua_Query_User::userGetUnknownWordsByAudio($audio_id, Lingua_Auth::getLinguaId());
    }

    //--------------------------------------------------------------------------
    public function wordGetTranslationAction() {
        //getting the word which will be translated
        $word = $this->getParam('word');
        //security key
        $key = $this->getParam('key');
        $token = Lingua_GlobalKeys::getKey('audio_page');

        if (strcmp($key, $token) !== 0) {
            echo "An security error has occured";
            exit;
        }

        //searching got word in local db        
        $data = Lingua_Query_User::userGetUnknowWordFromLocalDB($word);
        if ($data) {

            echo $data;
            exit;
        }

        //google translate api helper object
        $obj = new Lingua_Helper_Google();

        $data = $obj->translate($word, 'ru', 'en');
        echo $data;
        exit;
    }

    //--------------------------------------------------------------------------
    public function wordSpeechAction() {
        //getting the word which will be speech
        $word_id = $this->getParam('word');

        //security key
        $key = $this->getParam('key');
        $token = Lingua_GlobalKeys::getKey('audio_page');

        if (strcmp($key, $token) !== 0) {
            echo "An security error has occured";
            exit;
        }

        $obj = new Lingua_Helper_Google();
        $obj->createTmpFile($word_id);
        exit;
    }

    //--------------------------------------------------------------------------
    public function audioValidationAction() {
        $audios = Lingua_Query_Audio::audioGetAll_();
        //     Daz_Debug::dump($audios);
        foreach ($audios as $audio) {
            $obj = new Lingua_Object($audio);
            $page = new Page_Audiopage($obj);

            if (!$page->compareAction()) {
                echo "<a href='http://langwee.com/admin/panel/audio-edit/audio_id/" . $obj->audio_id . "'>" . $obj->title . "</a> | " . $obj->audio_lang . "<br />";
                flush();
                ob_flush();
            }
        }

        exit;
    }

    //--------------------------------------------------------------------------
    public function discussAction() {
        $this->_helper->layout->disableLayout();

        $from = $this->getParam('from');
        $limit = $this->getParam('limit');
        $audio_id = $this->getParam('audio_id');
        $user_role = Lingua_Query_User::userGetRole(Lingua_Auth::getLinguaId());
        if (!$limit)
            $limit = 20;
        if (!$from)
            $from = 0;

        $data = Lingua_Query_Audio_Discuss::discussGet($from, $limit, $audio_id);

        foreach ($data as $k => $d) {
            $data[$k]['photo'] = '/getimage/60/60';
            if ($d['lingua_id'] == Lingua_Auth::getLinguaId() || $user_role == "ADMIN") {
                $data[$k]['owner'] = true;
            } else {
                $data[$k]['owner'] = false;
            }
        }

        $data['own_photo'] = '/getimage/60/60';
        echo json_encode(array('data' => $data));

        exit;
    }

    //--------------------------------------------------------------------------
    public function discussAddAction() {
        $text = $this->getParam('text');
        $audio_id = $this->getParam('audio_id');
        $lingua_id = Lingua_Auth::getLinguaId();

        $result = Lingua_Query_Audio_Discuss::discussInsert($text, $audio_id, $lingua_id);
        echo json_encode(array('result' => $result));
        exit;
    }

    //--------------------------------------------------------------------------
    public function discussRemoveAction() {
        $discuss_id = $this->getParam('discuss_id');

        if (Lingua_Query_User::userGetRole(Lingua_Auth::getLinguaId()) == "ADMIN") {
            $result = Lingua_Query_Audio_Discuss::discussDelete($discuss_id);
        } else {
            $result = Lingua_Query_Audio_Discuss::discussDeleteOwn($discuss_id, Lingua_Auth::getLinguaId());
        }
        echo json_encode(array('result' => 'success', 'discuss_id' => $discuss_id));
        exit;
    }

    //--------------------------------------------------------------------------
    public function discussUpdatesAction() {
        $this->_helper->layout->disableLayout();

        $discuss_id = $this->getParam('id');
        $audio_id = $this->getParam('audio_id');
        $user_role = Lingua_Query_User::userGetRole(Lingua_Auth::getLinguaId());

        $data = Lingua_Query_Audio_Discuss::discussGet($discuss_id, 20, $audio_id);
        foreach ($data as $k => $d) {
            '/getimage/60/60';
            if ($d['lingua_id'] == Lingua_Auth::getLinguaId() || $user_role == "ADMIN") {
                $data[$k]['owner'] = true;
            } else {
                $data[$k]['owner'] = false;
            }
        }

        echo json_encode(array('data' => $data));

        exit;
    }

}
