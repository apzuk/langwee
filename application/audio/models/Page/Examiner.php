<?php

class Page_Examiner {
    private $SESS;
    private $AUDIO;
    private $exception = array("i", "if", "and", "you", "me", "to", "us", "in");

    public function __construct($audio_object, $page = null) {
        $this->SESS = new Daz_Session(__CLASS__ . '_' . $audio_object->audio_id . '_' . Lingua_Auth::getLinguaId());

        $this->AUDIO = $audio_object;
        $this->SESS->count = (int) $this->SESS->count;
        
        if(!$page) $this->cleanResult();
    }

    //--------------------------------------------------------------------------
    public function getExaminedCount() {
        return $this->SESS->count;
    }

    //--------------------------------------------------------------------------
    public function setExamine() {
        $this->SESS->count++;
    }

    //--------------------------------------------------------------------------
    public function cleanResult() {
        $this->SESS->count = 0;
    }

    //--------------------------------------------------------------------------
    public function isExpired() {
        return ($this->SESS->count > 5) ? true : false;
    }

    //--------------------------------------------------------------------------
    public function getExaminingRows() {
        $t1 = nl2br($this->AUDIO->text1); //source text
        $t2 = nl2br($this->AUDIO->text2); //target text

        //break to parts by <br />
        $data_t1 = preg_split('/<br\s\/>/', $t1, -1, PREG_SPLIT_NO_EMPTY);
        $data_t2 = preg_split('/<br\s\/>/', $t2, -1, PREG_SPLIT_NO_EMPTY);

        $data_new = $this->getRowsArray($data_t1, $data_t2);

        //select a line in the text random
        $random = rand(0, count($data_new['data1']) - 1);

        $data = array();

        //learing all spaces including \n, \t
        $data['src'] = $this->SESS->src_text[$this->SESS->count] = trim($data_new['data1'][$random]);
        $data['trg'] = $this->SESS->trg_text[$this->SESS->count] = trim($data_new['data2'][$random]);

        //clearing comma from end of the text
        return new Lingua_Object($data);
    }
    
    //--------------------------------------------------------------------------
    private function getRowsArray($data1, $data2) {
        $data_new = array();
        
        //unique array to get different rows
        $data1 = array_unique($data1);
        
        //check array rows to filter normal rows
        foreach ($data1 as $key=>$d) {
            $tmp = trim($d);
            if (!empty($tmp)&&$this->hasEnoughtWords($d, 3)) {
                $data_new['data1'][] = trim($d);
                $data_new['data2'][] = trim($data2[$key]);
            }
        }
        return $data_new;
    }
    
    //--------------------------------------------------------------------------
    private function hasEnoughtWords($row, $words_count) {
        //the row contains more the 3 words 
        // and that words shodd be different , max 2 such words in the row
        $data = preg_split('/\s+|,|\.|\[|\]|\(|\)/', $row, null, PREG_SPLIT_NO_EMPTY);
       
        foreach($data as $d) {
            $counter = 0;
            foreach($data as $cur) {
                $b = false;
                for($iterator = 0 ; $iterator < count($this->exception) ; $iterator++) {
                    if($this->exception[$iterator] == strtolower($d)) 
                    {
                        $b = true; 
                        break;
                    }
                }
                if($b) continue;
                
                if(strtolower($cur) == strtolower($d)) {
                    $counter++;
                }
            }
            if($counter > 2) return false;
        }
        return count($data) > $words_count ? true : false;
    }

}
