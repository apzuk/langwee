<?php

if (file_exists(PROD_INI)) {
    $conf_file = PROD_INI;
} else {
    $conf_file = APPLICATION_INI;
}

$configs = new Zend_Config_Ini($conf_file);
Zend_Registry::set('configs', $configs);

Daz_Config::init();
setlocale(LC_ALL, 'ru_RU.UTF8', 'rus_RUS.UTF8', 'Russian_Russia.UTF8');

$front = Zend_Controller_Front :: getInstance();
$front->addModuleDirectory(APPLICATION_PATH);
$front->registerPlugin(new Lingua_Plugin_Layout());

//registring custom routes
$router = $front->getRouter();

$router->addRoute(
        'activation', new Zend_Controller_Router_Route('/activate/:activation_code',
                array('module' => 'default', 'controller' => 'index', 'action' => 'activate'))
);

$router->addRoute(
        'video_view', new Zend_Controller_Router_Route('/video/:video_id',
                array('module' => 'video', 'controller' => 'page', 'action' => 'view'))
);

$router->addRoute(
        'user_view', new Zend_Controller_Router_Route('/user/:lingua_id',
                array('module' => 'profile', 'controller' => 'user', 'action' => 'view'))
);

$router->addRoute(
        'all_resources', new Zend_Controller_Router_Route('/resources',
                array('module' => 'resources', 'controller' => 'list', 'action' => 'all'))
);

$router->addRoute(
        'audio_view', new Zend_Controller_Router_Route('/audio/:audio_id',
                array('module' => 'audio', 'controller' => 'page', 'action' => 'view'))
);

$router->addRoute(
        'auth_block', new Zend_Controller_Router_Route('/blocked',
                array('module' => 'default', 'controller' => 'index', 'action' => 'blocked'))
);

$router->addRoute(
        'admin_login', new Zend_Controller_Router_Route('/lets-admin-me',
                array('module' => 'admin', 'controller' => 'index', 'action' => 'login'))
);

$router->addRoute(
        'user_profile', new Zend_Controller_Router_Route('/profile',
                array('module' => 'profile', 'controller' => 'user', 'action' => 'view'))
);

$router->addRoute(
        'login', new Zend_Controller_Router_Route('/login',
                array('module' => 'default', 'controller' => 'index', 'action' => 'login'))
);

$router->addRoute(
        'logout', new Zend_Controller_Router_Route('/logout',
                array('module' => 'default', 'controller' => 'index', 'action' => 'logout'))
);

$router->addRoute(
        'trainings', new Zend_Controller_Router_Route('/trainings',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'list'))
);

$router->addRoute(
        'constructor', new Zend_Controller_Router_Route('/constructor',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'constructor'))
);

$router->addRoute(
        'listening-translating', new Zend_Controller_Router_Route('/listening-translating',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'listening-translating'))
);

$router->addRoute(
        'wordTranslate', new Zend_Controller_Router_Route('/word-translate',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'word-translate'))
);

$router->addRoute(
        'translateWord', new Zend_Controller_Router_Route('/translate-word',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'translate-word'))
);

$router->addRoute(
        'listening', new Zend_Controller_Router_Route('/listening',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'listening'))
);

$router->addRoute(
        'crossword', new Zend_Controller_Router_Route('/crossword',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'crossword'))
);

$router->addRoute(
        'phraseTranslate', new Zend_Controller_Router_Route('/phrase-translate',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'phrase-translate'))
);

$router->addRoute(
        'translatePhrase', new Zend_Controller_Router_Route('/translate-phrase',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'translate-phrase'))
);

$router->addRoute(
        'spelling', new Zend_Controller_Router_Route('/spelling',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'spelling'))
);

$router->addRoute(
        'dictionary', new Zend_Controller_Router_Route('/dictionary',
                array('module' => 'profile', 'controller' => 'user', 'action' => 'dictionary'))
);

$router->addRoute(
        'chat', new Zend_Controller_Router_Route('/chat',
                array('module' => 'community', 'controller' => 'community', 'action' => 'chat'))
);

$router->addRoute(
        'chat-room', new Zend_Controller_Router_Route('/chat-room/:chat_room',
                array('module' => 'community', 'controller' => 'community', 'action' => 'chat-room'))
);

$router->addRoute(
        'chat-invite', new Zend_Controller_Router_Route('/chat-invite/:lingua_id',
                array('module' => 'community', 'controller' => 'community', 'action' => 'chat-invite'))
);

$router->addRoute(
        'friends', new Zend_Controller_Router_Route('/friends',
                array('module' => 'community', 'controller' => 'community', 'action' => 'friends'))
);


$router->addRoute(
        'search', new Zend_Controller_Router_Route('/search',
                array('module' => 'community', 'controller' => 'community', 'action' => 'search'))
);


$router->addRoute(
        'search-page', new Zend_Controller_Router_Route('/search-page',
                array('module' => 'community', 'controller' => 'community', 'action' => 'search-page'))
);



$router->addRoute(
        'partnership', new Zend_Controller_Router_Route('/partnership',
                array('module' => 'about', 'controller' => 'services', 'action' => 'schools'))
);


$router->addRoute(
        'payment', new Zend_Controller_Router_Route('/payment',
                array('module' => 'about', 'controller' => 'services', 'action' => 'payment'))
);


$router->addRoute(
        'about', new Zend_Controller_Router_Route('/about',
                array('module' => 'about', 'controller' => 'services', 'action' => 'about'))
);

$router->addRoute(
        'feedback', new Zend_Controller_Router_Route('/feedback',
                array('module' => 'about', 'controller' => 'services', 'action' => 'feedback'))
);

$router->addRoute(
        'faq', new Zend_Controller_Router_Route('/faq',
                array('module' => 'about', 'controller' => 'services', 'action' => 'faq'))
);

$router->addRoute(
        'reviews', new Zend_Controller_Router_Route('/reviews',
                array('module' => 'about', 'controller' => 'services', 'action' => 'reviews'))
);

$router->addRoute(
        'credit', new Zend_Controller_Router_Route('/credit',
                array('module' => 'about', 'controller' => 'services', 'action' => 'credit'))
);

$router->addRoute(
        'credit', new Zend_Controller_Router_Route('/credit',
                array('module' => 'about', 'controller' => 'services', 'action' => 'oops'))
);

$router->addRoute(
        'kurs', new Zend_Controller_Router_Route('/kurs',
                array('module' => 'default', 'controller' => 'index', 'action' => 'kurs'))
);

$router->addRoute(
        'speech', new Zend_Controller_Router_Route('/speech/:word_id',
                array('module' => 'video', 'controller' => 'page', 'action' => 'word-speech1'))
);


$router->addRoute(
        'getimage', new Zend_Controller_Router_Route('/getimage/:width/:height/:lingua_id',
                array('module' => 'profile', 'controller' => 'user', 'action' => 'getimage'))
);


$router->addRoute(
        'oops', new Zend_Controller_Router_Route('/oops',
                array('module' => 'about', 'controller' => 'services', 'action' => 'oops'))
);

$router->addRoute(
        'main', new Zend_Controller_Router_Route('/main',
                array('module' => 'profile', 'controller' => 'user', 'action' => 'main'))
);

$router->addRoute(
        'training-coaching', new Zend_Controller_Router_Route('/training-coaching',
                array('module' => 'trainings', 'controller' => 'training', 'action' => 'training-coaching'))
);


$router->addRoute(
        'langwee', new Zend_Controller_Router_Route('/langwee',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee'))
);

$router->addRoute(
        'langwee1', new Zend_Controller_Router_Route('/langwee1',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee1'))
);

$router->addRoute(
        'langwee2', new Zend_Controller_Router_Route('/langwee2',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee2'))
);

$router->addRoute(
        'langwee3', new Zend_Controller_Router_Route('/langwee3',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee3'))
);

$router->addRoute(
        'langwee4', new Zend_Controller_Router_Route('/langwee4',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee4'))
);

$router->addRoute(
        'langwee5', new Zend_Controller_Router_Route('/langwee5',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee5'))
);

$router->addRoute(
        'langwee6', new Zend_Controller_Router_Route('/langwee6',
                array('module' => 'about', 'controller' => 'services', 'action' => 'langwee6'))
);

$router->addRoute(
        'coaching', new Zend_Controller_Router_Route('/coaching',
                array('module' => 'about', 'controller' => 'services', 'action' => 'coaching'))
);

$router->addRoute(
        'teachers', new Zend_Controller_Router_Route('/teachers',
                array('module' => 'about', 'controller' => 'services', 'action' => 'teachers'))
);


$router->addRoute(
        'ocoaching', new Zend_Controller_Router_Route('/ocoaching',
                array('module' => 'about', 'controller' => 'services', 'action' => 'ocoaching'))
);



$router->addRoute(
        '/mail', new Zend_Controller_Router_Route('/mail',
                array('module' => 'about', 'controller' => 'services', 'action' => 'mail'))
);

$router->addRoute(
        '/coaching-success', new Zend_Controller_Router_Route('/coaching-success',
                array('module' => 'about', 'controller' => 'services', 'action' => 'coaching-success'))
);

$router->addRoute(
        'coaching-send', new Zend_Controller_Router_Route('/coaching-send',
                array('module' => 'about', 'controller' => 'services', 'action' => 'coaching-send'))
);

$router->addRoute(
    'lesson-view', new Zend_Controller_Router_Route('/lesson/:lesson_id',
        array('module' => 'lessons', 'controller' => 'lessons', 'action' => 'view_lesson'))
);

$router->addRoute(
    'addword', new Zend_Controller_Router_Route('/words/addword',
        array('module' => 'words', 'controller' => 'words', 'action' => 'addword'))
);

$router->addRoute(
    'getunknownwords', new Zend_Controller_Router_Route('/words/getunknownwords',
        array('module' => 'words', 'controller' => 'words', 'action' => 'getunknownwords'))
);


unset($front);
//------------------------------------------------------------------------------
// renderer
$renderer = new Zend_Controller_Action_Helper_ViewRenderer();
$renderer->setViewSuffix('phtml');
Zend_Controller_Action_HelperBroker :: addHelper($renderer);
unset($renderer);

// layout
$layout = Zend_Layout :: startMvc();
$layout->setLayoutPath(APPLICATION_PATH . '/layouts/scripts');
$layout->setViewSuffix('phtml');

// view 
$view = $layout->getView();
$view->doctype('XHTML1_TRANSITIONAL');
$view->addScriptPath(APPLICATION_PATH . '/layouts/scripts');
unset($layout, $view);

// dispatch
Zend_Controller_Front :: getInstance()->dispatch();