<?php

/**
 * Description of Resources
 *
 * @author aramp
 */
class Api_ResourcesController extends Lingua_Plugin_Action {

    public function resourceAudioArtistsGetAction() {
        //requested page
        $page = $this->getParam('page', 0);

        //requested count of artists
        $limit = $this->getParam('limit', 10);

        //artists first letter
        $letter = $this->getParam('letter', false);

        //selected resource language
        $lang = Lingua_Auth::getLanguage();

        include_once APPLICATION_PATH . '/resources/models/List/Resourcespage.php';
        //resources model
        $RESOURCE = $this->view->RESOURCES = new List_Resourcespage();
        $RESOURCE->selectedLetter($letter, 'audio');

        //if first letter of the artists is numberic
        if ($letter == 'num') {
            //audio artists list by selected canditions
            $audio_artists = Lingua_Query_Audio::audioArtistsLookupNum($lang, $page * $limit, $limit);

            //audio artists count by selected canditions
            $audio_artists_count = Lingua_Query_Audio::audioArtistsLookupNumCount($lang);
        } else {
            //audio artists list by selected canditions
            $audio_artists = Lingua_Query_Audio::audioArtistsLookup($letter, $lang, $page * $limit, $limit);

            //audio artists count by selected canditions
            $audio_artists_count = Lingua_Query_Audio::audioArtistsLookupCount($letter, $lang);
        }

        $d = array_chunk($audio_artists, 5, true);

        $left = isset($d[0]) ? $d[0] : array();
        $right = isset($d[1]) ? $d[1] : array();

        echo json_encode(array(
            'artists_left' => (string) Lingua_View_Resources_Filters_Audio::audioArtistsRender($left),
            'artists_right' => (string) Lingua_View_Resources_Filters_Audio::audioArtistsRender($right),
            'total' => $audio_artists_count,
            'resource' => 'audio',
            'letter' => $letter,
            'page' => $page)
        );

        exit;
    }

    //-------------------------------------------------------------------
    public function resourceAudioFilterAction() {
        $this->_helper->layout->disableLayout();
        include_once APPLICATION_PATH . '/resources/models/List/Resourcespage.php';

        //resources model
        $RESOURCE = $this->view->RESOURCES = new List_Resourcespage();

        //requested page
        $page = $this->getParam('page', 0);

        //requested limit 
        $limit = $this->getparam('limit', 10);

        //user selected language(default is english)
        $lang = Lingua_Auth::getLanguage();

        //selected letter
        $letter = $RESOURCE->selectedLetterGet('audio');

        $this->view->aalphabet_rus = $RESOURCE->resourceGetAlphabet('audio', 'rus');
        $this->view->aalphabet_eng = $RESOURCE->resourceGetAlphabet('audio', 'eng');

        //if first letter of the artists is numberic
        if ($letter == 'num') {
            //audio artists list by selected canditions
            $this->view->artists = Lingua_Query_Audio::audioArtistsLookupNum($lang, $page * $limit, $limit);

            //audio artists count by selected canditions
            $this->view->artists_count = Lingua_Query_Audio::audioArtistsLookupNumCount($lang);
        } else {
            //audio artists list by selected canditions
            $this->view->artists = Lingua_Query_Audio::audioArtistsLookup($letter, $lang, $page * $limit, $limit);

            //audio artists count by selected canditions
            $this->view->artists_count = Lingua_Query_Audio::audioArtistsLookupCount($letter, $lang);
        }

        $this->view->page = $page;
        $this->view->letter = $letter;
    }

    //-------------------------------------------------------------------
    public function resourceAudioArtistsGetResourcesAction() {
        //user selected language
        $lang = Lingua_Auth::getLanguage();

        //artists id
        $artists_id = $this->getParam('artist_id');

        //lookup for artists audio resources
        $resource = Lingua_Query_Resources::resourcesAudioGetByArtist($artists_id, $lang);

        //artist name
        $artist_name = key($resource);

        //split result to two parts
        $d = array_chunk($resource[$artist_name], 10, true);

        //left and right parts of artist resources
        $left = isset($d[0]) ? $d[0] : array();
        $right = isset($d[1]) ? $d[1] : array();

        //response
        echo json_encode(array(
            'left' => (string) Lingua_View_Resources_Filters_Audio::audioListRender($left, 'Аудиозаписи \'' . $artist_name . '\'', false, 'left', 0),
            'right' => (string) Lingua_View_Resources_Filters_Audio::audioListRender($right, false, false, 'right', 0)
        ));
        exit;
    }

    //-------------------------------------------------------------------
    public function resourceAudioGetAllAction() {
        //requred page
        $page = $this->getParam('page', 0);

        //result limit
        $limit = $this->getParam('limit', 20);

        //system artist id
        $artist_id = $this->getParam('artist_id');

        $r = Lingua_Query_Resources::resourcesAudioGetAll(false, Lingua_Auth::getLanguage(), $artist_id, $page * $limit, $limit);
        $c = Lingua_Query_Resources::resourcesAudioGetAllCount(false, Lingua_Auth::getLanguage(), $artist_id);
        $d = array_chunk($r, 10, true);

        //left and right parts of artist resources
        $left = isset($d[0]) ? $d[0] : array();
        $right = isset($d[1]) ? $d[1] : array();

        //response
        echo json_encode(array(
            'left' => (string) Lingua_View_Resources_Filters_Audio::audioListRender($left, 'Все аудиозаписи(найдено ' . $c . ' аудиозаписей)', false, 'left', $page * $limit),
            'right' => (string) Lingua_View_Resources_Filters_Audio::audioListRender($right, false, false, 'right', $page * $limit + 10),
            'total' => $c,
            'page' => $page,
            'container' => 'audio'
        ));

        exit;
    }

    //#########################VIDEO#######################
    //-------------------------------------------------------------------
    public function resourceVideoArtistsGetAction() {
        //requested page
        $page = $this->getParam('page', 0);

        //requested count of artists
        $limit = $this->getParam('limit', 10);

        //artists first letter
        $letter = $this->getParam('letter', false);

        //selected resource language
        $lang = Lingua_Auth::getLanguage();

        include_once APPLICATION_PATH . '/resources/models/List/Resourcespage.php';
        //resources model
        $RESOURCE = $this->view->RESOURCES = new List_Resourcespage();
        $RESOURCE->selectedLetter($letter, 'video');

        if ($letter == 'num') {
            //lookup artists which first char is numeric
            $video_artists = Lingua_Query_Video::videoArtistsLookupNum($lang, $page * $limit, $limit);

            //lookup artists count which first char is numeric
            $video_artists_count = Lingua_Query_Video::videoArtistsLookupNumCount($lang);
        } else {
            //audio artists list by selected canditions
            $video_artists = Lingua_Query_Video::videoArtistsLookup($letter, $lang, $page * $limit, $limit);

            //audio artists count by selected canditions
            $video_artists_count = Lingua_Query_Video::videoArtistsLookupCount($letter, $lang);
        }

        $d = array_chunk($video_artists, 5, true);

        $left = isset($d[0]) ? $d[0] : array();
        $right = isset($d[1]) ? $d[1] : array();

        echo json_encode(array(
            'artists_left' => (string) Lingua_View_Resources_Filters_Video::videoArtistsRender($left),
            'artists_right' => (string) Lingua_View_Resources_Filters_Video::videoArtistsRender($right),
            'total' => $video_artists_count,
            'resource' => 'video',
            'letter' => $letter,
            'page' => $page)
        );


        exit;
    }

    //-------------------------------------------------------------------
    public function resourceVideoFilterAction() {
        $this->_helper->layout->disableLayout();

        include_once APPLICATION_PATH . '/resources/models/List/Resourcespage.php';
        //resources model
        $RESOURCE = $this->view->RESOURCES = new List_Resourcespage();

        //requested page
        $page = $this->getParam('page', 0);

        //requested count of resul;t
        $limit = $this->getparam('limit', 10);

        //user selected language(default is english)
        $lang = Lingua_Auth::getLanguage();

        //selected letter
        $letter = $RESOURCE->selectedLetterGet('video');

        $this->view->aalphabet_rus = $RESOURCE->resourceGetAlphabet('video', 'rus');
        $this->view->aalphabet_eng = $RESOURCE->resourceGetAlphabet('video', 'eng');

        if ($letter == 'num') {
            //lookup artists which first char is numeric
            $this->view->artists = Lingua_Query_Video::videoArtistsLookupNum($lang, $page * $limit, $limit);

            //lookup artists count which first char is numeric
            $this->view->artists_count = Lingua_Query_Video::videoArtistsLookupNumCount($lang);
        } else {
            //audio artists list by selected canditions
            $this->view->artists = Lingua_Query_Video::videoArtistsLookup($letter, $lang, $page * $limit, $limit);

            //audio artists count by selected canditions
            $this->view->artists_count = Lingua_Query_Video::videoArtistsLookupCount($letter, $lang);
        }

        $this->view->page = $page;
        $this->view->letter = $letter;
    }

    //-------------------------------------------------------------------
    public function resourceVideoArtistsGetResourcesAction() {
        //requested page
        $page = $this->getParam('page', 0);

        //requested limit
        $limit = $this->getParam('limit', 20);

        //user selected language
        $lang = Lingua_Auth::getLanguage();

        //artists id
        $artists_id = $this->getParam('artist_id');

        //lookup for artists audio resources
        $resource = Lingua_Query_Resources::resourcesVideoGetByArtist($artists_id, $lang, $page, $limit);

        //artist name

        $artist_name = key($resource);

        //split result to two parts
        $d = array_chunk($resource[$artist_name], 10, true);

        //left and right parts of artist resources
        $left = isset($d[0]) ? $d[0] : array();
        $right = isset($d[1]) ? $d[1] : array();

        //response
        echo json_encode(array(
            'left' => (string) Lingua_View_Resources_Filters_Video::videoListRender($left, 'Аудиозаписи \'' . $artist_name . '\'', false, 'left', $page * $limit),
            'right' => (string) Lingua_View_Resources_Filters_Video::videoListRender($right, false, false, 'right', $page * $limit + 10)
        ));
        exit;
    }

    //-------------------------------------------------------------------
    public function resourceVideoGetAllAction() {
        //requred page
        $page = $this->getParam('page', 0);

        //result limit
        $limit = $this->getParam('limit', 20);

        //for artist
        $artist_id = $this->getParam('artist_id');

        $r = Lingua_Query_Resources::resourcesVideoGetAll(false, Lingua_Auth::getLanguage(), $artist_id, $page * $limit, $limit);
        $c = Lingua_Query_Resources::resourcesVideoGetAllCount(false, Lingua_Auth::getLanguage(), $artist_id);
        $d = array_chunk($r, 10, true);

        //left and right parts of artist resources
        $left = isset($d[0]) ? $d[0] : array();
        $right = isset($d[1]) ? $d[1] : array();

        //response
        echo json_encode(array(
            'left' => (string) Lingua_View_Resources_Filters_Video::videoListRender($left, 'Все видеозаписи (найдено ' . $c . ' видеозаписей)', false, 'left', $page * $limit),
            'right' => (string) Lingua_View_Resources_Filters_Video::videoListRender($right, false, false, 'right', $page * $limit + 10),
            'total' => $c,
            'page' => $page,
            'container' => 'video',
            'artist_id' => $artist_id
        ));

        exit;
    }

    //###############################categories##########################
    //-------------------------------------------------------------------
    public function resourceCategoryFilterAction() {
        $this->_helper->layout->disableLayout();
        $category = Lingua_Query_Resources_Category::categoryGet();

        $this->view->video_categories = isset($category['video']) ? $category['video'] : array();
    }

    //-------------------------------------------------------------------
    public function resourceCategoryGetResourcesAction() {
        //category id
        $category_id = $this->getParam('artist_id');

        //split video_cat_id pattern to resource type and category id
        $data = preg_split('/_/', $category_id);

        //type is a resource type (video/audio)
        $type = isset($data[0]) ? $data[0] : false;

        //selected category id
        $cat_id = isset($data[1]) ? $data[1] : false;

        //requested page
        $page = $this->getParam('page', 0);

        //result count
        $limit = $this->getParam('limit', 20);

        if (!$type || !$cat_id) {
            echo json_encode(array('result' => 'fail', 'reason' => 'somthing.wrong'));
            exit;
        }

        if ($type == 'video') {
            $data = Lingua_Query_Video::videogetByCategory($cat_id, $page * $limit, $limit);
            $c = Lingua_Query_Video::videogetByCategoryCount($cat_id);

            //split to aprats
            $d = array_chunk($data, 10, true);

            //left and right parts of artist resources
            $left = isset($d[0]) ? $d[0] : array();
            $right = isset($d[1]) ? $d[1] : array();

            echo json_encode(array(
                'left' => (string) Lingua_View_Resources_Filters_Video::videoListRender($left, 'Все видеозаписи (найдено ' . $c . ' видеозаписей)', false, 'left', $page * $limit),
                'right' => (string) Lingua_View_Resources_Filters_Video::videoListRender($right, false, false, 'right', $page * $limit + 10),
                'total' => $c,
                'page' => $page,
                'container' => 'video_cat'));
        }

        exit;
    }

    //###############################resources###########################
    //-------------------------------------------------------------------
    public function top10Action() {
        include_once APPLICATION_PATH . '/resources/models/List/Resourcespage.php';

        //resources model
        $RESOURCE = $this->view->RESOURCES = new List_Resourcespage();
        $r = $RESOURCE->resourcesGetTop10();

        //top 10 viewed videos list
        $top_video = isset($r['video']) ? $r['video'] : array();

        //top 10 viewed audios list
        $top_audio = isset($r['audio']) ? $r['audio'] : array();

        $left = Lingua_View_Resources_Filters_Audio::audioListRender($top_audio, 'Топ-10 аудиозаписей', true);
        $right = Lingua_View_Resources_Filters_Video::videoListRender($top_video, 'Топ-10 видеозаписи', true);

        //response
        echo json_encode(array(
            'left' => (string) $left,
            'right' => (string) $right
        ));
        exit;
    }

    //-------------------------------------------------------------------
    public function searchAction() {
        //search artist name
        $q = $this->getParam('q');
        $data = Lingua_Query_Resources::resourceSearch($q, Lingua_Auth::getLanguage());

        echo json_encode($data);
        exit;
    }

    //-------------------------------------------------------------------
    public function searchByAction() {
        //search keyword
        $val = $this->getParam('val');
        $val = preg_replace('/\s/', '', $val);

        //requested page
        $page = $this->getParam('page', 0);

        //resource type
        $resource = $this->getParam('type');

        //resource result count
        $count = $this->getParam('count');

        //current language
        $lang = Lingua_Auth::getLanguage();

        if (empty($val)) {
            echo json_encode(array('result' => 'string.empty'));
            exit;
        }

        //requested audio and video resources
        if (!$resource) {
            Lingua_Query_Resources::beginTransaction();

            //search for val
            $data = Lingua_Query_Resources::resourceSearch($val, Lingua_Auth::getLanguage());

            //result count for val
            $data_count = Lingua_Query_Resources::resourceSearchCount($val, Lingua_Auth::getLanguage());

            Lingua_Query_Resources::commit();

            //init
            list($left, $right) = array(array(), array());
            list($left_count, $right_count) = array(array(), array());

            foreach ($data as $d) {
                $obj = new Lingua_Object($d);
                if ($obj->resource == 'video') {
                    $right[] = $d;
                } elseif ($obj->resource == 'audio') {
                    $left[] = $d;
                }
            }

            //render html
            $left = Lingua_View_Resources_Filters_Audio::audioListRender($left, 'Результат поиска по "' . $val . '"', false, 'left', $page*10);
            $right = Lingua_View_Resources_Filters_Video::videoListRender($right, 'Результат поиска по "' . $val . '"', false, 'right', $page*10);

            //found result counts
            $left_count = isset($data_count['audio']) ? $data_count['audio']['count'] : 0;
            $right_count = isset($data_count['video']) ? $data_count['video']['count'] : 0;

            //response
            echo json_encode(array(
                'left' => (string) $left,
                'left_count' => (int) $left_count,
                'left_page' => 0,
                'right' => (string) $right,
                'right_count' => (int) $right_count,
                'right_page' => 0
            ));

            exit;
        } elseif ($resource == 'audio') {
            $data = Lingua_Query_Resources::resourceSearchAudio($val, $lang, $page * 10);
            $right = Lingua_View_Resources_Filters_Audio::audioListRender($data, 'Результат поиска по "' . $val . '"', false, 'left', $page * 10);

            //response
            echo json_encode(array(
                'left' => (string) $right,
                'left_count' => (int) $count,
                'left_page' => $page,
                'val' => $val
            ));

            exit;
        } elseif ($resource == 'video') {
            $data = Lingua_Query_Resources::resourceSearchVideo($val, $lang, $page * 10);
            $left = Lingua_View_Resources_Filters_Video::videoListRender($data, 'Результат поиска по "' . $val . '"', false, 'right', $page * 10);

            //response
            echo json_encode(array(
                'right' => (string) $left,
                'right_count' => (int) $count,
                'right_page' => $page,
                'val' => $val
            ));
        }

        exit;
    }

}

?>
