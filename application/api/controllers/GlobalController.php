<?php

/**
 * Description of GlobalController
 *
 * @author aramp
 */
class Api_GlobalController extends Lingua_Plugin_Action {

    public function globalChangeLanguageAction() {
        $lang = $this->getParam('short_name');
        $system_languages = Lingua_Auth::sysGetLanguages();

        $language = isset($system_languages[$lang]) ? $system_languages[$lang] : false;

        if (!$language) {
            echo json_encode(array('result' => 'fail', 'reason' => 'language.not.found'));
        }

        Lingua_Auth::setLanguage($language['system_languages_id'], $language['short_name']);

        include_once APPLICATION_PATH . '/resources/models/List/Resourcespage.php';
        $r = new List_Resourcespage();
        $r->clear();

        echo json_encode(array('result' => 'success', 'redirect' => '/resources'));
        exit;
    }

}

?>
