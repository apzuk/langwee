<?php

/**
 * Description of TestController
 *
 * @author Aram
 */
class Test_TestController extends Lingua_Plugin_Action
{

    //--------------------------------------------------------------------------
    public function parserAction()
    {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $starttime = $mtime;

        $client = new Zend_Http_Client();
        $client->setUri('http://lingvopro.abbyyonline.com/ru/Translate/en-ru/comma');

        $response = $client->request();
        $html = $response->getBody(); // the Html is the example above
        $dom = new Zend_Dom_Query($html);
        $results = $dom->query('div.js-article-html p img');

        foreach ($results as $result) {
            echo $result->getAttribute('src');
        }

        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $endtime = $mtime;
        $totaltime = ($endtime - $starttime);
        echo "This page was created in " . $totaltime . " seconds";

        exit;
    }

    //--------------------------------------------------------------------------
    public function parsePhrasesAction()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 'on');

        $client = new Zend_Http_Client();
        $client->setUri('http://www.study.ru/support/phrasebook/');

        $response = $client->request();
        $html = $response->getBody();
        $dom = new Zend_Dom_Query($html);

        $results = $dom->query('.mainbody table h2 a');

        foreach ($results as $result) {
            Lingua_Query_Test::fillPhrasesTheme($value);
        }

        echo 'done';
        exit;
    }

    //--------------------------------------------------------------------------
    public function realAction()
    {
        echo APPLICATION_PATH;
        exit;
    }

    //--------------------------------------------------------------------------
    public function insertAction()
    {
        $data = Lingua_Query_Test::getUsers();

        foreach ($data as $d) {
            Lingua_Query_Test::setUserStatus($d['lingua_id']);
            echo $d['lingua_id'] . '.. done!<br />';
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function setVideoViewsAction()
    {
        $videos = Lingua_Query_Video::videoGetAlll();

        foreach ($videos as $v) {
            echo $v . "<br />";
            Lingua_Query_Video::videoSetViewCount($v['video_id'], rand(300, 2315));
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function setAudioViewsAction()
    {
        $audios = Lingua_Query_Audio::audioGetAlll();

        foreach ($audios as $a) {
            Lingua_Query_Audio::audioSetViewCount($a['audio_id'], rand(300, 1215));
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function subscribeAction()
    {
        error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors', 'on');

        $data = Lingua_Query_User::userGetNotsubscribed();

        $cmapi = new Lingua_Site_Subscriber();
        $id_list = $cmapi->subscribeAll($data);

        Lingua_Query_User::userSetSubscribed($id_list);
        exit;
    }

    //--------------------------------------------------------------------------
    public function mysqlAction()
    {

        Lingua_Query_Resources::resourcesGetArtists(false, 'en', 0, 10);
        exit;
    }

    //--------------------------------------------------------------------------
    public function imageAction()
    {
        Lingua_Auth::sysGetLanguages();
        exit;
    }

    //--------------------------------------------------------------------------
    public function mailAction()
    {
        $email = 'aram.petrosyan.88@gmail.com';

        $mail = new Zend_Mail('utf-8');
        $mail->setFrom("info@langwee.com", "Код активации");
        $mail->addTo($email);
        $mail->setSubject('Активация аккаунта на сайте Langwee.com');

        $message = "Здравствуйте, " . $email . "\n";
        $message .= "Спасибо за регистрацию. Уверены, теперь вы полюбите мир Иностранных\nязыков и добьетесь высоких результатов в их изучении с Langwee и вашими любимыми кумирами. \n";
        $message .= "\n\nПерейдите по ссылке что бы активировать Ваш учетный запись http://www.langwee.com/activate/" . $activation_code;
        $message .= "\n\nВаш пароль: " . $password;
        $message .= "\n\nСледите за обновлениями на сайте, каждый день у нас появляется много интересного и нового.";
        $message .= "\n\nДо встречи на Langwee!\n";
        $message .= "С уважением,\n";
        $message .= "Команда Langwee.ru";

        $mail->setBodyText($message);

        try {
            $sent = $mail->send();
            echo 'success';
        } catch (Exception $e) {
            echo $e->getMessage();
            throw new Exception("Unable to send email");
            Lingua_Query_User::rollback();
        }

        exit;
    }

    //--------------------------------------------------------------------------
    public function testmailAction()
    {
        $email = 'langwee_test@mail.ru';

        $tr = new Zend_Mail_Transport_Smtp('langwee.com');
        Zend_Mail::setDefaultTransport($tr);

        $mail = new Zend_Mail('utf-8');
        $mail->setFrom("info@langwee.com", "Код активации");
        $mail->addTo($email);
        $mail->setSubject('Активация аккаунта на сайте Langwee.com');

        $message = "Здравствуйте, " . $email . "\n";

        $mail->setBodyText($message);

        try {
            $sent = $mail->send();
            echo 'success';
        } catch (Exception $e) {
            echo $e->getMessage();
            throw new Exception("Unable to send email");
            Lingua_Query_User::rollback();
        }

        exit;
    }

}

?>
