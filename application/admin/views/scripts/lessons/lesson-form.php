<?php $this->placeholder('mycss')->captureStart(); ?>
<style>
    .add-word-translate {
        width: 16px;
        height:  16px;
        background: url(/images/lessons/plus.png) no-repeat;
       
    } 
    .delete-word-translate {
        width: 16px;
        height:  16px;
        background: url(/images/lessons/minus.png) no-repeat;
        float: left;
    }
    .translate-input {
        float: left;
        margin-right: 5px;
        clear: both;
    } 
    .word-translate {
        overflow: hidden;
    }  
    .manage-links {
        clear: both;
    }
    .word_translates {
        overflow: hidden;
    }
</style>
<?php $this->placeholder('mycss')->captureEnd(); ?>
<form action="/admin/lessons/add" id="form" method="post">
    <input type="hidden" name="lesson_id" value="<?php echo $this->lesson->lesson_id; ?>" />
<div class="lesson-form">
    <div class="step">
        <span class="label">Название урока: </span><input type="text" name="lesson_title" value="<?php echo $this->lesson->lesson_title; ?>" />
    </div>
    <div>        
        Язык: 
        <select name="language_id">
            <?php foreach ($this->languages as $lang): ?>
                <?php if($this->lesson->language_id == $lang['system_languages_id']): ?>
                    <option value="<?php echo $lang['system_languages_id'];?>" selected="selected"><?php echo $lang['name']; ?></option>
                <?php else: ?>
                    <option value="<?php echo $lang['system_languages_id'];?>"><?php echo $lang['name']; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="step">
        <div class="title">
            Шаг 1. Текст
        </div>
        
        <div class="content">
            <h2>Язык-оригинал:</h2>
            <textarea name="source" id="lesson-text" class="tinymce"><?php echo $this->lesson->source; ?></textarea>
            <div class="addlinks">
                <a href="javascript:;" onclick="addWord()">Добавить слово</a>
                <a href="javascript:;" onclick="addPhrase()">Добавить фразу</a>
                <a href="javascript:;"  class="split-by-sentence">Разбить по предложениям</a> 
            </div>
            <h2>Перевод:</h2>
            <textarea name="translate" id="lesson-text-translate" class="tinymce"><?php echo $this->lesson->translate; ?></textarea>
            
            <div class="container">           
                <div id="phrases">
                    <h2>Фразы:</h2>
                    <?php $phrase_id = 0; ?>
                    <?php if ($this->lesson->phrases): ?>
                        <?php foreach($this->lesson->phrases as $phrase): ?>
                            <div class="phrase">
                                <input type="hidden" class="phrase_id" name="phrase_id[<?php echo $phrase_id; ?>]" value="<?php echo $phrase['phrase_id']; ?>" />
                                <input type="hidden" name="phrase[<?php echo $phrase_id; ?>]" value="<?php echo $phrase['phrase_text1']; ?>" />
                                <div class="text1"><span class="label">Фраза:</span> <?php echo $phrase['phrase_text1'];?></div>
                                <div class="text2"><span class="label">Перевод:</span> <input type="text" name="phrase_translate[<?php echo $phrase_id; ?>]" value="<?php echo $phrase['phrase_text2']; ?>" /></div>
                                <div class="manage-links"><span class="delete-phrase" onclick="deletePhrase($(this))">Удалить</span></div>
                            </div>
                        <?php $phrase_id++; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div id="words">
                    <h2>Слова:</h2>
                    <?php $word_id = 0; ?>
                    <?php if ($this->lesson->words): ?>
                        <?php foreach($this->lesson->words as $word): ?>
                            <div class="word">
                                <input type="hidden" class="word_id" name="word_id[<?php echo $word_id; ?>]" value="<?php echo $word['word_id']; ?>" />
                                <input type="hidden" name="word[<?php echo $word_id; ?>]" value="<?php echo $word['word_text']; ?>" />
                                <div class="text1"><span class="label">Слово:</span> <?php echo $word['word_text']; ?></div>
                                
                                <div class="text2">
                                    <span class="label">Перевод:</span><br />
                                        <div class="word_translates">
                                            <div class="word_translate">
                                                <?php if ($word['translates']): ?>
                                                    <?php foreach ($word['translates'] as $translate): ?>
                                                        <div class="translate-input"><input type="text" name="word_translate[<?php echo $word_id; ?>][]" value="<?php echo $translate['translate_text'];?>" /></div>
                                                        <div class="delete-word-translate" onclick="deleteWordTranslate($(this))"></div>
                                                    <?php endforeach; ?>   
                                                <?php else: ?>
                                                       <div class="translate-input"><input type="text" name="word_translate[<?php echo $word_id; ?>][]" value="" /></div>
                                                      <div class="delete-word-translate" onclick="deleteWordTranslate($(this))"></div>                                      
                                                <?php endif; ?> 
                                            </div> 
                                        </div>    
                                        <div class="add-word-translate" onclick="addWordTranslate($(this), <?php echo $word_id; ?>)"></div>                                
                                </div>
                                <div class="manage-links"><span class="delete-phrase" onclick="deleteWord($(this))">Удалить</span></div>
                            </div>
                        <?php $word_id++; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="step">
        <div class="title">
            Шаг 2. Аудиоистория
        </div>
        
        <div class="content">
            Адрес аудио: <input type="text" name="audio_history_url" value="<?php echo $this->lesson->audio_history_url; ?>" /><br />          
        </div>
    </div>
    <div class="step">
        <div class="title">
            Шаг 3. Аудио-вокабуляр
        </div>
        
        <div class="content">
            Адрес аудио: <input type="text" name="audio_vocabular_url" value="<?php echo $this->lesson->audio_vocabular_url; ?>" /><br />          
        </div>
    </div>
    <div class="step">
        <div class="title">
            Шаг 4. Аудио-министори
        </div>        
        <div class="content">
            Адрес аудио: <input type="text" name="audio_ministory_url" value="<?php echo $this->lesson->audio_ministory_url; ?>" /><br /> 
            Текст:<br />
            <textarea name="audio_ministory_text"><?php echo $this->lesson->audio_ministory_text; ?></textarea>         
        </div>
    </div>    
</div>
</form>
<?php $this->placeholder('myscript')->captureStart(); ?>

<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
            content_css : "/css/tinymce/style.css",
			// General options
			theme : "simple",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",theme_advanced_toolbar_location : "external",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "external",
			theme_advanced_resizing : true,

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
    var word_id = <?php echo $word_id; ?>;
    var phrase_id = <?php echo $phrase_id; ?>;
    
    function addWordTranslate(el, word_id)
    {
        var text = '<div class="word_translate">' +
                        '<div class="translate-input"><input type="text" name="word_translate['+word_id+'][]" value="" /></div>' +
                        '<div class="delete-word-translate" onclick="deleteWordTranslate($(this))"></div>'+
                    '</div>';
        el.parent().find('.word_translates').first().append(text);
    }
    
    // Добавление новой фразы
    function addPhrase()
    {
        var phrase = $('#lesson-text').tinymce().selection.getContent({format : 'text'});
        phrase = $.trim(phrase);
                
        if ($.trim(phrase) == '') return;        
        if (confirm('Добавить фразу "'+phrase+'"?')) {             
            var text =  '<div class="phrase">'+
                            '<input type="hidden" class="phrase_id" name="phrase_id['+phrase_id+']" value="0" />'+
                            '<input type="hidden" name="phrase['+phrase_id+']" value="'+phrase+'" />'+
                            '<div class="text1"><span class="label">Фраза:</span> '+phrase+'</div>'+
                            '<div class="text2"><span class="label">Перевод:</span> <input type="text" name="phrase_translate['+phrase_id+']" /></div>' +
                            '<div class="manage-links"><span class="delete-phrase" onclick="deletePhrase($(this))">Удалить</span></div>' +
                        '</div>';
            $("#phrases").append(text);
            phrase_id++;
        }
    }
    
    $('.split-by-sentence').click(function(){
        var text = $('#lesson-text').html();
        var text_translate = $('#lesson-text-translate').html();
        var reg = /([\.!\?])([^\/])/g;
        var tmp = text.replace(reg, '$1//$2');
        
        $('#lesson-text').html(tmp);        
        tmp = text_translate.replace(reg, '$1//$2');
        $('#lesson-text-translate').html(tmp);
    });
    
    // Добавление нового слова
    function addWord()
    {
        var word = $('#lesson-text').tinymce().selection.getContent({format : 'text'});
        word = $.trim(word);
        
        if ($.trim(word) == '') return;
        
        if (confirm('Добавить слово "'+word+'"?'))
        {
            var text =  '<div class="word">'+
                            '<input type="hidden" class="word_id" name="word_id" value="0" />'+
                            '<input type="hidden" name="word['+word_id+']" value="'+word+'" />'+
                            '<div class="text1"><span class="label">Слово:</span> '+word+'</div>'+
                            '<div class="text2">'+
                                '<span class="label">Перевод:</span>'+
                                '<div class="word_translates">' + 
                                    '<div class="translate-input"><input type="text" name="word_translate['+word_id+'][]" /></div>' + 
                                    '<div class="delete-word-translate" onclick="deleteWordTranslate($(this))"></div>' + 
                                '</div>' +
                                '<div class="add-word-translate" onclick="addWordTranslate($(this),' + word_id + ')"></div>' + 
                            '</div>' +                           
                            '<div class="manage-links"><span class="delete-phrase" onclick="deleteWord($(this))">Удалить</span></div>' +
                        '</div>';
            $("#words").append(text);
            word_id++;
        }
    }
    
    function deletePhrase(el)
    {
        if (confirm('Удалить фразу?'))
        {            
            var container = el.parent().parent().remove();
            
            var phrase_id = container.find('.phrase_id').first().val();
            
            if (phrase_id != 0)
            {
               $.post('/admin/lessons/ajaxphrasedelete', {phrase_id : phrase_id}); 
            }
            
            container.remove();
        }
    }
    
    function deleteWord(el)
    {
        if (confirm('Удалить слово?'))
        {
            var container = el.parent().parent().remove();
            
            var word_id = container.find('.word_id').first().val();
            
            if (word_id != 0)
            {
                $.post('/admin/lessons/ajaxworddelete', {word_id : word_id});
            }
            
            container.remove();
        }
    }
    
    function deleteWordTranslate(el)
    {
        el.parent().remove();
    }
    
</script>
<?php $this->placeholder('myscript')->captureEnd(); ?>