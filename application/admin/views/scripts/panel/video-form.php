<form action="/admin/panel/video-add" method="post" id="form">
    <fieldset class="container">
        <div>
            <table>
                <tr>
                    <td>Категория: </td>
                    <td>
                        <select name="categoty" id="category">
                            <option value="-1">-----Выбрать категорию-----</option>
                            <?php foreach ($this->categories as $category): ?>
                                <?php
                                $status = "";
                                if ($category['video_category_id'] == $this->video->video_category_id)
                                    $status = "selected";
                                ?>
                                <option <?= $status ?> value="<?= $category['video_category_id'] ?>">
                                    <?= $category['category_name']; ?>
                                </option>
                            <?php endforeach; ?>
                            <?php if (Lingua_Auth::hasPermission('video.category.add')): ?>
                                <option value="0">-----Новая категория-----</option>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Название:</td>
                    <td>
                        <input type="text" value="<?= $this->video->title; ?>" name="title" size="50" />
                    </td>
                </tr>
                <tr>
                    <td>Исполнитель: </td>
                    <td>
                        <input type="text" value="<?= $this->video->name; ?>" name="artist" size="50" />

                    </td>
                </tr>
                <tr>
                    <td>Url: </td>
                    <td>
                        <input type="text" value="<?= $this->video->video_url; ?>" id="url" name="url" size="50" />
                        <span id='video_loading' style="margin-left: 10px;"></span>
                        <br />
                        <span style="font-size: 11px;padding: 2px;color:#030303;">Пример: http://www.youtube.com/watch?v=yztjFZN-SaQ&feature=g-logo</span>

                    </td>
                </tr>
                <tr>
                    <td>
                        Язык:
                    </td> 
                    <td>
                        <select name="video_lang">
                            <?php foreach ($this->languages as $short => $lang): ?>
                                <?php if ($lang['sys_use'] == 'YES') : ?>
                                    <option value="<?= $lang['system_languages_id']; ?>" <?php if ($short == $this->video->system_language_id) echo "selected"; ?>>
                                        <?= $lang['name']; ?>
                                    </option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>

        <div id='youtube_video'></div>
        <hr />

        <div>
            <span style="float: left; margin-right: 10px;">Английский текст:</span>
            <textarea cols="60" rows="20" name="text1"><?= stripslashes($this->video->text1); ?></textarea>
        </div>

        <div>
            <span style="float: left; margin-right: 57px;">Перевод:</span>
            <textarea cols="60" rows="20" name="text2"><?= stripslashes($this->video->text2); ?></textarea>

        </div> 
    </fieldset>
</form>