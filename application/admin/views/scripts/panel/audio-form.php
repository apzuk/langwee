<form action="/admin/panel/audio-add" method="post" id="form">
    <fieldset class="container">
        <div>
            <table>
                <tr>
                    <td>Категория: </td>
                    <td>
                        <select name="categoty" id="category">
                            <option value="-1">-----Выбрать категорию-----</option>
                            <?php foreach ($this->categories as $category): ?>
                                <?php
                                $status = "";
                                if ($category['audio_category_id'] == $this->audio->audio_category_id)
                                    $status = "selected";
                                ?>
                                <option <?= $status ?> value="<?= $category['audio_category_id'] ?>">
                                    <?= $category['category_name']; ?>
                                </option>
                            <?php endforeach; ?>
                                
                            <?php if(Lingua_Auth::hasPermission('audio.category.add')):  ?>
                            <option value="0">-----Новая категория-----</option>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Название:</td>
                    <td>
                        <input type="text" value="<?= $this->audio->title; ?>" name="title" size="50" />
                    </td>
                </tr>
                <tr>
                    <td>Исполнитель: </td>
                    <td>
                        <input type="text" value="<?= $this->audio->name; ?>" name="artist" size="50" />

                    </td>
                </tr>
                <tr>
                    <td>Url: </td>
                    <td>
                        <input type="text" value="<?= $this->audio->audio_url; ?>" id="url" name="url" size="50" />
                        <span id='audio_loading' style="margin-left: 10px;"></span><br />
                        <span style="font-size: 11px;padding: 2px;color:#030303;">Пример: http://embed.prostopleer.com/track?id=B2mo69B4barhB10vu</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Язык:
                    </td> 
                    <td>
                        <select name="audio_lang">
                            <?php foreach ($this->languages as $short => $lang): ?>
                                <?php if ($lang['sys_use'] == 'YES') : ?>
                                    <option value="<?= $lang['system_languages_id']; ?>" <?php if ($short == $this->audio->system_language_id) echo "selected"; ?>>
                                        <?= $lang['name']; ?>
                                    </option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>

        <div id='youtube_audio'></div>
        <hr />

        <div>
            <span style="float: left; margin-right: 10px;">Английский текст:</span>
            <textarea cols="60" rows="20" name="text1"><?php echo stripslashes($this->audio->text1); ?></textarea>
        </div>

        <div>
            <span style="float: left; margin-right: 57px;">Перевод:</span>
            <textarea cols="60" rows="20" name="text2"><?php echo stripslashes($this->audio->text2); ?></textarea>

        </div> 
    </fieldset>
</form>