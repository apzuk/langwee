<?php

    class Lessons_List extends Lingua_View_List {
        const PAGE_LENGTH = 25;

        private $status;
        
        //----------------------------------------------------------------------
        public function __construct($status = null, $creator = null, $page = null) {
            $this->SESS = new Daz_Session(__CLASS__);
    
            if($status!="") {
                $this->SESS->status = $status;
            }
            
            
            
            if($creator != "") {
                $this->SESS->creator = $creator;
            }
            
            if($page!="") {
                $this->SESS->current_page = (int)$page;
            }
            
            $this->current_page = $this->SESS->current_page;
            $this->ACTION_URL = '/admin/lessons/'; 
            
            $this->creator = Lingua_Auth::getLinguaId();
            
            if(Lingua_Auth::hasPermission('lessons.view.all')){
                $this->creator = false;
            }  
        }
    
        //--------------------------------------------------------------------------
        public function init() {
            $this->getRecordCount();
            $this->setPager(self::PAGE_LENGTH);
        }
    
        //--------------------------------------------------------------------------
        public function getRecordCount() {
            $records_count = $this->records_count = Lingua_Query_Lessons::lessonsGetAllCount($this->SESS->status, $this->creator);
            return $records_count;
        }
    
        //--------------------------------------------------------------------------
        public function getRecords() {
            $records = $this->records = $records = Lingua_Query_Lessons::lessonsGetAll($this->SESS->status, $this->creator, $this->SESS->current_page * self::PAGE_LENGTH ,self::PAGE_LENGTH);
            return $records;
        }
    }