<?php

/**
 * Description of Registration
 *
 * @author Aram
 */
class User_Registration {

    private $SESS;

    public function __construct() {
        $this->SESS = new Daz_Session(__CLASS__);
    }

    //--------------------------------------------------------------------------
    public function getErrors() {
        return $this->SESS->error_list;
    }

    //--------------------------------------------------------------------------
    public function register($email, $password, $role_id = null, $infoData = null) {
        if (count($this->SESS->error_list))
            $this->SESS->error_list = array();


        if (!$this->checkEmail($email)) {
            $this->SESS->error_list[] = "Enter an valid email";
        }
        $result = Lingua_Query_User::userGetByEmail($email);
        if ($result) {
            $this->SESS->error_list[] = "Email exists";
        }

        if (count($this->SESS->error_list) == 0) {
            try {
                Lingua_Query_User::beginTransaction();

                if (!$role_id)
                    $role_id = Lingua_Query_Permission::roleGetId('STUDENT');
                if ($role_id === false)
                    throw new Exception('unable to get role id');

                $result = $lingua_id = Lingua_Query_User::userInsert($email, $password, $role_id);

                if ($result === false)
                    throw new Exception('unable to register');

                $activation_code = time() . md5($email) . $result;

                $result = Lingua_Query_User::userSetActivationPending($activation_code, $result);
                if ($result === false) {
                    throw new Exception('Unable to set activation code');
                }

                if ($infoData) {
                    $result = Lingua_Query_User::userSetInfo($infoData['firstname'], $infoData['surname'], $infoData['age'], $infoData['gender'], $infoData['photo'], $lingua_id);

                    if ($result === false)
                        throw new Exception('Unable to set user info');
                }

                //Create email

                $tr = new Zend_Mail_Transport_Smtp('langwee.com');
                Zend_Mail::setDefaultTransport($tr);

                $mail = new Zend_Mail('utf-8');
                $mail->setFrom("no-reply@linguastars.ru", "Код активации");
                $mail->addTo($email);
                $mail->setSubject('Lingua Star Activation Code');
                $mail->setBodyText('Your activation code is: ' . $activation_code . ', Login: ' . $email . ', password: ' .
                        $password . '. Click here to activate your account http://www.linguastars.ru/activate/' . $activation_code);

                //Send
                $sent = true;
                try {
                    $mail->send();
                } catch (Exception $e) {
                    $sent = false;
                }

                Lingua_Query_User::commit();
            } catch (Exception $e) {
                Lingua_Query_User::rollback();
                echo $e->getMessage();
                exit;
            }
        }
    }

    //--------------------------------------------------------------------------
    public function checkEmail($email) {
        if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
            return false;
        }

        return true;
    }

}

?>
