<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramp
 * Date: 8/20/12
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
class User_Olegtraining extends Lingua_View_List
{
    const PAGE_LENGTH = 25;
    //----------------------------------------------------------------------
    public function __construct($page = null) {
        $this->SESS = new Daz_Session(__CLASS__);
        $this->ACTION_URL = '/admin/user/oleg-trainings';

        if($page)  $this->SESS->page = $this->current_page = $page;
    }

    //--------------------------------------------------------------------------
    public function init() {
        $this->getRecordCount();
        $this->setPager(self::PAGE_LENGTH);
    }

    //--------------------------------------------------------------------------
    public function getRecords() {
        $records_count = $this->records = Lingua_Query_User::userGetOlegTraining($this->current_page * self::PAGE_LENGTH, self::PAGE_LENGTH);
        return $records_count;
    }

    //--------------------------------------------------------------------------
    public function getRecordCount() {
        $records = $this->records_count = $records = Lingua_Query_User::userGetOlegTrainingCount();
        return $records;
    }

    //--------------------------------------------------------------------------
    public function getpage() {
        return $this->SESS->page;
    }
}
