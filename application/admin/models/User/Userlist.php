<?php

/**
 * Description of List
 *
 * @author Aram
 */
abstract class Lingua_View_List {

    public $records = null;
    public $records_count = null;
    public $current_page;
    public $pages_num;
    public $SESS = null;
    public $ACTION_URL;

    //--------------------------------------------------------------------------
    public abstract function init();

    //--------------------------------------------------------------------------
    public function setPager($default_page_count = 2) {
        $this->pages_num = ceil($this->records_count / ($default_page_count));
    }

    //--------------------------------------------------------------------------
    public function tbarHeader() {
        $tag = new Daz_Tag('div', 'id', 'pager', 'align', 'center');

        $tag->push('table', 'width', '710px');
        $tag->push('tr');
        $tag->push('td', 'width', '30%');

        $tag->append('span', 'Всего найдено: ' . $this->records_count);
        $tag->pop();

        $tag->push('td', 'width', '30%', 'align', 'center');
        $prev = $this->ACTION_URL . '/page/';
        $prev .= ($this->current_page - 1) < 0 ? 0 : ($this->current_page - 1);

        $tag->push('a', 'href', $prev);
        $tag->append('img', 'style', 'margin-top: 2px', 'src', '/images/admin/prev.png');
        $tag->pop();

        $tag->push('span', ($this->current_page + 1) . ' из ' . ($this->pages_num));

        $next = $this->ACTION_URL . '/page/';
        $next .= ($this->current_page + 1) > $this->records_count ? $this->records_count + 1 : ($this->current_page + 1);
        $tag->pop();

        $tag->push('a', 'href', $next);
        $tag->append('img', 'src', '/images/admin/next.png');
        $tag->pop();

        $tag->push('td', 'width', '30%');

        $tag->pop();


        echo $tag;
    }

    //--------------------------------------------------------------------------
    public function currentPage() {
        return $this->current_page;
    }
    
    //--------------------------------------------------------------------------
    public function cleanResult() {
        $this->SESS->unsetAll();
    }
}

?>
