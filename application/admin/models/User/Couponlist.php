<?php

class User_Couponlist extends Lingua_View_List {

    const PAGE_LENGTH = 25;

    //WTF?! :)
    private $STATUSES = array(
        'PENDING' => array('status' => 'Неактивный', 'color' => 'red'),
        'ACTIVE' => array('status' => 'Активный', 'color' => 'green'));

    //----------------------------------------------------------------------
    public function __construct($email = null, $person = null, $status = null, $page = null) {
        $this->SESS = new Daz_Session(__CLASS__);
        $this->ACTION_URL = '/admin/user/coupons';

        if ($email) {
            $this->SESS->email = $email;
        }

        if ($person) {
            $q = preg_split('/\s+/', $person);
            $this->SESS->firstname = isset($q[0]) ? $q[0] : '';
            $this->SESS->surname = isset($q[1]) ? $q[1] : '';
        }

        if ($status !== false) {
            if ($status == 1)
                $this->SESS->status = 'pending';
            if ($status == 0)
                $this->SESS->status = 'approved';
        }

        $this->current_page = $page;
    }

    //--------------------------------------------------------------------------
    public function init() {
        $this->getRecordCount();
        $this->setPager(self::PAGE_LENGTH);
    }

    //--------------------------------------------------------------------------
    public function getRecordCount() {
        $records_count = $this->records_count = Lingua_Query_User::userGetCouponsCount($this->SESS->email, $this->SESS->firstname, $this->SESS->surname, $this->SESS->status);
        return $records_count;
    }

    //--------------------------------------------------------------------------
    public function getRecords() {
        $records = $this->records = $records = Lingua_Query_User::userGetCoupons(
                        $this->SESS->email, $this->SESS->firstname, $this->SESS->surname, $this->SESS->status, $this->current_page * self::PAGE_LENGTH, self::PAGE_LENGTH);
        return $records;
    }

    //--------------------------------------------------------------------------
    public function getStatus($status) {
        return ($this->STATUSES[$status]) ? $this->STATUSES[$status] : array('status' => 'неизвестный', 'color' => '#000');
    }

    //--------------------------------------------------------------------------
    public function getProp($val) {
        return $this->SESS->$val;
    }

    //--------------------------------------------------------------------------
    public function cleanResult() {
        $this->SESS->unsetAll();
    }

}

?>
