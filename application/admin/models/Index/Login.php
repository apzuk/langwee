<?php

/**
 * Description of login_from
 *
 * @author Aram
 */
class Index_Login {
    public $username = null;
    private $password = null;
    
    //--------------------------------------------------------------------------
    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }
    
    //--------------------------------------------------------------------------
    public function execute() {
        $lingua_id = Lingua_Query_User::getUserIdByUsernameAndPassword($this->username, $this->password);

        if(!$lingua_id && $this->username) { 
            Lingua_Auth::rejectUnlessAuth(); 
            exit;
        }
        
        if(!$lingua_id) {
            return false;
        }
        
        Lingua_Auth::setLinguaId($lingua_id);
        
        header('Location: /admin/panel/welcome');
    }
}

?>
