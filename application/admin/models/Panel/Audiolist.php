<?php

/**
 * Description of List
 *
 * @author Aram
 */
class Panel_Audiolist extends Lingua_View_List {

    const PAGE_LENGTH = 25;

    private $category_id;
    private $status;
    private $keyword;
    private $artist;

    //----------------------------------------------------------------------
    public function __construct($category_id = 0, $status = "", $keyword = "", $artist = "", $page = "") {
        $this->SESS = new Daz_Session(__CLASS__);

        if($category_id!="") {
            $this->SESS->category_id = $category_id;
        }
        
        if($status!="") {
            $this->SESS->status = $status;
        }
        
        $this->SESS->keyword = $keyword;
        
        if($artist!="") {
            $this->SESS->artist = $artist;
        }
        
        if($page!="") {
            $this->SESS->current_page = (int)$page;
        }
        
        $this->current_page = $this->SESS->current_page;
        $this->ACTION_URL = '/admin/panel/audio'; 
        
        $this->creator = Lingua_Auth::getLinguaId();
        
        if(Lingua_Auth::hasPermission('audio.view.all')){
            $this->creator = false;
        }  
    }

    //--------------------------------------------------------------------------
    public function init() {
        $this->getRecordCount();
        $this->setPager(self::PAGE_LENGTH);
    }

    //--------------------------------------------------------------------------
    public function getRecordCount() {
        $records_count = $this->records_count = Lingua_Query_Audio::audioGetAllCount($this->SESS->category_id, $this->SESS->status, $this->SESS->keyword, $this->SESS->artist, $this->creator);
        return $records_count;
    }

    //--------------------------------------------------------------------------
    public function getRecords() {
        $records = $this->records = $records = Lingua_Query_Audio::audioGetAll($this->SESS->category_id, $this->SESS->status, $this->SESS->keyword, $this->SESS->artist, $this->creator, $this->SESS->current_page * self::PAGE_LENGTH ,self::PAGE_LENGTH);
        return $records;
    }

}

?>
