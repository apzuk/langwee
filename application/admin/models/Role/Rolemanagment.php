<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rolemanagment
 *
 * @author Aram
 */
class Role_Rolemanagment {
    
    private $data;
    
    //--------------------------------------------------------------------------
    public function __construct($data) {
        $this->data = $data; 
        
        $new_data = array();
        foreach($data as $d) {
            $new_data[$d['role_name']][] = $d;
        }
        
        $this->data = $new_data;
    }
    
    //--------------------------------------------------------------------------
    public function getRoles() {
        return Lingua_Query_Permission::roleGetAll();
    }
    
    //--------------------------------------------------------------------------
    public function getPermissions() {
        return Lingua_Query_Permission::permissiGetAll();
    }
    
    //--------------------------------------------------------------------------
    public function hasRolePermission($role, $permission) {
        foreach($this->data[$role] as $r) {
            if($r['permission_name'] == $permission) {
                return true;
            }
        }
        
        return false;
    }
}

?>
