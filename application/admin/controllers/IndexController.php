<?php

/**
 * Description of IndexController
 *
 * @author Aram
 */
class Admin_IndexController extends Lingua_Plugin_Action {

    public function indexAction() {
        $this->view->title = "Welcome to admin part";
    }

    //--------------------------------------------------------------------------
    public function loginAction() {
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {
            $TFORM = $this->view->TFORM = new Index_Login($this->getParam('login'), $this->getParam('password'));
            $TFORM->execute();
        }

        $this->view->title = "Вход в систему";
    }

}