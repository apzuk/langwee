<?php

/**
 * Description of PanelController
 *
 * @author Aram
 */
class Admin_PanelController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function welcomeAction() {
        $this->view->title = "Добро пожаловать в панель администратора!";
    }
    
    public function lessonsAction(){
        $this->view->title = 'Тест';        
    }

    //--------------------------------------------------------------------------
    public function videoAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.managment');

        $this->view->title = "Видео манаджмент";

        $this->view->categories = Lingua_Query_Video_Category::categoryGetAll();
        $this->view->statuses = Lingua_Query_Video::videoGetStatuses();
        $this->view->artists = Lingua_Query_Video::videoGetArtists();
        $this->view->creators = Lingua_Query_Video::videoGetCreators();
        
        $category_id = $this->view->category_id = $this->getParam('video_category');
        $status = $this->view->status = $this->getParam('video_status');
        $keyword = $this->view->keyword = $this->getParam('keyword');
        $artist = $this->view->artist = $this->getParam('video_artists');
        $creator = $this->view->creator = $this->getParam('video_creator');
        $page = $this->getParam('page');

        $LIST = $this->view->LIST = new Panel_Videolist($category_id, $status, $keyword, $artist, $creator, $page);
        $LIST->init();
    }

    //--------------------------------------------------------------------------
    public function videoAddAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.add');

        $this->view->title = "Новое видео";

        $this->view->categories = Lingua_Query_Video_Category::categoryGetAll();
        $this->view->languages = Lingua_Auth::sysGetLanguages();
        
    }

    //--------------------------------------------------------------------------
    public function videoEditAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.save');

        $this->view->title = "Редактирование видео";

        $video_id = $this->getParam('video_id');
        if (!$video_id) {
            header('Location: /admin/panel/video');
            exit;
        }

        $data = Lingua_Query_Video::videoGet($video_id);
        $this->view->categories = Lingua_Query_Video_Category::categoryGetAll();
        $this->view->video = new Lingua_Object($data);
        $this->view->languages = Lingua_Auth::sysGetLanguages();
    }

    //--------------------------------------------------------------------------
    public function videoDeleteAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.delete');

        $this->_helper->layout->disableLayout();
        $video_id = $this->getParam('video_id');

        $result = Lingua_Query_Video::videoRemove($video_id);
        if ($result) {
            echo Lingua_View_SystemLog::successMessage("Видео удачно удаленно!");
        }

        exit;
    }

    //--------------------------------------------------------------------------
    public function videoGetFromUrlAction() {
        $this->_helper->layout->disableLayout();

        $this->view->url = $this->getParam('url');
    }

    //--------------------------------------------------------------------------
    public function videoSaveAction() {
        Lingua_Auth::rejectUnlessAuth();

        $type = $this->getParam('type');
        $video_id = $this->getParam('video_id');

        $title = $this->getParam('title');
        $category_id = $this->getParam('categoty');
        $url = $this->getParam('url');
        $artist_name = $this->getParam('artist');
        $url = Lingua_Helper_Youtube::getYoutubeEmbedLink($url);
        $text1 = $this->getParam('text1');
        $text2 = $this->getParam('text2');
        $creator = Lingua_Auth::getLinguaId();
        $video_lang_id = $this->getParam('video_lang');

        $artist_id = Lingua_Query_System::hasArtist($artist_name);
        
        if(!$artist_id) {
            $artist_id = Lingua_Query_System::systemAddArtist($artist_name);
        }
        
        if ($type == 'save') {
            $status = "EDITING";
            Lingua_Auth::rejectUnlessPermission('video.save');
        }

        if ($type == 'publish') {
            Lingua_Auth::rejectUnlessPermission('video.publish');

            $status = "PUBLISHED";
        }

        if (!$video_id || $video_id == -1) {
            /*
             * if there is no posted video_id we must create else update existing
             */

            if (Lingua_Query_Video::videoHasUrl($url)) {
                header('Location: /admin/panel/video-add');
                exit;
            }

            Lingua_Query_Video::videoAdd($title, $category_id, $url, $artist_id, $text1, $text2, $creator, $status, $video_lang_id);
        } else {
            $result = Lingua_Query_Video::videoEdit($title, $category_id, $url, $artist_id, $text1, $text2, $creator, $status, $video_id, $video_lang_id);

            if ($result) {
                Lingua_Query_Video::videoSetModifiedLog($creator, $video_id);
            }
        }

        header("Location: /admin/panel/video");
        exit;
    }

    //**************************************************************************
    //-------------------------Audio managing-----------------------------------
    public function audioAction() {
        
        error_reporting(E_ALL);
        ini_set('display_errors', 'on');
        
        
        Lingua_Auth::rejectUnlessPermission('audio.managment');
        
        $this->view->title = "Аудио манаджмент";

        $this->view->categories = Lingua_Query_Audio_Category::categoryGetAll();
        $this->view->statuses = Lingua_Query_Audio::audioGetStatuses();
        $this->view->artists = Lingua_Query_Audio::audioGetArtists();
        
        $category_id = $this->view->category_id = $this->getParam('audio_category');
        $status = $this->view->status = $this->getParam('audio_status');
        $keyword = $this->view->keyword = $this->getParam('keyword');
        $artist = $this->view->artist = $this->getParam('audio_artists');
        $page = $this->getParam('page');
        
        $LIST = $this->view->LIST = new Panel_Audiolist($category_id, $status, $keyword, $artist, $page);
        $LIST->init();
    }

    //--------------------------------------------------------------------------
    public function audioAddAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('audio.add');

        $this->view->title = "Новое аудио";

        $this->view->categories = Lingua_Query_Audio_Category::categoryGetAll();
        $this->view->languages = Lingua_Auth::sysGetLanguages();
    }

    //--------------------------------------------------------------------------
    public function audioGetFromUrlAction() {
        $this->_helper->layout->disableLayout();

        $this->view->url = $this->getParam('url');
    }
    
   

    //--------------------------------------------------------------------------
    public function audioSaveAction() {
        Lingua_Auth::rejectUnlessAuth();
        $type = $this->getParam('type');
        $audio_id = $this->getParam('audio_id');

        $title = $this->getParam('title');
        $category_id = $this->getParam('categoty');
        $url = $this->getParam('url');
        $artist_name = $this->getParam('artist');
        $audio_lang = $this->getParam('audio_lang');
        //TODO: Do somthing with audio url
        $url = $url;
        //--------------------------------

        $artist_id = Lingua_Query_System::hasArtist($artist_name);
        
        if(!$artist_id) {
            $artist_id = Lingua_Query_System::systemAddArtist($artist_name);
        }
        
        
        $text1 = $this->getParam('text1');
        $text2 = $this->getParam('text2');
        $creator = Lingua_Auth::getLinguaId();

        if ($type == 'save') {

            Lingua_Auth::rejectUnlessPermission('audio.save');
            $status = "EDITING";
        }

        if ($type == 'publish') {

            Lingua_Auth::rejectUnlessPermission('audio.publish');
            $status = "PUBLISHED";
        }

        if (!$audio_id || $audio_id == -1) {
            /*
             * if there is no posted audio_id we must create else update existing
             */

            if (Lingua_Query_Audio::audioHasUrl($url)) {
                header('Location: /admin/panel/audio-add');
                exit;
            }

            Lingua_Query_Audio::audioAdd($title, $category_id, $url, $artist_id, $text1, $text2, $creator, $status, $audio_lang);
        } else {
            $result = Lingua_Query_Audio::audioEdit($title, $category_id, $url, $artist_id, $text1, $text2, $creator, $status, $audio_id, $audio_lang);

            if ($result) {
                Lingua_Query_Audio::audioSetModifiedLog($creator, $audio_id);
            }
        }

        header("Location: /admin/panel/audio");
        exit;
    }

    //--------------------------------------------------------------------------
    public function audioEditAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('audio.save');

        $this->view->title = "Редактирование ауидо";

        $audio_id = $this->getParam('audio_id');
        if (!$audio_id) {
            header('Location: /admin/panel/audio');
            exit;
        }

        $data = Lingua_Query_Audio::audioGet($audio_id);
        $this->view->categories = Lingua_Query_Audio_Category::categoryGetAll();
        $this->view->audio = new Lingua_Object($data);

        $this->view->languages = Lingua_Auth::sysGetLanguages();
    }

    //--------------------------------------------------------------------------
    public function audioDeleteAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('audio.delete');

        $this->_helper->layout->disableLayout();
        $audio_id = $this->getParam('audio_id');

        $result = Lingua_Query_Audio::audioRemove($audio_id);
        if ($result) {
            echo Lingua_View_SystemLog::successMessage("Аудио удачно удаленно!");
        }

        exit;
    }

}