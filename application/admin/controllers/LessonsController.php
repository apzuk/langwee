<?php

/**
 * Description of LessonsController
 *
 * @author Denis
 */
class Admin_LessonsController extends Lingua_Plugin_Action {
    
    
    public function indexAction()
    {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('lessons.managment');
        
        $this->view->title = 'Список уроков';
        
        $status = $this->view->status = $this->getParam('status');
        $creator = $this->view->status = $this->getParam('creator');
        $page = $this->view->status = $this->getParam('page');
        
        $LIST = $this->view->LIST = new Lessons_List($status, $creator, $page);        
        $LIST->init();
    }   
    
    
    public function addAction()
    {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('lessons.add');
        $this->view->languages = Lingua_Auth::sysGetLanguages();
    }
    
    public function editAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('lessons.edit');
        error_reporting(E_ALL);
        $this->view->title = "Редактирование урока";

        $lesson_id = $this->getParam('lesson_id');
        if (!$lesson_id) {
            header('Location: /admin/lessons');
            exit;
        }
        
        $data = Lingua_Query_Lessons::lessonGet($lesson_id);
               
        $this->view->lesson = new Lingua_Object($data);
        $this->view->languages = Lingua_Auth::sysGetLanguages();
    }
    
    public function ajaxworddeleteAction()
    {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('lessons.edit');
        
        $word_id = $this->getParam('word_id');
        
        
        Lingua_Query_Word::deleteWord($word_id);
        exit();
    }
    
    public function ajaxphrasedeleteAction()
    {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('lessons.edit');
        
        $phrase_id = $this->getParam('phrase_id');
        
        
        Lingua_Query_Phrase::deletePhrase($phrase_id);
        exit();
    }
    
    public function saveAction() {        
        Lingua_Auth::rejectUnlessAuth();
        
        $type = $this->getParam('type');
        $lesson_id = $this->getParam('lesson_id');

        $source = $this->getParam('source');
        
        $lesson_title = $this->getParam('lesson_title');
        $translate = $this->getParam('translate');
        
        $language_id = $this->getParam('language_id');
        
        $phrase_id = $this->getParam('phrase_id');
        $phrase = $this->getParam('phrase');
        $phrase_translate = $this->getParam('phrase_translate');
        $phrases = array(
            'phrase_id' => $phrase_id,
            'phrase' => $phrase,
            'phrase_translate' => $phrase_translate
        );
        
        $word_id = $this->getParam('word_id');
        $word = $this->getParam('word');
        $word_translate = $_POST['word_translate'];         
        $words = array(
            'word_id' => $word_id,
            'word' => $word,
            'word_translate' => $word_translate
        );
        
        $audio_history_url = $this->getParam('audio_history_url');
        $audio_vocabular_url = $this->getParam('audio_vocabular_url');
        $audio_ministory_url = $this->getParam('audio_ministory_url');
        $audio_ministory_text = $this->getParam('audio_ministory_text');
        
       
        $creator = Lingua_Auth::getLinguaId();

        if ($type == 'save') {

            Lingua_Auth::rejectUnlessPermission('audio.save');
            $status = "EDITING";
        }

        if ($type == 'publish') {
            Lingua_Auth::rejectUnlessPermission('audio.publish');
            $status = "PUBLISHED";
        }

        $data = array(
            'source' => $source,
            'lesson_title' => $lesson_title,
            'language_id' => $language_id,
            'translate' => $translate,
            'audio_history_url' => $audio_history_url,
            'audio_vocabular_url' => $audio_vocabular_url,
            'audio_ministory_url' => $audio_ministory_url,
            'audio_ministory_text' => $audio_ministory_text,
            'created_by' => $creator,
            'status' => $status,
            'words' => $words,
            'phrases' => $phrases
        );

        if (!$lesson_id || $lesson_id == -1) {            
            Lingua_Query_Lessons::lessonAdd($data);
            header("Location: /admin/lessons");
        } else {
            $result = Lingua_Query_Lessons::lessonEdit($lesson_id, $data);
        }

        header("Location: /admin/lessons");
        exit;
    }
     
}
