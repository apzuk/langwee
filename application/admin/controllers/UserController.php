<?php

/**
 * Description of User
 *
 * @author Aram
 */
class Admin_UserController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function listAction() {
        Lingua_Auth::rejectUnlessAuth("ADMIN");
        $this->view->title = "Пользователи";

        $LIST = $this->view->LIST = new User_Userlist();
        $LIST->init();
    }

    //--------------------------------------------------------------------------
    public function newAction() {
        Lingua_Auth::rejectUnlessAuth("ADMIN");
        $this->view->title = "Регистрация нового пользователя";

        $this->view->roles = Lingua_Query_Permission::roleGetAll();

        if ($this->isPost()) {

            $uploader = new Lingua_Uploader($_FILES);
            $response = $uploader->process();
            $email = $this->getParam('email');
            $firstname = $this->getParam('firstname');
            $surname = $this->getParam('surname');
            $gender = $this->getParam('gender');
            $age = $this->getParam('age');
            $role_id = $this->getParam('role_id');

            $password = Lingua_GlobalConfigs::generatePassword(9);

            if (count($response['errors']) == 0) {
                $reg = new User_Registration();
                $photo = $response['success'][0]['uploaded'];
                $data = array('firstname' => $firstname, 'surname' => $surname, 'age' => $age, 'gender' => $gender, "photo" => $photo);

                $reg->register($email, $password, $role_id, $data);
            }
        }
    }

    //--------------------------------------------------------------------------
    public function couponsAction() {
        error_reporting(E_ALL);
        ini_set('display_errors', 'on');
        Lingua_Auth::rejectUnlessPermission('user.coupons');

        //posted params
        $email = $this->getParam('email');
        $person = $this->getParam('person');
        $status = $this->getParam('status');

        //page title
        $this->view->title = "Купоны";

        //requested page
        $page = $this->getParam('page');

        //coupon action model!
        $LIST = $this->view->LIST = new User_Couponlist($email, $person, $status, $page);
        $LIST->init();

        if ($this->getParam('clean')) {
            $LIST = new User_Couponlist();
            $LIST->cleanResult();
        }

        //posted params
        $this->view->email = $LIST->getProp('email');
        $this->view->person = $LIST->getProp('firstname') . ' ' . $LIST->getProp('surname');
        $this->view->status = $LIST->getProp('status');


        if ($this->view->status == 'pending')
            $this->view->status = 1;
        if ($this->view->status == 'approved')
            $this->view->status = 0;
    }

    //--------------------------------------------------------------------------
    public function couponsAddAction() {
        $email = $this->getParam('email');
        $coupon = $this->getParam('coupon');
        $pin = $this->getParam('pin');

        //loopup for lingua_id by email
        $lingua_id = Lingua_Query_User::userGetLinguaIdbyEmail($email);

        Lingua_Query_User::userSetCoupon($coupon, $pin, $lingua_id);

        header('Location: /admin/user/coupons');
        exit;
    }

    //--------------------------------------------------------------------------
    public function couponsActiveAction() {
        Lingua_Auth::rejectUnlessPermission('user.coupons');

        try {
            $lingua_id = $this->getParam('lingua_id');
            $action = $this->getParam('action_type');
            $oleg = false;
            
            switch ($action) { 
                case 1: $date = 60 * 60 * 24 * 30;
                    break;
                case 2: $date = 60 * 60 * 24 * 30 * 6;
                    break;
                case 3: $date = 60 * 60 * 24 * 365;
                    break;
                case 4: $date = 60 * 60 * 24 * 365;
                    $oleg = true;
                    break;
                case -1: break;
            }

            $email = Lingua_Query_User::userGetEmailBYId($lingua_id);

            $d = strtotime(date('Y-m-d h:i:s')) + $date;
            Lingua_Query_User::beginTransaction();
            Lingua_Query_User::userActivateAccount($lingua_id, date('Y-m-d h:i:s', $d));
            Lingua_Query_User::userSetCouponApproved($lingua_id);

            if($oleg) {
                Lingua_Query_User::accessOlegTrainings($lingua_id);
            }
            
            //Create email
            $tr = new Zend_Mail_Transport_Smtp('langwee.com');
            Zend_Mail::setDefaultTransport($tr);

            $mail = new Zend_Mail('utf-8');
            $mail->setFrom("info@langwee.com", "Langwee");
            $mail->addTo($email);
            $mail->setSubject('Ваш Аккаунт Langwee активирован');

            $message = "Здравствуйте, " . $email . "\n";
            $message .= "Рады вам сообщить, что ваш аккаунт в Langwee активирован на 12 месяцев.\n";
            $message .= "\n\nХотим пожелать вам больших результатов в обучении новых языков. Дадим маленький совет - Если вы будете работать хотябы с одним Видео/Аудио в день, отрабатывая в тренингах неизвестные слова, то уже через месяц вы будете свободно говорить на иностранном языке ;)";
            $message .= "\n\nЖелаем вам Успехов.\n";
            $message .= "С уважением,\n";
            $message .= "Команда Langwee.com";

            $mail->setBodyText($message);


            try {
                $sent = $mail->send();
                Lingua_Query_User::commit();
                
                echo 'success';
            } catch (Exception $e) {
                echo $e->getMessage();
                // throw new Exception("Unable to send email");
                Lingua_Query_User::rollback();
            }
        } catch (Exception $e) {
            Lingua_Query_User::rollback();
        }
        exit;
    }

}

?>
