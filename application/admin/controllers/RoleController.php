<?php

/**
 * Description of RoleController
 *
 * @author Aram
 */
class Admin_RoleController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function managmentAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('role.managment');
        $this->view->title = "Права пользователей";
        
        $this->view->roles = Lingua_Query_Permission::roleGetPermissions();
        $this->view->role = new Role_Rolemanagment($this->view->roles);
    }

    //--------------------------------------------------------------------------
    public function newAction() {
        $this->view->title = "Новое право";

        if ($this->isPost()) {
            $permission = $this->getParam('permission');
            $description = $this->getParam('description');
            
            Lingua_Query_Permission::permissionAdd($permission, $description);
        }
    }
    
    //--------------------------------------------------------------------------
    public function saveAction() {
        $data = $this->getParam('data');
        
        $new_data = array();
        foreach($data as $d) {
           $parts = explode('_', $d['id']);
                
           $role = $parts[0];
           $perm = $parts[1];
           
           $new_data[$role][$perm] = $d['val'];
        }
                
        foreach($new_data as $role_id=>$role) {
            foreach($role as $perm_id => $perm) {
                echo $perm;
                if($perm == 0) Lingua_Query_Permission::permissionRevole($role_id, $perm_id);
                else Lingua_Query_Permission::permissionGrant($role_id, $perm_id);
            }
        }
        
        exit;
    }

}

?>
