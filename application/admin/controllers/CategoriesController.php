<?php

/**
 * Description of Categories
 *
 * @author Aram 
 */
class Admin_CategoriesController extends Lingua_Plugin_Action {
    /*
     * video categories managment
     * 
     */

    //--------------------------------------------------------------------------
    public function videoAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.category.managment');

        $this->view->title = "Видео категории";
        if ($this->isPost()) {
            $category_name = $this->getParam('category_name');
            $category_rank = $this->getParam('category_rank');
            $category_id = $this->getParam('category_id');

            if ($category_id == -1 || !$category_id) {
                //TODO. add permission check
                //if there is no category id we should add category else update existing
                Lingua_Query_Video_Category::categoryAdd($category_name, $category_rank);
            } else {
                Lingua_Query_Video_Category::categoryUpdate($category_name, $category_rank, $category_id);
            }
        }

        $this->view->lists = Lingua_Query_Video_Category::categoryGetAll();
    }

    //--------------------------------------------------------------------------
    public function videoAddCategoryAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.category.add');

        $this->_helper->layout->disableLayout();

        exit;
    }

    //--------------------------------------------------------------------------
    public function videoCategoryAddFormAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.category.add');

        $this->_helper->layout->disableLayout();

        $cat_id = $this->getParam('cat_id');

        $this->view->category = Lingua_Query_Video_Category::categoryGet($cat_id);
        $this->view->lastest_rank = Lingua_Query_Video_Category::categoryLastestRank();
    }

    //--------------------------------------------------------------------------
    public function videoDeleteAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('video.category.delete');
        $cat_id = $this->getParam('cat_id');

        $hasVideos = Lingua_Query_Video_Category::categoryHasVideo($cat_id);
        if ($hasVideos) {
            echo Lingua_View_SystemLog::successMessage("Вы не можете удалить данную категорию. С начало удалите видео под категорией");
            exit;
        } else {
            $result = Lingua_Query_Video_Category::categoryDelete($cat_id);
            if ($result) {
                echo Lingua_View_SystemLog::failMessage("Категория успешно удалено");
            }
            exit;
        }

        exit;
    }

    /*
     * audio categories managment
     * 
     */

    //--------------------------------------------------------------------------
    public function audioAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('audio.category.managment');
        $this->view->title = "Аудио категории";

        if ($this->isPost()) {
            $category_name = $this->getParam('category_name');
            $category_rank = $this->getParam('category_rank');
            $category_id = $this->getParam('category_id');

            if ($category_id == -1 || !$category_id) {
                //TODO. add permission check
                //if there is no category id we should add category else update existing
                Lingua_Query_Audio_Category::categoryAdd($category_name, $category_rank);
            } else {
                Lingua_Query_Audio_Category::categoryUpdate($category_name, $category_rank, $category_id);
            }
        }

        $this->view->lists = Lingua_Query_Audio_Category::categoryGetAll();
    }

    //--------------------------------------------------------------------------
    public function audioCategoryAddFormAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('audio.category.add');
        $this->_helper->layout->disableLayout();

        $cat_id = $this->getParam('cat_id');

        $this->view->category = Lingua_Query_Audio_Category::categoryGet($cat_id);
        $this->view->lastest_rank = Lingua_Query_Audio_Category::categoryLastestRank();
    }

    //--------------------------------------------------------------------------
    public function audioDeleteAction() {
        Lingua_Auth::rejectUnlessAuth();
        Lingua_Auth::rejectUnlessPermission('audio.category.delete');
        $cat_id = $this->getParam('cat_id');

        $hasVideos = Lingua_Query_Audio_Category::categoryHasAudio($cat_id);
        if ($hasVideos) {
            echo Lingua_View_SystemLog::successMessage("Вы не можете удалить данную категорию. С начало удалите аудио под категорией");
            exit;
        } else {
            $result = Lingua_Query_Audio_Category::categoryDelete($cat_id);
            if ($result) {
                echo Lingua_View_SystemLog::failMessage("Категория успешно удалено");
            }
            exit;
        }

        exit;
    }

}

?>
