<?php

/**
 * Description of Cron
 *
 * @author Aram
 */
class Cron_CronjobsController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function checkUserStatusAction() {
        Lingua_Query_User::userUpdateOnlineStatus();
        exit;
    }

    //--------------------------------------------------------------------------
    public function generateAction() {
        for ($index = 19256; $index <= 20000; $index++) {

            $lingua_id = Lingua_Query_User::userInsert('testuser' . $index . '@testmail.com', 'test1234', 2);
            $gender = rand(0, 1);
            if ($gender == 0)
                $gender = 'Male';
            else
                $gender = 'Female';
            Lingua_Query_User::userSetInfo($this->generate_name(rand(6, 8)), $this->generate_name(rand(8, 10)), rand(10, 60), $gender, '', $lingua_id);

            $langs = array(0, 1, 2, 3, 4, 5, 6, 7);
            $types = array('KNOW', 'LEARNING');
            $lvl = array(2, 3, 4);
            $lang_new = $langs;
            for ($i = 0; $i < count($langs); $i++) {
                $k = rand(0, 7);
                $tmp = $langs[$i];
                $lang_new[$i] = $langs[$k];
                $lang_new[$k] = $tmp;
            }

            $lang_new = array_unique($lang_new);

            Lingua_Query_User::userSetLanguage($lang_new[0], 'MOTHERLANG', $lvl[rand(0, 2)], $lingua_id);
            foreach ($lang_new as $lang) {
                Lingua_Query_User::userSetLanguage($lang, $types[rand(0, 1)], $lvl[rand(0, 2)], $lingua_id);
            }

            echo 'testuser' . $index . '@testmail.com -> done!<br />';
            flush();
            ob_flush();
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function generate_name($length = 6) {
        $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ+-*#&@!?";
        $validCharNumber = strlen($validCharacters);

        $result = "";

        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }

        return $result;
    }

}

?>
