<?php

/**
 * Description of TrainingController
 *
 * @author Aram
 */
class Trainings_TrainingController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function listAction() {
        Lingua_Auth::rejectUnlessAuth('USER');

        $account = Lingua_Auth::isVIPAccount();
        $lingua_id = Lingua_Auth::getLinguaId();
        $trainings = Lingua_Query_System_Trainings::trainingsGetIds();

        $lang = Lingua_Auth::getLanguage();

        $data = array(
            'word-translate' => array(
                'img' => '/images/training/all/tren-word-trans.jpg',
                'title' => 'Перевод Слова',
                'available' => Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['word-translate']['id'])
            ),
            'translate-word' => array(
                'img' => '/images/training/all/tren-trans-word.jpg',
                'title' => 'Обратный Перевод Слова',
                'available' => Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['translate-word']['id'])
            ),
            'phrase-translate' => array(
                'img' => '/images/training/all/tren-trans-word.jpg',
                'title' => 'Перевод Фразы',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['phrase-translate']['id']) : -1
            ),
            'translate-phrase' => array(
                'img' => '/images/training/all/tren-trans-word.jpg',
                'title' => 'Обратный перевод фразы',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['translate-phrase']['id']) : -1
            ),
            'constructor' => array(
                'img' => '/images/training/all/tren-constructor.jpg',
                'title' => 'Конструктор',
                'available' => Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['constructor']['id'])
            ),
            'listening' => array(
                'img' => '/images/training/all/tren-audirovanie.jpg',
                'title' => 'Аудирование-Правописание',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['listening']['id']) : -1
            ),
            'listening-translating' => array(
                'img' => '/images/training/all/tren-audirovanie.jpg',
                'title' => 'Аудирование-Перевод',
                'available' => Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['listening-translating']['id'])
            ),
            'spelling' => array(
                'img' => '/images/default/main/tren-prawopisanie.jpg',
                'title' => 'Правописание',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['spelling']['id']) : -1
            ),
            'translate-phrase-favourite' => array(
                'img' => '/images/training/all/tren-trans-word.jpg',
                'title' => 'Перевод популярных фразы',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['phrase-translate']['id']) : -1
            ),
            'phrase-translate-favourite' => array(
                'img' => '/images/training/all/tren-trans-word.jpg',
                'title' => 'Обратный перевод популярных фраз',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['phrase-translate']['id']) : -1
            ),
            'crossword' => array(
                'img' => '/images/training/all/tren-crossword.jpg',
                'title' => 'Мой Кроссворд',
                'available' => $account ? Lingua_Query_User_Words::userWordAvailableCount($lingua_id, $lang, $trainings['crossword']['id']) : -1
            ),
        );

        $this->view->data = $data;
    }

    //--------------------------------------------------------------------------
    public function wordTranslateAction() {
        $this->_helper->layout->disableLayout();
        $clear = $this->getParam('clear');
        $result = $this->getParam('store', array());
        $lang = Lingua_Auth::getLanguage();

        $this->view->type = 'word_translate';

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Wordtranslate($lang);
        $training->store($result);

        if ($clear) {
            $training->clean();
            $count = $training->getUserAvailableWordCount();

            $returnData = array();
            if ($count < 10) {
                $returnData['empty'] = true;
                echo json_encode($returnData);
                exit;
            }
        }

        $data = $training->getRandomUserWords();
        $first = array_shift($data);
        $first = array_shift($first);

        $fobj = new Lingua_Object($first);
        $returnData['word'] = $fobj->word_src;
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = $fobj->user_word_id;
        $returnData['options'][$fobj->user_word_id] = $fobj->word_trg;

        foreach ($data['random'] as $k => $d) {
            $obj = new Lingua_Object($d);
            $returnData['options'][$obj->user_word_id] = preg_replace('/\s+/', '', strtolower($obj->word_trg));
        }

        $keys = array_keys($returnData['options']);
        shuffle($keys);

        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $returnData['options'][$key];
        }
        $returnData['options'] = $random;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function translateWordAction() {
        $this->_helper->layout->disableLayout();
        $clear = $this->getParam('clear');
        $result = $this->getParam('store', array());
        $this->view->type = 'word_translate';

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Translateword(Lingua_Auth::getLanguage());
        $training->store($result);

        if ($clear) {
            $training->clean();
            $count = $training->getUserAvailableWordCount();

            $returnData = array();
            if ($count < 10) {
                $returnData['empty'] = true;
                echo json_encode($returnData);
                exit;
            }
        }

        $data = $training->getRandomUserWords();
        $first = array_shift($data);
        $first = array_shift($first);

        $fobj = new Lingua_Object($first);
        $returnData['word'] = $fobj->word_trg;
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = $fobj->user_word_id;
        $returnData['options'][$fobj->user_word_id] = $fobj->word_src;

        foreach ($data['random'] as $k => $d) {
            $obj = new Lingua_Object($d);
            $returnData['options'][$obj->user_word_id] = preg_replace('/\s+/', '', strtolower($obj->word_src));
        }

        $keys = array_keys($returnData['options']);
        shuffle($keys);

        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $returnData['options'][$key];
        }
        $returnData['options'] = $random;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function constructorAction() {
        $this->_helper->layout->disableLayout();
        $clear = $this->getParam('clear');
        $result = $this->getParam('store', array());

        $this->view->type = 'constructor';
        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Constructor(Lingua_Auth::getLanguage());
        $training->store($result);

        if ($clear) {
            $training->clean();
            $count = $training->getUserAvailableWordCount();

            $returnData = array();
            if ($count < 10) {
                $returnData['empty'] = true;
                echo json_encode($returnData);
                exit;
            }
        }

        $data = new Lingua_Object($training->getRandomUserWord());

        $returnData['word'] = $data->word_src;
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = $data->user_word_id;
        $returnData['options'][$data->user_word_id] = $data->word_trg;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function listeningAction() {
        $this->_helper->layout->disableLayout();
        $clear = $this->getParam('clear');
        $result = $this->getParam('store', array());

        $this->view->type = 'constructor';
        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Listening(Lingua_Auth::getLanguage());
        $training->store($result);

        if ($clear) {
            $training->clean();
            $count = $training->getUserAvailableWordCount();

            $returnData = array();
            if ($count < 10) {
                $returnData['empty'] = true;
                echo json_encode($returnData);
                exit;
            }
        }

        $data = new Lingua_Object($training->getRandomUserWord());

        $returnData['word'] = $data->word_src;
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = $data->user_word_id;
        $returnData['options'][$data->user_word_id] = $data->word_trg;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function spellingAction() {
        $this->_helper->layout->disableLayout();
        $clear = $this->getParam('clear');
        $result = $this->getParam('store', array());

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Spelling(Lingua_Auth::getLanguage());
        $training->store($result);

        if ($clear) {
            $training->clean();
            $count = $training->getUserAvailableWordCount();

            $returnData = array();
            if ($count < 10) {
                $returnData['empty'] = true;
                echo json_encode($returnData);
                exit;
            }
        }

        $data = new Lingua_Object($training->getRandomUserWord());

        $returnData['word'] = $data->word_src;
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = $data->user_word_id;
        $returnData['options'][$data->user_word_id] = $data->word_trg;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function translatePhraseAction() {
        $this->_helper->layout->disableLayout();
        $lang = $this->getParam('lang');
        $clear = $this->getParam('clear');

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Phrasetranslate(Lingua_Auth::getLanguage());

        if ($clear)
            $training->clean();
        $data = $training->getPhrases();
        $first = array_shift($data);

        $returnData['word'] = $first[1];
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = 0;
        $returnData['options']['id_0'] = $first[0];

        foreach ($data as $k => $d) {
            $returnData['options']['id_' . ($k + 1)] = strtolower($d[0]);
        }

        $keys = array_keys($returnData['options']);
        shuffle($keys);
        shuffle($keys);


        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $returnData['options'][$key];
        }
        $returnData['options'] = $random;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function phraseTranslateAction() {
        $this->_helper->layout->disableLayout();
        $lang = $this->getParam('lang');
        $clear = $this->getParam('clear');

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Phrasetranslate(Lingua_Auth::getLanguage());

        if ($clear)
            $training->clean();
        $data = $training->getPhrases();
        $first = array_shift($data);

        $returnData['word'] = $first[0];
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = 0;
        $returnData['options']['id_0'] = $first[1];

        foreach ($data as $k => $d) {
            $returnData['options']['id_' . ($k + 1)] = strtolower($d[1]);
        }

        $keys = array_keys($returnData['options']);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $returnData['options'][$key];
        }

        $returnData['options'] = $random;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function listeningTranslatingAction() {
        $this->_helper->layout->disableLayout();
        $clear = $this->getParam('clear');
        $result = $this->getParam('store', array());

        $this->view->type = 'constructor';
        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss 
        Lingua_GlobalKeys::storeKey('video_page', $key);

        $training = new Training_Listening(Lingua_Auth::getLanguage());
        $training->store($result);

        if ($clear) {
            $training->clean();
            $count = $training->getUserAvailableWordCount();

            $returnData = array();
            if ($count < 10) {
                $returnData['empty'] = true;
                echo json_encode($returnData);
                exit;
            }
        }

        $data = new Lingua_Object($training->getRandomUserWord());

        $returnData['word'] = $data->word_src;
        $returnData['tries'] = $training->triesCount();
        $returnData['right'] = $data->user_word_id;
        $returnData['options'][$data->user_word_id] = $data->word_trg;

        echo json_encode(array('data' => $returnData));
        exit;
    }

    //--------------------------------------------------------------------------
    public function crosswordAction() {
        $crossword = new Training_Crossword();
        $this->view->words = $crossword->getWords();
    }

    //--------------------------------------------------------------------------
    public function storeAction() {
        $result = $this->getParam('result');

        $training = new Training_Wordtranslate();
        $training->store($result, 'TRAINED');

        echo 'done';
        exit;
    }

    //--------------------------------------------------------------------------
    public function trainingCoachingAction() {
        Lingua_Auth::rejectUnlessAuth('USER');
        $lingua_id = Lingua_Auth::getLinguaId();

        if (!Lingua_Query_User::hasOlegAccess($lingua_id)) {
            $this->view->denied = true;
        } else {
            $this->view->disable_langbar = true;
            $id = $this->getParam('id', 1);

            $xml_file = Daz_String::merge('[private]/inc/coaching.xml', array('private' => PRIVATE_PATH));
            $XML = simplexml_load_file($xml_file, 'SimpleXMLElement', LIBXML_NOCDATA);

            $this->view->id = $id;
            $this->view->data = $XML;
            
            foreach ($XML as $lesson) {
                if ($lesson->id == $id) {
                    $this->view->lesson = $lesson;
                    break;
                }
            }
        }
    }

}

?>
