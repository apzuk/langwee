<?php

/**
 * Description of Listening
 *
 * @author Aram
 */
class Training_Listening extends Lingua_Helper_Trainings {
    //--------------------------------------------------------------------------
    function __construct($lang = 'en') {
        $this->lang = $lang;
 
        //current training name
        $this->training_name = "listening";
        $this->setMyId();

        //init session 
        $this->SESS = new Daz_Session(__CLASS__);
    }
}

?>
