<?php

/**
 * Description of Listening
 *
 * @author Aram
 */
class Training_Spelling extends Lingua_Helper_Trainings {
    //--------------------------------------------------------------------------
    function __construct($lang = 'en') {
        $this->lang = $lang;
 
        //current training name
        $this->training_name = "spelling";
        $this->setMyId();

        //init session 
        $this->SESS = new Daz_Session(__CLASS__);
    }
}

?>
