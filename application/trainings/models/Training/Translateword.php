<?php

/**
 * Description of Translateword
 *
 * @author Aram
 */
class Training_Translateword extends Lingua_Helper_Trainings {

    //--------------------------------------------------------------------------
    function __construct($lang = 'en') {
        $this->lang = $lang;

        //current training name
        $this->training_name = "translate-word";
        $this->setMyId();

        //init session 
        $this->SESS = new Daz_Session(__CLASS__);
    }

}

?>
