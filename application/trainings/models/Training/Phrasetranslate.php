<?php

/**
 * Description of Wordtranslate
 *
 * @author Aram
 */
class Training_Phrasetranslate extends Lingua_Helper_Trainings {

    //--------------------------------------------------------------------------
    function __construct($lang = 'en') {
        $this->lang = $lang;

        //current training name
        $this->training_name = "phrase-translate";
        $this->setMyId();

        //init session 
        $this->SESS = new Daz_Session(__CLASS__);
    }

    //--------------------------------------------------------------------------
    function getPhrases() {
        $data = $this->getRandomVideosText($this->lang);
        $returnData = array();
        foreach ($data as $d) {
            //get random row in the video text with his translation
            $random = $this->getRandomRows($d);
            if (is_array($random) && count($random) > 0) {
                $returnData[] = $random;
            }
            
            if (count($returnData) == 5) {
                return $returnData;
            }
        }

        exit;
    }

    //--------------------------------------------------------------------------
    function getRandomRows($data) {
        //break text to apart width delimer like <br />
        $d1 = preg_split('/<br\s\/>/', nl2br($data['text1']), -1, PREG_SPLIT_NO_EMPTY);
        $d2 = preg_split('/<br\s\/>/', nl2br($data['text2']), -1, PREG_SPLIT_NO_EMPTY);

        $d = $this->filerArray($d1, $d2);
        $rindex = rand(0, count($d['data1']) - 1);

        if (count($d['data1']) == 0)
            return false;
        
        $returnData = array($d['data1'][$rindex], $d['data2'][$rindex]);
        return $returnData;
    }

    //--------------------------------------------------------------------------
    public function filerArray($arr1, $arr2) {
        $new_data = array('data1' => array(), 'data2' => array());
        foreach ($arr1 as $rowindex => $row) {
            $row = trim($row);
 
            //we don't need empty rows which can be 
            //by breaking with <br />
            if (empty($row)) {
                continue 1;
            }

            if (!isset($arr2[$rowindex]))
                continue 1; 
            else { 
                $s = trim($arr2[$rowindex]);   
                if(empty($s)) {  
                    continue 1;
                } 
            }

            $words = $this->getRowWordsCount($row);
            $count = count($words);
            $rowlen = 0;

            //the row words count must be between 3 and 6
            if ($count > 3 && $count < 6) {
                foreach ($words as $word) {
                    //if row has 3 such words, we just break it
                    if ($this->getWordCountInTheRow($words, $word) >= 3) {
                        continue 1;
                    }

                    $rowlen += strlen($word);
                }

                if ($rowlen > 50) {
                    continue 1;
                }
            } else {
                continue 1;
            }

            $new_data['data1'][] = trim($row);
            $new_data['data2'][] = trim($arr2[$rowindex]);
        }
        return $new_data;
    }

    //--------------------------------------------------------------------------
    public function getRowWordsCount($row) {
        $words = preg_split('/\s+/', $row);
        return $words;
    }

    //--------------------------------------------------------------------------
    public function getWordCountInTheRow($words, $word) {
        $counter = 0;
        foreach ($words as $w) {
            if (strtolower($w) == strtolower($word)) {
                $counter++;
            }
        }

        return $counter;
    }

}

?>
