<?php

/**
 * Description of Crossword
 *
 * @author Aram
 */
class Training_Crossword extends Lingua_Helper_Trainings {

    //--------------------------------------------------------------------------
    function __construct($lang = 'en') {
        $this->lang = $lang;

        //current training name
        $this->training_name = "constructor";
        $this->setMyId();

        //init session 
        $this->SESS = new Daz_Session(__CLASS__);
    }

    //--------------------------------------------------------------------------
    public function getWords() { 
        Lingua_Query_User_Words::beginTransaction();
        $words = array(); 
        
        $word = Lingua_Query_User_Words::userWordWithRandomWord(Lingua_Auth::getLinguaId(), $this->lang, $this->id);
        $words[] = $word; 
          
        for ($i = 0; $i < 9; $i++) {
            $index = rand(0, strlen($word['word_src']) - 1); 
            $random = $word['word_src'][$index];
            
            $word = Lingua_Query_User_Words::userWordWithRandomWordHasLetter(Lingua_Auth::getLinguaId(), $this->lang, $this->id, $random);
            $words[] = $word;
        }
                
        Lingua_Query_User_Words::commit();
        
        return $words;
    }

}

?>
