<?php

/**
 * Description of Wordtranslate
 *
 * @author Aram
 */
class Training_Wordtranslate extends Lingua_Helper_Trainings {

    //--------------------------------------------------------------------------
    function __construct($lang = 'en') {
        $this->lang = $lang;

        //current training name
        $this->training_name = "word-translate";
        $this->setMyId();

        //init session 
        $this->SESS = new Daz_Session(__CLASS__);
    }
}

?>
