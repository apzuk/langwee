<?php

/**
 * Description of Users
 *
 * @author Aram
 */
class Coummunity_Users {

    private $know = null;
    private $learning = null;
    private $users_learning = null;
    private $users_know = null;

    public function __construct($know = null, $learning = null) {
        $this->know = $know;
        $this->learning = $learning;
    }

    //--------------------------------------------------------------------------
    public function getRandomUsers($length = 36) {
        $this->users_learning = Lingua_Query_User::userGetRandom(null, $this->learning, Lingua_Auth::getLinguaId(), 12);
        $this->users_know = Lingua_Query_User::userGetRandom($this->know, null, Lingua_Auth::getLinguaId(), 12);
    }

    //--------------------------------------------------------------------------
    public function getUsers($index) {
        if ($index == 0) return $this->users_know;
        if ($index == 1) return $this->users_learning;
        
        return array();
    }
}
?>