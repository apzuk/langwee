<?php

/**
 * Description of Search
 *
 * @author Aram
 */
class Coummunity_Search
{

    private $limit = 21;
    private $SESS = null;

    function __construct($name = null, $yfrom = null, $yto = null, $learning = null, $know = null, $lvl = null, $gender, $page = null, $clear = false)
    {
        $this->SESS = new Daz_Session(__CLASS__);
        if (trim($name) == "имя, фамилия или никнейм")
            $name = "";

        if($clear) $this->SESS->page = 0;
        if ($page !== null && !empty($page)) {
            $this->SESS->page = $page;
        }

        if ($name !== null) {
            $names = preg_split('/\s+/', $name, 0, PREG_SPLIT_NO_EMPTY);
            $this->SESS->fname = array_shift($names);
            $this->SESS->sname = array_shift($names);

            $this->SESS->name = $name;
        }

        if ($gender) {
            if ($gender == 'man') {
                $this->SESS->gender = "Male";
            } elseif ($gender == 'woman') {
                $this->SESS->gender = "Female";
            } else {
                $this->SESS->gender = false;
            }
        }

        if (empty($this->SESS->sname)) {
            $this->SESS->nickname = $this->SESS->fname;
        }

        if ($yfrom !== null) {
            $this->SESS->yfrom = $yfrom;
            if ($this->SESS->yfrom == 'от') {
                $this->SESS->yfrom = false;
            }
        }

        if ($yto !== null) {
            $this->SESS->yto = $yto;
            if ($this->SESS->yto == 'до') {
                $this->SESS->yto = false;
            }
        }

        if ($learning !== null) {
            $this->SESS->learning = $learning;
        }

        if ($know !== null) {
            $this->SESS->know = $know;
        }

        if ($lvl !== null) {
            $this->SESS->lvl = $lvl;
        }
    }

    //--------------------------------------------------------------------------
    public function getRecordsCount()
    {
        return Lingua_Query_User::userSearchCount(
            $this->SESS->fname, $this->SESS->sname, $this->SESS->nickname, $this->SESS->yfrom, $this->SESS->yto, $this->SESS->learning, $this->SESS->know, $this->SESS->lvl, $this->SESS->gender, Lingua_Auth::getLinguaId());
    }

    //--------------------------------------------------------------------------
    public function getRecords()
    {
        return Lingua_Query_User::userSearch(
            $this->SESS->fname, $this->SESS->sname, $this->SESS->nickname, $this->SESS->yfrom, $this->SESS->yto, $this->SESS->learning, $this->SESS->know, $this->SESS->lvl, $this->SESS->gender, Lingua_Auth::getLinguaId(), $this->SESS->page * $this->limit, $this->limit);
    }

    //--------------------------------------------------------------------------
    public function get($prop)
    {
        return $this->SESS->$prop;
    }

    //--------------------------------------------------------------------------
    public function setPage($page)
    {
        $this->SESS->page = --$page;
    }

}

?>
