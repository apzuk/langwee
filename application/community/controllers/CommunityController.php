<?php

/**
 * Description of UserController
 *
 * @author Aram
 */
class Community_CommunityController extends Lingua_Plugin_Action
{

    //--------------------------------------------------------------------------
    public function chatAction()
    {
        Lingua_Auth::rejectUnlessAuth("USER");

        $USERS = $this->view->USERS = new Coummunity_Users();
        $USERS->getRandomUsers();

        $this->view->languages = Lingua_Query_System::languagesGetAll();
        $this->view->title = "Чат";
    }

    //--------------------------------------------------------------------------
    public function chatRoomAction()
    {
        Lingua_Auth::rejectUnlessAuth("USER");

        $chat_room_id = $this->view->chat = $this->getParam('chat_room');

        $schat = new Lingua_Selected_Chat($chat_room_id);
        $schat->rejectUnlessValid();

        $this->view->interlocutor = new Lingua_Object($schat->getInterlocutor());

        $this->view->me = $schat->getMe();

        $schat->getInterlocutorLanguages();
        $this->view->languages = Lingua_Query_System::languagesGetAll();
        $this->view->phrases = Lingua_Query_System::prasesGetByLanguage(1);

        $this->view->know = $schat->getLang('KNOW');
        $this->view->learning = $schat->getLang('LEARNING');
        $this->view->smiles = $schat->getSmiles();

        $this->view->title = "Чат";

        $this->view->flags = array(
            'Английский' => 'england.png',
            'Французский' => 'france.png',
            'Немецкий' => 'germany.png',
            'Италянский' => 'italy.png',
            'Русский' => 'russian.png',
            'Испанский' => 'spain.png'
        );

        $this->view->short = array(
            'Английский' => 'en',
            'Французский' => 'fr',
            'Немецкий' => 'de',
            'Италянский' => 'it',
            'Русский' => 'ru',
            'Испанский' => 'es'
        );

        $key = $this->view->key = Lingua_GlobalKeys::generateKey();
        Lingua_GlobalKeys::storeKey('chat_room', $key);
    }

    //--------------------------------------------------------------------------
    public function chatInviteAction()
    {
        //Loggned in user lingua_id
        $owner_lingua_id = Lingua_Auth::getLinguaId();

        //set invitation to follow lingua_id
        $lingua_id = $this->getParam('lingua_id');

        try {
            //begin the transaction
            Lingua_Query_Notification::beginTransaction();

            //check if the chat room exists redirect him to the chat room
            $isChat = Lingua_Query_Chat::isChatExist(Lingua_Auth::getLinguaId(), $lingua_id);

            if ((boolean)$isChat) {
                header('Location: /chat-room/' . $isChat);
                exit;
            }

            //create a chat room
            $chat_id = Lingua_Query_Chat::chatCreate($owner_lingua_id, $lingua_id);
            if ($chat_id === false)
                throw new Exception("unable to create a chat");
            //chat root is created , fuck yeah!
            //check if the notification exists redirect back :D
            $is = Lingua_Query_Notification::isNotificationExists($owner_lingua_id, $lingua_id, 1);
            if ($is) {
                header('Location: /chat');
                exit;
            }

            //if the notification does not exist set it!
            $result = Lingua_Query_Notification::notificationChatting($owner_lingua_id, $lingua_id);
            if ($result === false)
                throw new Exception("unable to set notification");

            //set а message for sender to wait for a response from the 
            $id = Lingua_Query_Chat::chatAddMessage($chat_id, "Пожалуйста подождите ответа собеседника!");
            if ($id === false)
                throw new Exception("Unable to set prepare message");

            //set user status for this chat avaliable
            Lingua_Query_Chat::chatSetUserAvaliableStatus($owner_lingua_id, $chat_id);

            Lingua_Query_Notification::commit();
            header("Location: /chat-room/" . $chat_id);
        } catch (Exception $e) {
            echo $e->getMessage();
            Lingua_Query_Notification::rollback();
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function notificationGetAction()
    {
        $this->_helper->layout->disableLayout();
        $lingua_id = Lingua_Auth::getLinguaId();

        $notification = Lingua_Query_Notification::notificationGet($lingua_id);
        $notification = new Lingua_Object($notification);
        $type = false;
        if ($notification->notification_type == "CHATTING") {
            $message = "Хочет пообщаться с Вами!";
            $type = "n_li";
        } elseif ($notification->notification_type == "CHAT_REJECT") {
            $message = "Отказался(ась) общаться с Вами!";
            $type = "n_li";
        } elseif ($notification->notification_type == "FRIEND_REQUEST") {
            $message = "Хочет добавить Вас в друзья";
            $type = "f_li";
        } elseif ($notification->notification_type == "FRIEND_REQUEST_REJECTED") {
            $message = "Отказался(ась) подружиться с Вами";
            $type = "f_li";
        } elseif ($notification->notification_type == "FRIEND_REQUEST_APPROVED") {
            $message = "Принял(а) Ваше предложение подружить!";
            $type = "f_li";
        } elseif ($notification->notification_type == "FRIEND_REMOVE") {
            $message = "Удалил(а) из списка своих друьзей!";
            $type = "f_li";
        } elseif ($notification->notification_type == "USER_BLOCK") {
            $message = "заблокировал(а) Вас";
            $type = "n_li";
        } else {
            echo json_encode(array('data' => array('empty' => true),
                'credit' => Lingua_Query_User_Credit::userCreditGet(Lingua_Auth::getLinguaId()),
                'paid' => Lingua_Query_User::userIsPaid($lingua_id)));
            exit;
        }

        if ($notification->notification_text == 2) {
            $this->view->sender_lingua_id = $notification->sender_lingua_id;
        }

        $photo = Lingua_Storage_User::getUrl($notification->sender_lingua_id, 95, 95);
        $person = $notification->person;
        $chat_id = Lingua_Query_Chat::chatGet($notification->sender_lingua_id, $lingua_id);
        $notification_id = $notification->user_notofication;

        $result = Daz_String::merge($notification->body, array(
            'photo' => $photo,
            'person' => $person,
            'chat_id' => $chat_id,
            'notification_id' => $notification_id,
            'message' => $message,
            'profability' => (int)$notification->profability,
            'lingua_id' => $notification->sender_lingua_id
        ));

        echo json_encode(array('data' => $result, 'type' => $type,
            'credit' => Lingua_Query_User_Credit::userCreditGet(Lingua_Auth::getLinguaId()),
            'paid' => Lingua_Query_User::userIsPaid($lingua_id)));
        exit;
    }

    //--------------------------------------------------------------------------
    public function notificationGotAction()
    {
        $id = $this->getParam('id');
        $lingua_id = Lingua_Auth::getLinguaId();

        $result = Lingua_Query_Notification::notificationSetGotStatus($id, $lingua_id);
        echo json_encode(array('result' => (boolean)$result));
        exit;
    }

    //--------------------------------------------------------------------------
    public function chatApproveAction()
    {
        $chat_id = $this->getParam('chat_id');
        $sender_lingua_id = $this->getParam('sender_lingua_id');
        $notification = $this->getParam('notification');

        try {
            Lingua_Query_Chat::beginTransaction();

            $result = Lingua_Query_Chat::chatApprove($chat_id, $sender_lingua_id, Lingua_Auth::getLinguaId());
            if ($result === false)
                throw new Exception('try again');

            $result = Lingua_Query_Notification::notificationSetApproved($notification, $sender_lingua_id);
            if ($result === false)
                throw new Exception('try again');

            $result = Lingua_Query_Chat::chatSetUserAvaliableStatus(Lingua_Auth::getLinguaId(), $chat_id);

            Lingua_Query_Chat::commit();
            header('Location: /chat-room/' . $chat_id);
            exit;
        } catch (Exception $e) {
            Lingua_Query_Chat::rollback();
            header('Location: /chat');
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function notificationRejectAction()
    {
        $notification_id = $this->getParam('id');
        $not = Lingua_Query_Notification::notificationGet(Lingua_Auth::getLinguaId(), $notification_id);
        $not = new Lingua_Object($not);
        // $not->dump();
        if ($not->notification_type == "CHATTING") {
            //invite for chat
            try {
                Lingua_Query_Notification::beginTransaction();
                $result = Lingua_Query_Notification::notificationSetGotStatus($not->user_notofication, Lingua_Auth::getLinguaId());
                if ($result === false)
                    throw new Exception('Can\'t set the notification');

                $result = Lingua_Query_Notification::notificationChatReject(Lingua_Auth::getLinguaId(), $not->sender_lingua_id);
                if ($result === false)
                    throw new Exception('Can\'t set chat reject notification');

                Lingua_Query_Notification::commit();
            } catch (Exception $e) {
                Lingua_Query_Notification::rollback();
            }
        }

        if ($not->notification_type == "FRIEND_REQUEST") {
            //invite for chat
            try {
                Lingua_Query_Notification::beginTransaction();
                $result = Lingua_Query_Notification::notificationSetGotStatus($not->user_notofication, Lingua_Auth::getLinguaId());
                if ($result === false)
                    throw new Exception('Can\'t set the notification');

                $id = Lingua_Query_Notification::notificationFriendRequestRejected(Lingua_Auth::getLinguaId(), $not->sender_lingua_id);
                if ($id === false) {
                    throw new Exception('Can\'t set the notification');
                }

                Lingua_Query_Notification::commit();
            } catch (Exception $e) {
                Lingua_Query_Notification::rollback();
            }
        }

        exit;
    }

    //--------------------------------------------------------------------------
    public function chatChangeStatusAction()
    {
        $status = $this->getParam('status');
        $chat_id = $this->getParam('chat_id');

        if ($status == "online")
            Lingua_Query_Chat::chatSetUserAvaliableStatus(Lingua_Auth::getLinguaId(), $chat_id);
        if ($status == "offline")
            Lingua_Query_Chat::chatSetUserUnavaliableStatus(Lingua_Auth::getLinguaId(), $chat_id);

        echo json_encode(array('status' => $status));
        exit;
    }

    //--------------------------------------------------------------------------
    public function sayAction()
    {
        $chat_id = $this->getParam('chat');
        $message = $this->getParam('message');

        $m = preg_replace('/\s+/', '', $message);
        if (empty($m))
            exit;

        $chat = new Lingua_Selected_Chat($chat_id, $message);

        if (!$chat->isValid()) {
            echo 'An error has occured';
            exit;
        }

        Lingua_Query_Chat::chatAddMessage($chat_id, $message, Lingua_Auth::getLinguaId());
        exit;
    }

    //--------------------------------------------------------------------------
    public function messagesGetAction()
    {
        $this->_helper->layout->disableLayout();
        $chat_id = $this->getParam('chat');
        $chat = new Lingua_Selected_Chat($chat_id);

        if (!$chat->isValid()) {
            echo 'An error has occured';
            exit;
        }

        $msg_id = $this->getParam('msg_id');

        $messages = $chat->getLastMessages($msg_id);
        $interlocutor = new Lingua_object($chat->getInterlocutor());
        $status = !$interlocutor->status || $interlocutor->status == "AVALIABLE" ? true : false;

        if ((boolean)count($messages)) {
            echo json_encode(array('data' => array('messages' => $messages, 'status' => $status)));
        } else {
            echo json_encode(array('data' => array('empty' => true, 'status' => $status)));
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function friendRequestAction()
    {
        //user which will be removed
        $to_lingua_id = $this->getParam('to');

        //onwer
        $my_lingua_id = Lingua_Auth::getLinguaId();

        //if users are already friends
        if (!Lingua_Query_User::userIsFriends($to_lingua_id, $my_lingua_id)) {

            //if request already sent, notification is exists in db just DO NOTHING
            if (!Lingua_Query_Notification::hasUserRequest($my_lingua_id, $to_lingua_id)) {

                //yeah , we are sending friend request
                Lingua_Query_Notification::notificationFriendRequest($my_lingua_id, $to_lingua_id);
            }

            echo 'success';
        } else {
            $this->_forward('friend-remove', 'community');
            echo 'removed';
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function friendRequestApproveAction()
    {
        $notification = $this->getParam('notification');
        $not = new Lingua_Object(Lingua_Query_Notification::notificationGet(Lingua_Auth::getLinguaId(), $notification));

        try {
            //start out stransacion
            Lingua_Query_Notification::beginTransaction();

            //approve current notification
            $result = Lingua_Query_Notification::notificationSetApproved($notification, Lingua_Auth::getLinguaId());

            if ($result === FALSE)
                throw new Exception('can\'t approve the notification');
            Lingua_Query_Notification::notificationFriendRequestApproved(Lingua_Auth::getLinguaId(), $not->sender_lingua_id);

            if (!Lingua_Query_User::userIsFriends(Lingua_Auth::getLinguaId(), $not->sender_lingua_id))
                Lingua_Query_User::userAddFriend(Lingua_Auth::getLinguaId(), $not->sender_lingua_id);

            Lingua_Query_Notification::commit();
        } catch (Exception $e) {
            Lingua_Query_Notification::rollback();
        }

        header('Location: /friends');
        exit;
    }

    //--------------------------------------------------------------------------
    public function notificationCloseAction()
    {
        $not = $this->getParam('notification');
        echo Lingua_Query_Notification::notificationSetApproved($not, Lingua_Auth::getLinguaId());
        exit;
    }

    //--------------------------------------------------------------------------
    public function friendsAction()
    {
        $this->view->title = "Мои друзья";
        $this->view->friends_list = Lingua_Query_User::userGetFriends(Lingua_Auth::getLinguaId(), 0, 36);
    }

    //--------------------------------------------------------------------------
    public function friendRemoveAction()
    {
        $lingua_id = $this->getParam('to');
        $my_id = Lingua_Auth::getLinguaId();

        try {
            Lingua_Query_User::beginTransaction();
            $is_friends = Lingua_Query_User::userIsFriends($lingua_id, $my_id);
            if ($is_friends === FALSE)
                throw new Exception('Не могу поличть данные о дружбе');

            $result = Lingua_Query_User::userRemoveFriend($lingua_id, $my_id);
            if ($result === FALSE)
                throw new Exception('Не могу удалять дружбу');

            $result = Lingua_Query_Notification::notificationFriendRemove($my_id, $lingua_id);
            if ($result === FALSE)
                throw new Exception('Не могу отправить оповещение об удаление из друзьей');

            Lingua_Query_Notification::commit();
            echo 'success';
        } catch (Exception $e) {
            Lingua_Query_Notification::rollback();
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function userGetLearningAction()
    {
        $this->_helper->layout->disableLayout();
        $learning = $this->getParam('lang');

        $USERS = $this->view->USERS = new Coummunity_Users(null, $learning);
        $USERS->getRandomUsers();
    }

    //--------------------------------------------------------------------------
    public function userGetKnowAction()
    {
        $this->_helper->layout->disableLayout();
        $know = $this->getParam('lang');

        $USERS = $this->view->USERS = new Coummunity_Users($know, null);
        $USERS->getRandomUsers();
    }

    //--------------------------------------------------------------------------
    public function searchPageAction()
    {
        if (!$this->getParam('page')) exit;

        $TFORM = new Coummunity_Search();
        $TFORM->setPage((int)$this->getParam('page'));
        echo 'success';
        exit;
    }

    //--------------------------------------------------------------------------
    public function searchAction()
    {
        //redirect to login page if user isn't loggned in
        Lingua_Auth::rejectUnlessAuth("USER");

        //requested page
        $page = $this->getParam('pnum', null);

        //requested limit
        $limit = $this->getParam('limit', 50);

        //search by name
        $name = $this->getParam('name', null);

        //search by age from
        $age_from = $this->getParam('age_from', null);

        //search by age to
        $age_to = $this->getParam('age_to', null);

        //search by learning language
        $learning_lang = $this->getParam('learning_lang', null);

        //search by knowing language
        $know_lang = $this->getParam('know_lang', null);

        $gender = $this->getParam('gender', false);

        //search by know level
        $lvl = $this->getParam('lvl', null);

        $clear = $this->isPost();
        $TFORM = $this->view->SEARCH_FROM = new Coummunity_Search($name, $age_from, $age_to, $learning_lang, $know_lang, $lvl, $gender, $page, $clear);

        $this->view->name = $TFORM->get('name');
        $this->view->age_from = $TFORM->get('yfrom');
        $this->view->age_to = $TFORM->get('yto');
        $this->view->learning_lang = $TFORM->get('learning');
        $this->view->know_lang = $TFORM->get('know');
        $gender = $TFORM->get('gender');

        if ($gender == 'Female') $this->view->gender = 'woman';
        elseif ($gender == 'Male') $this->view->gender = 'man';
        else $this->view->gender = 'all';

        $this->view->page = $TFORM->get('page');

        $language = Lingua_Query_System::languagesGetAll();
        $lvls = Lingua_Query_System::languagesGetLevels();

        $sys_languages_learning = array();
        $sys_languages_learning[0] = "Язык, который изучает";

        //$this->view->languages
        $lang_lvl = array();
        $lang_lvl[0] = "Любой уровень изучения";

        foreach ($lvls as $lvl) {
            $lang_lvl[$lvl['user_language_level_id']] = $lvl['name'];
        }

        $this->view->lvls = $lang_lvl;

        foreach ($language as $lang) {
            $sys_languages_learning[$lang['system_languages_id']] = $lang['name'];
        }
        $this->view->languages_learning = $sys_languages_learning;
        $this->view_lvl = $sys_languages_learning[0] = "Язык, которым владеет";
        $this->view->languages_know = $sys_languages_learning;

        $this->view->title = "Найти друзьей";
    }

    //--------------------------------------------------------------------------
    public function friendBlockAction()
    {
        $to = $this->getParam('to');
        try {
            Lingua_Query_User::beginTransaction();

            $result = Lingua_Query_User::userBlockUser($to, Lingua_Auth::getLinguaId());

            if (!$result) {
                //$this->_forward('friend-unblock', 'community');
            } else {
                Lingua_Query_Notification::notificationSetUserBlocked(Lingua_Auth::getLinguaId(), $to);
                echo 'success';
            }

            Lingua_Query_User::commit();
        } catch (Exception $e) {
            Lingua_Query_User::rollback();
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function friendUnblockAction()
    {
        $to = $this->getParam('to');

        $result = Lingua_Query_User::userUnblockUser($to, Lingua_Auth::getLinguaId());
        echo 'unblock';
        exit;
    }

    //--------------------------------------------------------------------------
    public function translateAction()
    {

        $key = Lingua_GlobalKeys::getKey('chat_room');
        $tokan = $this->getParam('key');

        if ($key != $tokan) {
            exit;
        }

        $word = $this->getParam('text');
        $trg = $this->getParam('trg');
        $src = $this->getParam('src');

        $data = Lingua_Query_User::userGetUnknowWordFromLocalDB($word, $trg, $src);
        if ($data) {
            echo $data;
            exit;
        }

        //google translate api helper object
        $obj = new Lingua_Helper_Google();
        $data = $obj->translate($word, $trg, $src);
        Lingua_Query_User::userAddUnknowWord($data, $word, $src, $trg, -1, '', -1, '');

        echo $data;
        exit;
    }

}

?>
