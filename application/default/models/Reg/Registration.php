<?php

/**
 * Description of Registration
 *
 * @author Aram
 */
class Reg_Registration
{

    private $SESS;

    public function __construct()
    {
        $this->SESS = new Daz_Session(__CLASS__);
    }

    //--------------------------------------------------------------------------
    public function getErrors()
    {
        return $this->SESS->error_list;
    }

    //--------------------------------------------------------------------------
    public function clearErrors()
    {
        $this->SESS->error_list = array();
        unset($this->SESS);
    }

    //--------------------------------------------------------------------------
    public function register($email, $password, $role_id = null, $infoData = null)
    {
        $this->clearErrors();

        if (!$this->checkEmail($email)) {
            $this->SESS->error_list['email'][] = "Неверный формат e-mail";
        }

        $result = Lingua_Query_User::userGetByEmail($email);
        if ($result) {
            $this->SESS->error_list['email'][] = "Введенный E-mail уже существует";
        }

        $this->checkPassword($password);

        if (count($this->SESS->error_list) == 0) {
            try {
                Lingua_Query_User::beginTransaction();

                if (!$role_id)
                    $role_id = Lingua_Query_Permission::roleGetId('STUDENT');
                if ($role_id === false)
                    throw new Exception('unable to get role id');

                $lingua_id = $result = Lingua_Query_User::userInsert($email, $password, $role_id);

                if ($result === false)
                    throw new Exception('unable to register');


                if ($infoData) {
                    $result = Lingua_Query_User::userSetInfo($infoData->firstname, $infoData->surname, $infoData->age, $infoData->gender, $infoData->photo, $result);
                }

                $activation_code = time() . md5($email) . $result;

                $result = Lingua_Query_User::userSetActivationPending($activation_code, $result);
                if ($result === false) {
                    throw new Exception('Unable to set activation code');
                }

                //Create email
                /* 
                 * 
        $tr = new Zend_Mail_Transport_Smtp('langwee.com');
        Zend_Mail::setDefaultTransport($tr);

                 * $mail = new Zend_Mail('utf-8');
                  $mail->setFrom("no-reply@linguastars.ru", "Код активации");
                  $mail->addTo($email);
                  $mail->setSubject('Активация аккаунта на сайте Langwee.ru');

                  $message = "Здравствуйте, " . $email . "\n";
                  $message .= "Спасибо за регистрацию. Уверены, теперь вы полюбите мир Иностранных\nязыков и добьетесь высоких результатов в их изучении с Langwee и вашими любимыми кумирами. \n";
                  $message .= "\n\nПерейдите по ссылке что бы активировать Ваш учетный запись http://www.langwee.com/activate/" . $activation_code;
                  $message .= "\n\nВаш пароль: " . $password;
                  $message .= "\n\nСледите за обновлениями на сайте, каждый день у нас появляется много интересного и нового.";
                  $message .= "\n\nДо встречи на Langwee!\n";
                  $message .= "С уважением,\n";
                  $message .= "Команда Langwee.ru";

                  $mail->setBodyText($message);

                  try {
                  $sent = $mail->send();
                  } catch (Exception $e) {
                  echo $e->getMessage();
                  throw new Exception("Unable to send email");
                  Lingua_Query_User::rollback();
                  } */

                $result = Lingua_Query_User_Credit::userCreditSet($lingua_id, 30);

                Lingua_Auth::setLinguaId($lingua_id);
                Lingua_Query_User::commit();
                header("Location: /profile");
            } catch (Exception $e) {
                Lingua_Query_User::rollback();
                echo $e->getMessage();
                exit;
            }
        }
    }

    //--------------------------------------------------------------------------
    public function checkEmail($email)
    {
        if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
            return false;
        }

        return true;
    }

    //--------------------------------------------------------------------------
    public function checkPassword($password)
    {
        $tmp = $password;
        $tmp = preg_replace('/\s+/', 'a', $tmp, -1);

        if (empty($tmp))
            $this->SESS->error_list['password'][] = "Введите пароль";
        if (strlen($tmp) < 6)
            $this->SESS->error_list['password'][] = "Пароль не может содержать менее 6 символов";
    }

}

?>
