<?php

/**
 * Description of IndexController
 *
 * @author Aram
 */
class IndexController extends Lingua_Plugin_Action {

    public function indexAction() {
        if (Lingua_Auth::isAuth()) {
            header("Location: /resources");
            exit;
        }

        if ($this->getRequest()->isPost()) {

            $reg = $this->view->register = new Reg_Registration();

            $email = $this->getParam('email');
            $password = $this->getParam('pass');

            $reg->register($email, $password);
            $this->view->errors = $reg->getErrors();

            //successfully registered
            if (count($this->view->errors) == 0) {
                $this->view->registration_success = true;
            }
            else
                $this->registration_success = false;
        }
    }

    //--------------------------------------------------------------------------
    public function logoutAction() {
        Lingua_Auth::logOut();
        header("Location: /");
        exit;
    }

    //--------------------------------------------------------------------------
    public function activateAction() {
        $activation_code = $this->getParam('activation_code');

        try {
            Lingua_Query_User::beginTransaction();


            $result = Lingua_Query_User::userActivePending($activation_code);
            if ($result === FALSE)
                throw new Exception('error: 0');

            $lingua_id = Lingua_Query_User::userGetByActivationCode($activation_code);
            if ($lingua_id === FALSE)
                throw new Exception('error: 1');
            Lingua_Auth::setLinguaId($lingua_id);

            $default_url = Lingua_Query_User::userGetdefaultUrl($lingua_id);
            if ($default_url === FALSE)
                throw new Exception('error: 2');

            $result = Lingua_Query_User_Credit::userCreditSet(Lingua_Auth::getLinguaId(), 30);
            if ($result === FALSE)
                throw new Exception('error: 3');

            $result = Lingua_Query_User_Credit_History::creditHistorySet(1, Lingua_Auth::getLinguaId());
            if ($result === FALSE)
                throw new Exception('error: 4');

            Lingua_Query_User::commit();
            header('Location: ' . $default_url);
        } catch (Exception $e) {
            echo $e->getMessage();
            Lingua_Query_User::rollback();
            exit;
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function loginAction() {
        $this->_helper->layout->disableLayout();

        Lingua_Query_User::beginTransaction();
        try {

            //params
            $email = $this->getParam('login_email');
            $password = $this->getParam('login_pass');

            //lookup for user
            $lingua_id = Lingua_Query_User::getUserIdByUsernameAndPassword($email, $password);

            //if user is not exist
            if (!$lingua_id && $email) {
                //response
                echo json_encode(array('result' => 'fail', 'message' => 'Неправильно введен логин или пароль'));
                exit;
            }

            //user actiovation state
            $active = true; //Lingua_Query_User::userIsAccountActiveted($lingua_id);
            if ($active === FALSE)
                throw new Exception('error: 0');

            //set found users id to the sessions
            Lingua_Auth::setLinguaId($lingua_id);
            $state = Lingua_Query_User::isFirstTimeLoginToday($lingua_id);

            if (!$active) {
                //response
                echo json_encode(array('result' => 'fail', 'message' => 'Неправильно введен логин или пароль'));
                exit;
            }

            if ($state === FALSE)
                throw new Exception('error: 1');

            if ($state && !Lingua_Auth::isVIPAccount()) {
                Lingua_GlobalConfigs::configStore('first_time_login', $state);

                $action_name = "login";
                $action = new Lingua_Object(Lingua_Query_System_Actions::systemActionsGet($action_name));

                $result = Lingua_Query_User_Credit::userCreditSet($lingua_id, 12);
                if ($result === FALSE)
                    throw new Exception('error: 2');

                $result = Lingua_Query_User_Credit_History::creditHistorySet($action->system_actions_id, $lingua_id);
                if ($result === FALSE)
                    throw new Exception('error: 4');
            }

            $result = Lingua_Query_User::userSetOnlineStatus($lingua_id);
            if ($result === FALSE)
                throw new Exception('error 5');


            //response
            echo json_encode(array('result' => 'success', 'message' => 'Вы успешно вошли в систему, переадресация...', 'redirect' => '/main'));
            Lingua_Query_User::commit();
            exit;
        } catch (Exception $e) {

            echo $e->getMessage();
            echo json_encode(array('result' => 'fail', 'message' => 'Сервер перезагружен!'));
            Lingua_Query_User::rollback();
            exit;
        }

        exit;
    }

    //--------------------------------------------------------------------------
    public function blockedAction() {
        $this->_helper->layout->disableLayout();
        $this->view->sess = new Daz_Session(Lingua_Auth::SESSION_PREFIX);
    }

    //--------------------------------------------------------------------------
    public function kursAction() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function rememberAction() {
        $email = $this->getParam('email');
        $user = new Lingua_Object(Lingua_Query_User::userGetConditions($email));

        if (!$user->lingua_id) {
            echo json_encode(array('result' => 'fail', 'reason' => 'notfound'));
            exit;
        }

        if (Lingua_Query_User::userHasRememberToday($user->lingua_id)) {
            echo json_encode(array('result' => 'fail', 'reason' => 'alreadysent'));
            exit;
        }

        //Create email        
        $tr = new Zend_Mail_Transport_Smtp('langwee.com');
        Zend_Mail::setDefaultTransport($tr);
        
        $mail = new Zend_Mail('utf-8');
        $mail->setFrom("info@langwee.com", "Langwee");
        $mail->addTo($email);
        $mail->setSubject('Восстановление пароля');


        $message = "Здравствуйте!\n";
        $message .= "К вашей почте " . $email . " пароль следующий: " . $user->password . "\n";
        $message .= "\n\nС наилучшими пожеланиями,";
        $message .= "\nКоманда Langwee.com";

        $mail->setBodyText($message);


        try {
            $mail->send();
            Lingua_Query_User::userSetEmailRemember($user->lingua_id);
            echo json_encode(array('result' => 'success'));
        } catch (Exception $e) {
            
        }
        exit;
    }

}

