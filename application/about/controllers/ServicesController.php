<?php

/**
 * Description of Categories
 *
 * @author Aram 
 */
class About_ServicesController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function aboutAction() {
        $this->view->title = "Langwee: О сервисе";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }

    public function schoolsAction() {
        $this->view->title = "Langwee: Школам/Oрганизациям";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }

    //--------------------------------------------------------------------------
    public function paymentAction() {
        $this->view->title = "Langwee: Оплата";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }

    //--------------------------------------------------------------------------
    public function feedbackAction() {
        $this->view->title = "Langwee: Обратная связь";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }

    //--------------------------------------------------------------------------
    public function faqAction() {
        $this->view->title = "Langwee: FAQ";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }

    //--------------------------------------------------------------------------
    public function reviewsAction() {
        $this->view->title = "Langwee: Отзывы";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }

    //--------------------------------------------------------------------------
    public function creditAction() {
        $this->view->title = "Langwee: Звезды";
        $this->view->keyword = "английский язык онлайн, английский онлайн, английский, язык, уроки, изучение, обучение, курсы, английский бесплатно, уроки английского,произношение,тест,немецкий, испанский, французский, итальянский";
        $this->view->desc = "Международная on-line школа изучения иностранных языков.";
    }
    

    //--------------------------------------------------------------------------
    public function oopsAction() {
        $this->view->title = "Langwee: Oops!";
    }

    //--------------------------------------------------------------------------
    public function langweeAction() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function langwee1Action() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function langwee2Action() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function langwee3Action() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function langwee4Action() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function langwee5Action() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function langwee6Action() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function coachingAction() {

        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function mailAction() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function coachingSuccessAction() {
        $this->_helper->layout->disableLayout();
    }
    
    
    public function sendreviewAction()
    {
        
    }
    
    public function teachersAction()
    {
        
    }

    //--------------------------------------------------------------------------
    public function ocoachingAction() {
        $this->_helper->layout->disableLayout();
    }

    //--------------------------------------------------------------------------
    public function coachingSendAction() {
        $name = $this->getParam('name');
        $email = $this->getparam('email');
        $phone = $this->getparam('phone');

        $mail = new Zend_Mail('utf-8');
        $mail->setFrom("info@langwee.com", "Подпись на коучинг");
        $mail->addTo('serjlife@gmail.com');
        $mail->setSubject('Новый подписчик');

        $message = "Имя: " . $name . "\n\n Эл. почта :" . $email . "\n\nТелефон:" . $phone;

        $mail->setBodyText($message);

        try {
            $sent = $mail->send();
            echo 'success';
        } catch (Exception $e) {
            echo 'fail';
        }

        exit;
    }

}

?>
