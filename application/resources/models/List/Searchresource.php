<?php

/**
 * Description of Search
 *
 * @author Aram
 */
class List_Searchresource {

    private $query = null;
    private $DATA = null;

    public function __construct($q, $video_page, $video_limit, $audio_page, $audio_limit, $lang = 'en') {
        $this->query = $q;

        $this->getRecords($video_page, $video_limit, $audio_page, $audio_limit, $lang);
        $this->getRecordsCount($video_limit, $audio_limit, $lang);
    }

    //--------------------------------------------------------------------------
    private function getRecords($video_page, $video_limit, $audio_page, $audio_limit, $lang) {
        try {
            Lingua_Query_Video::beginTransaction();
            
            Lingua_Query_User::setBigQueryMaxValue();
            if ($video_limit) {
                $this->DATA['resource']['video'] = Lingua_Query_Video::videoSearch($this->query, $video_page, $video_limit, $lang);
            }

            if ($audio_limit) {
                $this->DATA['resource']['audio'] = Lingua_Query_Audio::audioSearch($this->query, $audio_page, $audio_limit, $lang);
            }
            
            Lingua_Query_Video::commit();
        } catch (Exception $e) {
            Lingua_Query_Video::rollback();
        }
    }

    //--------------------------------------------------------------------------
    public function getRecordsCount($video_limit, $audio_limit, $lang) {
        if ($video_limit) {
            $this->DATA['count']['video'] = Lingua_Query_Video::videoSearchCount($this->query, $lang);
        }

        if ($audio_limit) {
            $this->DATA['count']['audio'] = Lingua_Query_Audio::audioSearchCount($this->query, $lang);
        }
    }

    //--------------------------------------------------------------------------
    public function getSearchResults($type) {
        return isset($this->DATA['resource'][$type]) ? $this->DATA['resource'][$type] : array();
    }

    //--------------------------------------------------------------------------
    public function getSearchResultsCount($type) {
        return isset($this->DATA['count'][$type]) ? $this->DATA['count'][$type] : 0;
    }

}

?>
