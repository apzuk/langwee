<?php

/**
 * Description of Resourcespage
 *
 * @author Aram
 */
class List_Resourcespage extends Lingua_View_Paging {

    /**
     * 
     * audio and video resources categories in subarray
     * $rcat['video'] = all video categories
     * $rcat['audio'] = all audio categories
     * 
     * @var array 
     * 
     */
    private $rcat = null;

    /**
     *
     * get all video and audio artists in subarray
     * $rart['video'] = all video artists
     * $rart['audio'] = all audio artists
     * 
     * @var array
     *  
     */
    private $rart = null;

    /**
     *
     * search artists by first letter 
     * if also can be a numeric
     * 
     * @var string
     *  
     */
    private $letter = null;

    /**
     * 
     * alphabet container
     * 
     * @var array
     * 
     */
    private $alphabet = null;

    /**
     *
     * artists count for each resource
     * 
     * @var array
     *  
     */
    private $rcount = null;

    /**
     *
     * session object
     * 
     * @var Daz_Session 
     */
    private $SESS = null;

    //-------------------------------------------------------------------
    public function __construct($letter = false) {
        $this->letter = $letter;

        $this->alphabet = array(
            'rus' => array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'),
            'eng' => array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я')
        );

        $this->SESS = new Daz_Session(__CLASS__);
    }

    //-------------------------------------------------------------------
    /**
     *
     * load all data in one transaction 
     * top 10 resources , video, audio categories , video, audio artists
     *  
     */
    public function load() {
        Lingua_Query_Resources::beginTransaction();

        try {
            //get top 10 audios and videos resources
            $this->SESS->rtop = Lingua_Query_Resources::resourcesGetTops('en');

            /* //get resources first 5 categories
              $this->rcat = Lingua_Query_Resources::resourcesGetCategories(0, 5);

              //get 10artists for each resource
              $this->rart = Lingua_Query_Resources::resourcesGetArtists($this->letter, $this->lang, 0, 10);
             */
            Lingua_Query_Resources::commit();
        } catch (Exception $e) {
            Lingua_Query_Resources::rollback();
        }
    }

    //-------------------------------------------------------------------
    /**
     *
     * return top 10 resources
     *  
     */
    public function resourcesGetTop10($type = false) {
        if (!$this->SESS->rtop) {
            $this->SESS->rtop = Lingua_Query_Resources::resourcesGetTops(Lingua_Auth::getLanguage('short'));
        }

        if ($type) {
            return isset($this->SESS->rtop[$type]) ? $this->rtop[$type] : array();
        } else {
            return $this->SESS->rtop;
        }
    }

    //-------------------------------------------------------------------
    /**
     *
     * return categories for each resource
     *  
     */
    public function resourceGetCategories($type, $page = 0, $limit = 5) {

        if (!$this->rcat) {
            $this->rcat = Lingua_Query_Resources::resourcesGetCategories($page, $limit);
        }

        return $type ? $this->rcat[$type] : $this->rcat;
    }

    //-------------------------------------------------------------------
    /**
     *
     * return artists for each resource
     *  
     */
    public function resourceGetArtists($type, $page = false, $limit = false) {
        if (!$this->rart) {
            $this->rart = Lingua_Query_Resources::resourcesGetArtists($page, $limit);
        }
        return $type ? $this->rcat[$type] : $this->rart;
    }

    //-------------------------------------------------------------------
    /**
     *
     * return alphabet
     *  
     */
    public function resourceGetAlphabet($resource, $lang) {
        if (!isset($this->SESS->rcount[$resource]) && $resource == 'video') {
            $this->SESS->rcount[$resource] = Lingua_Query_Resources::resourcesVideoGetExistingArtists(Lingua_Auth::getLanguage('short'));
        }

        if (!isset($this->SESS->rcount[$resource]) && $resource == 'audio') {
            $this->SESS->rcount[$resource] = Lingua_Query_Resources::resourcesAudioGetExistingArtists(Lingua_Auth::getLanguage('short'));
        }

        $this->rcount = $this->SESS->rcount;
        $r = isset($this->rcount[$resource]) ? $this->rcount[$resource] : array();
        $returnData = array('num' => false);
        $alphabet = $this->alphabet[$lang] ? $this->alphabet[$lang] : array();

        foreach ($alphabet as $letter) {
            if (isset($r[$resource][$letter])) {
                $returnData[$letter] = true;
            } else {
                $returnData[$letter] = false;
            }
        }

        if (isset($r[$resource])) {
            foreach ($r[$resource] as $letter => $val) {
                if (is_numeric($letter)) {
                    $returnData['num'] = true;
                    break;
                }
            }
        }

        return $returnData;
    }

    //-------------------------------------------------------------------
    /**
     *
     * 
     * store lastest selected letter for each resources in to the session
     *  
     */
    public function selectedLetter($letter, $resource) {
        $this->SESS->letter[$resource] = $letter;
    }

    //-------------------------------------------------------------------
    /**
     *
     * 
     * get lastest selected letter for each resource
     *  
     */
    public function selectedLetterGet($resource) {
        return isset($this->SESS->letter[$resource]) ? $this->SESS->letter[$resource] : false;
    }

    //-------------------------------------------------------------------
    /**
     *
     * 
     * Clear all stored session data
     *  
     */
    public function clear() {
        $this->SESS->unsetAll();
    }

}

?>
