<?php

/**
 * Description of Artistvideos
 *
 * @author Aram
 */
class List_Artistaudios {

    private $artist = null;
    
    private $cat_id;
    
    public function __construct($artistname = false) {
        $this->artist = $artistname;
        
        if(!$cat_id) $cat_id = -1;
        $this->cat_id = $cat_id;
    }
    
    //--------------------------------------------------------------------------
    public function getAtristAllAudios() {
        return Lingua_Query_Audio::audioSearchByArtist($this->artist, $this->cat_id);
    }

}

?>
