<?php

/**
 * Description of Artistvideos
 *
 * @author Aram
 */
class List_Artistvideos {

    private $artist = null;
    private $cat_id = null;
    private $lang = null;
    
    public function __construct($artistname = false, $cat_id = false, $lang = null) {
        $this->artist = $artistname;
        $this->lang = $lang;
        
        if(!$cat_id) $cat_id = -1;
        $this->cat_id = $cat_id;
    }
    
    //--------------------------------------------------------------------------
    public function getAtristAllVideos() {
        return Lingua_Query_Video::videoSearchByArtist($this->artist, $this->cat_id, $this->lang);
    }
    
    //--------------------------------------------------------------------------
    public function getAtristAllVideosCount() {
        return Lingua_Query_Video::videoSearchByArtistCount($this->artist, $this->cat_id, $this->lang);
    }

}

?>
