<?php

/**
 * Description of PageController
 *
 * @author Aram
 */
class Resources_ListController extends Lingua_Plugin_Action {

    public function allAction() {
        //reject if user is not auth
        Lingua_Auth::rejectUnlessAuth("USER");

        //resources model
        $RESOURCE = $this->view->RESOURCES = new List_Resourcespage(Lingua_Auth::getLinguaId('short'));

        $r = $RESOURCE->resourcesGetTop10();

        //top 10 viewed videos list
        $this->view->top_video = isset($r['video']) ? $r['video'] : array();

        //top 10 viewed audios list
        $this->view->top_audio = isset($r['audio']) ? $r['audio'] : array();

        $this->view->languages = Lingua_Auth::sysGetLanguages();
        $this->view->lang = Lingua_Auth::getLanguage('short');

        $action_name = "login";
        $actions = new Lingua_Object(Lingua_Query_System_Actions::systemActionsGet($action_name));

        $msg_state = Lingua_Query_System_Messages::messageIsShow($actions->system_actions_id, Lingua_Auth::getLinguaId());
        $login_state = Lingua_GlobalConfigs::configGet('first_time_login');

        if (!$msg_state && $login_state) {
            $this->view->show_message = true;
            $this->view->message = "Каждый день заходя на сайт Вы получаете в подарок <b>12 звезд.</b><br />
            Звезда позволяет посмотреть авторский перевод/изучить одно новое слово/ пройти 1 тренировку \"проверь себя\".<br />
            Заходи каждый день на сайт и получай ежедневный звездный бонус!";

            $this->view->title = "Подарок! Ежедневный звездный бонус!";
            Lingua_GlobalConfigs::configStore('first_time_login', false);
        }
    }


    //--------------------------------------------------------------------------
    public function videoArtistsFilterAction() {
        $letter = $this->getParam('letter');

        if ($letter == 'ВСЕ')
            $letter = false;

        $lang = $this->getParam('lang');
        $page = $this->getParam('page');
        $length = 10;

        $RESOURCES = new List_Resourcespage($length, $lang);
        $data = Lingua_Query_Video::videoSreachArtists($letter, $lang, $page * $length, $length);
        $data_count = Lingua_Query_Video::videoSreachArtistsCount($letter, $lang);

        echo json_encode(array('artists' => $data, 'found' => $data_count, 'artists_count' => $RESOURCES->artistsCountAll()));
        exit;
    }

    //--------------------------------------------------------------------------
    public function getVideosByArtistAction() {
        $this->_helper->layout->disableLayout();
        $artistname = $this->artistname = $this->getParam('artist_name');

        $this->view->VIDEOS = new List_Artistvideos($artistname);
    }

    //--------------------------------------------------------------------------
    public function allVideosAction() {
        $this->_helper->layout->disableLayout();

        $cat_id = $this->getParam('cat_id');
        $lang = $this->getParam('lang', 'en');
        $page = $this->getParam('page', 0);
        $length = 20;

        $this->view->VIDEOS = new List_Artistvideos(false, $cat_id, $lang);
    }

    //--------------------------------------------------------------------------
    public function audioArtistsFilterAction() {
        $letter = $this->getParam('letter');

        if ($letter == 'ВСЕ')
            $letter = false;

        $lang = $this->getParam('lang');
        $page = $this->getParam('page');
        $length = 10;

        $RESOURCES = new List_Resourcespage($length, $lang);

        $length = 10;
        $data = Lingua_Query_Audio::audioSreachArtists($letter, $lang, $page * $length, $length);
        $data_count = Lingua_Query_Audio::audioSreachArtistsCount($letter, $lang);

        echo json_encode(array('artists' => $data, 'found' => $data_count, 'artists_count' => $RESOURCES->artistsCountAll()));
        exit;
    }

    //--------------------------------------------------------------------------
    public function getAudiosByArtistAction() {
        $this->_helper->layout->disableLayout();
        $artistname = $this->artistname = $this->getParam('artist_name');

        $this->view->AUDIOS = new List_Artistaudios($artistname);
    }

    //--------------------------------------------------------------------------
    public function allAudiosAction() {
        $this->_helper->layout->disableLayout();

        $cat_id = $this->getParam('cat_id');

        $this->view->AUDIOS = new List_Artistaudios($cat_id);
    }

    //--------------------------------------------------------------------------
    public function searchAction() {
        $this->_helper->layout->disableLayout();
        $q = $this->view->query = $this->getParam('s');

        $page = $this->view->page = 0;
        $limit = 10;
        $lang = $this->getParam('lang');

        $this->view->RESULT = new List_Searchresource($q, $page, $limit, $page, $limit, $lang);
    }

    //--------------------------------------------------------------------------
    public function videoNextPageAction() {
        $this->_helper->layout->disableLayout();
        $q = $this->view->query = $this->getParam('s');

        $lang = $this->getParam('lang');
        $page = $this->view->page = $this->getParam('page');
        $limit = 10;

        $this->view->RESULT = new List_Searchresource($q, $page * 10, $limit, 0, 0, $lang);
    }

    //--------------------------------------------------------------------------
    public function videoNextPagesAction() {
        $this->_helper->layout->disableLayout();
        $q = $this->view->query = $this->getParam('s');

        $lang = $this->getParam('lang');
        $page = $this->view->page = $this->getParam('page');
        $limit = 20;

        $this->view->RESULT = new List_Searchresource($q, $page * 20, $limit, 0, 0, $lang);
    }

    //--------------------------------------------------------------------------
    public function audioNextPageAction() {
        $this->_helper->layout->disableLayout();
        $q = $this->view->query = $this->getParam('s');

        $lang = $this->getParam('lang');
        $page = $this->view->page = $this->getParam('page');
        $limit = 10;

        $this->view->RESULT = new List_Searchresource($q, 0, 0, $page * 10, $limit, $lang);
    }

    //--------------------------------------------------------------------------
    public function videoGetAllAction() {
        $artist = $this->getParam('artist', false);
        $artists = $this->getParam('artists', false);
        $cat_id = $this->getParam('cat_id', -1);
        $lang = $this->getParam('lang', 'en');
        $page = $this->getParam('page', 0);
        $length = 20;

        try {
            Lingua_Query_Video::beginTransaction();
            Lingua_Query_User::setBigQueryMaxValue();

            $data_count = Lingua_Query_Video::videoSearchByArtistCount($artist, $cat_id, $lang);

            if ($page > ceil($data_count / 20))
                $page = ceil($data_count / 20);

            if ($page <= 0)
                $page = 1;

            $data = Lingua_Query_Video::videoSearchByArtist($artist, $cat_id, $lang, ($page - 1) * $length, $length);

            foreach ($data as $k => $d) {
                $data[$k]['created_ts'] = strftime("%d, %B %Y", strtotime($d['created_ts']));
            }
            if ($artists) {
                $artists_video = Lingua_Query_Video::videoSreachArtists($letter, $lang, $page * $length, $length);
                $artists_video_count = Lingua_Query_Video::videoSreachArtistsCount($letter, $lang);

                echo json_encode(array('data' => $data, 'count' => $data_count, 'page' => $page,
                    'video_artists' => $artists_video, 'video_artists_count' => $artists_video_count,
                    'audio_artists' => $artists_audio, 'audio_artists_count' => $artists_audio_count));
            }
            else
                echo json_encode(array('data' => $data, 'count' => $data_count, 'page' => $page));

            Lingua_Query_Video::commit();
        } catch (Exception $e) {
            Lingua_Query_Video::rollback();
        }


        exit;
    }

    //--------------------------------------------------------------------------
    public function audioGetAllAction() {
        $artist = $this->getParam('artist', false);
        $artists = $this->getParam('artists', false);
        $cat_id = $this->getParam('cat_id', -1);
        $lang = $this->getParam('lang', 'en');
        $page = $this->getParam('page', 0);
        $length = 20;

        $data_count = Lingua_Query_Audio::audioSearchByArtistCount($artist, $cat_id, $lang);

        if ($page > ceil($data_count / 20))
            $page = ceil($data_count / 20);

        if ($page <= 0)
            $page = 1;

        $data = Lingua_Query_Audio::audioSearchByArtist($artist, $cat_id, $lang, ($page - 1) * $length, $length);

        foreach ($data as $k => $d) {
            $data[$k]['created_ts'] = strftime("%d, %B %Y", strtotime($d['created_ts']));
        }
        if ($artists) {
            $artists_audio = Lingua_Query_Audio::audioSreachArtists($letter, $lang, $page * $length, $length);
            $artists_audio_count = Lingua_Query_Audio::audioSreachArtistsCount($letter, $lang);

            echo json_encode(array('data' => $data, 'count' => $data_count, 'page' => $page,
                'video_artists' => $artists_video, 'video_artists_count' => $artists_video_count,
                'audio_artists' => $artists_audio, 'audio_artists_count' => $artists_audio_count));
        }
        else
            echo json_encode(array('data' => $data, 'count' => $data_count, 'page' => $page));
        exit;
    }

}
