<?php

/**
 * Description of PageController
 *
 * @author Aram
 */
class Video_PageController extends Lingua_Plugin_Action {

    //--------------------------------------------------------------------------
    public function viewAction() {
        Lingua_Auth::rejectUnlessAuth("USER");

        //loggined user lingua id
        $lingua_id = Lingua_Auth::getLinguaId();

        //getting current video id
        $video_id = $this->getParam('video_id');

        // set the view counter
        // if current user has been view this video this function gonna to do nothing
        $result = Lingua_Query_Video::videoSetView($video_id, $lingua_id);
        if ($result) {
            Lingua_Query_Video::videoCompileViews($video_id);
        }

        // select video by got id
        $VIDEO = new Lingua_Selected_Video($video_id);

        //if tha video is not avaliable or isn't exists redirect to error page
        $VIDEO->rejectUnlessValid();

        //getting how many time this video has been viwed
        $this->view->VIDEO = $VIDEO->VIDEO;

        $text = nl2br($VIDEO->VIDEO->text1);
        //split string by spaces, ',' and . to get words count in the string
        $data = preg_split('/\s+|,|\.|\[|\]|\(|\)/', $text, null, PREG_SPLIT_NO_EMPTY);
        
        $this->view->word_count = count($data);
        $this->view->words = Lingua_Query_User::userGetUnknownWordsByVideo($video_id, $lingua_id);

        //----------------------------------------------------------------------
        $this->view->PAGE = $PAGE = new Page_Videopage($VIDEO->VIDEO);
        $paid = $this->view->paid_status = Lingua_Query_User::userIsPaid($lingua_id);

        if (!$paid) {
            $this->view->credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
        }

        //generet a string with 30 chars length and high level strange
        $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);

        //store generated key for api call to google translatesss
        Lingua_GlobalKeys::storeKey('video_page', $key);
    }

    //--------------------------------------------------------------------------
    public function getUserUnknowWordsAction() {
        $video_id = $this->getParam('video_id');
        //getting user unknow words with translations
        $words = Lingua_Query_User::userGetUnknownWordsByVideo($video_id, Lingua_Auth::getLinguaId());

        echo json_encode(array('data' => $words));
        exit;
    }

    //--------------------------------------------------------------------------
    public function videoGetTranslationAction() {
        try {
            Lingua_Query_System_Actions::beginTransaction();
            $action_name = "get-text-translation";
            $account_state = Lingua_Auth::isVIPAccount();
            $lingua_id = Lingua_Auth::getLinguaId();

            if (!$account_state) {
                //if the payment message is not shown to user, show it!
                $action = Lingua_Query_System_Actions::systemActionsGet($action_name);
                $action = new Lingua_Object($action);

                $status = Lingua_Query_System_Messages::messageIsShow($action->system_actions_id, $lingua_id);
                if (!$status) {
                    $message = "Во Free статусе авторский перевод ограничен запасом звезд:
1 авторский перевод=1 звезда.<br /><br />
Узнайте как можно добыть звезды или получите Premium статус для полного доступа ко всем возможностям Langwee и снятием всех ограничений!";

                    Lingua_Query_System_Actions::commit();
                    echo json_encode(array('result' => 'fail', 'reason' => 'showMessage', 'message' => $message, 'action' => $action_name));
                    exit;
                }

                $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
                if ($credit <= $action->stars) {
                    echo json_encode(array('result' => 'fail', 'reason' => 'noCredit'));
                    Lingua_Query_System_Actions::commit();
                    exit;
                }
            }
            $video_id = $this->getParam('video_id');

            // select video by got id
            $VIDEO = new Lingua_Selected_Video($video_id);

            $PAGE = new Page_Videopage($VIDEO->VIDEO);
            $words = $PAGE->getTextTrgWords();

            echo json_encode(array('data' => $words));
            if (!$account_state) {
                Lingua_Query_User_Credit::userCreditWithdraw($lingua_id, $action->stars);
                Lingua_Query_User_Credit_History::creditHistorySet($action->system_actions_id, $lingua_id);
            }
            Lingua_Query_System_Actions::commit();
        } catch (Exception $e) {
            
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function videoExamineAction() {
        $this->_helper->layout->disableLayout();
        $lingua_id = Lingua_Auth::getLinguaId();

        $action_name = "examine";

        $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
        $action = Lingua_Query_System_Actions::systemActionsGet($action_name);
        $action = new Lingua_Object($action);

        if (!Lingua_Auth::isVIPAccount() && $credit <= $action->stars) {
            echo 'У Вас нет достаточно звезд для этой действии!';
            exit;
        }

        $video_id = $this->view->video_id = $this->getParam('video_id');
        $page = $this->getParam('page');

        $VIDEO = new Lingua_Selected_Video($video_id);
        $this->view->VIDEO = $VIDEO->VIDEO;
        $VIDEO->rejectUnlessValid();

        $examiner = $this->view->examiner = new Page_Examiner($VIDEO->VIDEO, $page);

        if (!Lingua_Auth::isVIPAccount()) {
            if (!$page || !$examiner->hasSession()) {
                $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);

                Lingua_Query_User_Credit::userCreditWithdraw($lingua_id, $action->stars);
                Lingua_Query_User_Credit_History::creditHistorySet($action->system_actions_id, $lingua_id);
            }
        }

        $examiner->init($page);
        $examiner->setExamine();

        if (!$examiner->isExpired())
            $this->view->title = $examiner->getTitle();
        else
            $this->view->title = "";

        if (!$examiner->isExpired())
            $this->view->title = $examiner->getTitle();
        else
            $this->view->title = "";
    }

    //--------------------------------------------------------------------------
    public function videoCanExamineAction() {
        $lingua_id = Lingua_Auth::getLinguaId();

        if (!Lingua_Auth::isVIPAccount()) {
            $action_name = "examine";
            $action = Lingua_Query_System_Actions::systemActionsGet($action_name);
            $action = new Lingua_Object($action);

            $state = Lingua_Query_System_Messages::messageIsShow($action->system_actions_id, $lingua_id);

            if (!$state) {
                $message = "Во Free статусе тренировка \"проверь себя\" ограничена запасом звезд:
1 тренирока \"проверь себя\"=1 звезда.<br /><br />
Узнайте как можно добыть звезды или получите Premium статус для полного доступа ко всем возможностям Langwee и снятием всех ограничений!?";

                echo json_encode(array('result' => 'fail', 'reason' => 'showMessage', 'message' => $message, 'action' => $action_name));
                exit;
            } else {

                $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
                if ($credit <= $action->stars) {
                    echo json_encode(array('result' => 'fail', 'reason' => 'noCredit'));
                    exit;
                }

                echo json_encode(array('result' => 'success'));
                exit;
            }
        }

        echo json_encode(array('result' => 'success'));
        exit;
    }

    //--------------------------------------------------------------------------
    public function wordAddAction() {
        //reject if user is not logged in
        Lingua_Auth::rejectUnlessAuth("USER");
        $account_state = Lingua_Auth::isVIPAccount();
        $lingua_id = Lingua_Auth::getLinguaId();

        if (!$account_state) {
            //if the payment message is not shown to user, show it!
            $action = Lingua_Query_System_Actions::systemActionsGet('translate');
            $action = new Lingua_Object($action);

            $status = Lingua_Query_System_Messages::messageIsShow($action->system_actions_id, $lingua_id);
            if (!$status) {
                $message = "Во Free статусе количество новых слов, которые вы можете переводить и добавлять в словарь, ограничено запасом звезд:
<b>1 слово=1 звезда</b>.<br /><br />
Узнайте как можно добыть звезды или получите Premium статус для полного доступа ко всем возможностям Langwee и снятием всех ограничений!";
                echo json_encode(array('result' => 'fail', 'reason' => 'showMessage', 'message' => $message, 'action' => 'translate'));
                exit;
            }

            $credit = Lingua_Query_User_Credit::userCreditGet($lingua_id);
            if ($credit <= $action->stars) {
                echo json_encode(array('result' => 'fail', 'reason' => 'noCredit'));
                exit;
            }
        }

        //source word english, france, german , italiy , etc.
        $word_src = $this->getParam('word');

        //video id which belongs the word
        $video_id = $this->getParam('video_id');

        //word language
        $word_src_lang = $this->getParam('video_lang', 'en');
        $word_trg_lang = $this->getParam('trg_lang', 6);

        //word context in the text
        $context = $this->getParam('context');
        $context = preg_replace('/\s+|\n/', ' ', $context);
        $context = trim($context);

        //get world translation
        $word = new Lingua_Object(Lingua_Query_User::userGetUnknowWordFromLocalDB($word_src));
        if (!$word->user_word_id) {
            //google translate api helper object
            $obj = new Lingua_Helper_Google();

            $data = $obj->translate($word_src, $word_trg_lang, $word_src_lang);
            $word_trg = $data;
            $word = new Lingua_Object(array('word_trg' => $word_trg));
        }

        //get the video id from which the word has taken
        $w_trg = preg_replace('/\s/', '', $word->word_trg);
        $w_src = preg_replace('/\s/', '', $word_src);

        $w_trg = strtolower($w_trg);
        $w_src = strtolower($w_src);

        if (empty($w_trg) || empty($w_src) || $w_trg == $w_src) {
            echo json_encode(array('result' => 'fail', 'reason' => 'noTrasnaltion'));
            exit;
        }

        $word_id = Lingua_Query_User::userAddUnknowVideoWord($w_trg, $w_src, $word_src_lang, $word_trg_lang, Lingua_Auth::getLinguaId(), $video_id, $context);

        if (!$word_id)
            $word_id = $word->user_word_id;
        echo json_encode(array('result' => 'success', 'word_src' => $word_src, 'word_trg' => $word_trg, 'video_id' => $video_id, 'user_word_id' => $word_id));

        if (!$account_state) {
            Lingua_Query_User_Credit::userCreditWithdraw($lingua_id, $action->stars);
            Lingua_Query_User_Credit_History::creditHistorySet($action->system_actions_id, $lingua_id);
        }
        exit;
    }

    //--------------------------------------------------------------------------
    public function closeMessageAction() {
        $action_name = $this->getParam('action_name');

        $action = Lingua_Query_System_Actions::systemActionsGet($action_name);
        $action = new Lingua_Object($action);

        if ($action->system_actions_id)
            Lingua_Query_System_Messages::messageSetShown($action->system_actions_id, Lingua_Auth::getLinguaId());
        echo 'done';
        exit;
    }

    //--------------------------------------------------------------------------
    public function wordTranslationViewAction() {
        $video_id = $this->getParam('video_id');

        $this->_helper->layout->disableLayout();
        $this->view->words = Lingua_Query_User::userGetUnknownWordsByVideo($video_id, Lingua_Auth::getLinguaId());
    }

    //--------------------------------------------------------------------------
    public function wordGetTranslationAction() {
        Lingua_Auth::rejectUnlessAuth("USER");
        //getting the word which will be translated
        $word = $this->getParam('word');
        $lang_src = $this->getParam('lang_src', 'en');
        $lang_trg = $this->getParam('lang_trg', 'ru');

        //security key
        $key = $this->getParam('key');
        $token = Lingua_GlobalKeys::getKey('video_page');

        if (strcmp($key, $token) !== 0) {
            echo "An security error has occured";
            exit;
        }

        //searching got word in local db        
        $data = Lingua_Query_User::userGetUnknowWordFromLocalDB($word, $lang_trg, $lang_src);
        if ($data) {
            echo $data;
            exit;
        }

        //google translate api helper object
        $obj = new Lingua_Helper_Google();

        $data = $obj->translate($word, $lang_trg, $lang_src);
        echo $data;
        exit;
    }

    //--------------------------------------------------------------------------
    public function wordSpeech1Action() {
        //getting the word which will be speech
        $word_id = $this->getParam('word_id');
        
        $obj = new Lingua_Helper_Google();
        $obj->outputSound($word_id);
        exit;
    }

    //--------------------------------------------------------------------------
    public function discussAction() {
        $this->_helper->layout->disableLayout();

        $from = $this->getParam('from');
        $limit = $this->getParam('limit');
        $video_id = $this->getParam('video_id');
        $user_role = Lingua_Query_User::userGetRole(Lingua_Auth::getLinguaId());
        if (!$limit)
            $limit = 20;
        if (!$from)
            $from = 0;

        $data = Lingua_Query_Video_Discuss::discussGet($from, $limit, $video_id);

        foreach ($data as $k => $d) {
            $data[$k]['photo'] = Lingua_Storage_User::getUrl($d['lingua_id'], 60, 60);
            if ($d['lingua_id'] == Lingua_Auth::getLinguaId() || $user_role == "ADMIN") {
                $data[$k]['owner'] = true;
            } else {
                $data[$k]['owner'] = false;
            }
        }

        $data['own_photo'] = Lingua_Storage_User::getUrl(Lingua_Auth::getLinguaId(), 60, 60);
        echo json_encode(array('data' => $data));

        exit;
    }

    //--------------------------------------------------------------------------
    public function discussAddAction() {
        $text = $this->getParam('text');
        $video_id = $this->getParam('video_id');
        $lingua_id = Lingua_Auth::getLinguaId();

        $result = Lingua_Query_Video_Discuss::discussInsert($text, $video_id, $lingua_id);
        echo json_encode(array('result' => $result));
        exit;
    }

    //--------------------------------------------------------------------------
    public function discussRemoveAction() {
        $discuss_id = $this->getParam('discuss_id');

        if (Lingua_Query_User::userGetRole(Lingua_Auth::getLinguaId()) == "ADMIN") {
            $result = Lingua_Query_Video_Discuss::discussDelete($discuss_id);
        } else {
            $result = Lingua_Query_Video_Discuss::discussDeleteOwn($discuss_id, Lingua_Auth::getLinguaId());
        }
        echo json_encode(array('result' => 'success', 'discuss_id' => $discuss_id));
        exit;
    }

    //--------------------------------------------------------------------------
    public function discussUpdatesAction() {
        $this->_helper->layout->disableLayout();

        $discuss_id = $this->getParam('id');
        $video_id = $this->getParam('video_id');
        $user_role = Lingua_Query_User::userGetRole(Lingua_Auth::getLinguaId());

        $data = Lingua_Query_Video_Discuss::discussGet($discuss_id, 20, $video_id);
        foreach ($data as $k => $d) {
            $data[$k]['photo'] = '/getimage/60/60';
            if ($d['lingua_id'] == Lingua_Auth::getLinguaId() || $user_role == "ADMIN") {
                $data[$k]['owner'] = true;
            } else {
                $data[$k]['owner'] = false;
            }
        }

        echo json_encode(array('data' => $data));

        exit;
    }

    //--------------------------------------------------------------------------
    public function videoValidationAction() {
        $videos = Lingua_Query_Video::videoGetAll_();
        //     Daz_Debug::dump($videos);
        foreach ($videos as $video) {
            $obj = new Lingua_Object($video);
            $page = new Page_Videopage($obj);

            if (!$page->compareAction()) {
                echo "<a href='http://langwee.com/admin/panel/video-edit/video_id/" . $obj->video_id . "'>" . $obj->title . "</a> | " . $obj->video_lang . "<br />";
                flush();
                ob_flush();
            }
        }

        exit;
    }

}
