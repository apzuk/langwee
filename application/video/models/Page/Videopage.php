<?php

/**
 * Description of Videopage
 *
 * @author Aram
 */
class Page_Videopage {

    private $TEXT_1;
    private $TEXT_2;
    
    private $symbols = array(',','.','[',']',')','(', '?', ':');

    function __construct($video_obj) {
        $this->TEXT_1 = $video_obj->text1;
        $this->TEXT_2 = $video_obj->text2;
    }

    //--------------------------------------------------------------------------
    public function getTextSrcWords($user_words) {
        $data = array();
        $text = nl2br($this->TEXT_1);
        //split string by <br />
        
        $lines = preg_split('/<br\s+\/>/', $text, null, PREG_SPLIT_NO_EMPTY);
        $lines_trg = nl2br($this->TEXT_2);
        foreach ($lines as $key => $line) {
            
            $words = preg_split('/\s+/', $line, null, PREG_SPLIT_NO_EMPTY);
            
            foreach ($words as $word) {
                $tmp = array();
                
                $fchar = substr($word, 0, 1);
                $lchar = substr($word, strlen($word)-1, strlen($word));
                
                if(in_array($fchar, $this->symbols)) {
                     $word = str_replace($fchar, '', $word);
                     $tmp['fchar'] = $fchar;
                }
                
                if(in_array($lchar, $this->symbols)) {
                     $word = str_replace($lchar, '', $word);
                     $tmp['lchar'] = $lchar;
                }
                
                $class = 'words';
                if($this->in_array_r($word, $user_words)) {
                    $class = 'selected';
                }
                
                $tmp['word'] = $word;
                $tmp['class'] = $class;
                
                $data[$key][] = $tmp;
            }
            $tmp = array();
            
            if(isset($lines_trg[$key]) && strlen($lines_trg[$key])>66)
                $tmp['word'] = "<br /><br />";
            else $tmp['word'] = "<br />";
            $tmp['class'] = "words_space";
            
            $data[$key][] = $tmp;
        }
        return $data;
    }
    
    //--------------------------------------------------------------------------
    public function getTextTrgWords($user_words = array()) {
        $data = array();
        $text = nl2br($this->TEXT_2);
        //split string by <br />
        
        $lines = preg_split('/<br\s+\/>/', $text, null, PREG_SPLIT_NO_EMPTY);
        
        foreach ($lines as $key => $line) {
            
            $words = preg_split('/\s+/', $line, null, PREG_SPLIT_NO_EMPTY);
            
            foreach ($words as $word) {
                $tmp = array();
                
                $fchar = substr($word, 0, 1);
                $lchar = substr($word, strlen($word)-1, strlen($word));
                
                if(in_array($fchar, $this->symbols)) {
                     $word = str_replace($fchar, '', $word);
                     $tmp['fchar'] = $fchar;
                }
                
                if(in_array($lchar, $this->symbols)) {
                     $word = str_replace($lchar, '', $word);
                     $tmp['lchar'] = $lchar;
                }
                
                $class = 'words';
                if($this->in_array_r(strtolower($word), $user_words)) {
                    $class = 'selected';
                }
                
                $tmp['word'] = $word;
                $tmp['class'] = $class;
                
                $data[$key][] = $tmp;
            }
            $tmp = array();
            $tmp['word'] = "<br />";
            $tmp['class'] = "words_space";
            
            $data[$key][] = $tmp;
        }
        
        
        return $data;
    }

    //--------------------------------------------------------------------------
    public function in_array_r($needle, $haystack) {
        if(!is_array($haystack)) return false;
        
        foreach ($haystack as $item) {
           foreach($item as $i) {
               if(strtolower($i) == strtolower($needle))
                   return true;
           }
        }

        return false;
    }
    
    //--------------------------------------------------------------------------
    public function compareAction() {
        $data1 = $this->getTextSrcWords(array());        
        $data2 = $this->getTextTrgWords(array());
        
        return count($data1) == count($data2);
    }

}

?>
