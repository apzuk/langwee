<?php

class Page_Examiner {

    private $SESS;
    private $VIDEO;
    private $exception = array("i", "if", "and", "you", "me", "to", "us", "in");
    private $step = 0;
    private $session = false;
    private $messages = array(
        '1. Перевод предложений с иностранного на русский',
        '2. Перевод предложений с русского на иностранный',
        '3. Перевод всего текста с иностранного языка на русский язык'
    );

    public function __construct($video_object) {
        $this->VIDEO = $video_object;
        $this->SESS = new Daz_Session(__CLASS__ . '_' . $video_object->video_id . '_' . Lingua_Auth::getLinguaId());
    }

    //--------------------------------------------------------------------------
    public function init($page) {
        $this->SESS->count[$this->step] = (int) $this->SESS->count[$this->step];
        
        if (!$page) {
            $this->cleanResult();
        }
    }

    //--------------------------------------------------------------------------
    public function hasSession() {
        return isset($this->SESS->count[$this->step]);
    }

    //--------------------------------------------------------------------------
    public function getCurrentExaminedCount() {
        return $this->SESS->count[$this->step];
    }

    //--------------------------------------------------------------------------
    public function currentMessage() {
        return isset($this->messages[$this->step]) ? $this->messages[$this->step] : '';
    }

    //--------------------------------------------------------------------------
    public function getTitle() {
        return isset($this->messages[$this->step]) ? $this->messages[$this->step] : '';
    }

    //--------------------------------------------------------------------------
    public function switchStep() {
        $this->step++;
        $this->SESS->count[$this->step] = (int) $this->SESS->count[$this->step];
    }

    //--------------------------------------------------------------------------
    public function setExamine() {
        if(!$this->SESS->count[$this->step]) $this->SESS->count[$this->step] = (int) $this->SESS->count[$this->step];
        $this->SESS->count[$this->step]++;

        if ($this->isExpiredStep()) {
            $this->step++;
            $this->SESS->count[$this->step] = (int) $this->SESS->count[$this->step];
            $this->SESS->count[$this->step]++;
        }
    }

    //--------------------------------------------------------------------------
    public function cleanResult() {
        $this->SESS->unsetAll();
    }

    //--------------------------------------------------------------------------
    public function isExpired() {
        if ($this->step == 1 && $this->isExpiredStep())
            return true;
        return false;
    }

    //--------------------------------------------------------------------------
    public function isExpiredStep() {
        if ($this->step == 0 || $this->step == 1) {
            return ($this->SESS->count[$this->step] > 5) ? true : false;
        } else {
            return ($this->SESS->count[$this->step] > 1) ? true : false;
        }
    }

    //--------------------------------------------------------------------------
    public function getExaminingRows() {
        $t1 = nl2br($this->VIDEO->text1); //source text
        $t2 = nl2br($this->VIDEO->text2); //target text
        //break to parts by <br />
        $data_t1 = preg_split('/<br\s\/>/', $t1, -1, PREG_SPLIT_NO_EMPTY);
        $data_t2 = preg_split('/<br\s\/>/', $t2, -1, PREG_SPLIT_NO_EMPTY);

        $data_new = $this->getRowsArray($data_t1, $data_t2);

        //select a line in the text random
        $random = rand(0, count($data_new['data1']) - 1);
        $data = array();

        //learing all spaces including \n, \t
        if ($this->step == 0) { //from foreign to russian
            $data['src'] = $this->SESS->src_text[$this->SESS->count[$this->step]] = trim($data_new['data1'][$random]);
            $data['trg'] = $this->SESS->trg_text[$this->SESS->count[$this->step]] = trim($data_new['data2'][$random]);
        } elseif ($this->step == 1) { //from russian to foreign
            $data['trg'] = $this->SESS->src_text[$this->SESS->count[$this->step]] = trim($data_new['data1'][$random]);
            $data['src'] = $this->SESS->trg_text[$this->SESS->count[$this->step]] = trim($data_new['data2'][$random]);
        }
        //clearing comma from end of the text
        return new Lingua_Object($data);
    }

    //--------------------------------------------------------------------------
    private function getRowsArray($data1, $data2) {
        $data_new = array();

        //unique array to get different rows
        $data1 = array_unique($data1);

        //check array rows to filter normal rows
        foreach ($data1 as $key => $d) {
            $tmp = trim($d);
            if (!empty($tmp) && $this->hasEnoughtWords($d, 3)) {
                $data_new['data1'][] = trim($d);
                $data_new['data2'][] = trim($data2[$key]);
            }
        }
        return $data_new;
    }

    //--------------------------------------------------------------------------
    private function hasEnoughtWords($row, $words_count) {
        //the row contains more the 3 words 
        // and that words shodd be different , max 2 such words in the row
        $data = preg_split('/\s+|,|\.|\[|\]|\(|\)/', $row, null, PREG_SPLIT_NO_EMPTY);

        foreach ($data as $d) {
            $counter = 0;
            foreach ($data as $cur) {
                $b = false;
                for ($iterator = 0; $iterator < count($this->exception); $iterator++) {
                    if ($this->exception[$iterator] == strtolower($d)) {
                        $b = true;
                        break;
                    }
                }
                if ($b)
                    continue;

                if (strtolower($cur) == strtolower($d)) {
                    $counter++;
                }
            }
            if ($counter > 2)
                return false;
        }
        return count($data) > $words_count ? true : false;
    }

}
