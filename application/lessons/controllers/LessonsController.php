<?php
    class Lessons_LessonsController extends Lingua_Plugin_Action {
        public function indexAction()
        {
            
        }
        
        public function viewlessonAction()
        {
            $lesson_id = $this->getParam('lesson_id');
            
            $data = Lingua_Query_Lessons::lessonFormattedGet($lesson_id);
            
           // denis($data);
            $this->view->lesson = new Lingua_Object($data);
            
            //generet a string with 30 chars length and high level strange
            $key = $this->view->key = Lingua_GlobalKeys::generateKey(30, 4);
    
            //store generated key for api call to google translatesss
            Lingua_GlobalKeys::storeKey('audio_page', $key);
        }
    }