<?php

/**
 * Description of Uploader
 *
 * @author Aram
 */
class Lingua_Uploader {

    private $files;
    private $options = array(
        'file_type' => 'image',
        'file_max_size' => 2097152 // 1024*1024*2 = 2mb
    );
    private $upload_path;
    private $response = array();

    function __construct($files, $upload_path = null) {
        $this->files = $files;

        if (!$upload_path) {
            $this->upload_path = APPLICATION_PATH . "/../public_html/data/users/photo";
        } else
            $this->upload_path = $upload_path;
        $this->response['errors'] = array();
        $this->response['success'] = array();
    }

    //--------------------------------------------------------------------------
    public function process($flag = true, $subdir = "original") {
        foreach ($this->files as $file) {
            if (!isset($file['name']) || empty($file['name']))
                continue;
            if ($this->options['file_type'] == 'image') {
                if ($this->options['file_max_size'] < $file['size']) {
                    $this->response['errors'][] = array('issue' => $file['name'], 'error_message' => 'File size larger then ' . $this->options['file_max_size']);
                } elseif (false === ($im = $this->isImage($file))) {
                    $this->response['errors'][] = array('issue' => $file['name'], 'error_message' => 'File is not a image');
                } else {
                    //all is corrent , we can proccess to upload files
                    //upload file to original folder
                    $tmp_name = $this->generateName();
                    $new_name = $tmp_name . '.' . $this->getExtension($file['name']);
                    $new_file = Daz_Config::get('user.photo') . '/' . $new_name;

                    $copy = copy($file['tmp_name'], $new_file);

                    if ($im != 'jpg') {
                        $name = Daz_Config::get('user.photo') . '/' . $new_name;
                        $new = Daz_Config::get('user.photo') . '/' . $tmp_name . '.jpg';

                        $sys_comm = Daz_String::merge('convert [name][0] [new]', array('name' => $name, 'new' => $new));

                        //echo $sys_comm;
                        try {
                            $r = system($sys_comm);
                            unlink($name);
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                    }
                    $this->response['success'][] = array('issue' => $file['name'], 'uploaded' => $tmp_name . '.jpg',
                        'success_message' => 'successfully uploaded');
                }
            }
        }

        return $this->response;
    }

    //--------------------------------------------------------------------------
    private function makeThumnail($src, $width_tmp, $height_tmp, $folder_name, $file_name) {
        $im = imagecreatefromjpeg($src);

        $width = imagesx($im);
        $height = imagesy($im);

        if ($width > $height) {
            $newwidth = $width_tmp;
            $newheight = ($height / $width) * $newwidth;
        } else {
            $newheight = $height_tmp;
            $newwidth = ($width / $height) * $newheight;
        }

        $tmp = imagecreatetruecolor($newwidth, $newheight);

        imagecopyresampled($tmp, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        $filename = $this->upload_path . $folder_name . $file_name;

        imagejpeg($tmp, $filename, 100);
        imagedestroy($tmp);
    }

    //--------------------------------------------------------------------------
    private function generateName($length = 25) {
        $date = date("Y-m-d h:i:s");

        $chars = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm123456789";
        $strrand = "";

        for ($iterator = 0; $iterator < $length; $iterator++) {
            $strrand .= $chars[rand(0, $length)];
        }

        return md5($date) . '_' . md5($strrand);
    }

    //--------------------------------------------------------------------------
    private function getExtension($file_name) {
        $file_part = explode('.', $file_name);
        $ext = end($file_part);
        return strtolower($ext);
    }

    //--------------------------------------------------------------------------
    private function isImage($file) {

        $type = strtolower($this->getExtension($file['name']));
        $im = false;

        switch ($type) {
            case "jpg":
            case "jpeg":
                $im = imagecreatefromjpeg($file['tmp_name']);
                break;
            case "png":
                $im = imagecreatefrompng($file['tmp_name']);
                break;
            case 'gif':
                $im = imagecreatefromgif($file['tmp_name']);
                break;
        }

        return ($im === false) ? false : $type;
    }

}

?>
