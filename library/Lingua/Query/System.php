<?php

/**
 * Description of Misc
 *
 * @author Aram
 */
class Lingua_Query_System extends Lingua_Site_Connection {

    public static function countriesGetAll() {
        $SQL = self::statement();

        $SQL->sql('SELECT system_countries_id, name_ru');
        $SQL->sql('FROM system_countries');
        $SQL->sql('WHERE name_ru!=""');
        $SQL->sql('ORDER BY rank ASC');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function languagesGetAll() {
        $SQL = self::statement();

        $SQL->sql('SELECT system_languages_id, name, short_name, sys_use, icon');
        $SQL->sql('FROM system_languages');
        $SQL->sql('ORDER BY rank ASC');

        //run
        return self::selectNested($SQL, 'short_name');
    }

    //--------------------------------------------------------------------------
    public static function prasesGetByLanguage($language_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM community_chat_phrase');
        $SQL->sql('WHERE system_language_id=?')->setInt($language_id);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function languagesGetLevels() {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM user_languages_level');
        $SQL->sql('ORDER BY user_language_level_id');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function hasArtist($name) {
        $SQL = self::statement();

        $SQL->sql('SELECT system_artist_id AS id');
        $SQL->sql('FROM system_artists');
        $SQL->sql('WHERE name=?')->set($name);
        
        //run 
        return self::selectValue($SQL, 'id');
    }
    
    //--------------------------------------------------------------------------
    public static function systemAddArtist($name) {
        $SQL = self::statement();
        
        $SQL->sql('INSERT INTO system_artists SET');
        $SQL->sql('name = ?')->set($name);
        
        //run
        return self::queryInsert($SQL);
    }

}

?>
