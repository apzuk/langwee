<?php

/**
 * Description of Notification
 *
 * @author Aram
 */
class Lingua_Query_Notification extends Lingua_Site_Connection {

    public static function notificationChatting($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($receiver_lingua_id);
        $SQL->sql('type = "ACTION", ');
        $SQL->sql('notification_type_id = 1');
        $SQL->sql('ON DUPLICATE KEY');
        $SQL->sql('UPDATE type="ACTION"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationChatReject($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($receiver_lingua_id);
        $SQL->sql('type = "AUTO", ');
        $SQL->sql('notification_type_id = 2');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationFriendRequestApproved($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($receiver_lingua_id);
        $SQL->sql('type = "AUTO", ');
        $SQL->sql('notification_type_id = 4');
        $SQL->sql('ON DUPLICATE KEY UPDATE status="AUTO"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationFriendRequestRejected($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($receiver_lingua_id);
        $SQL->sql('type = "AUTO", ');
        $SQL->sql('notification_type_id = 5');
        $SQL->sql('ON DUPLICATE KEY UPDATE status="AUTO"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationFriendRequest($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($receiver_lingua_id);
        $SQL->sql('type = "ACTION", ');
        $SQL->sql('notification_type_id = 3');
        $SQL->sql('ON DUPLICATE KEY UPDATE type="ACTION"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationGet($lingua_id, $not_id = null) {
        $SQL = self::statement();

        $SQL->sql('SELECT n.*, CONCAT(i.firstname , " " ,i.surname) AS person, i.nickname, t.*, p.profability');
        $SQL->sql('FROM (user_notification n, user_notification_type t, user_info i)');
        $SQL->sql('LEFT JOIN user_profability p ON p.lingua_id = n.sender_lingua_id');
        $SQL->sql('WHERE n.receiver_lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('AND n.notification_type_id = t.notification_type_id');
        $SQL->sql('AND i.lingua_id = n.sender_lingua_id');
        $SQL->sql('AND status="WAITING" AND (user_notofication = ? OR FALSE=?)')->setInt($not_id)->setBoolean($not_id);
        $SQL->sql('LIMIT 1');

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationSetApproved($notification_id, $lingua_id = null) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_notification SET');
        $SQL->sql('status = "GOT"');
        $SQL->sql('WHERE user_notofication = ? AND')->setInt($notification_id);
        $SQL->sql('(receiver_lingua_id=? OR FALSE = ?)')->setInt($lingua_id)->setBoolean($lingua_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationFriendRemove($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($receiver_lingua_id);
        $SQL->sql('type = "AUTO", ');
        $SQL->sql('notification_type_id = 6');
        $SQL->sql('ON DUPLICATE KEY UPDATE type="AUTO"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function hasUserRequest($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM user_notification');
        $SQL->sql('  WHERE sender_lingua_id=? AND receiver_lingua_id=?')->setInt($sender_lingua_id)->setInt($receiver_lingua_id);
        $SQL->sql('  AND type=2 AND status="WAITING"');
        $SQL->sql(') AS result');

        //run
        return (Boolean) self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function notificationSetGotStatus($not_id, $lingua_id = false) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_notification SET');
        $SQL->sql('status = "GOT"');
        $SQL->sql('WHERE user_notofication = ?')->setInt($not_id);
        $SQL->sql('AND receiver_lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function notificationSetUserBlocked($sender_id, $getter_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_notification SET');
        $SQL->sql('sender_lingua_id = ?,')->setInt($sender_id);
        $SQL->sql('receiver_lingua_id = ?,')->setInt($getter_id);
        $SQL->sql('type = "AUTO", ');
        $SQL->sql('notification_type_id = 7');
        $SQL->sql('ON DUPLICATE KEY UPDATE type="AUTO"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function isNotificationExists($sender_id, $getter_id, $type) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS ( ');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM user_notification');
        $SQL->sql('  WHERE sender_lingua_id = ?')->setInt($sender_id);
        $SQL->sql('     AND receiver_lingua_id = ?')->setInt($getter_id);
        $SQL->sql('     AND notification_type_id = ?')->setInt($type);
        $SQL->sql('     AND status="WAITING"');
        $SQL->sql(' ) AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

}

?>
