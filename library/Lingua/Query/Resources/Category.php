<?php

/**
 * Description of category
 *
 * @author Aram
 */
class Lingua_Query_Resources_Category extends Lingua_Site_Connection {

    public static function categoryGet() {
        $SQL = self::statement();

        $SQL->sql('(SELECT c.video_category_id AS category_id,c.category_name, "video" AS resource');
        $SQL->sql('FROm video v ');
        $SQL->sql('LEFT JOIN video_category c');
        $SQL->sql('   ON c.video_category_id = v.video_category_id');
        $SQL->sql('WHERE !ISNULL(v.video_category_id)');
        $SQL->sql('   AND v.status = "PUBLISHED"');
        $SQL->sql('GROUP BY v.video_category_id');
        $SQL->sql('LIMIT 0, 5)');

        $SQL->sql('UNION');

        $SQL->sql('(SELECT c.audio_category_id AS category_id, c.category_name, "audio" AS resource');
        $SQL->sql('FROm audio a ');
        $SQL->sql('LEFT JOIN audio_category c');
        $SQL->sql('   ON c.audio_category_id = a.audio_category_id');
        $SQL->sql('WHERE !ISNULL(a.audio_category_id)');
        $SQL->sql('   AND a.status = "PUBLISHED"');
        $SQL->sql('GROUP BY a.audio_category_id');
        $SQL->sql('LIMIT 0, 5)');

        //run
        return self::selectNested($SQL, 'resource', 'category_name');
    }

}

