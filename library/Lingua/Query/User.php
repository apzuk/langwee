<?php

/**
 * Description of User
 *
 * @author Aram
 */
class Lingua_Query_User extends Lingua_Site_Connection {

    //--------------------------------------------------------------------------
    public static function getUserIdByUsernameAndPassword($username, $password) {
        $SQL = self::statement();

        $SQL->sql('SELECT lingua_id');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE login=? AND')->set($username);
        $SQL->sql('password=?')->set($password);

        //run
        return self::selectValue($SQL, 'lingua_id');
    }

    //--------------------------------------------------------------------------
    public static function userSetActivationPending($code, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_activation SET');
        $SQL->sql('activation_code = ?,')->set($code);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetFull($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.lingua_id, r.role_name, i.firstname, uc.credit, a.paid_status, a.active_till, ucpn.number, ucpn.pin, ae.url,');
        $SQL->sql('i.surname, concat(i.firstname, " ", i.surname) as person, a.login, i.age, i.phone, i.gender, i.photo, i.nickname,');
        $SQL->sql('i.birth_date, a.login, a.system_countries_id as country, c.name_ru AS country_name, us.user_online_status as status');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN user_alfaevent ae ON ae.lingua_id=a.lingua_id');
        $SQL->sql('LEFT JOIN user_coupon ucpn ON ucpn.lingua_id = a.lingua_id');
        $SQL->sql('LEFT JOIN system_countries c ON c.system_countries_id=a.system_countries_id');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id=a.lingua_id');
        $SQL->sql('LEFT JOIN acl_role r ON r.acl_role_id=a.role_id');
        $SQL->sql('LEFT JOIN user_status us ON us.lingua_id = a.lingua_id');
        $SQL->sql('LEFT JOIN user_credit uc ON uc.lingua_id = a.lingua_id');
        $SQL->sql('WHERE a.lingua_id=?')->setInt($lingua_id);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetShort($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.lingua_id, cd.credit,  concat(i.firstname, " ", i.surname) as person, p.profability AS total,');
        $SQL->sql('i.photo, a.login, a.system_countries_id as country, c.name_ru AS country_name');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN user_credit AS cd ON cd.lingua_id = a.lingua_id');
        $SQL->sql('LEFT JOIN system_countries c ON c.system_countries_id=a.system_countries_id');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id=a.lingua_id');
        $SQL->sql('LEFT JOIN acl_role r ON r.acl_role_id=a.role_id');
        $SQL->sql('LEFT JOIN user_profability p ON p.lingua_id = a.lingua_id');
        $SQL->sql('WHERE a.lingua_id=?')->setInt($lingua_id);

        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetLanguages($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT l.system_language_id, l.type, s.name AS language_name, lv.user_language_level_id, lv.name as level_name');
        $SQL->sql('FROM user_languages l, system_languages s, user_languages_level lv');
        $SQL->sql('WHERE lingua_id = ? AND l.system_language_id = s.system_languages_id')->setInt($lingua_id);
        $SQL->sql('AND l.user_languages_level_id = lv.user_language_level_id');

        //run
        return self::selectNested($SQL, 'type', 'system_language_id');
    }

    //--------------------------------------------------------------------------
    public static function userInsert($email, $password, $role_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_account SET');
        $SQL->sql('login=?,')->set($email);
        $SQL->sql('password=?,')->set($password);
        $SQL->sql('role_id=?')->setInt($role_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userActivePending($activation_code) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_activation SET');
        $SQL->sql('status=?')->set('ACTIVE');
        $SQL->sql('WHERE activation_code=? AND status="PENDING"')->set($activation_code);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetByEmail($email) {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE login=?')->set($email);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userAddUnknowVideoWord($word_trg, $word_src, $word_src_lang, $word_trg_lang, $lingua_id, $type_id, $context) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_word SET');
        $SQL->sql('word_trg=?,')->set($word_trg);
        $SQL->sql('word_src=?,')->set($word_src);
        $SQL->sql('word_src_lang=?,')->setInt($word_src_lang);
        $SQL->sql('word_trg_lang=?,')->setInt($word_trg_lang);
        $SQL->sql('word_context=?,')->set($context);
        $SQL->sql('lingua_id =?,')->setint($lingua_id);
        $SQL->sql('video_id =?')->setint($type_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE user_word_id=user_word_id');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userAddUnknowAudioWord($word_trg, $word_src, $word_src_lang, $word_trg_lang, $lingua_id, $type_id, $context) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_word SET');
        $SQL->sql('word_trg=?,')->set($word_trg);
        $SQL->sql('word_src=?,')->set($word_src);
        $SQL->sql('word_src_lang=?,')->setInt($word_src_lang);
        $SQL->sql('word_trg_lang=?,')->setInt($word_trg_lang);
        $SQL->sql('word_context=?,')->set($context);
        $SQL->sql('lingua_id =?,')->setint($lingua_id);
        $SQL->sql('audio_id =?')->setint($type_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE user_word_id=user_word_id');
        
        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetUnknownWordsByVideo($video_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT user_word_id, word_src, word_trg, word_context');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);
        $SQL->sql('AND video_id = ?')->setint($video_id);
        $SQL->sql('ORDER BY create_ts');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetUnknownWordsByAudio($audio_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT user_word_id, word_src, word_trg, word_context');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);
        $SQL->sql('AND audio_id = ?')->setint($audio_id);
        $SQL->sql('ORDER BY create_ts');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetByActivationCode($code) {
        $SQL = self::statement();

        $SQL->sql('SELECT lingua_id');
        $SQL->sql('FROM user_activation');
        $SQL->sql('WHERE activation_code=?')->set($code);

        //run
        return self::selectValue($SQL, 'lingua_id');
    }

    //--------------------------------------------------------------------------
    public static function userGetWordCount($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);

        //run
        return (int) self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userGetUnknowWordFromLocalDB($word_src, $trg = false, $src = false) {
        $SQL = self::statement();

        $SQL->sql('SELECT word_trg, user_word_id');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE word_src = ?')->set($word_src);
        $SQL->sql('AND word_src_lang = ? AND word_trg_lang = ?')->setInt($src)->setInt($trg);
        $SQL->sql('GROUP BY word_src');

        //run
        return self::selectValue($SQL, 'word_trg');
    }

    //--------------------------------------------------------------------------
    public static function userGetWordById($word_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT word_src_lang, word_trg_lang, word_src');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE user_word_id=?')->setInt($word_id);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userList($page, $length) {
        $SQL = self::statement();

        $SQL->sql('SELECT u.login, u.create_ts, a.activation_date, i.firstname, i.surname, i.age, r.role_name, a.status');
        $SQL->sql('FROM (user_account u, user_activation a, acl_role r)');
        $SQL->sql('LEFT JOIN user_info i ON a.lingua_id = i.lingua_id');
        $SQL->sql('WHERE u.lingua_id = a.lingua_id');
        $SQL->sql('AND r.acl_role_id = u.role_id');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userListCount() {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM (user_account u, user_activation a, acl_role r)');
        $SQL->sql('LEFT JOIN user_info i ON a.lingua_id = i.lingua_id');
        $SQL->sql('WHERE u.lingua_id = a.lingua_id');
        $SQL->sql('AND r.acl_role_id = u.role_id');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userSetInfo($firstname, $surname, $age, $gender, $photo, $lingua_id, $phone, $nickname = '', $birth_date = '') {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_info SET');
        $SQL->sql('firstname = ?,')->set($firstname);
        $SQL->sql('surname = ?,')->set($surname);
        $SQL->sql('age = ?,')->setInt($age);
        $SQL->sql('gender = ?,')->set($gender);
        $SQL->sql('photo = ?,')->set($photo);
        $SQL->sql('nickname = ?,')->set($nickname);
        $SQL->sql('birth_date = ?,')->set($birth_date);
        $SQL->sql('phone = ?,')->set($phone);
        $SQL->sql('lingua_id = ?')->set($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('firstname = ?,')->set($firstname);
        $SQL->sql('surname = ?,')->set($surname);
        $SQL->sql('age = ?,')->setInt($age);
        $SQL->sql('gender = ?,')->set($gender);
        $SQL->sql('phone = ?,')->set($phone);
        $SQL->sql('photo = ?,')->set($photo);
        $SQL->sql('nickname = ?,')->set($nickname);
        $SQL->sql('birth_date = ?,')->set($birth_date);
        $SQL->sql('modified_date = NOW()');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetCoupon($coupon_number, $coupon_pin, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_coupon SET');
        $SQL->sql('number = ?,')->set($coupon_number);
        $SQL->sql('pin = ?,')->set($coupon_pin);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('number = ?,')->set($coupon_number);
        $SQL->sql('pin = ?')->set($coupon_pin);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userCouponCountGetNew() {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM user_coupon');
        $SQL->sql('WHERE view="pending"');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userSetCountry($coutry, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('system_countries_id = ?')->setInt($coutry);
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);
        //run 
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetLanguage($system_language_id, $type, $lvl, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_languages SET');
        $SQL->sql('system_language_id = ?,')->setInt($system_language_id);
        $SQL->sql('type=?,')->set($type);
        $SQL->sql('user_languages_level_id = ?,')->setInt($lvl);
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('system_language_id = ?,')->setInt($system_language_id);
        $SQL->sql('type=?,')->set($type);
        $SQL->sql('user_languages_level_id = ?,')->setInt($lvl);
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetdefaultUrl($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT default_url');
        $SQL->sql('FROM acl_role r, user_account a');
        $SQL->sql('WHERE r.acl_role_id = a.role_id');
        $SQL->sql('AND a.lingua_id=?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'default_url');
    }

    //--------------------------------------------------------------------------
    public static function userGetPhoto($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT photo');
        $SQL->sql('FROM user_info');
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'photo');
    }

    //--------------------------------------------------------------------------
    public static function userGetRole($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT r.role_name');
        $SQL->sql('FROM user_account a , acl_role r');
        $SQL->sql('WHERE a.role_id = r.acl_role_id AND');
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'role_name');
    }

    //--------------------------------------------------------------------------
    public static function userWordCount($lingua_id, $lang, $training_id, $limit = 10) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM user_word u');
        $SQL->sql('WHERE lingua_id = ? AND word_lang = ?')->setInt($lingua_id)->set($lang);
        $SQL->sql('  AND user_word_id NOT IN (');
        $SQL->sql('     SELECT user_word_id FROM user_trained_words');
        $SQL->sql('     WHERE stsyem_trainigs_id = ? AND lingua_id = u.lingua_id')->setInt($training_id);
        $SQL->sql('       AND (status = "TRAINED" OR (status = "AGAIN" AND DATEDIFF(`trained_ts`, NOW()) < 2 ))');
        $SQL->sql('      )');
        $SQL->sql('LIMIT 10')->setInt($limit);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userGetRandom($know, $learning, $lingua_id, $length) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(a.lingua_id) , CONCAT(i.surname, " ", i.firstname) AS person, c.name_ru AS country, i.photo, s.user_online_status AS status');
        $SQL->sql('FROM (user_account a, user_info i, system_countries c, user_languages l, user_status s)');
        $SQL->sql('WHERE a.system_countries_id = c.system_countries_id');
        $SQL->sql('AND a.lingua_id = i.lingua_id');
        $SQL->sql('AND ( (l.system_language_id = ? AND type="KNOW") OR FALSE=? )')->setInt($know)->setBoolean($know);
        $SQL->sql('AND ( (l.system_language_id = ? AND type="LEARNING") OR FALSE=? )')->setInt($learning)->setBoolean($learning);
        $SQL->sql('AND (i.surname !="" OR i.firstname!="")');
        $SQL->sql('AND a.lingua_id != ?')->setInt($lingua_id);
        $SQL->sql('AND a.lingua_id = s.lingua_id AND s.user_online_status="ONLINE"');
        $SQL->sql('AND l.lingua_id = a.lingua_id');
        $SQL->sql('AND NOT EXISTS (');
        $SQL->sql('   SELECT blocked_lingua_id');
        $SQL->sql('   FROM user_block_list');
        $SQL->sql('   WHERE lingua_id = a.lingua_id AND blocked_lingua_id = ?')->setInt($lingua_id);
        $SQL->sql(')');
        $SQL->sql('ORDER BY RAND() LIMIT ?')->setInt($length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetOnlineStatus($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_status SET');
        $SQL->sql('   user_online_status = ?,')->set("ONLINE");
        $SQL->sql('   user_login_date=null,');
        $SQL->sql('   user_action_date=NOW(),');
        $SQL->sql('   lingua_id=?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('   user_online_status = ?,')->set("ONLINE");
        $SQL->sql('   user_login_date=NOW()');

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetLastAction($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_status SET');
        $SQL->sql('   user_online_status = ?,')->set("ONLINE");
        $SQL->sql('   user_action_date=NOW(),');
        $SQL->sql('   lingua_id=?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('   user_online_status = ?,')->set("ONLINE");
        $SQL->sql('   user_action_date=NOW()');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userUpdateOnlineStatus() {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_status SET');
        $SQL->sql('   user_online_status = ?')->set("OFFLINE");
        $SQL->sql('WHERE TIME_TO_SEC(TIMEDIFF(NOW(), user_action_date)) / 60 <= 20 OR ISNULL(user_action_date)');
        $SQL->sql('LIMIT 1000');

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetStatus($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT user_online_status');
        $SQL->sql('FROM user_status');
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'user_online_status');
    }

    //--------------------------------------------------------------------------
    public static function userSetStatus($status, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_status SET');
        $SQL->sql('user_online_status = ?')->set($status);
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userRemoveLanguages($lingua_id, $type) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_languages');
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('AND type = ?')->set($type);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userAddFriend($lingua_id, $friend_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_friend_list SET');
        $SQL->sql('lingua_id = ?,')->setInt($lingua_id);
        $SQL->sql('friend_lingua_id = ?')->setInt($friend_id);

        //run
        self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetFriends($lingua_id, $start, $length = 36) {
        $SQL = self::statement();

        $SQL->sql('SELECT CONCAT(i.firstname, " ", i.surname) as person, i.photo, c.name_ru AS country, a.lingua_id, us.user_online_status AS status');
        $SQL->sql('FROM (user_friend_list f, system_countries c, user_account a )');
        $SQL->sql('   LEFT JOIN user_info i ON i.lingua_id = a.lingua_id');
        $SQL->sql('   LEFT JOIN user_status us ON us.lingua_id = a.lingua_id');
        $SQL->sql('WHERE ((f.lingua_id != ? AND f.friend_lingua_id = ? AND a.lingua_id = f.lingua_id) OR')->setInt($lingua_id)->setInt($lingua_id);
        $SQL->sql('   (f.lingua_id = ? AND f.friend_lingua_id != ? AND a.lingua_id = f.friend_lingua_id))')->setInt($lingua_id)->setInt($lingua_id);
        $SQL->sql('    AND c.system_countries_id = a.system_countries_id');
        $SQL->sql('LIMIT ?, ?')->setInt($start)->setInt($length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userIsFriends($lingua_id_1, $lingua_id_2) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('  SELECT *');
        $SQL->sql('    FROM user_friend_list');
        $SQL->sql('    WHERE (lingua_id = ? AND friend_lingua_id = ?)')->setInt($lingua_id_1)->setInt($lingua_id_2);
        $SQL->sql('    OR (friend_lingua_id = ? AND lingua_id = ?)')->setInt($lingua_id_1)->setInt($lingua_id_2);
        $SQL->sql(') AS result');
        //run
        return (Boolean) self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userRemoveFriend($lingua_id, $friend_id) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_friend_list');
        $SQL->sql('WHERE (lingua_id = ? AND friend_lingua_id = ?)')->setInt($lingua_id)->setInt($friend_id);
        $SQL->sql('OR (friend_lingua_id = ? AND lingua_id = ?)')->setInt($lingua_id)->setInt($friend_id);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetFriendsCount($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM user_friend_list fl');
        $SQL->sql('   LEFT JOIN user_status us ON us.lingua_id = fl.lingua_id');
        $SQL->sql('WHERE (fl.lingua_id = ? OR friend_lingua_id=?)')->setInt($lingua_id)->setInt($lingua_id);
        $SQL->sql('AND us.user_online_status = "ONLINE"');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userSearch($fname, $sname, $nickname, $yfrom, $yto, $learning, $know, $lvl, $gender, $lingua_id, $page = 0, $length = 50) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.lingua_id, CONCAT(i.firstname," ", i.surname) AS person, i.photo');
        $SQL->sql('FROM (user_account a, user_info i, user_activation ua)');
        $SQL->sql('  LEFT JOIN user_profability p ON p.lingua_id = a.lingua_id');
        $SQL->sql('WHERE a.lingua_id = i.lingua_id');
        $SQL->sql('  AND a.lingua_id = ua.lingua_id');
        $SQL->sql('  AND');
        $SQL->sql('  (');
        $SQL->sql('     (');
        $SQL->sql('         (i.firstname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($fname)->setBoolean($fname);
        $SQL->sql('         AND');
        $SQL->sql('         (i.surname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($sname)->setBoolean($sname);
        $SQL->sql('      )');
        $SQL->sql('      OR');
        $SQL->sql('      (');
        $SQL->sql('         (i.firstname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($sname)->setBoolean($sname);
        $SQL->sql('         AND');
        $SQL->sql('         (i.surname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($fname)->setBoolean($fname);
        $SQL->sql('      )');
        $SQL->sql('  )');
        $SQL->sql('  AND');
        $SQL->sql('  (');
        $SQL->sql('     (i.age > ? OR ? = FALSE)')->setInt($yfrom)->setBoolean($yfrom);
        $SQL->sql('     AND');
        $SQL->sql('     (i.age < ? OR ? = FALSE)')->setInt($yto)->setBoolean($yto);
        $SQL->sql('  )');
        $SQL->sql('  AND (i.gender = ? OR ? = FALSE)')->set($gender)->setBoolean($gender);
        $SQL->sql('  AND EXISTS (');
        $SQL->sql('     SELECT *');
        $SQL->sql('     FROM user_languages');
        $SQL->sql('     WHERE (lingua_id = a.lingua_id AND type = "LEARNING" AND system_language_id = ?) OR ? = FALSE')->setInt($learning)->setBoolean($learning);
        $SQL->sql('  )');
        $SQL->sql('  AND EXISTS (');
        $SQL->sql('     SELECT *');
        $SQL->sql('     FROM user_languages');
        $SQL->sql('     WHERE (lingua_id = a.lingua_id AND type = "KNOW" AND system_language_id = ?) OR ? = FALSE')->setInt($know)->setBoolean($know);
        $SQL->sql('  )');
        $SQL->sql('  AND (i.firstname != "" OR i.surname != "")');
        $SQL->sql('  AND a.lingua_id != ?')->setInt($lingua_id);
        $SQL->sql('  ORDER BY profability DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($length);
        
        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSearchCount($fname, $sname, $nickname, $yfrom, $yto, $learning, $know, $lvl, $gender, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(a.lingua_id) AS result');
        $SQL->sql('FROM (user_account a, user_info i, user_activation ua)');
        $SQL->sql('WHERE a.lingua_id = i.lingua_id');
        $SQL->sql('  AND a.lingua_id = ua.lingua_id');
        $SQL->sql('  AND');
        $SQL->sql('  (');
        $SQL->sql('     (');
        $SQL->sql('         (i.firstname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($fname)->setBoolean($fname);
        $SQL->sql('         AND');
        $SQL->sql('         (i.surname LIKE CONCAT("%" , ?, "%") OR ? = FALSE)')->set($sname)->setBoolean($sname);
        $SQL->sql('      )');
        $SQL->sql('      OR');
        $SQL->sql('      (');
        $SQL->sql('         (i.firstname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($sname)->setBoolean($sname);
        $SQL->sql('         AND');
        $SQL->sql('         (i.surname LIKE CONCAT("%", ?, "%") OR ? = FALSE)')->set($fname)->setBoolean($fname);
        $SQL->sql('      )');
        $SQL->sql('  )');
        $SQL->sql('  AND');
        $SQL->sql('  (');
        $SQL->sql('     (i.age > ? OR ? = FALSE)')->setInt($yfrom)->setBoolean($yfrom);
        $SQL->sql('     AND');
        $SQL->sql('     (i.age < ? OR ? = FALSE)')->setInt($yto)->setBoolean($yto);
        $SQL->sql('  )');
        $SQL->sql('  AND (i.gender = ? OR ? = FALSE)')->set($gender)->setBoolean($gender);
        $SQL->sql('  AND EXISTS (');
        $SQL->sql('     SELECT *');
        $SQL->sql('     FROM user_languages');
        $SQL->sql('     WHERE (lingua_id = a.lingua_id AND type = "LEARNING" AND system_language_id = ?) OR ? = FALSE')->setInt($learning)->setBoolean($learning);
        $SQL->sql('  )');
        $SQL->sql('  AND EXISTS (');
        $SQL->sql('     SELECT *');
        $SQL->sql('     FROM user_languages');
        $SQL->sql('     WHERE (lingua_id = a.lingua_id AND type = "KNOW" AND system_language_id = ?) OR ? = FALSE')->setInt($know)->setBoolean($know);
        $SQL->sql('  )');
        $SQL->sql('  AND (i.firstname != "" OR i.surname != "")');
        $SQL->sql('  AND a.lingua_id != ?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userSetUploadedAssociction($photo_name, $word_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_uploaded_association SET');
        $SQL->sql('user_word_id = ?,')->setInt($word_id);
        $SQL->sql('photoname = ?,')->set($photo_name);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userRemoveWord($word_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_word');
        $SQL->sql('WHERE user_word_id = ? AND')->setInt($word_id);
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetProfability($total, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_profability SET');
        $SQL->sql('profability = ?,')->setInt($total);
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE profability = ?')->setInt($total);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userBlockUser($to, $from) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_block_list SET');
        $SQL->sql('lingua_id = ?,')->setInt($from);
        $SQL->sql('blocked_lingua_id = ?')->setInt($to);
        $SQL->sql('ON DUPLICATE KEY UPDATE lingua_id=?')->setInt($from);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userUnblockUser($to, $from) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_block_list WHERE');
        $SQL->sql('lingua_id = ? AND')->setInt($from);
        $SQL->sql('blocked_lingua_id = ?')->setInt($to);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function setBigQueryMaxValue() {
        $SQL = self::statement();

        $SQL->sql('SET SQL_BIG_SELECTS=1');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetLogInTime($lingua_id) {
        $SQL = self::statement();

        $SQL->sql("INSERT INTO  user_login_log SET");
        $SQL->sql("login_time = NOW(),");
        $SQL->sql("lingua_id=?")->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetLogoutTime($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_login_log SET');
        $SQL->sql('logout_time = NOW()');
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('ORDER BY login_time DESC');
        $SQL->sql('LIMIT 1');

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userLastOnlineDuration($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT TIME_TO_SEC(TIMEDIFF(logout_time, login_time))/60 AS minutes');
        $SQL->sql('FROM user_login_log');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);
        $SQL->sql('ORDER BY login_time DESC');
        $SQL->sql('LIMIT 1');

        //run
        return self::selectValue($SQL, 'minutes');
    }

    //--------------------------------------------------------------------------
    public static function userSetOnlineDuration($duration, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_online_duration SET');
        $SQL->sql('duration=?,')->setInt($duration);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('duration=duration+?')->setInt($duration);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userIsPaid($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS (');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM user_account');
        $SQL->sql('  WHERE lingua_id=? AND paid_status="PAID"')->setInt($lingua_id);
        $SQL->sql('    AND active_till > NOW()');
        $SQL->sql(') AS result');
        
        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userIsAccountActiveted($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS (');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM user_activation');
        $SQL->sql('   WHERE lingua_id=? AND status="ACTIVE"')->setInt($lingua_id);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function isFirstTimeLoginToday($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT NOT EXISTS(');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM user_status');
        $SQL->sql('   WHERE lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('   AND DATE(user_login_date) = DATE(NOW())');
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function isCouponRegistred($number, $pin) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM user_coupon');
        $SQL->sql('  WHERE number=? AND pin=?')->set($number)->set($pin);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userGetCoupons($email, $firstname, $surname, $status, $page, $length) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.*, c.number, c.pin, DATE(c.create_ts) as date, c.view, i.photo, i.phone,');
        $SQL->sql('      CONCAT(i.surname, " ", i.firstname) AS person');
        $SQL->sql('FROM user_account a, user_coupon c');
        $SQL->sql('  LEFT JOIN user_info i ON i.lingua_id = c.lingua_id');
        $SQL->sql('WHERE c.lingua_id = a.lingua_id AND');
        $SQL->sql('  (a.login = ? OR ? = FALSE) AND')->set($email)->setBoolean($email);
        $SQL->sql('  (c.view = ? OR ? = FALSE) AND')->set($status)->setBoolean($status);
        $SQL->sql('  (i.firstname = ? OR FALSE=?) AND')->set($firstname)->setBoolean($firstname);
        $SQL->sql('  (i.surname = ? OR FALSE=?)')->set($surname)->setBoolean($surname);
        $SQL->sql('LIMIT ? , ?')->setInt($page)->setInt($length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetCouponsCount($email, $firstname, $surname, $status) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) As result');
        $SQL->sql('FROM user_account a, user_coupon c');
        $SQL->sql('  LEFT JOIN user_info i ON i.lingua_id = c.lingua_id');
        $SQL->sql('WHERE c.lingua_id = a.lingua_id AND');
        $SQL->sql('  (a.login = ? OR ? = FALSE) AND')->set($email)->setBoolean($email);
        $SQL->sql('  (c.view = ? OR ? = FALSE) AND')->set($status)->setBoolean($status);
        $SQL->sql('  (i.firstname = ? OR FALSE=?) AND')->set($firstname)->setBoolean($firstname);
        $SQL->sql('  (i.surname = ? OR FALSE=?)')->set($surname)->setBoolean($surname);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userActivateAccount($lingua_id, $date) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('paid_status = "PAID",');
        $SQL->sql('active_till = ?,')->set($date);
        $SQL->sql('activate_date = NOW()');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetCouponApproved($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_coupon SET');
        $SQL->sql('view = "approved"');
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetEmailBYId($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT login');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'login');
    }

    //--------------------------------------------------------------------------
    public static function userSetAlfaEvent($alpha_event, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_alfaevent SET');
        $SQL->sql('lingua_id = ?,')->setInt($lingua_id);
        $SQL->sql('url = ?')->set($alpha_event);
        $SQL->sql('ON DUPLICATE KEY UPDATE url=?')->set($alpha_event);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetConditions($email) {
        $SQL = self::statement();

        $SQL->sql('SELECT lingua_id, login, password');
        $SQL->sql(' FROM user_account');
        $SQL->sql('WHERE login=?')->set($email);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userHasRememberToday($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS( ');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM user_remember_email');
        $SQL->sql('   WHERE lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('   AND DATE(create_ts) = DATE(NOW())');
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userSetEmailRemember($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_remember_email SET');
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetNotsubscribed() {
        $SQL = self :: statement();

        $SQL->sql('SELECT a.lingua_id, i.surname, i.firstname, a.login');
        $SQL->sql('   FROM user_account a');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = a.lingua_id');
        $SQL->sql('WHERE a.lingua_id > 113641');

        //run  
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userSetSubscribed($lingua_ids) {
        $SQL = self :: statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('is_subscribed = "YES"');
        $SQL->sql('WHERE lingua_id IN %s', $SQL->uniqueIntArray($lingua_ids));

        //run 
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetLinguaIdbyEmail($email) {
        $SQL = self::statement();

        $SQL->sql('SELECT lingua_id');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE login = ?')->set($email);

        //run
        return self::selectValue($SQL, 'lingua_id');
    }
    
    //--------------------------------------------------------------------------
    public static function accessOlegTrainings($lingua_id) {
        $SQL = self::statement();
        
        $SQL->sql('INSERT INTO oleg_training SET');
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);
        
        //run
        return self::queryInsert($SQL);
    }
    
    //--------------------------------------------------------------------------
    public static function hasOlegAccess($lingua_id) {
        $SQL = self::statement();
        
        $SQL->sql('SELECT EXISTS (');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM oleg_training');
        $SQL->sql('   WHERE lingua_id = ?')->setInt($lingua_id);
        $SQL->sql(') AS result');
        
        //run
        return self::selectValue($SQL, 'result');
    }

}
