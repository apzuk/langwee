<?php

/**
 * Description of Credit
 *
 * @author Aram
 */
class Lingua_Query_User_Credit extends Lingua_Site_Connection {

    public static function userCreditSet($lingua_id, $credit) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_credit SET');
        $SQL->sql('lingua_id = ?,')->setInt($lingua_id);
        $SQL->sql('credit = ?')->setInt($credit);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('credit=credit+?')->setInt($credit);
        
        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userCreditGet($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT credit');
        $SQL->sql('FROM user_credit');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'credit');
    }
    
    //--------------------------------------------------------------------------
    public static function userCreditWithdraw($lingua_id, $count) {
        $SQL = self::statement();
        
        $SQL->sql('UPDATE user_credit SET');
        $SQL->sql('credit = credit - ?')->setInt($count);
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);
        
        //run
        return self::queryUpdate($SQL);
    }

}

?>
