<?php

/**
 * Description of History
 *
 * @author Aram
 */
class Lingua_Query_User_Credit_History extends Lingua_Site_Connection {

    public static function creditHistorySet($system_actions_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_credit_history SET');
        $SQL->sql('system_actions_id = ?,')->setInt($system_actions_id);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

}

?>
