<?php

/**
 * Description of Words
 *
 * @author Aram
 */
class Lingua_Query_User_Words extends Lingua_Site_Connection {

    //--------------------------------------------------------------------------
    public static function wordAdd($word_id, $lingua_id, $training_id, $status) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_trained_words SET');
        $SQL->sql('user_word_id = ?,')->setInt($word_id);
        $SQL->sql('stsyem_trainigs_id = ?,')->setInt($training_id);
        $SQL->sql('lingua_id = ?,')->setInt($lingua_id);
        $SQL->sql('status = ?')->set($status);
        $SQL->sql('ON DUPLICATE KEY UPDATE status="TRAINED"');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userWordAvailableCount($lingua_id, $lang, $training_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM user_word w');
        $SQL->sql('WHERE lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('AND word_src_lang = ?')->setInt($lang);
        $SQL->SQL('AND w.user_word_id NOT IN (');
        $SQL->sql('   SELECT user_word_id');
        $SQL->sql('   FROM user_trained_words');
        $SQL->sql('   WHERE lingua_id = w.lingua_id');
        $SQL->sql('   AND stsyem_trainigs_id = ?')->setInt($training_id);
        $SQL->sql('   AND (status = "TRAINED" OR (status = "AGAIN" AND DATEDIFF(`trained_ts`, NOW()) < 2 ))');
        $SQL->sql(')');
        $SQL->sql('LIMIT 10');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userWordWithRandomWords($lingua_id, $lang, $training_id, $limit = 4) {
        $SQL = self::statement();

        $SQL->sql('(SELECT u.user_word_id, u.word_trg, u.word_src, "first" as type');
        $SQL->sql('  FROM `user_word` u');
        $SQL->sql('WHERE lingua_id = ? AND word_src_lang = ?')->setInt($lingua_id)->setInt($lang);
        $SQL->sql('  AND user_word_id NOT IN (');
        $SQL->sql('     SELECT user_word_id FROM user_trained_words');
        $SQL->sql('     WHERE stsyem_trainigs_id = ? AND lingua_id = u.lingua_id')->setInt($training_id);
        $SQL->sql('       AND (status = "TRAINED" OR (status = "AGAIN" AND DATEDIFF(`trained_ts`, NOW()) < 2 ))');
        $SQL->sql('      )');
        $SQL->sql('ORDER BY RAND() limit 1)');
        $SQL->sql('UNION');
        $SQL->sql('(SELECT u.user_word_id, u.word_trg, u.word_src, "random" as type');
        $SQL->sql('  FROM `user_word` u');
        $SQL->sql('WHERE lingua_id != -1 AND word_src_lang = ?')->setInt($lang);
        $SQL->sql('ORDER BY RAND() LIMIT ?)')->setInt($limit);

        //run
        return self::selectNested($SQL, 'type', 'user_word_id');
    }

    //--------------------------------------------------------------------------
    public static function userWordWithRandomWord($lingua_id, $lang, $training_id, $limit = 4) {
        $SQL = self::statement();

        $SQL->sql('(SELECT u.user_word_id, u.word_trg, u.word_src, "first" as type');
        $SQL->sql('  FROM `user_word` u');
        $SQL->sql('WHERE lingua_id = ? AND word_src_lang = ?')->setInt($lingua_id)->set($lang);
        $SQL->sql('  AND user_word_id NOT IN (');
        $SQL->sql('     SELECT user_word_id FROM user_trained_words');
        $SQL->sql('     WHERE stsyem_trainigs_id = ? AND lingua_id = u.lingua_id')->setInt($training_id);
        $SQL->sql('       AND (status = "TRAINED" OR (status = "AGAIN" AND DATEDIFF(`trained_ts`, NOW()) < 2 ))');
        $SQL->sql('      )');
        $SQL->sql('ORDER BY RAND() limit 1)');

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userWordWithRandomWordHasLetter($lingua_id, $lang, $training_id, $letter) {
        $SQL = self::statement();

        $SQL->sql('(SELECT u.user_word_id, u.word_trg, u.word_src, u.type, u.type_id');
        $SQL->sql('  FROM `user_word` u');
        $SQL->sql('WHERE lingua_id = ? AND word_lang = ? AND')->setInt($lingua_id)->set($lang);
        $SQL->sql('u.word_src LIKE CONCAT("%", ?, "%")')->set($letter);
        $SQL->sql('  AND user_word_id NOT IN (');
        $SQL->sql('     SELECT user_word_id FROM user_trained_words');
        $SQL->sql('     WHERE stsyem_trainigs_id = ? AND lingua_id = u.lingua_id')->setInt($training_id);
        $SQL->sql('       AND (status = "TRAINED" OR (status = "AGAIN" AND DATEDIFF(`trained_ts`, NOW()) < 2 ))');
        $SQL->sql('      )');
        $SQL->sql('ORDER BY RAND() limit 1)');

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userWordGetCount($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function userGetWordsHistory($lang, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(word_src), word_trg, DATE(create_ts) AS register_date, user_word_id');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE lingua_id=? AND')->setInt($lingua_id);
        $SQL->sql('   (word_src_lang=? OR FALSE=?)')->setInt($lang)->setBoolean($lang);
        $SQL->sql('GROUP BY word_src');
        $SQL->sql('ORDER BY create_ts DESC');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetWordDetails($word_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT uw.*, COALESCE(v.title, a.title) AS title, COALESCE(w.video_id, w.audio_id) AS type_id');
        $SQL->sql('FROM user_word w');
        $SQL->sql('LEFT JOIN user_word uw ON uw.word_src = w.word_src');
        $SQL->sql('LEFT JOIN video v ON (v.video_id = uw.video_id)');
        $SQL->sql('LEFT JOIN audio a ON (a.audio_id = uw.audio_id )');
        $SQL->sql('WHERE w.user_word_id = ? AND uw.lingua_id=?')->setInt($word_id)->setInt($lingua_id);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function userGetWordLanguage($word_id) {
        $SQL = self::statament();

        $SQL->sql('SELECT system_language_id AS lang');
        $SQL->sql('FROM user_word');
        $SQL->sql('WHERE user_word_id = ?')->setInt($word_id);

        //run
        return self::selectValue($SQL, 'lang');
    }

}

?>
