<?php

/**
 * Description of Resources
 *
 * @author Aram 
 */
class Lingua_Query_Resources extends Lingua_Site_Connection {

    //-----------------------------------------------------------------
    public static function resourcesGetTops($lang) {
        $SQL = self::statement();

        //top 10 of video resources
        $SQL->sql('(SELECT v.title, v.video_id AS resource_id, (rating + fake) AS rating,');
        $SQL->sql('    s.name AS artist, v.created_ts, "video" AS resource');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_rating r');
        $SQL->sql('   ON r.video_id = v.video_id');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('   ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('INNER JOIN system_languages l');
        $SQL->sql('   ON l.system_languages_id = v.system_language_id');
        $SQL->sql('WHERE l.short_name = ? AND v.status="PUBLISHED"')->set($lang);
        $SQL->sql('ORDER BY (rating+fake) DESC');
        $SQL->sql('LIMIT 0, 10)');

        $SQL->sql('UNION');

        //top 10 of audio resources
        $SQL->sql('(SELECT a.title, a.audio_id AS resource_id, (rating + fake) AS rating,');
        $SQL->sql('    s.name AS artist, a.created_ts, "audio" AS resource');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN audio_rating r');
        $SQL->sql('   ON r.audio_id = a.audio_id');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('   ON s.system_artist_id = a.system_artist_id');
        $SQL->sql('INNER JOIN system_languages l');
        $SQL->sql('   ON l.system_languages_id = a.system_language_id');
        $SQL->sql('WHERE l.short_name = ? AND a.status="PUBLISHED"')->set($lang);
        $SQL->sql('ORDER BY (rating+fake) DESC');
        $SQL->sql('LIMIT 0, 10)');

        //run
        return self::selectNested($SQL, 'resource', 'resource_id');
    }

    //--------------------------------------------------------------------------
    public static function resourceSearch($q, $lang) {
        $SQL = self::statement();

        $SQL->sql('(SELECT DISTINCT(s.name) , v.video_id AS resource_id, v.title, "video" AS resource, (r.fake + r.rating) AS rating');
        $SQL->sql('  FROM video v');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('  ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('LEFT JOIN video_rating r');
        $SQL->sql('  ON v.video_id = r.video_id');
        $SQL->sql('WHERE ((v.title LIKE CONCAT("%", ?, "%") OR FALSE = TRUE)')->set($q);
        $SQL->sql('  OR (s.name LIKE CONCAT("%", ?, "%") OR FALSE = TRUE))')->set($q);
        $SQL->sql('  AND v.system_language_id = ?')->setInt($lang);
        $SQL->sql('LIMIT 0, 10)');

        $SQL->sql('UNION');

        $SQL->sql('(SELECT DISTINCT(s.name) , a.audio_id AS resource_id, a.title, "audio" AS resource,  (r.fake + r.rating) AS rating');
        $SQL->sql('  FROM audio a');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('  ON s.system_artist_id = a.system_artist_id');
        $SQL->sql('LEFT JOIN audio_rating r');
        $SQL->sql('  ON a.audio_id = r.audio_id');
        $SQL->sql('WHERE ((a.title LIKE CONCAT("%", ?, "%") OR FALSE = TRUE)')->set($q);
        $SQL->sql('  OR (s.name LIKE CONCAT("%", ?, "%") OR FALSE = TRUE))')->set($q);
        $SQL->sql('  AND a.system_language_id = ?')->setInt($lang);
        $SQL->sql('LIMIT 0, 10)');
        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function resourceSearchVideo($q, $lang, $page) {
        $SQL = self::statement();

        $SQL->sql('(SELECT DISTINCT(s.name) , v.video_id AS resource_id, v.title, "video" AS resource, (r.fake + r.rating) AS rating');
        $SQL->sql('  FROM video v');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('  ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('LEFT JOIN video_rating r');
        $SQL->sql('  ON v.video_id = r.video_id');
        $SQL->sql('WHERE ((v.title LIKE CONCAT("%", ?, "%") OR FALSE = TRUE)')->set($q);
        $SQL->sql('  OR (s.name LIKE CONCAT("%", ?, "%") OR FALSE = TRUE))')->set($q);
        $SQL->sql('  AND v.system_language_id = ?')->setInt($lang);
        $SQL->sql('LIMIT ?, 10)')->setInt($page);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function resourceSearchAudio($q, $lang, $page) {
        $SQL = self::statement();

        $SQL->sql('(SELECT DISTINCT(s.name) , a.audio_id AS resource_id, a.title, "audio" AS resource,  (r.fake + r.rating) AS rating');
        $SQL->sql('  FROM audio a');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('  ON s.system_artist_id = a.system_artist_id');
        $SQL->sql('LEFT JOIN audio_rating r');
        $SQL->sql('  ON a.audio_id = r.audio_id');
        $SQL->sql('WHERE ((a.title LIKE CONCAT("%", ?, "%") OR FALSE = TRUE)')->set($q);
        $SQL->sql('  OR (s.name LIKE CONCAT("%", ?, "%") OR FALSE = TRUE))')->set($q);
        $SQL->sql('  AND a.system_language_id = ?')->setInt($lang);
        $SQL->sql('LIMIT ?, 10)')->setInt($page);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function resourceSearchCount($q, $lang) {
        $SQL = self::statement();

        $SQL->sql('(SELECT count(*) AS count, "video" AS resource');
        $SQL->sql('  FROM video v');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('  ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('WHERE ((v.title LIKE CONCAT("%", ?, "%") OR FALSE = TRUE)')->set($q);
        $SQL->sql('  OR (s.name LIKE CONCAT("%", ?, "%") OR FALSE = TRUE))')->set($q);
        $SQL->sql('  AND v.system_language_id = ?')->setInt($lang);
        $SQL->sql('LIMIT 0, 10)');

        $SQL->sql('UNION');

        $SQL->sql('(SELECT count(*) AS count, "audio" AS resource');
        $SQL->sql('  FROM audio a');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('  ON s.system_artist_id = a.system_artist_id');
        $SQL->sql('WHERE ((a.title LIKE CONCAT("%", ?, "%") OR FALSE = TRUE)')->set($q);
        $SQL->sql('  OR (s.name LIKE CONCAT("%", ?, "%") OR FALSE = TRUE))')->set($q);
        $SQL->sql('  AND a.system_language_id = ?')->setInt($lang);
        $SQL->sql('LIMIT 0, 10)');

        //run
        return self::selectNested($SQL, 'resource');
    }

    //--------------------------------------------------------------------------
    public static function resourcesGetCategories($page, $limit) {
        $SQL = self::statement();

        $SQL->sql('(SELECT `category_name`, `video_category_id` AS category_id , "video" as resource');
        $SQL->sql('FROM video_category');
        $SQL->sql('LIMIT ? , ?)')->setInt($page)->setInt($limit);
        $SQL->sql('UNION');
        $SQL->sql('(SELECT `category_name`, `audio_category_id` AS category_id , "audio" as resource');
        $SQL->sql('FROM audio_category');
        $SQL->sql('LIMIT ? , ?)')->setInt($page)->setInt($limit);

        //run
        return self::selectNested($SQL, 'resource', 'category_id');
    }

    ######################AUDIO####################
    //--------------------------------------------------------------------------

    public static function resourcesGetVideoArtists($letter, $lang, $page = 0, $limit = 10) {
        $SQL = self::statement();

        $SQL->sql('(SELECT s.`name` AS artist, ');
        $SQL->sql('  (SELECT COUNT(*) FROM video WHERE system_artist_id = s.system_artist_id) as rcount');
        $SQL->sql('FROM `system_artists` s');
        $SQL->sql('WHERE EXISTS (');
        $SQL->sql('   SELECT v.video_id');
        $SQL->sql('   FROM video v');
        $SQL->sql('      INNER JOIN system_languages l');
        $SQL->sql('         ON l.system_languages_id = v.system_language_id');
        $SQL->sql('   WHERE  `status` = ?')->set('PUBLISHED');
        $SQL->sql('     AND l.short_name = ?')->set($lang);
        $SQL->sql('     AND ISNULL( video_category_id ) ');
        $SQL->sql('     AND s.system_artist_id = system_artist_id) AND(');
        $SQL->sql('     (SUBSTRING( s.name, 1, 1 ) =  ?')->set($letter);
        $SQL->sql('     OR SUBSTRING( s.name, 1, 1 ) BETWEEN 0 AND 9) OR ');
        $SQL->sql('     ? = FALSE) ')->setBoolean($lang);
        $SQL->sql('LIMIT ? , ?)')->setInt($page)->setInt($limit);

        //run
        return self::selectNested($SQL, 'resource', 'artist');
    }

    //--------------------------------------------------------------------------
    public static function resourcesGetAudioArtists($letter, $lang, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('(SELECT s.`name` AS artist, ');
        $SQL->sql('  (SELECT COUNT(*) FROM video WHERE system_artist_id = s.system_artist_id) as rcount');
        $SQL->sql('FROM `system_artists` s');
        $SQL->sql('WHERE EXISTS (');
        $SQL->sql('   SELECT a.audio_id');
        $SQL->sql('   FROM audio a');
        $SQL->sql('      INNER JOIN system_languages l');
        $SQL->sql('         ON l.system_languages_id = a.system_language_id');
        $SQL->sql('   WHERE  `status` = ?')->set('PUBLISHED');
        $SQL->sql('     AND l.short_name = ?')->set($lang);
        $SQL->sql('     AND ISNULL( audio_category_id ) ');
        $SQL->sql('     AND s.system_artist_id = system_artist_id) AND(');
        $SQL->sql('     (SUBSTRING( s.name, 1, 1 ) =  ?')->set($letter);
        $SQL->sql('     OR SUBSTRING( s.name, 1, 1 ) BETWEEN 0 AND 9) OR ');
        $SQL->sql('     ? = FALSE) ')->setBoolean($lang);
        $SQL->sql('LIMIT ? , ?)')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function resourcesGetArtistsCount($lang, $letter) {
        $SQL = self::statement();

        $SQL->sql('SELECT count(name) as count, "video" AS resource ');
        $SQL->sql('FROM system_artists s');
        $SQL->sql('WHERE ');
        $SQL->sql('   (SUBSTRING(s.name, 1, 1) = ? OR ? = FALSE) AND');
        $SQL->set($letter)->setBoolean($letter);
        $SQL->sql('EXISTS (');
        $SQL->sql('   SELECT * ');
        $SQL->sql('   FROM video v, system_languages l');
        $SQL->sql(' WHERE s.system_artist_id = v.system_artist_id AND');
        $SQL->sql('   v.system_language_id = l.system_languages_id');
        $SQL->sql('   AND short_name = ?')->set($lang);
        $SQL->sql(')');

        //run
        return self::selectNested($SQL, 'resource');
    }

    //--------------------------------------------------------------------------
    public static function resourcesAudioGetByArtist($artist_id, $lang) {
        $SQL = self::statement();

        $SQL->sql('(SELECT a.title, a.audio_id AS resource_id, (rating + fake) AS rating,');
        $SQL->sql('    s.name AS artist, a.created_ts, "audio" AS resource');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN audio_rating r');
        $SQL->sql('   ON r.audio_id = a.audio_id');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('   ON s.system_artist_id = a.system_artist_id');
        $SQL->sql('INNER JOIN system_languages l');
        $SQL->sql('   ON l.system_languages_id = a.system_language_id');
        $SQL->sql('WHERE l.system_languages_id = ?')->setInt($lang);
        $SQL->sql('  AND s.system_artist_id = ?')->setInt($artist_id);
        $SQL->sql('  AND a.status  = "PUBLISHED"');
        $SQL->sql('ORDER BY (rating+fake) DESC');
        $SQL->sql('LIMIT 0, 10)');

        //run
        return self::selectNested($SQL, 'artist', 'resource_id');
    }

    //--------------------------------------------------------------------------
    public static function resourcesAudioGetAll($keyword, $lang, $artist, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT (r.rating+r.fake) AS rating, a.audio_id AS resource_id, a.title, a.system_artist_id');
        $SQL->sql('FROM audio_rating r');
        $SQL->sql('INNER JOIN audio a ON (');
        $SQL->sql('  a.audio_id = r.audio_id AND');
        $SQL->sql('  ISNULL(audio_category_id) AND ');
        $SQL->sql('  a.status="PUBLISHED" AND');
        $SQL->sql('  a.system_language_id = ? and')->setInt($lang);
        $SQL->sql('  (a.system_artist_id = ? OR ? = FALSE))')->set($artist)->setBoolean($artist);
        $SQL->sql('  ORDER BY (r.rating + r.fake) DESC');
        $SQL->sql('  LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectNested($SQL, 'resource_id');
    }

    //--------------------------------------------------------------------------
    public static function resourcesAudioGetAllCount($keyword, $lang, $artist) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS count');
        $SQL->sql('FROM audio_rating r');
        $SQL->sql('INNER JOIN audio a ON (');
        $SQL->sql('  a.audio_id = r.audio_id AND');
        $SQL->sql('  ISNULL(audio_category_id) AND ');
        $SQL->sql('  a.status="PUBLISHED" AND');
        $SQL->sql('  a.system_language_id = ? and')->setInt($lang);
        $SQL->sql('  (a.system_artist_id = ? OR ? = FALSE))')->set($artist)->setBoolean($artist);
        $SQL->sql('  ORDER BY (r.rating + r.fake) DESC');
        //run
        return self::selectValue($SQL, 'count');
    }

    /* //--------------------------------------------------------------------------
      public static function resourcesGetExistingArtists($lang) {
      $SQL = self::statement();

      $SQL->sql('(SELECT UCASE(substring(name , 1 , 1)) AS letter, "video" AS resource');
      $SQL->sql('FROM video v');
      $SQL->sql('LEFT JOIN system_artists s ON v.system_artist_id = s.system_artist_id');
      $SQL->sql('where v.system_language_id = ?')->set($lang);
      $SQL->sql('GROUP BY letter)');

      $SQL->sql('UNION');

      $SQL->sql('(SELECT UCASE(substring(name , 1 , 1)) AS letter, "audio" AS resource');
      $SQL->sql('FROM audio a');
      $SQL->sql('LEFT JOIN system_artists s ON a.system_artist_id = s.system_artist_id');
      $SQL->sql('WHERE a.system_language_id = ?')->set($lang);
      $SQL->sql('GROUP BY letter)');

      //run
      return self::selectNested($SQL, 'resource', 'letter');
      }
     */

    //--------------------------------------------------------------------------
    public static function resourcesVideoGetExistingArtists($lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT SUBSTR(s.name,1,1) as letter, "video" AS resource');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN system_artists s on s.system_artist_id = v.system_artist_id');
        $SQL->sql('LEFT JOIN system_languages l ON l.system_languages_id = v.system_language_id');
        $SQL->sql('WHERE l.short_name = ? AND ISNULL(v.video_category_id)')->set($lang);
        $SQL->sql('  AND v.status="PUBLISHED"');
        $SQL->sql('GROUP BY letter');
        
        //run
        return self::selectNested($SQL, 'resource', 'letter');
    }

    //--------------------------------------------------------------------------
    public static function resourcesAudioGetExistingArtists($lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT SUBSTR(s.name,1,1) as letter, "audio" AS resource');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN system_artists s on s.system_artist_id = a.system_artist_id');
        $SQL->sql('LEFT JOIN system_languages l ON l.system_languages_id = a.system_language_id');
        $SQL->sql('WHERE l.short_name = ? AND ISNULL(a.audio_category_id)')->set($lang);
        $SQL->sql('  AND a.status="PUBLISHED"');
        $SQL->sql('GROUP BY letter');

        //run
        return self::selectNested($SQL, 'resource', 'letter');
    }

    //--------------------------------------------------------------------------
    public static function resourcesVideoGetAll($keyword, $lang, $artist, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT (r.rating+r.fake) AS rating, v.video_id AS resource_id, v.title, v.system_artist_id');
        $SQL->sql('FROM video_rating r');
        $SQL->sql('INNER JOIN video v ON (');
        $SQL->sql('  v.video_id = r.video_id AND');
        $SQL->sql('  ISNULL(video_category_id) AND ');
        $SQL->sql('  v.status="PUBLISHED" AND');
        $SQL->sql('  v.system_language_id = ? and')->setInt($lang);
        $SQL->sql('  (v.system_artist_id = ? OR ? = FALSE))')->set($artist)->setBoolean($artist);
        $SQL->sql('  ORDER BY (r.rating + r.fake) DESC');
        $SQL->sql('  LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectNested($SQL, 'resource_id');
    }

    //--------------------------------------------------------------------------
    public static function resourcesVideoGetAllCount($keyword, $lang, $artist) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS count');
        $SQL->sql('FROM video_rating r');
        $SQL->sql('INNER JOIN video v ON (');
        $SQL->sql('  v.video_id = r.video_id AND');
        $SQL->sql('  ISNULL(video_category_id) AND ');
        $SQL->sql('  v.status="PUBLISHED" AND');
        $SQL->sql('  v.system_language_id = ? and')->setInt($lang);
        $SQL->sql('  (v.system_artist_id = ? OR ? = FALSE))')->set($artist)->setBoolean($artist);
        $SQL->sql('  ORDER BY (r.rating + r.fake) DESC');
        //run
        return self::selectValue($SQL, 'count');
    }

    //--------------------------------------------------------------------------
    public static function resourcesVideoGetByArtist($artist_id, $lang, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('(SELECT v.title, v.video_id AS resource_id, (rating + fake) AS rating,');
        $SQL->sql('    s.name AS artist, v.created_ts, "audio" AS resource');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_rating r');
        $SQL->sql('   ON r.video_id = v.video_id');
        $SQL->sql('LEFT JOIN system_artists s');
        $SQL->sql('   ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('INNER JOIN system_languages l');
        $SQL->sql('   ON l.system_languages_id = v.system_language_id');
        $SQL->sql('WHERE l.system_languages_id = ?')->setInt($lang);
        $SQL->sql('  AND s.system_artist_id = ?')->setInt($artist_id);
        $SQL->sql('  AND v.status  = "PUBLISHED"');
        $SQL->sql('ORDER BY (rating+fake) DESC');
        $SQL->sql('LIMIT ?, ?)')->setInt($page)->setInt($limit);

        //run
        return self::selectNested($SQL, 'artist', 'resource_id');
    }

}

?>
