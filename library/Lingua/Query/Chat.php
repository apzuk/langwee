<?php

/**
 * Description of Chat
 *
 * @author Aram
 */
class Lingua_Query_Chat extends Lingua_Site_Connection {

    public static function chatCreate($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO community_chat_rooms SET');
        $SQL->sql('creator_lingua_id = ?,')->setInt($sender_lingua_id);
        $SQL->sql('receiver_lingua_id = ?')->setInt($receiver_lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function chatAddMessage($chat_id, $message, $lingua_id = -1) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO community_chat_messages SET');
        $SQL->sql('message = ?,')->set($message);
        $SQL->sql('community_chat_room_id = ?,')->setInt($chat_id);
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);
        
        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function chatGet($sender_lingua_id, $receiver_lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT community_chat_room_id');
        $SQL->sql('FROM community_chat_rooms');
        $SQL->sql('WHERE creator_lingua_id=?')->setInt($sender_lingua_id);
        $SQL->sql('AND receiver_lingua_id=?')->setInt($receiver_lingua_id);

        //run
        return self::selectValue($SQL, 'community_chat_room_id');
    }

    //--------------------------------------------------------------------------
    public static function chatGetById($chat_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT cm.*');
        $SQL->sql('FROM community_chat_rooms cm');
        $SQL->sql('WHERE cm.community_chat_room_id = ?')->setInt($chat_id);
        $SQL->sql('AND (creator_lingua_id = ?)')->setInt($lingua_id);
        
        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function chatApprove($chat_id, $sender_lingua_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE community_chat_rooms SET');
        $SQL->sql('status="APPROVED"');
        $SQL->sql('WHERE creator_lingua_id = ? AND receiver_lingua_id = ? AND')->setInt($sender_lingua_id)->setInt($lingua_id);
        $SQL->sql('community_chat_room_id = ?')->setInt($chat_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function chatGetMessages($chat_id, $mgs_id ,$length = 15) {
        $SQL = self::statement();

        $SQL->sql('SELECT m.community_chat_messages as message_id, m.message, m.create_ts, m.community_chat_room_id, CONCAT(i.firstname, " ", i.surname) AS person, i.lingua_id, i.nickname');
        $SQL->sql('FROM community_chat_messages m , user_info i');
        $SQL->sql('WHERE community_chat_room_id = ? AND i.lingua_id = m.lingua_id')->setInt($chat_id);
        $SQL->sql('AND m.community_chat_messages > ?')->setInt($mgs_id);
        
        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function chatSetUserAvaliableStatus($lingua_id, $chat_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_chat_status SET');
        $SQL->sql('chat_id = ?,')->setInt($chat_id);
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE status = "AVALIABLE"');
        
        //run
        return self::queryInsert($SQL);
    }
    
    //--------------------------------------------------------------------------
    public static function chatSetUserUnavaliableStatus($lingua_id, $chat_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_chat_status SET');
        $SQL->sql('chat_id = ?,')->setInt($chat_id);
        $SQL->sql('lingua_id = ?,')->setInt($lingua_id);
        $SQL->sql('status = "NOTAVALIABLE"');
        $SQL->sql('ON DUPLICATE KEY UPDATE status = "NOTAVALIABLE"');
        
        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function isChatExist($lingua_id1, $lingua_id2) {
        $SQL = self::statement();

        $SQL->sql('SELECT community_chat_room_id AS result');
        $SQL->sql('FROM community_chat_rooms');
        $SQL->sql('WHERE (creator_lingua_id = ? AND receiver_lingua_id = ?)')->setInt($lingua_id1)->setInt($lingua_id2);
        $SQL->sql('OR (creator_lingua_id = ? AND receiver_lingua_id = ?)')->setInt($lingua_id2)->setInt($lingua_id1);

        //run
        return self::selectValue($SQL, 'result');
    }
    
    //--------------------------------------------------------------------------
    public static function userGetFull($lingua_id, $chat_id) {
        $SQL = self::statement();
        
        $SQL->sql('SELECT a.login, CONCAT(i.firstname, " " ,i.surname) AS person, s.status, i.photo');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = a.lingua_id');
        $SQL->sql('LEFT JOIN user_chat_status s ON (s.lingua_id = a.lingua_id AND chat_id = ?)')->setInt($chat_id);
        $SQL->sql('WHERE a.lingua_id = ?')->setInt($lingua_id);
        
        //run
        return self::selectRow($SQL);
    }

}

?>
