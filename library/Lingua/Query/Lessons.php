<?php
    /**
     * Lingua_Query_Lessons
     * 
     * @package langwee
     * @author Denis Karviga
     * @copyright 2012
     * @version $Id$
     * @access public
     */
    class Lingua_Query_Lessons extends Lingua_Site_Connection
    {
        /**
         * Lingua_Query_Lessons::lessonsGetAllCount()
         * 
         * @param mixed $status
         * @param mixed $creator
         * @return
         */
        public static function lessonsGetAllCount($status = null, $creator = null)
        {
            $SQL = self::statement();

            $SQL->sql('SELECT COUNT(*) AS result');
            $SQL->sql('FROM lessons l');
            $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = l.created_by');
            $SQL->sql('WHERE l.status != ?')->set("REMOVED");
            $SQL->sql('   AND (l.status=? OR FALSE=?)')->set($status)->setBoolean($status);
            $SQL->sql('   AND (created_by = ? OR FALSE=?) ')->setInt($creator)->setBoolean($creator);    
            
            return self::selectValue($SQL, 'result');
        }
        
        /**
         * Lingua_Query_Lessons::lessonGet()
         * 
         * @param mixed $lesson_id
         * @param bool $publish
         * @return
         */
        public static function lessonGet($lesson_id, $publish = false) {
            $SQL = self::statement();
    
            $SQL->sql('SELECT *');
            $SQL->sql('FROM lessons l');
            $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = l.created_by');
            $SQL->sql('WHERE l.status != ?')->set("REMOVER");
            $SQL->sql(' AND (l.status = ? OR FALSE = ?)')->set($publish)->setBoolean($publish);
            $SQL->sql(' AND l.lesson_id = ?')->setInt($lesson_id);
            
            $data =  self::selectRow($SQL);
            
            $data['words'] = Lingua_Query_Word::getLessonWords($lesson_id, true);
            $data['phrases'] = Lingua_Query_Phrase::getLessonPhrases($lesson_id);
            
            return $data;
        }
        
        /**
         * Lingua_Query_Lessons::lessonFormattedGet()
         * 
         * Получение урока с отформатированным текстом
         * 
         * @param mixed $lesson_id
         * @return
         */
        public static function lessonFormattedGet($lesson_id)
        {
            $sql = self::statement();
            
            $sql->sql('SELECT *');
            $sql->sql('FROM `lessons`');
            $sql->sql('WHERE `lesson_id` = ?')->setInt($lesson_id);
            
            error_reporting(E_ALL);
            $data = self::selectRow($sql);
            
            $data['source'] = self::formatText($data['source']);
            $data['translate'] = self::formatText($data['translate']);
            
                 
            $phrases = Lingua_Query_Phrase::getLessonPhrases($lesson_id);            
            $words = Lingua_Query_Word::getLessonWords($lesson_id); 
            
            $data['audio_ministory_text'] = preg_replace('/\r\n/', '<br />', $data['audio_ministory_text']);
            
            if ($phrases)
                foreach ($phrases as $phrase)
                {
                    $data['source'] = preg_replace('/([\^ \.,\?!])'.$phrase['phrase_text1'].'([\^ \.,\?!])/ims', '$1<span class="select-phrase">'.$phrase['phrase_text1'].'</span>$2', $data['source'], 1);
                } 
            //denis($words);
            if($words)
                foreach($words as $word)
                {
                    $data['source'] = preg_replace('/([\^ \.,\?!])'.$phrase['phrase_text1'].'([\^ \.,\?!])/ims', '$1<span class="select-word">'.$phrase['phrase_text1'].'</span>$2', $data['source'], 1);
                }
                         
            return $data;
        }
        
        private static function formatText($text)
        {
            $paragraphs = array();
            
            if (preg_match_all('/<p>(.+?)<\/p>/ims', $text, $paragraphs))
            {                
                $paragraphs = $paragraphs[1];
            }
            else
            {
                $paragraphs = array(
                    0 => $text,               
                );
            }
            
            $sentence_num = 0;
            foreach ($paragraphs as &$p)
            {
                $sentences = preg_split('/\/\//ims', $p);
                foreach ($sentences as $k => &$sentence)
                {
                    $sentence = preg_replace('/([A-Za-zA-Яа-я]+)/', '<span class="word">$1</span>', $sentence);
                    if (trim($sentence) != '')
                    {   
                        $sentence = '<span class="sentence sentence-'.$sentence_num.'" data-num="'.$sentence_num.'">'.trim($sentence).'</span>';
                        $sentence_num++;
                    } else {
                        unset($sentences[$k]);
                    }
                }
                
                $p = '<p>'.join(' ', $sentences).'</p>';
            }
            
            $text = join('', $paragraphs);
            
            return $text;  
        }
        
        /**
         * Lingua_Query_Lessons::lessonEdit()
         * 
         * @param int $lesson_id
         * @param string $lesson_title
         * @param string $source
         * @param string $translate
         * @param array  $phrases
         * @param array  $words
         * @param string $audio_history_url
         * @param string $audio_vocabular_url
         * @param string $audio_ministory_url
         * @param string $audio_ministory_text
         * @param string $creator
         * @param string $status
         * 
         * @return void
         */
        public static function lessonEdit($lesson_id, $data)
        {
            
            //denis($data);
            $sql = self::statement();
            
            $sql->sql('UPDATE lessons');
            $sql->sql('SET  source = ?,')->set($data['source']);
            $sql->sql('lesson_title = ?,')->set($data['lesson_title']);
            $sql->sql('language_id = ?,')->set($data['language_id']);
            $sql->sql('translate = ?,')->set($data['translate']);
            $sql->sql('audio_history_url = ?,')->set($data['audio_history_url']);
            $sql->sql('audio_vocabular_url = ?,')->set($data['audio_vocabular_url']);
            $sql->sql('audio_ministory_url = ?,')->set($data['audio_ministory_url']);
            $sql->sql('audio_ministory_text = ?,')->set($data['audio_ministory_text']);
            $sql->sql('created_by = ?,')->setInt($data['creator']);
            $sql->sql('status = ?')->set($data['status']);
            $sql->sql('WHERE lesson_id = ?')->setInt($lesson_id);
            
            self::queryUpdate($sql);
            $words = $data['words'];
            $phrases = $data['phrases'];
           
            if ($words['word']) 
            {
                foreach($words['word'] as $k => $v)
                {
                    if (!$words['word_id'][$k])
                        Lingua_Query_Word::addWord($words['word'][$k], $words['word_translate'][$k], $lesson_id);
                    else
                        Lingua_Query_Word::updateWord($words['word_id'][$k], $words['word'][$k], $words['word_translate'][$k]);
                }
            }
            
            if ($phrases['phrase'])
            {
                foreach ($phrases['phrase'] as $k=>$v)
                {
                    if (trim($phrases['phrase_translate'][$k]) != '')
                    {
                        if (!$phrases['phrase_id'][$k])
                            Lingua_Query_Phrase::addPhrase($phrases['phrase'][$k], $phrases['phrase_translate'][$k], $lesson_id);
                        else
                            Lingua_Query_Phrase::updatePhrase($phrases['phrase_id'][$k], $phrases['phrase'][$k], $phrases['phrase_translate'][$k]);
                    } else {
                        if ($phrases['phrase_id'][$k])
                            Lingua_Query_Phrase::deletePhrase($phrases['phrase_id'][$k]);
                    }
                }
            }
        }
        
        /**
         * Lingua_Query_Lessons::lessonAdd()
         * 
         * @param mixed $lesson_title
         * @param mixed $source
         * @param mixed $translate
         * @param mixed $phrases
         * @param mixed $words
         * @param mixed $audio_history_url
         * @param mixed $audio_vocabular_url
         * @param mixed $audio_ministory_url
         * @param mixed $audio_ministory_text
         * @param mixed $creator
         * @param mixed $status
         * @return void
         */
        public static function lessonAdd($data) 
        {   
            //denis($words);
            error_reporting(E_ALL);
            $SQL = self::statement();            
            $SQL->sql('INSERT INTO lessons');
            $SQL->sql('SET  source = ?,')->set($data['source']);
            $SQL->sql('lesson_title = ?,')->set($data['lesson_title']);
            $SQL->sql('translate = ?,')->set($data['translate']);
            $SQL->sql('audio_history_url = ?,')->set($data['audio_history_url']);
            $SQL->sql('audio_vocabular_url = ?,')->set($data['audio_vocabular_url']);
            $SQL->sql('audio_ministory_url = ?,')->set($data['audio_ministory_url']);
            $SQL->sql('audio_ministory_text = ?,')->set($data['audio_ministory_text']);
            $SQL->sql('created_by = ?,')->setInt($data['creator']);
            $SQL->sql('status = ?')->set($data['status']);
                                    
            $lesson_id = self::queryInsert($SQL);
            
            $words = $data['words'];
            $phrases = $data['phrases'];
            
            if ($words['word']) 
            {
                foreach($words['word'] as $k => $v)
                {                   
                    Lingua_Query_Word::addWord($words['word'][$k], $words['word_translate'][$k], $lesson_id);                   
                }
            }
            
            if ($phrases['phrase'])
            {
                foreach ($phrases['phrase'] as $k=>$v)
                {
                    if (trim($phrases['phrase_translate'][$k]) != '')
                        Lingua_Query_Phrase::addPhrase($phrases['phrase'][$k], $phrases['phrase_translate'][$k], $lesson_id);
                }
            }
            
        }        
        
        /**
         * Lingua_Query_Lessons::lessonsGetAll()
         * 
         * @param mixed $status
         * @param mixed $creator
         * @param mixed $offset
         * @param mixed $limit
         * @return
         */
        public static function lessonsGetAll($status, $creator, $offset, $limit)
        {
            $SQL = self::statement();
            
            $SQL->sql('SELECT *');
            $SQL->sql('FROM lessons l');
            $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = l.created_by');
            $SQL->sql('WHERE l.status!=?')->set("REMOVED");
            $SQL->sql('   AND (l.status=? OR FALSE=?)')->set($status)->setBoolean($status);
            $SQL->sql('   AND (created_by = ? OR FALSE=?) ')->setInt($creator)->setBoolean($creator);
            $SQL->sql('ORDER BY l.lesson_id DESC');
            $SQL->sql('LIMIT ?, ?')->setInt($offset)->setInt($limit);
            
            $data = self::selectMany($SQL);
            
            return $data;           
        }
    }
