<?php

/**
 * Description of Discuss
 *
 * @author Aram
 */
class Lingua_Query_Video_Discuss extends Lingua_Site_Connection {

    public static function discussGet($from, $limit, $video_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT c.comment, c.create_ts, c.modified_ts, a.login, i.nickname, i.photo, a.lingua_id, c.video_comment_id');
        $SQL->sql('FROM (video_comments c, user_account a)');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id=a.lingua_id');
        $SQL->sql('WHERE video_id=? AND video_comment_id>? AND')->setInt($video_id)->setInt($from);
        $SQL->sql('c.lingua_id = a.lingua_id');
        $SQL->sql('ORDER BY video_comment_id DESC');
        $SQL->sql('LIMIT ?')->setInt($limit);
        
        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function discussInsert($text, $video_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO video_comments SET');
        $SQL->sql('comment = ?,')->set($text);
        $SQL->sql('video_id=?,')->setInt($video_id);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function discussDelete($discuss_id) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM video_comments');
        $SQL->sql('WHERE video_comment_id = ?')->setInt($discuss_id);
        
        //run
        return self::queryDelete($SQL);
    }
    
    //--------------------------------------------------------------------------
    public static function discussDeleteOwn($discuss_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM video_comments');
        $SQL->sql('WHERE video_comment_id = ? AND lingua_id=?')->setInt($discuss_id)->setInt($lingua_id);
        
        //run
        return self::queryDelete($SQL);
    }

}

?>
