<?php

/**
 * Description of Category
 *
 * @author Aram
 */
class Lingua_Query_Video_Category extends Lingua_Site_Connection {

    public static function categoryGetAll() {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM video_category');
        $SQL->sql('WHERE video_category_id!=-1');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function categoryLastestRank() {
        $SQL = self::statement();

        $SQL->sql('SELECT MAX(category_rank) AS result');
        $SQL->sql('FROM video_category');

        //run
        return (int) self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function categoryAdd($category_name, $category_rank) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO video_category SET');
        $SQL->sql('category_name=?,')->set($category_name);
        $SQL->sql('category_rank=?')->setInt($category_rank);
        $SQL->sql('ON DUPLICATE KEY UPDATE category_name=category_name');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function categoryUpdate($category_name, $category_rank, $category_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE video_category SET');
        $SQL->sql('category_name=?,')->set($category_name);
        $SQL->sql('category_rank=?')->setInt($category_rank);
        $SQL->sql('WHERE video_category_id=?')->setInt($category_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function categoryGet($cat_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT category_name, category_rank, video_category_id');
        $SQL->sql('FROM video_category');
        $SQL->sql('WHERE video_category_id=?')->setInt($cat_id);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function categoryHasVideo($cat_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS');
        $SQL->sql('  (SELECT * FROM video');
        $SQL->sql('   WHERE status!="DISABLED" AND video_category_id = ?')->setInt($cat_id)->setInt($cat_id);
        $SQL->sql('  ) AS result');

        //run
        return self::selectRow($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function categoryDelete($cat_id) {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM video_category');
        $SQL->sql('WHERE video_category_id=?')->setInt($cat_id);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function categoryGetLimited($page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM video_category');
        $SQL->sql('WHERE video_category_id!=-1');
        $SQL->sql('LIMIT ?,?')->setInt($page)->setInt($limit);
        
        //run
        return self::selectMany($SQL);
    }
}
