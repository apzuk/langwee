<?php
    /**
     * Lingua_Query_Phrase
     * 
     * @package langwee
     * @author Denis Karviga
     * @copyright 2012
     * @version $Id$
     * @access public
     */
    class Lingua_Query_Phrase extends Lingua_Site_Connection {
        
        
        /**
         * Lingua_Query_Phrase::addPhrase()
         * 
         * @param mixed $phrase_text1
         * @param mixed $phrase_text2
         * @param mixed $lesson_id
         * @return void
         */
        public static function addPhrase($phrase_text1, $phrase_text2, $lesson_id)
        {
            $sql = self::statement();  
            
            $sql->sql('INSERT INTO phrases');
            $sql->sql('SET phrase_text1 = ?,')->set(trim($phrase_text1));
            $sql->sql('phrase_text2 = ?,')->set(trim($phrase_text2));
            $sql->sql('lesson_id = ?')->set($lesson_id);
            
            $phrase_id = self::queryInsert($sql);   
        }
        
        /**
         * Lingua_Query_Phrase::updatePhrase()
         * 
         * @param mixed $phrase_id
         * @param mixed $phrase_text1
         * @param mixed $phrase_text2
         * @return void
         */
        public static function updatePhrase($phrase_id, $phrase_text1, $phrase_text2)
        {
            $sql = self::statement();
            
            $sql->sql('UPDATE phrases');
            $sql->sql('SET phrase_text1 = ?,')->set(trim($phrase_text1));
            $sql->sql('phrase_text2 = ?')->set(trim($phrase_text2));
            $sql->sql('WHERE phrase_id = ?')->setInt($phrase_id);
            
            self::queryUpdate($sql);
        }
        
        public static function deletePhrase($phrase_id)
        {
            $sql = self::statement();
            
            $sql->sql('DELETE FROM phrases WHERE phrase_id = ?')->setInt($phrase_id);
            
            return self::queryDelete($sql);
        }
        
        /**
         * Lingua_Query_Phrase::getLessonPhrases()
         * 
         * @param mixed $lesson_id
         * @return
         */
        public static function getLessonPhrases($lesson_id)
        {
            $sql = self::statement();
            
            $sql->sql('SELECT *');
            $sql->sql('FROM phrases');
            $sql->sql('WHERE lesson_id = ?')->setInt($lesson_id);
            
            return self::selectMany($sql);
        }
    }