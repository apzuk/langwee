<?php
    class Lingua_Query_Word extends Lingua_Site_Connection {
        
        /**
         * Lingua_Query_Word::addWord()
         * 
         * @param string $word_text
         * @param array $word_translates
         * @param int $lesson_id
         * @return void
         */
        public static function addWord($word_text, $word_translates, $lesson_id)
        {
            $sql = self::statement();  
            //denis($wo)
            $sql->sql('INSERT INTO `words`');
            $sql->sql('SET word_text = ?,')->set(trim($word_text));
            $sql->sql('lesson_id = ?')->set($lesson_id);
            
            $word_id = self::queryInsert($sql);   
            
            self::saveWordTranslates($word_id, $word_translates);
        }
        
        /**
         * Lingua_Query_Word::updateWord()
         * 
         * @param int $word_id
         * @param string $word_text
         * @param array $word_translates
         * @return
         */
        public static function updateWord($word_id, $word_text, $word_translates)
        {
            $sql = self::statement();
            
            $sql->sql('UPDATE words');
            $sql->sql('SET word_text = ?')->set($word_text);
            
            self::queryUpdate($sql);
            //denis($word_translates);
            self::saveWordTranslates($word_id, $word_translates);           
        }
        
        
        /**
         * Lingua_Query_Word::saveWordTranslates()
         * 
         * @param int $word_id
         * @param array $word_translates
         * @return
         */
        private function saveWordTranslates($word_id, $word_translates)
        {
            $sql = self::statement();
            
            $sql->sql('DELETE FROM `word_translates`');
            $sql->sql('WHERE word_id = ?')->setInt($word_id);
            
            self::queryDelete($sql);
            
            
            
            if ($word_translates)
            {               
                foreach ($word_translates as $translate)
                {     
                    $sql = self::statement();
                    error_reporting(E_ALL);
                    $sql->sql('INSERT INTO `word_translates`');
                    $sql->sql('SET word_id = ?,')->setInt($word_id);
                    $sql->sql('translate_text = ?')->set($translate);                    
                    self::queryInsert($sql);
                }
            }
            
        }
        
        
        /**
         * Lingua_Query_Word::getLessonWords()
         * 
         * @param int $lesson_id
         * @param bool $with_translates
         * @return
         */
        public static function getLessonWords($lesson_id, $with_translates = false)
        {
            $sql = self::statement();
            
            $sql->sql('SELECT *');
            $sql->sql('FROM words');
            $sql->sql('WHERE lesson_id = ?')->setInt($lesson_id);
            
            $data = self::selectMany($sql);
            
            if ($with_translates)
            {
                if ($data)
                    foreach($data as $k => $item)
                    {
                        $data[$k]['translates'] = self::getWordTranslates($item['word_id']);
                    }
            }
            return $data;
        }
        
        /**
         * Lingua_Query_Word::getWordTranslates()
         * 
         * @param int $word_id
         * @return
         */
        public static function getWordTranslates($word_id)
        {
            $sql = self::statement();
            
            $sql->sql('SELECT *');
            $sql->sql('FROM word_translates');
            $sql->sql('WHERE word_id = ?')->setInt($word_id);
            
            return self::selectMany($sql);
        }
        
        /**
         * Lingua_Query_Word::deleteWord()
         * 
         * @param int $word_id
         * @return void
         */
        public static function deleteWord($word_id)
        {
            $sql = self::statement();            
            $sql->sql('DELETE FROM word_translates WHERE word_id = ?')->setInt($word_id);            
            self::queryDelete($sql);
            
            
            $sql = self::statement();
            $sql->sql('DELETE FROM words WHERE word_id = ?')->setInt($word_id);            
            self::queryDelete($sql);
        }
    }