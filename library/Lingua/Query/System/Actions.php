<?php

/**
 * Description of Actions
 *
 * @author Aram
 */
class Lingua_Query_System_Actions extends Lingua_Site_Connection {

    public static function systemActionsGet($action_name) {
        $SQL = self::statement();
        
        $SQL->sql('SELECT *');
        $SQL->sql('FROM system_actions');
        $SQL->sql('WHERE action = ?')->set($action_name);
        
        //run
        return self::selectRow($SQL);
    }

}

?>
