<?php

/**
 * Description of Messages
 *
 * @author Aram
 */
class Lingua_Query_System_Messages extends Lingua_Site_Connection{

    public static function messageIsShow($action, $lingua_id) {
        $SQL = self::statement();
        
        $SQL->sql('SELECT EXISTS (');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM system_payment_message');
        $SQL->sql('  WHERE system_actions_id=? AND lingua_id=?')->setInt($action)->setInt($lingua_id);
        $SQL->sql(') AS result');
        
        //run
        return self::selectValue($SQL, 'result');
    }
    
    //--------------------------------------------------------------------------
    public static function messageSetShown($system_actions_id, $lingua_id) {
        $SQL = self::statement();
        
        $SQL->sql('INSERT INTO system_payment_message SET');
        $SQL->sql('system_actions_id = ? ,')->setInt($system_actions_id);
        $SQL->sql('lingua_id=?')->setInt($lingua_id);
        
        //run
        return self::queryInsert($SQL);
    }

}

?>
