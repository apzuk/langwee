<?php

/**
 * Description of Trainings
 *
 * @author Aram
 */
class Lingua_Query_System_Trainings extends Lingua_Site_Connection {

    //--------------------------------------------------------------------------
    public static function getTrainingId($name) {
        $SQL = self::statement();

        $SQL->sql('SELECT system_trainings_id AS result');
        $SQL->sql('FROM system_trainings');
        $SQL->sql('WHERE name = ?')->set($name);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function trainingsGetIds() {
        $SQL = self::statement();

        $SQL->sql('SELECT system_trainings_id AS id, name');
        $SQL->sql('FROM system_trainings');

        //run
        return self::selectNested($SQL, 'name');
    }

}

?>
