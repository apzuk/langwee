<?php
/**
 * Description of Test
 *
 * @author Aram
 */
class Lingua_Query_Test extends Lingua_Site_Connection
{
    //--------------------------------------------------------------------------
    public static function getUsers()
    {
        $SQL = self::statement();

        $SQL->sql('SELECT lingua_id');
        $SQL->sql('FROM user_account');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function setUserStatus($lingua_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_status SET');
        $SQL->sql('user_online_status = "ONLINE",');
        $SQL->sql('lingua_id = ?')->setInt($lingua_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function fillPhrasesTheme($prase)
    {
        $SQL = self::statement();

        $SQL->sql("INSERT INTO system_pharses_theme SET");
        $SQL->sql('theme = ?,')->set($prase);
        $SQL->sql('category = "famous"');
        //run
        return self::queryInsert($SQL);
    }
}

?>
