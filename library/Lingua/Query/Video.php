<?php

/**
 * Description of Video
 *
 * @author Aram
 */
class Lingua_Query_Video extends Lingua_Site_Connection {

    public static function videoAdd($title, $category_id, $url, $artist_id, $text1, $text2, $creator, $status, $video_lang) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO video SET');
        $SQL->sql('title=?,')->set($title);
        $SQL->sql('video_category_id=?,')->setInt($category_id);
        $SQL->sql('video_url=?,')->set($url);
        $SQL->sql('system_artist_id=?,')->setInt($artist_id);
        $SQL->sql('text1=?,')->set($text1);
        $SQL->sql('text2=?,')->set($text2);
        $SQL->sql('status=?,')->set($status);
        $SQL->sql('system_language_id=?,')->setInt($video_lang);
        $SQL->sql('created_by=?')->setInt($creator);
        $SQL->sql('ON DUPLICATE KEY UPDATE title=title');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoEdit($title, $category_id, $url, $artist_id, $text1, $text2, $creator, $status, $video_id, $video_lang) {
        $SQL = self::statement();

        $SQL->sql('UPDATE video SET');
        $SQL->sql('title=?,')->set($title);
        $SQL->sql('video_category_id=?,')->setInt($category_id);
        $SQL->sql('video_url=?,')->set($url);
        $SQL->sql('system_artist_id=?,')->set($artist_id);
        $SQL->sql('text1=?,')->set($text1);
        $SQL->sql('text2=?,')->set($text2);
        $SQL->sql('status=?,')->set($status);
        $SQL->sql('system_language_id=?,')->setInt($video_lang);
        $SQL->sql('WHERE video_id=?')->setInt($video_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoArtistsLookup($letter, $lang, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(r.name) AS artist, v.system_artist_id AS artist_id, ');
        $SQL->sql('   (SELECT COUNT(*)');
        $SQL->sql('   FROM video WHERE system_artist_id = r.system_artist_id');
        $SQL->sql('   AND status="PUBLISHED" AND system_language_id = ?) AS rcount')->set($lang);
        $SQL->sql('FROM system_artists r');
        $SQL->sql('INNER JOIN video v ON v.system_artist_id = r.system_artist_id');
        $SQL->sql('WHERE ');
        $SQL->sql('  (SUBSTR(r.name, 1, 1) = ? OR ? = FALSE)')->set($letter)->setBoolean($letter);
        $SQL->sql('   AND system_language_id = ? AND v.status="PUBLISHED" and isnull(v.video_category_id)')->set($lang);
        $SQL->sql('ORDER BY rcount DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoArtistsLookupCount($letter, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT count(distinct(v.system_artist_id)) AS result');
        $SQL->sql('FROM video v ');
        $SQL->sql('LEFT JOIN system_artists s ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('LEFT JOIN system_languages l ON l.system_languages_id = v.system_language_id');
        $SQL->sql('WHERE (SUBSTR(s.name, 1, 1) = ? OR ? = FALSE)')->set($letter)->setBoolean($letter);
        $SQL->sql('AND l.system_languages_id = ? AND v.status="PUBLISHED" and isnull(v.video_category_id)')->setInt($lang);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoArtistsLookupNum($lang, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(r.name) AS artist, v.system_artist_id AS artist_id, ');
        $SQL->sql('   (SELECT COUNT(*)');
        $SQL->sql('   FROM video WHERE system_artist_id = r.system_artist_id');
        $SQL->sql('   AND status="PUBLISHED" AND system_language_id = ?) AS rcount')->set($lang);
        $SQL->sql('FROM system_artists r');
        $SQL->sql('INNER JOIN video v ON v.system_artist_id = r.system_artist_id');
        $SQL->sql('WHERE SUBSTR(r.name, 1, 1) BETWEEN "0" AND "9"');
        $SQL->sql('   AND system_language_id = ? AND v.status="PUBLISHED" and isnull(v.video_category_id)')->set($lang);
        $SQL->sql('ORDER BY rcount DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoArtistsLookupNumCount($lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT count(distinct(v.system_artist_id)) AS result');
        $SQL->sql('FROM video v ');
        $SQL->sql('LEFT JOIN system_artists s ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('LEFT JOIN system_languages l ON l.system_languages_id = v.system_language_id');
        $SQL->sql('WHERE (SUBSTR(s.name, 1, 1) BETWEEN "0" AND "9")');
        $SQL->sql('AND l.system_languages_id = ? AND v.status="PUBLISHED" and isnull(v.video_category_id)')->setInt($lang);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoSetModifiedLog($modified_by, $video_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO video_modified_dates SET');
        $SQL->sql('modified_by = ?,')->setInt($modified_by);
        $SQL->sql('video_id=?')->setInt($video_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoGetAlll() {
        $SQL = self::statement();

        $SQL->sql('SELECT video_id FROM video');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoGetAll($cat_id, $st, $kw, $art, $creator, $page, $length) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.video_id, v.title, v.video_url, v.text1, v.text2, i.firstname, i.surname,');
        $SQL->sql('   c.category_name, v.created_ts, v.status, art.name as video_artist');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = v.created_by');
        $SQL->sql('LEFT JOIN video_category c ON c.video_category_id = v.video_category_id');
        $SQL->sql('LEFT JOIN system_artists art ON art.system_artist_id=v.system_artist_id');
        $SQL->sql('WHERE v.status!=?')->set("REMOVED");
        $SQL->sql('AND (v.status=? OR FALSE=?)')->set($st)->setBoolean($st);
        $SQL->sql('AND (v.video_category_id=? OR FALSE=?)')->setInt($cat_id)->setBoolean($cat_id);
        $SQL->sql('AND (v.title LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($kw)->setBoolean($kw);
        $SQL->sql('AND (art.name LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($art)->setBoolean($art);
        $SQL->sql('AND (created_by = ? OR FALSE=?) ')->setInt($creator)->setBoolean($creator);
        $SQL->sql('ORDER BY created_ts');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoGetAllCount($cat_id, $st, $kw, $art, $creator) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = v.created_by');
        $SQL->sql('LEFT JOIN video_category c ON c.video_category_id = v.video_category_id');
        $SQL->sql('LEFT JOIN system_artists art ON art.system_artist_id=v.system_artist_id');
        $SQL->sql('WHERE v.status!=?')->set("REMOVED");
        $SQL->sql('   AND (v.status=? OR FALSE=?)')->set($st)->setBoolean($st);
        $SQL->sql('   AND (v.video_category_id=? OR FALSE=?)')->setInt($cat_id)->setBoolean($cat_id);
        $SQL->sql('   AND (v.title LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($kw)->setBoolean($kw);
        $SQL->sql('   AND (art.name LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($art)->setBoolean($art);
        $SQL->sql('   AND (created_by = ? OR FALSE=?) ')->setInt($creator)->setBoolean($creator);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoRemove($video_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE video SET');
        $SQL->sql('status=?')->set("REMOVED");
        $SQL->sql('WHERE video_id=?')->setInt($video_id);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoGet($video_id, $publish = false) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.video_id, s.name ,v.title, v.video_url, v.text1, v.text2, i.firstname, i.surname,');
        $SQL->sql('  s.name, c.video_category_id, c.category_name, v.created_ts, l.short_name, (r.fake+r.rating) as rating, v.system_language_id');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN system_artists s ON s.system_artist_id = v.system_artist_id');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = v.created_by');
        $SQL->sql('LEFT JOIN video_category c ON c.video_category_id = v.video_category_id');
        $SQL->sql('LEFT JOIN system_languages l ON l.system_languages_id = v.system_language_id');
        $SQL->sql('LEFT JOIN video_rating r ON r.video_id = v.video_id');
        $SQL->sql('WHERE v.status!=? AND v.video_id=?')->set("REMOVED")->setInt($video_id);
        $SQL->sql('AND (v.status = ? OR FALSE=?)')->set($publish)->setBoolean($publish);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoGetViewsCount($video_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT (COUNT(DISTINCT(lingua_id)) + f.count) AS result');
        $SQL->sql('FROM video_views v');
        $SQL->sql('  LEFT JOIN video_fake_views f ON f.video_id = v.video_id');
        $SQL->sql('WHERE v.video_id=?')->setInt($video_id);

        //run
        return (int) self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoSetView($video_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO video_views SET');
        $SQL->sql('lingua_id=?,')->setInt($lingua_id);
        $SQL->sql('video_id=?')->setInt($video_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE videy_views_id=videy_views_id');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoCompileViews($video_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT compile_video_rating(?) AS r')->setInt($video_id);

        //run
        return self::selectValue($SQL, 'r');
    }

    //--------------------------------------------------------------------------
    public static function videoGetStatuses() {
        $SQL = self::statement();

        $SQL->sql('SELECT status');
        $SQL->sql('FROM video');
        $SQL->sql('GROUP BY status');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoHasUrl($url) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM video');
        $SQL->sql('  WHERE video_url=?')->set($url);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoGetForUser($lingua_id, $count) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.*');
        $SQL->sql('FROM video_views w');
        $SQL->sql('LEFT JOIN video v ON v.video_id = w.video_id');
        $SQL->sql('WHERE lingua_id=? AND v.status=?')->setInt($lingua_id)->set("PUBLISHED");
        $SQL->sql('ORDER BY view_date DESC');
        $SQL->sql('LIMIT 0, ?')->setInt($count);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoGetArtists() {
        $SQL = self::statement();

        $SQL->sql('SELECT name AS video_artist');
        $SQL->sql('FROM system_artists');
        $SQL->sql('GROUP BY name');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSreachArtists($letter, $lang, $page, $page_max_length) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(`video_artist`)');
        $SQL->sql('FROM video');
        $SQL->sql('WHERE (video_artist LIKE CONCAT(? , "%") OR ? = FALSE)')->set($letter)->setBoolean($letter);
        $SQL->sql('AND video_category_id=-1 AND video_artist!=""');
        $SQL->sql('AND (video_lang = ? OR ? = FALSE)')->set($lang)->setBoolean($lang);
        $SQL->sql('GROUP BY system_artist_id');
        $SQL->sql('ORDER BY SUBSTR(video_artist, 1, 1)');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($page_max_length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSreachArtistsCount($letter, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(DISTINCT(`video_artist`))  AS result');
        $SQL->sql('FROM video');
        $SQL->sql('WHERE ( video_artist LIKE CONCAT(? , "%") OR (SUBSTR(`video_artist`,1,1) BETWEEN 1 AND 9) OR FALSE = ?) AND video_artist!=""')->set($letter)->setBoolean($letter);
        $SQL->sql('AND video_category_id=-1');
        $SQL->sql('AND (video_lang = ? OR ? = FALSE)')->set($lang)->setBoolean($lang);
        ;

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoSreachArtistsAllCount() {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(DISTINCT(`video_artist`)) AS result');
        $SQL->sql('FROM video');
        $SQL->sql('WHERE video_category_id=-1 AND video_artist!=""');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoGetTop10($lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.title, v.video_id, v.video_artist, v.created_ts ,(count(w.video_id)+f.count) as rating');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_views w ON w.video_id = v.video_id');
        $SQL->sql('LEFT JOIN video_fake_views f ON f.video_id = v.video_id');
        $SQL->sql('WHERE v.status=?')->set('PUBLISHED');
        $SQL->sql('AND v.video_category_id=-1 AND v.video_lang = ?')->set($lang);
        $SQL->sql('GROUP BY w.video_id, v.video_id');
        $SQL->sql('ORDER BY rating DESC');
        $SQL->sql('LIMIT 0, 10');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSearchByArtist($artistname, $cat_id, $lang = false, $page = 0, $page_length = 20) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.title, v.video_id, v.video_artist, v.created_ts ,(count(w.video_id)+f.count) as rating');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_views w ON w.video_id = v.video_id');
        $SQL->sql('LEFT JOIN video_fake_views f ON f.video_id = v.video_id');
        $SQL->sql('LEFT JOIN system_artists art ON art.system_artist_id=v.system_artist_id');
        $SQL->sql('WHERE v.status=?')->set('PUBLISHED');
        $SQL->sql('  AND (v.video_category_id=? OR ? = -1)')->setInt($cat_id)->setInt($cat_id);
        $SQL->sql('  AND (video_artist = ? OR  FALSE = ?)')->set($artistname)->setBoolean($artistname);
        $SQL->sql('  AND (video_lang = ? OR  FALSE = ?)')->set($lang)->setBoolean($lang);
        $SQL->sql('GROUP BY w.video_id, v.video_id');
        $SQL->sql('ORDER BY rating DESC, art.system_artist_id');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($page_length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSearchByArtistCount($artistname, $cat_id, $lang = false) {
        $SQL = self::statement();

        $SQL->sql('SELECT count(DISTINCT(v.video_id)) AS result');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_views w ON w.video_id = v.video_id');
        $SQL->sql('WHERE v.status=?')->set('PUBLISHED');
        $SQL->sql('AND (v.video_category_id=? OR ? = -1)')->setInt($cat_id)->setInt($cat_id);
        $SQL->sql('AND (video_artist = ? OR FALSE = ?)')->set($artistname)->setBoolean($artistname);
        $SQL->sql('AND (video_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);


        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoGetCreators() {
        $SQL = self::statement();

        $SQL->sql('SELECT CONCAT(i.firstname, " " , i.surname) AS creator, i.lingua_id');
        $SQL->sql('FROM user_info i');
        $SQL->sql('LEFT JOIN video v ON v.created_by = i.lingua_id');
        $SQL->sql('GROUP BY v.created_by');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSearch($q, $page, $limit, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.title, v.video_id, v.video_artist, v.created_ts ,(count(w.video_id)+f.count) as rating');
        $SQL->sql('FROM (video v, video_category c)');
        $SQL->sql('LEFT JOIN video_views w ON w.video_id = v.video_id');
        $SQL->sql('LEFT JOIN video_fake_views f ON f.video_id = v.video_id');
        $SQL->sql('WHERE (`video_artist` LIKE CONCAT("%", ? , "%") OR title LIKE CONCAT("%", ? , "%") OR')->set($q)->set($q);
        $SQL->sql('FALSE = ?)')->setBoolean($q);
        $SQL->sql('AND v.video_category_id = c.video_category_id AND v.status = ?')->set("PUBLISHED");
        $SQL->sql('AND (v.video_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);
        $SQL->sql('GROUP BY w.video_id, v.video_id');
        $SQL->sql('ORDER BY rating DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSearchCount($q, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM video');
        $SQL->sql('WHERE (video_artist LIKE CONCAT("%", ? , "%") OR title LIKE CONCAT("%", ? , "%"))')->set($q)->set($q);
        $SQL->sql('AND (video_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);
        $SQL->sql('AND status=?')->set("PUBLISHED");

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function videoGetArtistsCountByLetters($lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT SUBSTR(`video_artist`,1,1) as alpha, COUNT(*) as artists_count');
        $SQL->sql('FROM video');
        $SQL->sql('WHERE video_lang=?')->set($lang);
        $SQL->sql('GROUP BY SUBSTR(`video_artist`,1,1)');

        //run
        return self::selectMany($SQL, 'alpha', 'artists_count');
    }

    //--------------------------------------------------------------------------
    public static function videoWordLanguage($word_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.video_lang');
        $SQL->sql('FROM user_word w');
        $SQL->sql('LEFT JOIN video v ON v.video_id = w.type_id');
        $SQL->sql('WHERE w.type="video" AND user_word_id = ?')->setInt($word_id);

        //run
        return self::selectValue($SQL, 'video_lang');
    }

    //--------------------------------------------------------------------------
    public static function videoGetRandom($lang, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT v.text1, v.text2');
        $SQL->sql('FROM video v, video_views w');
        $SQL->sql('WHERE v.system_language_id = ? AND v.text2!="" AND')->set($lang);
        $SQL->sql('  w.video_id = v.video_id AND w.lingua_id = ?')->setInt($lingua_id);
        $SQL->sql('ORDER BY RAND()');
        $SQL->sql('LIMIT 10');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videoSetViewCount($video_id, $count) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO video_fake_views SET');
        $SQL->sql('video_id = ?,')->setInt($video_id);
        $SQL->sql('count = ?')->setInt($count);
        $SQL->sql('ON DUPLICATE KEY UPDATE count=?')->setInt($count);
        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function videogetByCategory($cat_id, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT (r.rating+r.fake) AS rating, v.video_id AS resource_id,');
        $SQL->sql('   v.title, v.system_artist_id, c.category_name ');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_category c');
        $SQL->sql('   ON c.video_category_id = v.video_category_id');
        $SQL->sql('LEFT JOIN video_rating r');
        $SQL->sql('   ON r.video_id = v.video_id');
        $SQL->sql('WHERE v.status="PUBLISHED"');
        $SQL->sql('   AND v.video_category_id = ?')->setInt($cat_id);
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectNested($SQL, 'resource_id');
    }

    //--------------------------------------------------------------------------
    public static function videogetByCategoryCount($cat_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(v.video_id) AS result');
        $SQL->sql('FROM video v');
        $SQL->sql('LEFT JOIN video_category c');
        $SQL->sql('   ON c.video_category_id = v.video_category_id');
        $SQL->sql('LEFT JOIN video_rating r');
        $SQL->sql('   ON r.video_id = v.video_id');
        $SQL->sql('WHERE v.status="PUBLISHED"');
        $SQL->sql('AND v.video_category_id = ?')->setInt($cat_id);

        //run
        return self::selectValue($SQL, 'result');
    }

}

