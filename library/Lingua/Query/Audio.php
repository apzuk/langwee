<?php

/**
 * Description of audio
 *
 * @author Aram
 */
class Lingua_Query_Audio extends Lingua_Site_Connection {

    public static function audioAdd($title, $category_id, $url, $artist, $text1, $text2, $creator, $status, $audio_lang) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO audio SET');
        $SQL->sql('title=?,')->set($title);
        $SQL->sql('audio_category_id=?,')->setInt($category_id);
        $SQL->sql('audio_url=?,')->set($url);
        $SQL->sql('system_artist_id=?,')->setInt($artist);
        $SQL->sql('text1=?,')->set($text1);
        $SQL->sql('text2=?,')->set($text2);
        $SQL->sql('status=?,')->set($status);
        $SQL->sql('system_language_id=?,')->setInt($audio_lang);
        $SQL->sql('created_by=?')->setInt($creator);
        $SQL->sql('ON DUPLICATE KEY UPDATE title=title');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioArtistsLookup($letter, $lang, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(r.name) AS artist, a.system_artist_id AS artist_id, ');
        $SQL->sql('  (SELECT COUNT(*) FROM audio WHERE system_artist_id = r.system_artist_id AND status="PUBLISHED") AS rcount');
        $SQL->sql('FROM system_artists r');
        $SQL->sql('INNER JOIN audio a ON a.system_artist_id = r.system_artist_id');
        $SQL->sql('WHERE ');
        $SQL->sql('(SUBSTR(r.name, 1, 1) = ? OR ? = FALSE)')->set($letter)->setBoolean($letter);
        $SQL->sql('AND system_language_id = ? AND a.status="PUBLISHED"')->setInt($lang);
        $SQL->sql('ORDER BY SUBSTR(r.name, 1, 1) ');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioArtistsLookupCount($letter, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(DISTINCT(r.name)) AS result');
        $SQL->sql('FROM system_artists r');
        $SQL->sql('INNER JOIN audio a ON a.system_artist_id = r.system_artist_id');
        $SQL->sql('WHERE ');
        $SQL->sql('(SUBSTR(r.name, 1, 1) = ? OR ? = FALSE)')->set($letter)->setBoolean($letter);
        $SQL->sql('AND system_language_id = ? AND a.status="PUBLISHED"')->setInt($lang);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioArtistsLookupNum($lang, $page, $limit) {
        $SQL = self::statement();

        $SQL->sql('SELECT DISTINCT(r.name) AS artist, a.system_artist_id AS artist_id, ');
        $SQL->sql('  (SELECT COUNT(*) FROM audio WHERE system_artist_id = r.system_artist_id AND status="PUBLISHED") AS rcount');
        $SQL->sql('FROM system_artists r');
        $SQL->sql('INNER JOIN audio a ON a.system_artist_id = r.system_artist_id');
        $SQL->sql('WHERE ');
        $SQL->sql('SUBSTR(r.name, 1, 1) BETWEEN "0" AND "9"');
        $SQL->sql('AND system_language_id = ? AND a.status="PUBLISHED"')->setInt($lang);
        $SQL->sql('ORDER BY SUBSTR(r.name, 1, 1)');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioArtistsLookupNumCount($lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(DISTINCT(r.name)) AS result');
        $SQL->sql('FROM system_artists r');
        $SQL->sql('INNER JOIN audio a ON a.system_artist_id = r.system_artist_id');
        $SQL->sql('WHERE SUBSTR(r.name, 1, 1) BETWEEN "0" AND "9"');
        $SQL->sql('AND system_language_id = ? AND a.status="PUBLISHED"')->setInt($lang);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioEdit($title, $category_id, $url, $artist, $text1, $text2, $creator, $status, $audio_id, $audio_lang) {
        $SQL = self::statement();

        $SQL->sql('UPDATE audio SET');
        $SQL->sql('title=?,')->set($title);
        $SQL->sql('audio_category_id=?,')->setInt($category_id);
        $SQL->sql('audio_url=?,')->set($url);
        $SQL->sql('system_artist_id=?,')->setInt($artist);
        $SQL->sql('text1=?,')->set($text1);
        $SQL->sql('text2=?,')->set($text2);
        $SQL->sql('status=?,')->set($status);
        $SQL->sql('system_language_id=?,')->setInt($audio_lang);
        $SQL->sql('created_by=?')->setInt($creator);
        $SQL->sql('WHERE audio_id=?')->setInt($audio_id);

        //run
        return self::queryUpdate($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSetModifiedLog($modified_by, $audio_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO audio_modified_dates SET');
        $SQL->sql('modified_by = ?,')->setInt($modified_by);
        $SQL->sql('audio_id=?')->setInt($audio_id);

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioGetAll_() {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM audio a');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioGetAll($cat_id, $st, $kw, $art, $creator, $offset, $length) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.audio_id, a.title, a.audio_url, a.text1, a.text2, i.firstname, i.surname,');
        $SQL->sql('   c.category_name, a.created_ts, a.status');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = a.created_by');
        $SQL->sql('LEFT JOIN audio_category c ON c.audio_category_id = a.audio_category_id');
        $SQL->sql('LEFT JOIN system_artists art ON art.system_artist_id = a.system_artist_id');
        $SQL->sql('WHERE a.status!=?')->set("REMOVED");
        $SQL->sql('AND (a.status=? OR FALSE=?)')->set($st)->setBoolean($st);
        $SQL->sql('AND (a.audio_category_id=? OR FALSE=?)')->setInt($cat_id)->setBoolean($cat_id);
        $SQL->sql('AND (a.title LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($kw)->setBoolean($kw);
        $SQL->sql('AND (art.name LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($art)->setBoolean($art);
        $SQL->sql('AND (created_by = ? OR FALSE=?) ')->setInt($creator)->setBoolean($creator);
        $SQL->sql('ORDER BY created_ts DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($offset)->setInt($length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioGetAllCount($cat_id, $st, $kw, $art, $creator) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = a.created_by');
        $SQL->sql('LEFT JOIN audio_category c ON c.audio_category_id = a.audio_category_id');
        $SQL->sql('LEFT JOIN system_artists art ON art.system_artist_id = a.system_artist_id');
        $SQL->sql('WHERE a.status!=?')->set("REMOVED");
        $SQL->sql('AND (a.status=? OR FALSE=?)')->set($st)->setBoolean($st);
        $SQL->sql('AND (a.audio_category_id=? OR FALSE=?)')->setInt($cat_id)->setBoolean($cat_id);
        $SQL->sql('AND (a.title LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($kw)->setBoolean($kw);
        $SQL->sql('AND (art.name LIKE CONCAT("%" , ? , "%") OR FALSE=?)')->set($art)->setBoolean($art);
        $SQL->sql('AND (created_by = ? OR FALSE=?) ')->setInt($creator)->setBoolean($creator);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioRemove($audio_id) {
        $SQL = self::statement();

        $SQL->sql('UPDATE audio SET');
        $SQL->sql('status=?')->set("REMOVED");
        $SQL->sql('WHERE audio_id=?')->setInt($audio_id);

        //run
        return self::queryDelete($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioGet($audio_id, $publish = false) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.audio_id, s.name ,a.title, a.audio_url, a.text1, a.text2, i.firstname, i.surname,');
        $SQL->sql('  s.name, c.audio_category_id, c.category_name, a.created_ts, l.short_name, (r.fake+r.rating) as rating, a.system_language_id');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN system_artists s ON s.system_artist_id = a.system_artist_id');
        $SQL->sql('LEFT JOIN user_info i ON i.lingua_id = a.created_by');
        $SQL->sql('LEFT JOIN audio_category c ON c.audio_category_id = a.audio_category_id');
        $SQL->sql('LEFT JOIN system_languages l ON l.system_languages_id = a.system_language_id');
        $SQL->sql('LEFT JOIN audio_rating r ON r.audio_id = a.audio_id');
        $SQL->sql('WHERE a.status!=? AND a.audio_id=?')->set("REMOVED")->setInt($audio_id);
        $SQL->sql('AND (a.status = ? OR FALSE=?)')->set($publish)->setBoolean($publish);

        //run
        return self::selectRow($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSetView($audio_id, $lingua_id) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO audio_views SET');
        $SQL->sql('lingua_id=?,')->setInt($lingua_id);
        $SQL->sql('audio_id=?')->setInt($audio_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE audio_view_id=audio_view_id');

        //run
        return self::queryInsert($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioCompileViews($audio_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT compile_audio_rating(?) AS r')->setInt($audio_id);

        //run
        return self::selectValue($SQL, 'r');
    }

    //--------------------------------------------------------------------------
    public static function audioGetStatuses() {
        $SQL = self::statement();

        $SQL->sql('SELECT status');
        $SQL->sql('FROM audio');
        $SQL->sql('GROUP BY status');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioHasUrl($url) {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('  SELECT *');
        $SQL->sql('  FROM audio');
        $SQL->sql('  WHERE audio_url=?')->set($url);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioGetForUser($lingua_id, $count) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.*');
        $SQL->sql('FROM audio_views w');
        $SQL->sql('LEFT JOIN audio a ON a.audio_id = w.audio_id');
        $SQL->sql('WHERE lingua_id=?')->setInt($lingua_id);
        $SQL->sql('ORDER BY view_date DESC');
        $SQL->sql('LIMIT 0, ?')->setInt($count);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioGetArtists() {
        $SQL = self::statement();

        $SQL->sql('SELECT name AS audio_artist');
        $SQL->sql('FROM system_artists');
        $SQL->sql('GROUP BY name');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSreachArtists($letter, $lang, $page, $page_max_length) {
        $SQL = self::statement();

        $SQL->sql('SELECT audio_artist');
        $SQL->sql('FROM audio');
        $SQL->sql('WHERE ( audio_artist LIKE CONCAT(? , "%") OR FALSE = ?)')->set($letter)->setBoolean($letter);
        $SQL->sql('AND (audio_lang = ? OR ? = FALSE)')->set($lang)->setBoolean($lang);
        $SQL->sql('AND audio_category_id=-1');
        $SQL->sql('GROUP BY audio_artist');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($page_max_length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSreachArtistsCount($letter, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(DISTINCT(`audio_artist`)) AS result');
        $SQL->sql('FROM audio');
        $SQL->sql('WHERE ( audio_artist LIKE CONCAT(? , "%") OR FALSE = ?)')->set($letter)->setBoolean($letter);
        $SQL->sql('AND (audio_lang = ? OR ? = FALSE)')->set($lang)->setBoolean($lang);
        $SQL->sql('AND audio_category_id=-1');

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioSearchByArtist($artistname, $cat_id, $lang, $page = 0, $page_length = 20) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.title, a.audio_id, a.audio_artist, a.created_ts ,(count(w.audio_id)+f.count) as rating');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN audio_views w ON w.audio_id = a.audio_id');
        $SQL->sql('LEFT JOIN audio_fake_views f ON f.audio_id = a.audio_id');
        $SQL->sql('WHERE a.status=?')->set('PUBLISHED');
        $SQL->sql('AND (a.audio_category_id=? OR ?=-1)')->setint($cat_id)->setint($cat_id);
        $SQL->sql('AND (audio_artist = ? OR FALSE = ?)')->set($artistname)->setBoolean($artistname);
        $SQL->sql('AND (audio_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);
        $SQL->sql('GROUP BY w.audio_id, a.audio_id');
        $SQL->sql('ORDER BY rating DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($page_length);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSearchByArtistCount($artistname, $cat_id, $lang = false) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(DISTINCT(a.audio_id)) AS result');
        $SQL->sql('FROM audio a');
        $SQL->sql('LEFT JOIN audio_views w ON w.audio_id = a.audio_id');
        $SQL->sql('WHERE a.status=?')->set('PUBLISHED');
        $SQL->sql('AND (a.audio_category_id=? OR ?=-1)')->setint($cat_id)->setint($cat_id);
        $SQL->sql('AND (audio_artist = ? OR FALSE = ?)')->set($artistname)->setBoolean($artistname);
        $SQL->sql('AND (audio_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioSearch($q, $page, $limit, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT a.title, a.audio_id, a.audio_artist, a.created_ts ,(count(w.audio_id)+f.count) as rating');
        $SQL->sql('FROM (audio a, audio_category c)');
        $SQL->sql('LEFT JOIN audio_views w ON w.audio_id = a.audio_id');
        $SQL->sql('LEFT JOIN audio_fake_views f ON f.audio_id = a.audio_id');
        $SQL->sql('WHERE (audio_artist LIKE CONCAT("%", ? , "%") OR title LIKE CONCAT("%", ? , "%"))')->set($q)->set($q);
        $SQL->sql('AND a.audio_category_id = c.audio_category_id AND a.status = ?')->set("PUBLISHED");
        $SQL->sql('AND (a.audio_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);
        $SQL->sql('GROUP BY w.audio_id, a.audio_id');
        $SQL->sql('ORDER BY rating DESC');
        $SQL->sql('LIMIT ?, ?')->setInt($page)->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSearchCount($q, $lang) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM `audio`');
        $SQL->sql('WHERE (`audio_artist` LIKE CONCAT("%", ? , "%") OR title LIKE CONCAT("%", ? , "%"))')->set($q)->set($q);
        $SQL->sql('AND (audio_lang = ? OR FALSE = ?)')->set($lang)->setBoolean($lang);
        $SQL->sql('AND status=?')->set("PUBLISHED");

        //run
        return self::selectValue($SQL, 'result');
    }

    //--------------------------------------------------------------------------
    public static function audioGetAlll() {
        $SQL = self::statement();

        $SQL->sql('SELECT audio_id FROM audio');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function audioSetViewCount($audio_id, $count) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO audio_fake_views SET');
        $SQL->sql('audio_id = ?,')->setInt($audio_id);
        $SQL->sql('count = ?')->setInt($count);

        //run
        return self::queryInsert($SQL);
    }

}