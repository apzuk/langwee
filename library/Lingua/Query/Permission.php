<?php

/**
 * Description of Permission
 *
 * @author Aram
 */
class Lingua_Query_Permission extends Lingua_Site_Connection {

    //--------------------------------------------------------------------------
    public static function permissionGetAll($role_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT p.permission_name, p.permission_desc');
        $SQL->sql('FROM acl_role r');
        $SQL->sql('LEFT JOIN `acl_role_permissions` rp ON rp.`acl_role_id` = r.`acl_role_id`');
        $SQL->sql('LEFT JOIN `acl_permissions` p ON p.`acl_permissions_id` = rp.`acl_permissions_id`');
        $SQL->sql('WHERE r.`acl_role_id` = ?')->setInt($role_id);

        //run
        return self::selectMany($SQL, 'permission_name', 'permission_desc');
    }
    
    //--------------------------------------------------------------------------
    public static function permissionGetUser($lingua_id) {
        $SQL = self::statement();

        $SQL->sql('SELECT p.permission_name, p.permission_desc');
        $SQL->sql('FROM (acl_role r, user_account u)');
        $SQL->sql('LEFT JOIN `acl_role_permissions` rp ON rp.`acl_role_id` = r.`acl_role_id`');
        $SQL->sql('LEFT JOIN `acl_permissions` p ON p.`acl_permissions_id` = rp.`acl_permissions_id`');
        $SQL->sql('WHERE u.lingua_id = ? AND u.role_id = r.acl_role_id')->setInt($lingua_id);
        
        //run
        return self::selectMany($SQL, 'permission_name', 'permission_desc');
    }

    //--------------------------------------------------------------------------
    public static function roleGetId($role_name) {
        $SQL = self::statement();

        $SQL->sql('SELECT acl_role_id');
        $SQL->sql('FROM acl_role');
        $SQL->sql('WHERE role_name=?')->set($role_name);

        //run
        return self::selectValue($SQL, 'acl_role_id');
    }

    //--------------------------------------------------------------------------
    public static function roleGetAll() {
        $SQL = self::statement();

        $SQL->sql('SELECT acl_role_id, role_name');
        $SQL->sql('FROM acl_role');

        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function roleGetPermissions() {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM acl_role r');
        $SQL->sql('LEFT JOIN acl_role_permissions p ON p.acl_role_id = r.acl_role_id');
        $SQL->sql('LEFT JOIN acl_permissions ap ON ap.acl_permissions_id = p.acl_permissions_id');
        
        //run
        return self::selectMany($SQL);
    }

    //--------------------------------------------------------------------------
    public static function permissionAdd($permission, $description) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO acl_permissions SET');
        $SQL->sql('permission_name=?,')->set($permission);
        $SQL->sql('permission_desc=?')->set($description);

        //run
        return self::queryInsert($SQL);
    }
    
    //--------------------------------------------------------------------------
    public static function permissiGetAll() {
        $SQL = self::statement();
        
        $SQL -> sql('SELECT *');
        $SQL -> sql('FROM acl_permissions');
        
        //run
        return self::selectMany($SQL);
    }
    
    //--------------------------------------------------------------------------
    public static function permissionRevole($role_id, $perm_id) {
        $SQL = self::statement();
        
        $SQL -> sql('DELETE FROM acl_role_permissions');
        $SQL -> sql('WHERE acl_role_id=? AND acl_permissions_id=?')->setInt($role_id)->setInt($perm_id);
        
        //run
        return self::queryDelete($SQL);
    }
    
    //--------------------------------------------------------------------------
    public static function permissionGrant($role_id, $perm_id) {
        $SQL = self::statement();
        
        $SQL -> sql('INSERT INTO acl_role_permissions SET');
        $SQL -> sql('acl_role_id=?,')->setInt($role_id);
        $SQL -> sql('acl_permissions_id=?')->setInt($perm_id);
        $SQL -> sql('ON DUPLICATE KEY UPDATE');
        $SQL -> sql('acl_role_id=?,')->setInt($role_id);
        $SQL -> sql('acl_permissions_id=?')->setInt($perm_id);
        
        //run
        return self::queryInsert($SQL);
    }

}