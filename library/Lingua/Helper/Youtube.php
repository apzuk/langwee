<?php

/**
 * Description of Youtube
 *
 * @author Aram
 */
class Lingua_Helper_Youtube {

    public static function getYoutubeEmbedLink($link) {
        if (preg_match('/\/embed\//', $link)) {
            return $link;
        }
        preg_match("/v=([^&]+)/i", $link, $matches);

        if (isset($matches[1])) {
            $new_url = "http://youtube.com/embed/" . $matches[1];
        }
        else $new_url = $link;
        return $new_url;
    }

}

?>
