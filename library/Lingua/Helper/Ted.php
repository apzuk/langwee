<?php

/**
 * Description of Ted
 *
 * @author Administrator
 */
class Lingua_Helper_Ted {

    //--------------------------------------------------------------------------
    public static function tedGetUrl($url) {
        //example http://www.ted.com/talks/marco_tempest_a_magical_tale_with_augmented_reality.html
        preg_match_all('/\/(\w+)\.html/', $url, $matches);
        
        if(Lingua_Auth::isAdmin()) {
            echo $matches[1][0];
        }
        
        return isset($matches[1][0]) ? $matches[1][0] : "";
    }

}

?>
