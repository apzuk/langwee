<?php

/**
 * Description of Google
 *
 * @author user
 */
class Lingua_Helper_Google {
    // this is the API endpoint, as specified by Google

    const ENDPOINT = 'https://www.googleapis.com/language/translate/v2';

    // holder for you API key, specified when an instance is created
    protected $_apiKey;

    // constructor, accepts Google API key as its only argument
    public function __construct($apiKey = "AIzaSyBYU0NMsIYxzfw-zcXTfMpVOViqE2wSz4s") {
        $this->_apiKey = $apiKey;
    }

    //AIzaSyAfPj1_JZ9hUgdRuKSKc3Yi0nAlbG7mXhY
    // translate the text/html in $data. Translates to the language
    // in $target. Can optionally specify the source language
    public function translate($data, $target, $source = '') {
        //convert source language id to short name
        $source = Lingua_Auth::languageGetShortName($source);

        //convert target language id to short name
        $target = Lingua_Auth::languageGetShortName($target);

        // only include the source data if it's been specified
        if (strlen($source) > 0) {
            $values['source'] = $source;
        }

        // this is the form data to be included with the request
        $values = array(
            'key' => $this->_apiKey,
            'target' => $target,
            'source' => $source,
            'q' => $data,
            'multires' => 1,
            'otf' => 1,
            'client' => 't',
            'multires' => 10,
            'pc' => 1,
            'sc' => 1
        );

        // turn the form data array into raw format so it can be used with cURL
        $formData = http_build_query($values);
        // create a connection to the API endpoint
        $ch = curl_init(self::ENDPOINT);

        // tell cURL to return the response rather than outputting it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // write the form data to the request in the post body
        curl_setopt($ch, CURLOPT_POSTFIELDS, $formData);

        // include the header to make Google treat this post request as a get request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));

        // execute the HTTP request 
        $json = curl_exec($ch);
        curl_close($ch);

        // decode the response data
        $data = json_decode($json, true);
        // ensure the returned data is valid

        if (!is_array($data) || !array_key_exists('data', $data)) {
            throw new Exception('Unable to find data key');
        }

        // ensure the returned data is valid
        if (!array_key_exists('translations', $data['data'])) {
            throw new Exception('Unable to find translations key');
        }

        if (!is_array($data['data']['translations'])) {
            throw new Exception('Expected array for translations');
        }

        // loop over the translations and return the first one.
        // if you wanted to handle multiple translations in a single call
        // you would need to modify how this returns data
        foreach ($data['data']['translations'] as $translation) {
            return $translation['translatedText'];
        }

        // assume failure since success would've returned just above
        throw new Exception('Translation failed');
    }

    //--------------------------------------------------------------------------
    public function createTmpFile($word_id, $lang_src) {
        $word = Lingua_Query_User::userGetWordById($word_id);
        $url = "http://translate.google.com/translate_tts?ie=UTF-8&tl=" . $lang_src . "&q=" . $word . "&total=1&idx=0&textlen=5&prev=input";
        $url = ($url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

        $content = curl_exec($ch);
        curl_close($ch);

        $speech_dir = APPLICATION_PATH . "/../public_html/data/speech/";
        //clearing user older mp3-s
        if (($handle = opendir($speech_dir))) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($entry = readdir($handle))) {
                if (strpos($entry, Lingua_Auth::getLinguaId()) !== FALSE) {
                    unlink($speech_dir . $entry);
                }
            }

            closedir($handle);
        }
        $file = fopen($speech_dir . Lingua_Auth::getLinguaId() . "_" . $word_id . ".mp3", "wt") or die("err");
        fputs($file, $content);
        fclose($file);
    }

    //--------------------------------------------------------------------------
    public function outputSound($word_id) {
        $word = Lingua_Query_User::userGetWordById($word_id);
	
	//source word
	$word_src = $word['word_src'];        

        //convert source language id to short name
        $source = Lingua_Auth::languageGetShortName( $word['word_src_lang']);
        
        //speech params
        $params = "?ie=UTF-8&tl=" . $source . "&q=" . $word_src . "&total=1&idx=0&textlen=5&prev=input";
        
        //mp3 resounse
        $content = file_get_contents('http://apdev.info/1.php?param=' . urlencode($params));
       
	//header audio
        header('Content-Type: audio/mpeg');
        echo $content;
    }

}

