<?php

/**
 * Description of Dictionary
 *
 * @author Aram
 */
class Lingua_Helper_Dictionary {

    private $DATA;
    private $today;
    private $yesterday;
    private $prevtwoday;
    private $week;
    private $prevweek;
    private $prevtwoweek;
    private $prevmonth;
    private $WORDS = array();

    //--------------------------------------------------------------------------
    public function __construct($data) {
        $this->DATA = $data;
        $new_data = array();
        foreach ($this->DATA as $data) {
            $new_data[$data['register_date']][$data['user_word_id']] = $data;
        }

        $this->DATA = $new_data;
        $this->init();
    }

    //--------------------------------------------------------------------------
    public function getAll() {
        return $this->WORDS;
    }

    //--------------------------------------------------------------------------
    public function init() {

        //today date
        $this->today = date('Y-m-d');

        //yesterday date
        $this->yesterday = date('Y-m-d', strtotime($this->today) - 24 * 60 * 60);

        //two days ago date
        $this->prevtwoday = date('Y-m-d', strtotime($this->today) - 2 * 24 * 60 * 60);

        //this week dates
        $last_monday = date('Y-m-d', strtotime('last monday'));
        $week_last_day = date('Y-m-d', strtotime($last_monday) + 7 * 24 * 60 * 60);

        $this->week = array($last_monday, $week_last_day);

        //last week dates
        $last_monday = date('Y-m-d', strtotime('last monday') - 7 * 24 * 60 * 60);
        $week_last_day = date('Y-m-d', strtotime($last_monday) + 6 * 24 * 60 * 60);

        $this->prevweek = array($last_monday, $week_last_day);
        
        //last two week dates
        $last_monday = date('Y-m-d', strtotime('last monday') - 2 * 7 * 24 * 60 * 60);
        $week_last_day = date('Y-m-d', strtotime($last_monday) + 6 * 24 * 60 * 60);

        $this->prevtwoweek = array($last_monday, $week_last_day);
        //prev month dates
        $last_month_start = date('Y-m-d', strtotime('first day of last month'));
        $last_month_end = date('Y-m-d', strtotime('last day of last month'));
        $this->prevmonth = array($last_month_start, $last_month_end);

        $this->WORDS['today'] = isset($this->DATA[$this->today]) ? $this->DATA[$this->today] : array();
        $this->WORDS['yesterday'] = isset($this->DATA[$this->yesterday]) ? $this->DATA[$this->yesterday] : array();
        $this->WORDS['prev2days'] = isset($this->DATA[$this->prevtwoday]) ? $this->DATA[$this->prevtwoday] : array();
        
       
        //----------------------------------------------------------------------
        $this->WORDS['prevweek'] = array();
        for ($i = $this->prevweek[0]; strtotime($i) <= strtotime($this->prevweek[1]);) {
            if (isset($this->DATA[$i])) {
                $new_data = array();

                foreach ($this->DATA[$i] as $key => $value) {
                    $new_data[$key] = $value;
                }

                $this->WORDS['prevweek'] = $new_data;
            }
            $i = date('Y-m-d', strtotime($i) + 24 * 60 * 60);
        }

        //----------------------------------------------------------------------
        $this->WORDS['prev2weeks'] = array();
        for ($i = $this->prevtwoweek[0]; $i <= $this->prevtwoweek[1];) {
            if (isset($this->DATA[$i])) {
                $new_data = array();

                foreach ($this->DATA[$i] as $key => $value) {
                    $new_data[$key] = $value;
                }

                $this->WORDS['prev2weeks'] = $new_data;
            }
            $i = date('Y-m-d', strtotime($i) + 24 * 60 * 60);
        }

        //----------------------------------------------------------------------
        $this->WORDS['prevmonth'] = array();
        for ($i = $this->prevmonth[0]; $i <= $this->prevmonth[1];) {
            if (isset($this->DATA[$i])) {
                $new_data = array();

                foreach ($this->DATA[$i] as $key => $value) {
                    $new_data[$key] = $value;
                }

                $this->WORDS['prevmonth'] = $new_data;
            }
            $i = date('Y-m-d', strtotime($i) + 24 * 60 * 60);
        }
    }

}

?>
