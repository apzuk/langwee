<?php

/**
 * Description of Trainings
 *
 * @author Aram
 */
class Lingua_Helper_User {

    //--------------------------------------------------------------------------
    public static function userGetProfability($user) {
        $total = 0;

        if ($user->firstname || $user->surname) {
            $total += 20;
        }

        if ($user->nickname) {
            $total += 10;
        }

        if ($user->age) {
            $total += 10;
        }

        if ($user->country) {
            $total += 5;
        }

        if ((boolean) count($user->mother_lang)) {
            $total += 10;
        }

        if ((boolean) count($user->learning)) {
            $total += 5;
        }

        if ((boolean) count($user->know)) {
            $total += 5;
        }

        if ((boolean) count($user->gender)) {
            $total += 10;
        }

        if ($user->photo) {
            $real_path = Lingua_Storage_User::getUserPhoto($user->photo, 95);
            if (file_exists(PUBLIC_PATH . $real_path)) {
                $total += 25;
            }
        }
        
        return $total;
    }

}

?>
