<?php

/**
 * Description of Trainings
 *
 * @author Aram
 */
class Lingua_Helper_Trainings {

    protected $SESS = null;
    protected $training_name = null;
    protected $lang = null;
    protected $id = null; /* current training id */
    protected $available_count;
    protected $tries = 1;

    //--------------------------------------------------------------------------
    public function getRandomUserWords() {
        $this->setTries();
        return Lingua_Query_User_Words::userWordWithRandomWords(Lingua_Auth::getLinguaId(), $this->lang, $this->id);
    }

    //--------------------------------------------------------------------------
    public function setTries() {
        if (!$this->SESS->tries)
            $this->SESS->tries = 0;
        $this->SESS->tries++;
    }
         
    //--------------------------------------------------------------------------
    public function getRandomUserWord() {
        $this->setTries();
        return Lingua_Query_User_Words::userWordWithRandomWord(Lingua_Auth::getLinguaId(), $this->lang, $this->id);
    }

    //--------------------------------------------------------------------------
    public function getUserAvailableWordCount() {
        return $this->available_count = Lingua_Query_User_Words::userWordAvailableCount(Lingua_Auth::getLinguaId(), $this->lang, $this->id);
    }

    //--------------------------------------------------------------------------
    public function getRandomVideosText($lang) {
        $this->setTries();
        return Lingua_Query_Video::videoGetRandom($lang, Lingua_Auth::getLinguaId());
    }

    //--------------------------------------------------------------------------
    public function clean() {
        $this->SESS->unsetAll();
        $this->SESS->try = 0;
    }

    //--------------------------------------------------------------------------
    public function hasEnoughtWords() {
        return ($this->SESS->user_word[$this->lang] < 10 ? false : true);
    }

    //--------------------------------------------------------------------------
    public function triesCount() {
        return $this->SESS->tries;
    }

    //--------------------------------------------------------------------------
    public function setMyId() {
        $this->id = Lingua_Query_System_Trainings::getTrainingId($this->training_name);
    }

    //--------------------------------------------------------------------------
    public function store($result, $status = "AGAIN") {
        foreach ($result as $r) {
            Lingua_Query_User_Words::wordAdd($r[0], Lingua_Auth::getLinguaId(), $this->id, $status);
        }
    }

}

?>
