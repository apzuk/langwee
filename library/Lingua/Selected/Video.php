<?php

/**
 * Description of Video
 *
 * @author Aram
 */
class Lingua_Selected_Video {

    const REJECT_URL = '/invalid/video/video_id/[video_id]';

    public $VIDEO;
    public $isValid;

    public function __construct($video_id) {

        $this->isValid = Lingua_Query_Video::videoGet($video_id, "PUBLISHED");
        
        if ($this->isValid) {
            $this->VIDEO = new Lingua_Object($this->isValid);
        }
        
    }

    //--------------------------------------------------------------------------
    public function rejectUnlessValid() {
        if (!$this->isValid) {
            header("Location: " . self::REJECT_URL);
            exit;
        }
    }
    
    //--------------------------------------------------------------------------
    public function getRandomLine() {
        
        $englishText = preg_split('/(<br\s+\/>)/', nl2br($this->VIDEO->text1), -1, PREG_SPLIT_NO_EMPTY);
        $russianText = preg_split('/(<br\s+\/>)/', nl2br($this->VIDEO->text2), -1, PREG_SPLIT_NO_EMPTY);
        
        foreach($englishText as $k=>$v) {
            $trimed = $englishText[$k];
                
            if(!empty($trimed)) {
                $englishText[] = $trimed;
            }
        }
        
        foreach($russianText as $k=>$v) {
            $trimed = trim($russianText[$index]);
            
            if(!empty($trimed)) {
                $russianText[] = $trimed;
            }
        }
    }

}

?>
