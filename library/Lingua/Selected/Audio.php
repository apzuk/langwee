<?php
/**
 * Description of User
 *
 * @author Aram
 */
class Lingua_Selected_Audio {
    const REJECT_URL = '/invalid/audio/audio_id/[audio_id]';

    public $AUDIO;
    public $isValid;

    public function __construct($audio_id) {
        $this->isValid = Lingua_Query_Audio::audioGet($audio_id, "PUBLISHED");
        if ($this->isValid) {
            $this->AUDIO = new Lingua_Object($this->isValid);
        }
        
    }

    //--------------------------------------------------------------------------
    public function rejectUnlessValid() {
        if (!$this->isValid) {
            header("Location: " . self::REJECT_URL);
            exit;
        }
    }
    
    //--------------------------------------------------------------------------
    public function getRandomLine() {
        
        $englishText = preg_split('/(<br\s+\/>)/', nl2br($this->AUDIO->text1), -1, PREG_SPLIT_NO_EMPTY);
        $russianText = preg_split('/(<br\s+\/>)/', nl2br($this->AUDIO->text2), -1, PREG_SPLIT_NO_EMPTY);
        
        foreach($englishText as $k=>$v) {
            $trimed = $englishText[$k];
                
            if(!empty($trimed)) {
                $englishText[] = $trimed;
            }
        }
        
        foreach($russianText as $k=>$v) {
            $trimed = trim($russianText[$index]);
            
            if(!empty($trimed)) {
                $russianText[] = $trimed;
            }
        }
    }

}

?>
