<?php

/**
 * Description of Chat
 *
 * @author Aram
 */
class Lingua_Selected_Chat {

    const REJECT_URL = '/invalid/chat/chat/[chat_id]';

    public $CHAT = null;
    private $isValid = null;
    public $selected = null;
    public $interlocutor_id = null;
    public $interlocutor_langs = null;
    public $me_id;
    private $smiles = null;

    public function __construct($chat_id) {
        $this->CHAT = new Lingua_Object(Lingua_Query_Chat::chatGetById($chat_id, Lingua_Auth::getLinguaId()));
        
        $this->interlocutor_id = $this->CHAT->creator_lingua_id == Lingua_Auth::getLinguaId() ? $this->CHAT->receiver_lingua_id : $this->CHAT->creator_lingua_id;
        $this->me_id = $this->CHAT->creator_lingua_id != Lingua_Auth::getLinguaId() ? $this->CHAT->receiver_lingua_id : $this->CHAT->creator_lingua_id;

        $this->selected = $chat_id;
        $this->isValid = (Boolean) $this->CHAT->community_chat_room_id;

        $this->smiles = array(
            "rockn_roll" => "rockn_roll.png",
            "smile" => "smile.png",
            "bad_smile" => "bad_smile.png",
            "beyond_endurance" => "beyond_endurance.png",
            "big_smile" => "big_smile.png",
            "cry" => "cry.png",
            "embarrassed" => "embarrassed.png",
            "greeding" => "greeding.png",
            "i_have_no_idea" => "i_have_no_idea.png",
            "just_out" => "just_out.png",
            "misdoubt" => "misdoubt.png",
            "pretty_smile" => "pretty_smile.png",
            "shame" => "shame.png",
            "sigh" => "sigh.png",
            "surprise" => "surprise.png",
            "the_devil" => "the_devil.png",
            "unbelievable" => "unbelievable.png",
            "unhappy" => "unhappy.png",
            "what" => "what.png",
            "wicked" => "wicked.png"
        );
    }

    //--------------------------------------------------------------------------
    public function isValid() {
        return $this->isValid;
    }

    //--------------------------------------------------------------------------
    public function getSmiles() {
        return $this->smiles;
    }

    //--------------------------------------------------------------------------
    public function rejectUnlessValid() {
        if (!$this->isValid) {
            header("Location: " . Daz_String::merge(self::REJECT_URL, array('chat_id' => $this->selected)));
            exit;
        }
    }

    //--------------------------------------------------------------------------
    public function getMe() {
        return new Lingua_Object(Lingua_Query_User::userGetFull($this->me_id));
    }

    //--------------------------------------------------------------------------
    public function getInterlocutor() {
        return Lingua_Query_Chat::userGetFull($this->interlocutor_id, $this->selected);
    }

    //--------------------------------------------------------------------------
    public function getInterlocutorLanguages() {
        $this->interlocutor_langs = Lingua_Query_User::userGetLanguages($this->interlocutor_id);
    }

    //--------------------------------------------------------------------------
    public function getLang($key) {
        $language = isset($this->interlocutor_langs[$key]) ? $this->interlocutor_langs[$key] : array();

        return $language;
    }

    //--------------------------------------------------------------------------
    public function getLastMessages($mgs_id, $length = 8) {
        $messages = Lingua_Query_Chat::chatGetMessages($this->selected, $mgs_id, $length);
        
        $returnData = array();
        foreach($messages as $key => $message) {
            $returnData[$key] = $message;
            $returnData[$key]['message'] = preg_replace_callback('/::(\w+)::/', array(&$this, 'process'), $message['message']);  
        }
        
        return $returnData;
    }
    
    //--------------------------------------------------------------------------
    public function process($param) {
        $p = isset($param[1]) ? $param[1] : 0;
        $smile = isset($this->smiles[$p]) ? $this->smiles[$p] : $p;
        $image_url = "<img src='/images/community/smiles/" . $smile ."' />";
        return $image_url;
    }

}

?>
