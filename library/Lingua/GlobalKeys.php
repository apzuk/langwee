<?php

/**
 * Description of GlobalKeys
 *
 * @author user
 */
class Lingua_GlobalKeys {

    private static $SESS = null;

    public static function init() {
        self::$SESS = new Daz_Session(__CLASS__);
    }

    //--------------------------------------------------------------------------
    public static function storeKey($key, $value) {
        
        if (!self::$SESS) {
            self::init();
        }

        self::$SESS->$key = $value;
    }

    //--------------------------------------------------------------------------
    public static function getKey($key) {
        if (!self::$SESS) {
            self::init();
        }

        return self::$SESS->$key;
    }

    //--------------------------------------------------------------------------
    public static function generateKey($length = 30, $strength = 0) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

}

?>
