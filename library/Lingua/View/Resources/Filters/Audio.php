<?php

/**
 * Description of Audio
 *
 * @author aramp
 */
class Lingua_View_Resources_Filters_Audio {

    public static function audioArtistsRender($artists) {
        //if there is not artist return
        if (!count($artists))
            return;

        //take first element
        $artist = array_shift($artists);
        $val = $artist['artist'] . '(' . $artist['rcount'] . ')';
        $tag = new Daz_Tag('li');
        $tag->push('a', 'href', '#', 'click', 'return false', 'src', $artist['artist_id'], 'class', 'artists', $val);
        $tag->pop();

        //render li 
        foreach ($artists as $artist) {
            $val = $artist['artist'] . '(' . $artist['rcount'] . ')';

            $tag->push('li');
            $tag->append('a', 'href', '#', 'click', 'return false', 'src', $artist['artist_id'], 'class', 'artists', $val);
            $tag->pop();
        }

        //response
        return $tag;
    }

    //---------------------------------------------------------------------
    public static function audioListRender($data, $title = false, $bottom = false, $side = 'left', $page = 0) {
        $side = Lingua_Util::pickOne($side, 'left', 'right');

        if (!$side)
            exit;

        $tag = new Daz_Tag('div', 'class', 'conten-middle-center-' . $side . '-title'); //title

        if ($title) {
            $tag->append('img', 'src', '/images/resources/audio-big-icon.png', 'alt', 'английский');
            $tag->append('h2', $title);
        }
        $tag->pop(); //close title;


        $tag->push('div', 'class', 'conten-middle-center-left-cont');  //left content
        $tag->push('ul'); //reources list 

        if (count($data) == 0 && $title) {
            $tag->append('li', 'class', 'empty', 'Аудиозаписей не найдено.');
        }

        $index = 1;
        foreach ($data as $resource_id => $resource) {
            $obj = new Lingua_Object($resource);

            $tag->push('li', 'class', $index % 2 == 0 ? 'bg' : 'nobg');
            $tag->push('div');

            $tag->append('b', 'class', ($index == 1) ? 'col' : 'nocol', ($index + $page) . ( ( ($index + $page) != 10) ? '.' : '' ));
            $tag->append('img', 'src', '/images/resources/audio-icon.png', 'alt', 'английский');

            $tag->push('a', 'href', '/audio/' . $obj->resource_id);
            $tag->append('nobr', $obj->title);
            $tag->pop();

            $tag->push('p');
            $tag->append('nobr', $obj->category_name);
            $tag->pop();

            $tag->push('span');

            $date = strftime("%d, %B %Y", strtotime($obj->created_ts));

            $tag->append('p', (string) $date);
            $tag->append('img', 'src', '/images/resources/rate-star.png', 'alt', 'английский');
            $tag->append('b', (int) $obj->rating);

            $tag->pop();

            $tag->pop(); //close div
            $tag->pop(); //close li

            $index++;
        }
        $tag->pop(); //close ul;

        if ($bottom && count($data) > 0) {
            //bottom button
            $tag->push('div', 'class', 'conten-middle-center-left-cont-all');
            $tag->append('a', 'href', '#', 'onclick', 'return false;', 'class', 'all_audios', 'Все аудиозаписи');
            $tag->pop();
        }

        $tag->pop(); //close left content div
        return $tag;
    }

}

