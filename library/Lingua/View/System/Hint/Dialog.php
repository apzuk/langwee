<?php

/**
 * Description of messages
 *
 * @author Aram
 */
class Lingua_View_System_Hint_Dialog {

    //--------------------------------------------------------------------------
    public static function render($message, $title = "Подсказка") {
        //render mask
        $tag = new Daz_Tag('div', 'id', 'mask','');
        $tag->pop();

        $tag->push('div', 'id', 'message'); //message main div

        $tag->push('div', 'class', 'header', 'align', 'center'); // header div
        $tag->append('p', 'class', 'title', $title); //title bar
        //close button
        $tag->push('a', 'href', '#', 'onclick', 'return false', 'class', 'close');
        $tag->append('img', 'src', '/images/video/page/close.png');
        $tag->pop();

        $tag->pop(); //close title div

        $tag->push('div', 'class', 'body'); //body
        $tag->append('p', 'class', 'text', $message);
        $tag->pop();

        $tag->pop(); // close header tag

        $tag->pop(); //close main div
        
        return $tag;
    }

}

?>
