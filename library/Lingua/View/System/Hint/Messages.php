<?php

/**
 * Description of messages
 *
 * @author Aram
 */
class Lingua_View_System_Hint_Messages {

    //--------------------------------------------------------------------------
    public static function loginMessage() {
        //render mask

        $tag = new Daz_Tag('div', 'id', 'mask');
        $tag->pop();
        
        //message dialog render
        $tag->push('div', 'id', 'messages');
    }

}

?>
