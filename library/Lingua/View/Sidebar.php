<?php

class Lingua_View_Sidebar {

    const XML_FILE = '[priv_dir]/inc/sidebar.xml';

    // constructor builds these values
    private $is_edit_mode = false;
    private $XML = null;

    //----------------------------------------------------------------------
    public static function link($href, $text, $perm) {
        // no permission, do nothing
        if (!Lingua_Auth :: hasPermission($perm)) {
            return;
        }

        // render xhtml
        $XML = new Daz_Tag('li', 'class', $style);
        $XML->append('a', 'href', $href, 'class', $style, $text);
        $XML->pop(); // li
        print $XML;
    }

    //----------------------------------------------------------------------
    public static function render() {
        $MENU = new self();
        return (string) $MENU;
    }

    //----------------------------------------------------------------------
    public function __construct() {
        // is this edit mode on a form?
        $TFORM = Zend_Layout :: getMvcInstance()->getView()->TFORM;
        $this->is_edit_mode = $TFORM && method_exists($TFORM, 'is_edit_mode') && $TFORM->is_edit_mode();

        // load xml menu
        $this->XML = simplexml_load_file(Daz_String :: merge(self :: XML_FILE, array(
                    'priv_dir' => PRIVATE_PATH
                )));
    }

    //----------------------------------------------------------------------
    /**
     * If the XML node 'src' attribute contains a page we are on or contains a
     * <match> element with a matched 'src' attribute, then we are matched.
     * Recursively search 'menu' and 'selected' nodes under any passed in node.
     */
    private function isMatch($node) {
        $skip = (string) $node['skip'];
        if ($skip) {
            return false;
        }

        // loop through possible sub-menus
        $menus = array(
            'menu',
            'selected'
        );
        foreach ($menus as $menuname) {
            // if any sub-menu match, this menu matches
            if (count($node->$menuname)) {
                foreach ($node->$menuname as $menu) {
                    if ($this->isMatch($menu)) {
                        return true;
                    }
                }
            }
        }

        // nope, didn't match
        return false;
    }

    //----------------------------------------------------------------------
    private function testAuthEnabled($node) {

        // this tab requires specific permissions
        $permission = (string) $node['permission'];
        if ($permission && !Lingua_Auth :: hasPermission($permission)) {
            return false;
        }

        // must be fine
        return true;
    }

    //----------------------------------------------------------------------
    private function isModeMatch($node) {
        // what's the mode?
        $mode = (string) $node['mode'];

        // no mode set
        if (!$mode) {
            return true;
        }

        // 'edit' mode is set and matches
        elseif ($mode == 'edit' && $this->is_edit_mode) {
            return true;
        }

        // 'new' mode is set and matches
        elseif ($mode == 'new' && !$this->is_edit_mode) {
            return true;
        }

        // what the hell is this?
        return false;
    }

    //----------------------------------------------------------------------
    /**
     * Draw the level 1 (tabs) and level2 (sub tabs) and level3 (sidebar)
     * navigation.
     */
    public function __toString() {
        // convert XML to PHP menu array
        $menu = $this->readXmlMenu();

        // container
        $XML = new Daz_Tag('div', 'class', 'sidebar');

        // LEVEL 1
        foreach ($menu as $one) {
            // load menu elements
            list ($label, $src, $icon, $on, $submenu) = $one;

            // no sub-items, skip it
            if (!count($submenu)) {
                continue;
            }

            // build ui element
            $style = trim($icon . ' ' . ($on ? 'on' : 'off'));
            $XML->append('strong', 'class', $style, $label);
            $XML->push('ul');

            // LEVEL 2
            foreach ($submenu as $two) {
                list ($label, $src, $icon, $on, $submenu) = $two;

                // build menu ui element
                $style = trim($icon . ' ' . ($on ? 'on' : 'off'));
                $XML->push('li', 'class', $style);
                $XML->push('a', 'href', $src, 'class', $style);

                $label = preg_replace_callback('/::(\w+)::/', array(&$this, 'process'), $label);

                $XML->text($label);
                $XML->pop(2); // a, li
            }

            // finish section
            $XML->pop(); // ul
        }

        // render the menu
        return htmlspecialchars_decode((string) $XML);
    }

    //----------------------------------------------------------------------
    private function process($match) {
        if ($match[1] == "coupon")
            return "<b style='color: red'>" . (int) Lingua_Query_User::userCouponCountGetNew() . " новый</b>";
    }

    //----------------------------------------------------------------------
    private function readXmlMenu($node = null) {
        // start with the first node
        if (!$node) {
            return $this->readXmlMenu($this->XML);
        }

        // process menu
        $data = array();
        foreach ($node->menu as $menu) {
            if (!$this->testAuthEnabled($menu)) {
                continue;
            }

            // is this on?
            $on = $this->isMatch($menu);

            // read strings
            $label = (string) $menu['label'];
            $src = (string) $menu['src'];
            $icon = (string) $menu['icon'];

            // save this menu item
            $data[] = array(
                $label,
                $src,
                $icon,
                $on,
                $this->readXmlMenu($menu)
            );
        }
        return $data;
    }

    //----------------------------------------------------------------------
}