<?php

/**
 * Description of SystemLog
 *
 * @author Aram
 */
class Lingua_View_SystemLog {

    //--------------------------------------------------------------------------
    public static function successMessage($str) {
        $tag = new Daz_Tag('div', 'class', 'success');
        $tag->push('span', 'class', 'success_i');
        $tag->append('text', $str);
        $tag->pop();
        
        return $tag;
    }
    
    //--------------------------------------------------------------------------
    public static function failMessage($str) {
        $tag = new Daz_Tag('div', 'class', 'fail');
        $tag->push('span', 'class', 'fail_i');
        $tag->append('text', $str);
        $tag->pop();
        
        return $tag;
    }

}

?>
