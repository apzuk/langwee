<?php
/**
 * Description of Resources
 *
 * @author Aram
 */
class Lingua_View_Resources {
    
    //--------------------------------------------------------------------------
    public static function videoListRender($data) {
        //create ul element
        $tag = new Daz_Tag('ul');
        
        $index = 1;
        //looping throuth all videos and render
        foreach($data as $d) {
            $obj = new Lingua_Object($d);
                  
            //create li element
            $tag->push('li', 'class', ($index%2) == 0 ? 'bg' : 'no_bg');
            
            //body
            $tag->push('div');
            
            //append element b, the index of the video
            $tag->append('b', 'class', ($index == 1) ? 'col' : 'no_col', (($index != 10) ? '.' : ''));
            
            //append the video img logo 
            $tag->append('img', 'src', '/images/resources/video-icon.png', 'alt', 'видеоклипы');
            
            //push video title as link to itself
            $tag->append('a', 'href', '/video/' . $obj->video_id, 'target', '_blank');
            
            $tag->push('p');
            $tag->append('nobr', $obj->title);
            $tag->pop();
            
            //append video info
            $tag->push('span');
            
            //append video created date
            $tag->append('p', strftime("%d, %B %Y"));
            
            //append video raiting
            $tag->append('img', 'src', '/images/resources/rate-star.png', 'alt', 'рейтинг');
            $tag->append('p', $obj->rating);
            
            //close video info span block
            $tag->pop();
            
            //close body div
            $tag->pop();
            
            //close li
            $tag->pop();
            
            //close ul
            $tag->pop();
        }
        
        return $tag;
    }
    
    //--------------------------------------------------------------------------
    public static function videoSingleRender($video_obj) {
        
    }
}

?>
