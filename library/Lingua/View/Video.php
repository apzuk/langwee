<?php

/**
 * Description of Video
 *
 * @author Aram
 */
class Lingua_View_Video {

    //--------------------------------------------------------------------------
    public static function videoPlayerViewRender($video) {
        //video link
        $tag = new Daz_Tag('div', 'class', 'conten-middle-presentn-txt');
        $tag->append('a', 'href', '/resources', 'Главная, ' . $video->category_name);
        $tag->pop();

        //video player container div
        $tag->push('div', 'class', 'conten-middle-video');

        //video title div 
        $tag->push('div', 'class', 'conten-middle-video-txt');

        //video title label
        $tag->push('b');
        $tag->append('nobr', $video->title);
        $tag->pop();
        $tag->push('p');
        $tag->text('Просмотров:');
        $tag->append('span', $video->rating);
        $tag->pop();

        //close video title div
        $tag->pop();

        //video player div
        $tag->push('div', 'class', 'conten-middle-video-player');


        if (strpos($video->video_url, 'youtube.com') !== FALSE) {
            $tag->push('iframe', 'width', '445', 'height', '320', 'src', $video->video_url . '?wmode=opaque', 'frameborder', '0', 'allowfullscreen', true);

            $tag->text('');
            $tag->pop();
        } elseif (strpos($video->video_url, 'ted.com') !== FALSE) {
            $url = Lingua_Helper_Ted::tedGetUrl($video->video_url);

            $tag->push('object', 'width', 445, 'height', 374);

            $tag->append('param', 'name', 'movie', 'value', 'http://video.ted.com/assets/player/swf/EmbedPlayer.swf', '');
            $tag->append('param', 'name', 'allowFullScreen', 'value', 'true');
            $tag->append('param', 'name', 'allowScriptAccess', 'value', 'always');
            $tag->append('param', 'name', 'wmode', 'value', 'transparent', '');
            $tag->append('param', 'name', 'bgColor', 'value', '#ffffff', '');
            $tag->append('param', 'name', 'flashvars', 'value', 'vu=http://video.ted.com/talk/stream/2011X/Blank/ChristophAdami_2011X-320k.mp4&su=http://images.ted.com/images/ted/tedindex/embed-posters/ChristophAdami_2011X-embed.jpg&vw=400&vh=374&ap=0&ti=1237&lang=&introDuration=15330&adDuration=4000&postAdDuration=830&adKeys=talk=' . $url . ';year=2011;theme=inspired_by_nature;theme=unconventional_explanations;theme=evolution_s_genius;event=TEDxUIUC;tag=biology;tag=evolution;tag=life;tag=science;&preAdTag=tconf.ted/embed;tile=1;sz=445x374');
            $tag->append('embed', 'src', 'http://video.ted.com/assets/player/swf/EmbedPlayer.swf', 'pluginspace', 'http://www.macromedia.com/go/getflashplayer', 'type', 'application/x-shockwave-flash', 'wmode', 'transparent', 'bgColor', '#ffffff', 'width', '440', 'height', '374', 'allowFullScreen', 'true', 'allowScriptAccess', 'always', 'flashvars', 'vu=http://video.ted.com/talk/stream/2011X/Blank/ChristophAdami_2011X-320k.mp4&su=http://images.ted.com/images/ted/tedindex/embed-posters/ChristophAdami_2011X-embed.jpg&vw=445&vh=374&ap=0&ti=1237&lang=&introDuration=15330&adDuration=4000&postAdDuration=830&adKeys=talk=' . $url . ';year=2011;theme=inspired_by_nature;theme=unconventional_explanations;theme=evolution_s_genius;event=TEDxUIUC;tag=biology;tag=evolution;tag=life;tag=science;&preAdTag=tconf.ted/embed;tile=1;sz=445x374;');

            $tag->pop();
        }

        //close video player div
        $tag->pop();

        //social netword icons
        $tag->push('div', 'class', 'conten-middle-video-player-soc-icon');
        $tag->push('div', 'id', 'vk_like');
        $tag->text('');
        $tag->pop();

        //close social network icons 
        $tag->pop();

        //close video player container div
        $tag->pop();

        return $tag;
    }

}
?>

