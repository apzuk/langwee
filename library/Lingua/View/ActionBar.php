<?php

class Lingua_View_ActionBar {

    private $links = array();

    //----------------------------------------------------------------------
    public function __construct($links = null) {
        if ($links) {
            $this->links = $links;
        }
    }

    //----------------------------------------------------------------------
    public function add($name, $label, $href = null, $enabled = true, $func = null) {
        $this->links[] = array(
            'id' => $name,
            'label' => $label,
            'href' => $href,
            'func' => $func,
            'enabled' => $enabled
        );
    }

    //----------------------------------------------------------------------
    public function render($action = "") {
        // start toolbar
        $XML = new Daz_Tag('fieldset', 'class', 'act');
        $XML->append('legend', $action);
        $XML->push('form', 'action', Daz_Server :: get('PHP_SELF'));

        // add buttons
        foreach ($this->links as $link) {
            $link = new Lingua_Object($link);

            // td
            $XML->push('div', 'class', 'link');
            $XML->push('input', 'type', 'button', 'id', $link->id, 'name', $link->id);
            $XML->attr('class', 'link', 'value', $link->label);

            if (!$link->enabled) {
                $XML->attr('disabled',''); 
            }
            
            // we have a url
            if ($link->href) {
                $link->func = sprintf("window.location.href = '%s';", $link->href);
            }

            // javascript function
            if ($link->func) {
                $XML->attr('onclick', $link->func);
            }

            // end button
            $XML->pop(2); // input, div
        }


        // end toolbar
        $XML->pop(2); // form, fieldset
        // return xhtml
        return (string) $XML;
    }

    //----------------------------------------------------------------------
}