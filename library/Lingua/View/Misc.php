<?php

/**
 * Description of Misc
 *
 * @author Aram
 */
class Lingua_View_Misc {

    private static $USER = null;
    private static $USER_FRIENDS_COUNT = null;

    //--------------------------------------------------------------------------
    public static function init() {
        $user = Lingua_Query_User::userGetShort(Lingua_Auth::getLinguaId());

        self::$USER_FRIENDS_COUNT = (int) Lingua_Query_User::userGetFriendsCount(Lingua_Auth::getLinguaId());
        self::$USER = new Lingua_Object($user);
    }

    //--------------------------------------------------------------------------
    public static function getNickNameView() {
        if (!self::$USER)
            self::init();

        $USER = self::$USER;
        //generate user nickname        
        if ($USER->person) {
            $nickname = $USER->person;
        } elseif ($USER->nickname) {
            $nickname = $USER->nickname;
        } else {
            $nickname = $USER->login;
        }

        $tag = new Daz_Tag('div', 'class', 'conten-top-menu');
        $tag->append('p', $nickname);
        $tag->push('ul');
        $tag->push('li');
        $tag->append('a', 'href', '/profile', 'мой кабинет');
        $tag->pop();

        $tag->push('li');
        $tag->append('a', 'href', '/logout', 'выйти');
        $tag->pop();
        $tag->pop();
        $tag->pop();

        return $tag;
    }

    //--------------------------------------------------------------------------
    public static function getRightTopView() {
        if (!self::$USER)
            self::init();

        $USER = self::$USER;
        $tag = new Daz_Tag('ul');

        $tag->push('li', 'class', 'n_li');
        $tag->push('a', 'href', '#', 'class', 'notifications');
        $tag->append('img', 'src', '/images/community/shar1.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->pop();
        $tag->push('li', 'class', 'f_li');
        $tag->push('a', 'href', '#', 'class', 'notifications');
        $tag->append('img', 'src', '/images/community/people1.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->pop();
        $tag->push('li', 'class', 'm_li');
        $tag->push('a', 'href', '#', 'class', 'notifications');
        $tag->append('img', 'src', '/images/community/letter1.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->pop();

        $tag->push('div', 'class', 'header_menu2_status');
        if ($USER->status == "ONLINE")
            $tag->append('img', 'src', '/images/community/activ-stat.png', 'alt', 'Английский онлайн');
        else
            $tag->append('img', 'src', '/images/community/chat-set-status-offline-icon.png', 'alt', 'Английский онлайн');
        $tag->pop();

        $tag->push('div', 'class', 'header_menu2_icon');
        $tag->append('img', 'src', '/images/community/community-user-icon.png', 'style', 'margin-top:8px;', 'alt', 'Английский онлайн');
        $tag->pop();

        $tag->push('div', 'class', 'header_menu2_fr_count');
        $tag->append('h4', self::$USER_FRIENDS_COUNT);
        $tag->pop();

        $tag->push('div', 'class', 'header_menu2_credit');
        $txt = $USER->credit;
        if (Lingua_Auth::isVIPAccount()) {
            $txt = "Standart";

            $tag->append('h1', 'class', 'standart', 'style', 'padding-left:5px; cursor: default', $txt);
        } else {
            $tag->append('img', 'src', '/images/video/page/star.png', 'alt', 'Английский онлайн');

            $tag->append('h1', 'class', 'normal', $txt);
        }
        $tag->pop();

        return $tag;
    }

    //--------------------------------------------------------------------------
    public static function getLeftTopView() {
        if (!self::$USER)
            self::init();

        $USER = self::$USER;

        $tag = new Daz_Tag('ul');

        //create li audio/video section
        $tag->push('li');
        $tag->append('a', 'href', '/resources', 'style', 'float:left', 'Аудио/Видео');
        $tag->append('img', 'src', '/images/default/main/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();

        //create li trainings
        $tag->push('li', 'id', 'li_m2');
        $tag->append('a', 'href', '#', 'onclick', 'return false;', 'style', 'float:left', 'Тренинги');
        $tag->append('img', 'src', '/images/default/main/menu-vert-line.png', 'alt', 'Как выучить Английский');

        //creating div block for community drop down
        $tag->push('div', 'class', 'header_menu_trainings', 'id', 'training_drob_menu');

        //create ul for drop down block
        $tag->push('ul');

        //create li subject for system trainings
        $tag->push('li');
        $tag->append('a', 'href', '/trainings', 'Тренинги');
        $tag->pop();

        //create li subject for video/audio trainings
        $tag->push('li');
        $tag->append('a', 'href', '/training-coaching', 'Коучинг');
        $tag->pop();

        $tag->pop(); //close ul

        $tag->pop(); //close drop down div

        $tag->pop();

        //create li dictionary
        $tag->push('li');
        $tag->append('a', 'href', '/dictionary', 'style', 'float:left', 'Словарь');
        $tag->append('img', 'src', '/images/default/main/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();

        //create li цоммуниты
        $tag->push('li', 'id', 'li_m4');
        $tag->append('a', 'href', '#', 'onclick', 'return false;', 'Сообщество');
        $tag->pop();

        //end of ul
        $tag->pop();

        //creating div block for community drop down
        $tag->push('div', 'class', 'header_menu1_community', 'id', 'div_drob_menu');

        //create ul for drop down block
        $tag->push('ul');

        //create li subject for chat
        $tag->push('li');
        $tag->append('a', 'href', '/chat', 'Чат');
        $tag->pop();

        //create li subject for my friends
        $tag->push('li');
        $tag->append('a', 'href', '/friends', 'Мои друзья');
        $tag->pop();

        //create li subject for find friends
        $tag->push('li');
        $tag->append('a', 'href', '/search', 'Найти друзей');
        $tag->pop();

        //create li subject for find friends
        $tag->push('li', 'style', 'border:none');
        $tag->append('a', 'href', '#', 'Пригласить друзей');
        $tag->pop();

        //close drop down div
        $tag->pop();

        //close drop down div
        $tag->pop();

        return $tag;
    }

    //--------------------------------------------------------------------------
    public static function footerViewRender() {
        //create main div
        $tag = new Daz_Tag('div', 'id', 'footer');

        $tag->push('div', 'class', 'footer-center');

        //copyright div
        $tag->push('div', 'class', 'footer-center-copy');
        $tag->append('p', 'Copyright 2012');
        $tag->append('p', 'Все права защищены');

        //close copyright div
        $tag->pop();

        //create div soc icons
        $tag->push('div', 'class', 'footer-center-soc-iconki');
        $tag->text('');
        $tag->append('div', 'id', 'vk_like', ' ');

        $tag->pop();
        //close soc icons div
        //create footer links div
        $tag->push('div', 'class', 'footer-center-menu');

        //create ul element 
        $tag->push('ul');

        //appending links
        $tag->push('li');
        $tag->append('a', 'href', '/feedback', 'style', 'float:left', 'Обратная связь');
        $tag->append('img', 'src', '/images/video/page/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->push('li');
        $tag->append('a', 'href', '/credit', 'style', 'float:left', 'Звезды');
        $tag->append('img', 'src', '/images/video/page/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->push('li');
        $tag->append('a', 'href', '/faq', 'style', 'float:left', 'FAQ');
        $tag->append('img', 'src', '/images/video/page/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->push('li');
        $tag->append('a', 'href', '/payment', 'style', 'float:left', 'Оплата');
        $tag->append('img', 'src', '/images/video/page/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->push('li');
        $tag->append('a', 'href', '/partnership', 'style', 'float:left', 'Школам/Oрганизациям');
        $tag->append('img', 'src', '/images/video/page/menu-vert-line.png', 'alt', 'Как выучить Английский');
        $tag->pop();
        $tag->push('li');
        $tag->append('a', 'href', '/reviews', 'Отзывы');
        $tag->pop();

        //close ul 
        $tag->pop();

        //close footer links div
        $tag->pop();

        //close footer center div
        $tag->pop();

        //close main div
        $tag->pop();

        $tag->pop();

        echo $tag;
    }

}

?>