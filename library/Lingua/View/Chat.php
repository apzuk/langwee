<?

/**
 * Description of Chat
 *
 * @author Aram
 */
class Lingua_View_Chat {

    //--------------------------------------------------------------------------
    public static function chatRandomListRender($data) {
        $tag = new Daz_Tag('ul');
        foreach ($data as $d) {
            $obj = new Lingua_Object($d);

            //start item drawing
            $tag->push('li');

            //user image
            $tag->push('span');
            $tag->push('a', 'href', '#', 'onclick', 'return false;');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($obj->lingua_id, 60, 60), 'alt', 'лингвист', 'class', 'trigger', 'id', $obj->lingua_id);
            $tag->pop();
            $tag->pop();

            //user name 
            $tag->push('h2');
            $tag->append('a', 'href', '/user/' . $obj->lingua_id, $obj->person);
            $tag->pop();

            //user country
            $tag->push('p', $obj->country);
            $tag->pop();

            //chat now! link
            $tag->append('a', 'href', '/chat-invite/' . $obj->lingua_id, 'target', '_blank', ' Общайтесь сейчас »');
            $tag->pop();
        }
        echo $tag;
    }

    //--------------------------------------------------------------------------
    public static function chatFriendsListRender($data) {
        $tag = new Daz_Tag('ul', 'style', 'width: 100%');

        foreach ($data as $d) {
            $obj = new Lingua_Object($d);

            $tag->push('li');
            $tag->push('span');
            $tag->push('a', 'href', 'javascript:void(0)');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($obj->lingua_id, 60, 60), 'alt', 'лингвист', 'class', 'trigger', 'id', $obj->lingua_id);
            $tag->pop();
            $tag->pop();
            $tag->push('h2');
            $tag->push('a', 'href', '/user/' . $obj->lingua_id, $obj->person);
            if ($obj->status == "ONLINE") {
                $tag->append('img', 'src', '/images/community/user_online.gif', 'style', 'float:right');
            }
            $tag->pop();
            $tag->pop();
            $tag->push('p', $obj->country);
            $tag->pop();
            $tag->append('a', 'href', '/chat-invite/' . $obj->lingua_id, 'target', '_blank', ' Общайтесь сейчас »');
            $tag->pop();
        }
        echo $tag;
    }

    //
    ////--------------------------------------------------------------------------
    public static function chatFoundListRender($data) {
        $tag = new Daz_Tag('ul');

        foreach ($data as $d) {
            $obj = new Lingua_Object($d);

            $tag->push('li');
            $tag->push('span');
            $tag->push('a', 'href', 'javascript:void(0)');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($obj->lingua_id, 60, 60), 'alt', 'лингвист', 'class', 'trigger', 'id', $obj->lingua_id);
            $tag->pop();
            $tag->pop();
            $tag->push('h2');
            $tag->push('a', 'href', '/user/' . $obj->lingua_id, $obj->person);
            if ($obj->status == "ONLINE") {
                $tag->append('img', 'src', '/images/community/user_online.gif', 'style', 'float:right');
            }
            $tag->pop();
            $tag->pop();
            $tag->push('p', $obj->country);
            $tag->pop();
            $tag->append('a', 'href', '/chat-invite/' . $obj->lingua_id, 'target', '_blank', ' Общайтесь сейчас »');
            $tag->pop();
        }
        echo $tag;
    }

    //--------------------------------------------------------------------------
    public static function chatGetInviteView($sender_lingua_id, $chat_id, $notification) {
        $user = Lingua_Query_User::userGetFull($sender_lingua_id);
        $user = new Lingua_Object($user);

        if ($user->lingua_id) {
            $tag = new Daz_Tag('div', 'id', 'popup');
            $tag->push('div', 'class', 'popup_left_foto');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($user->lingua_id, 60, 60));
            $tag->pop();
            $tag->pop();
            $tag->push('div', 'class', 'popup_right');
            $tag->push('div', 'class', 'popup_right_name');
            $tag->push('h3', $user->person);
            $tag->pop();
            $tag->pop();
            $tag->append('h3', 'хочет с Вами пообщаться');
            $tag->push('a', 'href', '/community/community/chat-approve/chat_id/' . $chat_id . '/notification/' . $notification, 'target', '_blank', 'Принять');
            $tag->push('input', 'type', 'button', 'name', 'approve', 'value', 'Отказать!');
            $tag->pop();
            $tag->pop();

            return $tag;
        } else {
            echo '';
        }
    }

    //--------------------------------------------------------------------------
    public static function fiendRequestGetInviteView($sender_lingua_id, $notification) {
        $user = Lingua_Query_User::userGetFull($sender_lingua_id);
        $user = new Lingua_Object($user);

        if ($user->lingua_id) {
            $tag = new Daz_Tag('div', 'class', 'popup_left');
            $tag->push('div', 'class', 'popup_left_foto');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($user->lingua_id, 60, 60));
            $tag->pop();
            $tag->pop();
            $tag->push('div', 'class', 'popup_right');
            $tag->push('div', 'class', 'popup_right_name');
            $tag->push('h3', $user->person);
            $tag->pop();
            $tag->pop();
            $tag->append('h3', 'хочет добавить Вас в друзья!');
            $tag->push('a', 'href', '/community/community/friend-request-approve/notification/' . $notification, 'target', '_blank', 'Принять');
            $tag->push('a', 'href', '/community/community/friend-request-reject/notification/' . $notification, 'target', '_blank', 'Отказать!');
            $tag->pop();
            $tag->pop();

            return $tag;
        } else {
            echo '';
        }
    }

    //--------------------------------------------------------------------------
    public static function fiendRequestAcceptedGetView($sender_lingua_id, $notification) {
        $user = Lingua_Query_User::userGetFull($sender_lingua_id);
        $user = new Lingua_Object($user);

        if ($user->lingua_id) {
            $tag = new Daz_Tag('div', 'class', 'popup_left');
            $tag->push('div', 'class', 'popup_left_foto');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($user->lingua_id, 60, 60));
            $tag->pop();
            $tag->pop();
            $tag->push('div', 'class', 'popup_right');
            $tag->push('div', 'class', 'popup_right_name');
            $tag->push('h3', $user->person);
            $tag->pop();
            $tag->pop();
            $tag->append('h3', 'Добавил(а) Вас в друзья');
            $tag->push('a', 'href', '#', 'id', $notification, 'class', 'close_notification', 'Закрыть');
            $tag->pop();
            $tag->pop();

            return $tag;
        } else {
            echo '';
        }
    }

    //--------------------------------------------------------------------------
    public static function friendRemoveGetView($sender_lingua_id, $notification) {
        $user = Lingua_Query_User::userGetFull($sender_lingua_id);
        $user = new Lingua_Object($user);

        if ($user->lingua_id) {
            $tag = new Daz_Tag('div', 'class', 'popup_left');
            $tag->push('div', 'class', 'popup_left_foto');
            $tag->append('img', 'src', Lingua_Storage_User::getUrl($user->lingua_id, 60, 60));
            $tag->pop();
            $tag->pop();
            $tag->push('div', 'class', 'popup_right');
            $tag->push('div', 'class', 'popup_right_name');
            $tag->push('h3', $user->person);
            $tag->pop();
            $tag->pop();
            $tag->append('h3', 'Удалил(а) Вас из своих друзья');
            $tag->push('a', 'href', '#', 'id', $notification, 'class', 'close_notification', 'Закрыть');
            $tag->pop();
            $tag->pop();

            return $tag;
        } else {
            echo '';
        }
    }

}

?>
