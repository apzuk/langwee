<?php

/**
 * Description of Action
 *
 * @author Aram
 */
class Lingua_Plugin_Action extends Zend_Controller_Action {

    private $startTime;

    //--------------------------------------------------------------------------
    public function preDispatch() {

        //  Lingua_Auth::rejectUnlessAuth();
        $this->_helper->viewRenderer->setViewSuffix('phtml');
        $lingua_id = Lingua_Auth::getLinguaId();

        if (Lingua_Auth::getLinguaId()) {
            if ($this->getRequest()->getActionName() !== 'notification-get') {
                Lingua_Query_User::userSetLastAction($lingua_id);
            }
        }

        $this->startTimeCapture();
    }

    //--------------------------------------------------------------------------
    public function getParam($key, $default = false) {
        $params = $this->getParams();
        return isset($params[$key]) ? $params[$key] : $default;
    }

    //--------------------------------------------------------------------------
    public function getParams() {
        return $this->getRequest()->getParams();
    }

    //--------------------------------------------------------------------------
    public function isPost() {
        return $this->getRequest()->isPost();
    }

    //--------------------------------------------------------------------------
    public function startTimeCapture() {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];

        //script execution time
        $this->startTime = $mtime;
    }

    //--------------------------------------------------------------------------
    public function outputExecutiontime() {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $endtime = $mtime;
        $totaltime = ($endtime - $this->startTime);
        echo "This page was created in " . $totaltime . " seconds";
    }

}