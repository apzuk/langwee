<?php
class Lingua_Plugin_Layout extends Zend_Controller_Plugin_Abstract {
    //----------------------------------------------------------------------
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        // layout name matches module name
        $module = $request->getModuleName();
        $lingua_id = Lingua_Auth::getLinguaId();
        
        if($module == "admin") {
            $role = Lingua_Query_User::userGetRole($lingua_id);
            
            if($role == 'STUDENT') header("Location: /");
        }
        
        $layout = Zend_Layout :: getMvcInstance()->setLayout($module);
        
        // add module directory "models" to docpath
        $module_form_path = APPLICATION_PATH . '/' . $module . '/models';
        set_include_path($module_form_path . PATH_SEPARATOR . get_include_path());
    }

    //----------------------------------------------------------------------
}