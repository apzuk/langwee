<?php

/**
 * Description of User
 *
 * @author Aram
 */
class Lingua_Storage_User {

    private static $DEFAULT = "/data/default/user/avatar/default_avatar.jpg";
    private static $cached = "[lingua_id]/[xsize]x[ysize]";
    private static $original = "";

    //--------------------------------------------------------------------------
    public static function getUserPhoto($filename, $size = 60) {
        $avatar_path = '/data/users/photo/' . $size . 'x' . $size . '/' . $filename;

        if (!empty($filename) && file_exists(APPLICATION_PATH . "/../public_html/" . $avatar_path))
            return $avatar_path;
        else
            return self::$DEFAULT;
    }

    //--------------------------------------------------------------------------
    public static function removeUserFile($lingua_id, $filename) {
        $file = Daz_Config::get('user.photo') . '/' . $filename;
        $cache = Daz_Config::get('user.cache') . $lingua_id;

        if (file_exists($file)) {
            unlink($file);
        }
        system('/bin/rm -rf ' . escapeshellarg($cache));
    }

    //--------------------------------------------------------------------------
    public static function getUserPhotoById($lingua_id) {
        $photo = Lingua_Query_User::userGetPhoto($lingua_id);
        return self::getUserPhoto($photo);
    }

    //--------------------------------------------------------------------------
    public static function getUserImageFit($lingua_id, $xsize, $ysize) {
        //get user cached folder path
        $user_chaced_dir = Daz_Config::get('user.cache') . Daz_String::merge(self :: $cached, array(
                    'lingua_id' => $lingua_id,
                    'xsize' => $xsize,
                    'ysize' => $ysize)
        );
        $photo = Lingua_Query_User::userGetPhoto($lingua_id);
        $user_original_dir = Daz_Config::get('user.photo') . '/';
        
        if (!file_exists($user_chaced_dir  . '/' .  $lingua_id . '.jpg') && file_exists($user_original_dir . $photo)) {
            $original = $user_original_dir . $photo;
            mkdir($user_chaced_dir, 0755, true);

            $sys_command = Daz_String::merge('convert -resize "[x]x[y]" [original] [output]', array(
                        'x' => $xsize,
                        'y' => $ysize,
                        'original' => $original,
                        'output' => $user_chaced_dir . '/' . $lingua_id . '.jpg')
            );
            
            system($sys_command);
        }

        $image = $user_chaced_dir . '/' . $lingua_id . '.jpg';

        if (!file_exists($image)) {
            $image = Daz_Config::get('user.default');
        }

        header('Content-Type: image/jpg');
        echo file_get_contents($image);
    }

    //--------------------------------------------------------------------------
    public static function getUrl($lingua_id, $width, $height) {
        $url = sprintf('/getimage/%d/%d/%d', $width, $height, $lingua_id);
        return $url;
    }

}

?>
