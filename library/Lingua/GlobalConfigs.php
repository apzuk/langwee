<?php

/**
 * Description of GlobalConfigs
 *
 * @author Aram
 */
class Lingua_GlobalConfigs {

    private static $SESS = null;
    private static $LANGS = array(
        "en" => "Английский", "es" => "Испанский",
        "it" => "Итальянский", "fr" => "Французский",
        "de" => "Немецкий");

    //--------------------------------------------------------------------------
    public static function systemGetLanguages() {
        return self::$LANGS;
    }

    //--------------------------------------------------------------------------
    public static function generatePassword($length = 25) {
        $chars = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm123456789";
        $strrand = "";

        for ($iterator = 0; $iterator < $length; $iterator++) {
            $strrand .= $chars[rand(0, $length)];
        }

        return $strrand;
    }

    //--------------------------------------------------------------------------
    public static function init() {
        self::$SESS = new Daz_Session(__CLASS__);
    }

    //--------------------------------------------------------------------------
    public static function configStore($key, $value) {
        if (!self::$SESS) {
            self::init();
        }

        self::$SESS->$key = $value;
    }

    //--------------------------------------------------------------------------
    public static function configGet($key) {
        if (!self::$SESS) {
            self::init();
        }

        return self::$SESS->$key;
    }

}

?>
