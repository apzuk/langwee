<?php

/**
 * Description of Subscriber
 *
 * @author Aram
 */
class Lingua_Site_Subscriber extends Lingua_Helper_MCAPI {
    /**
     *
     * @var string
     * 
     * ChimpMain API key 
     */

    const API = "4e255c67560a990a7aa9d1597308b93e-us5";

    //---------------------------------------------------------------------
    public function __construct() {
        parent::MCAPI(self :: API);
    }

    //---------------------------------------------------------------------
    /*
     * 
     * Subscribe the list of given users
     */
    public function subscribeAll($data) {
        foreach ($data as $d) {
            $obj = new Lingua_Object($d);

            $marge_vars = array('FNAME' => $obj->firstname, 'LNAME' => $obj->surname);
            $this->listSubscribe('a057c6f495', $obj->login, $marge_vars, 'html', false);
        }

        $this->printError();
    }

    //---------------------------------------------------------------------
    /**
     *
     * If error has occured while trying to execute the http call
     * to the chimp mail server print it! Only for admin 
     * 
     */
    public function printError() {
        if ($this->errorCode) {
            echo "Unable to load listSubscribe()!\n";
            echo "\tCode=" . $this->errorCode . "\n";
            echo "\tMsg=" . $this->errorMessage . "\n";
        }
    }

}

?>
