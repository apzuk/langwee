<?php

/**
 * Description of Auth
 *
 * @author Aram
 */
class Lingua_Auth {

    const SESSION_PREFIX = 'LINGUA_AUTH';
    const URL_BLOCKED = '/blocked';
    const URL_LOGIN = '/admin/';
    const URL_USER_LOGIN = '/';

    private static $USER = null;
    private static $SESS = null;
    private static $PERM = null;
    private static $SYSLANG = null;

    //--------------------------------------------------------------------------
    public static function getUser() {
        $data = Lingua_Query_User :: userGetFull(self::getLinguaId());
        return self::$USER = new Lingua_Object($data);
    }

    //--------------------------------------------------------------------------
    public static function isVIPAccount() {
        return Lingua_Query_User::userIsPaid(self :: getLinguaId());
    }

    //--------------------------------------------------------------------------
    public static function init() {
        if (!self::$SESS) {
            self :: $SESS = new Daz_Session(self :: SESSION_PREFIX);

            self :: $SESS->setExpirationSeconds(time() * 400 * 24 * 365 * 60 * 60);
        }
    }

    //--------------------------------------------------------------------------
    public static function setLinguaId($lingua_id) {
        self :: init();
        self :: $SESS->unsetAll();

        $data = Lingua_Query_User :: userGetFull($lingua_id);

        if (!isset($data['lingua_id'])) {
            return false;
        }

        self :: $SESS->USER = new Lingua_Object($data);
        self :: $SESS->user_id = $data['lingua_id'];
    }

    //--------------------------------------------------------------------------
    public static function sysGetLanguages() {
        if (!self :: $SYSLANG) {
            self :: $SYSLANG = Lingua_Query_System::languagesGetAll();
        }

        return self :: $SYSLANG;
    }

    //--------------------------------------------------------------------------
    public static function getLinguaId() {
        self :: init();
        return self :: $SESS->user_id;
    }

    //--------------------------------------------------------------------------
    public static function isAuth() {
        self :: init();
        return (boolean) self :: $SESS->user_id;
    }

    //----------------------------------------------------------------------
    public static function rejectUnlessAuth($reject_to = "ADMIN", $want_url = null) {
        
        if (self :: isAuth()) {
            return true;
        }

        $php_self = Daz_Server :: get('PHP_SELF');
        if (!$want_url && $php_self) {
            $want_url = $php_self;
        }

        self :: wantUrlSet($want_url);
        if ($reject_to == "ADMIN")
            header('Location: ' . self::URL_LOGIN);
        else
            header('Location: ' . self::URL_USER_LOGIN);
        exit;
    }

    //----------------------------------------------------------------------
    public static function wantUrlGet($default = null) {
        self :: init();

        $want_url = self :: $SESS->want_url;

        return $want_url ? $want_url : $default;
    }

    //----------------------------------------------------------------------
    public static function userGetRole() {
        return self :: $SESS->USER->role_name;
    }

    //----------------------------------------------------------------------
    public static function wantUrlSet($want_url = null) {
        self :: init();

        self :: $SESS->want_url = $want_url;
    }

    //----------------------------------------------------------------------
    public static function rejectUnlessPermission($right) {
        self :: rejectUnlessAuth();

        if (!self :: hasPermission($right)) {
            self :: $SESS->blocked_permission = $right;
            self :: $SESS->blocked_url = Daz_Server :: get('PHP_SELF');
            header('Location: ' . self :: URL_BLOCKED);
            exit;
        }

        return true;
    }

    //----------------------------------------------------------------------
    public static function logOut() {
        self :: init();

        self :: $SESS->unsetAll();

        self :: $USER = null;
        self :: $PERM = null;
    }

    //----------------------------------------------------------------------
    public static function hasPermission($right) {

        if (!self :: $PERM) {
            $user_id = self :: getLinguaId();
            self :: $PERM = (array) Lingua_Query_Permission :: permissionGetUser($user_id);
        }

        return isset(self :: $PERM[$right]);
    }

    //----------------------------------------------------------------------
    public static function isAdmin() {
        if (!self :: $SESS->USER) {
            return false;
        }
        return self :: $SESS->USER->role_name == "ADMIN" ? true : false;
    }

    //----------------------------------------------------------------------
    public static function getLanguage($type = true) {
        if ($type === true) {
            return self :: $SESS->language ? self :: $SESS->language : 1;
        } elseif ($type == 'short') {
            return self :: $SESS->short_name ? self :: $SESS->short_name : 'en';
        }
    }

    //----------------------------------------------------------------------
    public static function setLanguage($lang_id, $short_name) {
        self :: $SESS->short_name = $short_name;
        self :: $SESS->language = $lang_id;
    }

    //----------------------------------------------------------------------
    public static function languageGetShortName($lang_id) {
        if (!self :: $SYSLANG) {
            self :: sysGetLanguages();
        }
        
        foreach (self::$SYSLANG as $short_name => $lang) {
            if ($lang['system_languages_id'] == $lang_id)
                return $short_name;
        }
    }

}

?>
