var $left_block = $('div.conten-middle-center-left-cont-bl');
var $translate_block = $('div.conten-middle-center-right-cont-bl');
var $lang = $('div.conten-middle-lang');
var map = {
    today: 'Сегодня', 
    yesterday: 'Вчера', 
    prev2days: '2 дня назад', 
    prevweek: 'Неделю назад', 
    prev2weeks: '2 недели назад',
    prevmonth: 'Месяц назад' 
};


var DICTIONARY = {
    selected_word: -1,
    words: null,
    lingua_id: -1,
    selected_language: -1,
    
    run: function(lang) {
        DICTIONARY.selected_language = lang;
        
        DICTIONARY.request('/profile/user/dictionary-get-all', {
            lang: DICTIONARY.selected_language
        }, DICTIONARY.dictionaryLoaded);  
        
        //languages switcher init
        $('a', $lang).each(function(i, e) {
            $(e).click(function(event) {
                event.preventDefault();
                
                $.post('/api/global/global-change-language', {
                    short_name: this.id
                }, function(data) {
                    if(data.result == 'success') {
                        window.location.href = '/dictionary';
                    }
                    else if(data.result == 'fail') {
                    //show somehow returned reason of the fail
                    //data.reason
                    }
                }, 'json');
            });
        });
    },
    
    request: function(url, data, callback) {
        $.post(url, data, callback);
    },
    
    wordDetailsLoaded: function(data) {
        var $word = $.parseJSON(data);
        
        DICTIONARY.selected_word = $word.data.user_word_id;
        DICTIONARY.dictionaryDrawWordDetails($word.data);
    },
    
    init: function(){
        var b = false;
        $('li.words').each(function(index, element) {
            $(element).css('cursor', 'pointer');
            
            if(!b && element.id!='') {
                var str_id = element.id.split('_');
                var id = str_id[1];
                
                DICTIONARY.selected_word = id;
                $(element).css('background-color', '#FFB366');
                DICTIONARY.request('/profile/user/dictionary-get-word', {
                    word_id: id
                } ,DICTIONARY.wordDetailsLoaded);
                b = true;
            }
                
            $(element).click(function(e) {
                if(element.id == '') return;
                
                $('#word_' + DICTIONARY.selected_word).css('background-color', '#fff2e1');
                $(element).css('background-color', '#FFB366');
                
                var str_id = element.id.split('_');
                var id = str_id[1];
                
                DICTIONARY.request('/profile/user/dictionary-get-word', {
                    word_id: id
                } ,DICTIONARY.wordDetailsLoaded);
                
            });
        });
        
        $('li.words img').each(function(index, element) {
            $(element).click(function(e){
                if(!confirm('Вы уверени что хотите удалить это слова из Вашего словаря?!', 'Удалить слово')) {
                    return;
                }
                
                var parent = $(this).parent().get(0);
                var str_id = parent.id.split('_');
                var id = str_id[1];
                
                DICTIONARY.dictionaryRemoveWord(id);
            });
        });
    },
    
    dictionaryDrawWordDetails: function($word) {
        if($word == null) return;
        
        $translate_block.fadeOut(function() {
            var html = '<div class="conten-middle-center-right-cont-bl-txt"><h2>' + $word.word_src + '</h2>' + 
            '<h3>&nbsp; - ' + $word.word_trg + '</h3></div>'+

            '<div class="conten-middle-center-right-cont-bl-dict">' + 
            '<div class="conten-middle-center-right-cont-bl-dict-audio">' +  
            '<img src="/images/video/page/1330288171_audio-volume-high.png" title="Прослушать"/><p>' + 
            '</p></div>'+
            '<div class="conten-middle-center-right-cont-bl-dict-chang-tr"><a href="#"></a></div>'+
            '<div class="conten-middle-center-right-cont-bl-dict-block">' + 
            '<p>Вы переводили ' + $word.count + ' раз:</p>';
            
            for(var key in $word.details) {
                var context = $word.details[key]['word_context'];
                context = context.replace($word.word_src, '<span>' + $word.word_src + '</span>');
                
                html += 
                '<a href="/' + $word.details[key]['type'] + '/' + $word.details[key]['type_id'] + '" target="_blank">' + 
                $word.details[key]['title'] + '</a>' + 
                '<div style="clear:both;">' + 
                '<b>' + context + '</b>';
            /* <!------------------------------------------------------>
                            <a href="#">Aerosmith – Sing for the moment</a>
                            <div style="clear:both;"></div>
                            <b>Sing for the <span>moment</span></b>
                        </div>';*/
            }
            /* */
            html += '</div></div>';
            $translate_block.html(html);
            $translate_block.fadeIn(function(){
                $('div.conten-middle-center-right-cont-bl-dict-audio').unbind('click');
                $('div.conten-middle-center-right-cont-bl-dict-audio').click(function(){
                    DICTIONARY.speak($word.user_word_id);
                }); 
            });
        });
        
    },
    
    dictionaryRemoveWord: function(id) {
        DICTIONARY.request('/profile/user/dictionary-remove-word', {
            word_id: id
        }, DICTIONARY.dictionaryWordRemoved);
    },
    
    dictionaryWordRemoved: function(data) {
        var result = $.parseJSON(data);
        
        if(result.type == 'success') {
            DICTIONARY.dictionaryHideWord(result.word_id);
        }
        else {
            alert('Неопределеная ошибка при удалении слова!');
        }
    },
    
    dictionaryHideWord: function(word_id) {
        $("#word_"+word_id).slideUp('slow');  
    },
    
    //--------------------------------------------------------------------------
    dictionaryLoaded: function(data) {
        var $history = $.parseJSON(data);
        DICTIONARY.words = $history.data;
        DICTIONARY.lingua_id = $history.lingua_id;
        
        var html = "";
        $.each($history.data, function(key, value) {
            html += DICTIONARY.dictionaryDrawLeftSide(map[key], value);
        });
        
        $translate_block.fadeOut('slow');
        
        $left_block.fadeOut('slow', function(){
            $left_block.html(html);
            $left_block.fadeIn('slow', function(){
                DICTIONARY.init();
            });
            
        });
    },
    
    //--------------------------------------------------------------------------
    dictionaryDrawLeftSide : function(title, data) {
        var html = '<h2>' + title;
        html += '<ul class="word_click">';
        
        if($.isEmptyObject(data)) {
            html += '<li class="words">' + '<p>не найдено</p>' + '</li>';
        }
        
        $.each(data, function(key, value) {
            html += '<li class="words" id="word_'+value.user_word_id+'">' + 
            '<p>' + value.word_src + '</p>' + 
            '<img src="/images/profile/dictionary/delete-word-dic.png" title="удалить" />' + 
            '</li>';
        });
        
        html += '</h2></ul>';
        
        return html;
    },
    
    //--------------------------------------------------------------------------
    speak: function(word_id) {
        var flashObj = DICTIONARY.getFlashMovieObject("myFlash");
        flashObj.play_sound(word_id);
    },
    
    getFlashMovieObject: function(movieName){
        if (window.document[movieName]){
            return window.document[movieName];
        }
        if (navigator.appName.indexOf("Microsoft Internet")==-1){
            if (document.embeds && document.embeds[movieName])
                return document.embeds[movieName];
        }
        else{
            return document.getElementById(movieName);
        }
    }
}
