$(document).ready(function(){
    init_popup();
});
    
function init_popup() {
    $('#buble_right, #buble_left').each(function () {
        var distance = 10;
        var time = 0;
        var hideDelay = 0;
 
        var hideDelayTimer = null;
 
        var beingShown = false;
        var shown = false;
        var trigger = $('.trigger', this);
        var info = $('.popup', this).css('opacity', 0);
            
        trigger.each(function(index, element) {
            $([element, info.get(0)]).mouseover(function () {
                if(this.id == undefined && this.id == "") return;
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    var lingua_id = this.id;
                        
                    info.html("");
                    beingShown = true;
                    var pos = $(element).parent().position();
                    info.css({
                        top: pos.top + 15,
                        left: pos.left + 15,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                        
                    $.post('/profile/user/user-get-short/lingua_id/'+this.id, function(data){
                        info.html(data);
                        init_links(lingua_id);
                    // reset position of info box
                    }); 
                }
                return;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });
 
                }, hideDelay);
 
                return false;
            });
        });
                     
    });
}
        
function init_links(lingua_id) {
    $('a.friend_request').click(function(e){
        e.preventDefault();
        var img = $(this).find('img');
        img.attr('src', '/images/community/ajax-loader.gif');
            
        $.post('/community/community/friend-request/to/'+lingua_id, function(data){
            if(data == 'success') {
                img.attr('src', '/images/community/activ-stat.png');
            }
            else if(data == 'removed') {
                img.attr('src', '/images/community/add-friends-icon.png');
            }
            
            $(this).removeClass('friend_request');
        })
    });
        
    $('a.friend_remove').click(function(e){
        e.preventDefault();
        var img = $(this).find('img');
        img.attr('src', '/images/community/ajax-loader.gif');
            
        $.post('/community/community/friend-remove/to/'+lingua_id, function(data){
            if(data == 'success') {
                img.attr('src', '/images/community/add-friends-icon.png');
            }
            $(this).removeClass('friend_remove');     
            
            window.location.href = '/friends';
        })
    });
    
    $('a.block_user').click(function(e){
        e.preventDefault();
        
        var img = $(this).find('img');
        img.attr('src', '/images/community/ajax-loader.gif');
        
        $.post('/community/community/friend-block/to/'+lingua_id, function(data){
            if(data == 'success') {
                img.attr('src', '/images/community/add-friends-icon.png');
            } 
            $(this).removeClass('block_user');     
        })
    });
    
    $('a.new_message').click(function(e){
        e.preventDefault();
    });
}
    
    
$("[name='user_learning']").change(function(e){
    $.post('/community/community/user-get-learning/lang/' + $(this).val(), function(data) {
        $("div.conten-middle-center-right-cont").html(data);
        init_popup();
    });
});
    
$("[name='user_know']").change(function(e){
    $.post('/community/community/user-get-know/lang/' + $(this).val(), function(data) {
        $("div.conten-middle-center-left-cont").html(data);
        init_popup();
    });
});