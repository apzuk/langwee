var $mb = [$('#li_1'), $('#li_2'), $('#li_3')]; //menu buttons
var $mc = [$('#div_1'), $('#div_2'), $('#div_3')]; //menu container div
var $fu = ['/api/resources/resource-audio-filter', '/api/resources/resource-video-filter', '/api/resources/resource-category-filter']; //filter url
var $au = ['/api/resources/resource-audio-artists-get', '/api/resources/resource-video-artists-get']; //artists filter url

var $left_content = $('div.conten-middle-center-left');
var $right_content = $('div.conten-middle-center-right');
var $top10 = $('div.conten-middle-presentn-txt a');
var $lang = $('div.conten-middle-lang');
var $all_aud = $('a.all_audios');
var $all_vid = $('a.all_videos');
var $uall = ['/api/resources/resource-audio-get-all', '/api/resources/resource-video-get-all', '/api/resources/resource-category-get-resources']; //arists resources get
var $search = $('input[name="searching"]');
var $search_btn = $('input[name="find"]');

var r = {
    lstatus: [false, false, false], // is data already loaded?!
    // if it is we should only show div without requesting data
                                    
    bstatus: [false, false, false], // busy status , is it already send the request?!
    sletter: [false, false, false], // selected letter
    result:  [false, false, false],
    
    run: function() {
        //video, audio, categories buttons initilizing...
        $.each($mb, function(i, e) {
            $(e).click(function() {
                //data already exists
                if(r.lstatus[i]) {
                    //close all shown divs
                    var st = false;
                    $.each($mc, function(k, s) {
                        //if div is shown kill him!!!
                        if(s.is(":visible")) {
                            st = true; 
                            s.slideUp('slow', function() {
                                if(s != $mc[i]) $mc[i].slideDown('slow');
                            });
                        }
                    });
                    
                    if(!st) {
                        $mc[i].slideDown('slow');
                    }
                    return;
                }
               
                //request for data
                $.post($fu[i], function(data) {
                    var st = false;
                    
                    r.lstatus[i] = true;
                    
                    //close all shown divs
                    $.each($mc, function(k, s) {
                        //if div is shown kill him!!!
                        if(s.is(":visible")) {
                            st = true; 
                            //yeah , I found it and killing!
                            s.slideUp('slow', function() {             
                                $mc[i].html(data);
                                $mc[i].slideDown('slow');
                    
                                r.sletter[i] = $('a.alpabet.selected',$mc[i]).attr('alt');
                                r.init($mc[i], i);
                                r.pinit($mc[i], i);
                                r.ainit($mc[i], i);
                            });
                        }
                    });
                    
                    //if nobody has been kill just show selected
                    if(!st) {
                        $mc[i].html(data);
                        $mc[i].slideDown('slow');
                        
                        r.sletter[i] = $('a.alpabet.selected',$mc[i]).attr('alt');
                    
                        r.init($mc[i], i);
                        r.pinit($mc[i], i);
                        r.ainit($mc[i], i);
                    }
                });
            });
        });
        
        $top10.click(function(e) {
            e.preventDefault();
            
            $left_content.css({
                opacity: 0.55
            });
            $right_content.css({
                opacity: 0.55
            });
            
            var b = false;
            $.each($mc, function(i, o) {
                if(o.is(":visible")) {
                    b = true;
                    $(o).slideUp('slow', function() {
                        r.load_top10();
                    });
                }
            });
            
            if(!b) {
                r.load_top10();
            }
        });
        
        //languages switcher init
        $('a', $lang).each(function(i, e) {
            $(e).click(function(event) {
                event.preventDefault();
                
                $.post('/api/global/global-change-language', {
                    short_name: this.id
                }, function(data) {
                    if(data.result == 'success') {
                        window.location.href = data.redirect;
                    }
                    else if(data.result == 'fail') {
                    //show somehow returned reason of the fail
                    //data.reason
                    }
                }, 'json');
            });
        });
        
        $all_aud.click(function(event) {
            event.preventDefault();
            r.rgetall($uall[0], {}, "r.apinit");
        });
        
        $all_vid.click(function(event) {
            event.preventDefault();
            r.rgetall($uall[1], {}, "r.apinit");
        });
        
        $search.focus(function(e) {
            e.preventDefault();
            $(this).removeClass('off').addClass('on');
            
            
            if($(this).val() == "Поиск: введите название") {
                $(this).val('');
            }
        }).blur(function(e){
            e.preventDefault();
            $(this).removeClass('on').addClass('off');
            
            var s = $(this).val();
            if(s.replace(/\s+/g, '') == "") {
                $(this).val('Поиск: введите название');
            }
            
        });
        
        $.ui.autocomplete.prototype._renderItem = function(ul, item){
            var term = this.term;
            var resource = item.resource == 'video' ? "(видео)" : "(аудио)"; 
            var re = new RegExp("(" + term + ")", "gi") ;
            var t = item.label.replace(re,"<i style='font-weight: bold; font-size: 13px; color:#b1182a'>$1</i>");
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a href='/video/" + item.resource_id + "' target='_blank'>" + t + 
                " <span style='font-size: 10px;  color:#b1182a'>" + resource + "<span></a>" )
            .appendTo( ul );
        };
        
        $search.keyup(function(e) {
            if(e.which == 13) {
                //close menu
                $.each($mc, function(k, s) {
                    //if div is shown kill him!!!
                    if(s.is(":visible")) {
                        st = true; 
                        //yeah , I found it and killing!
                        s.slideUp('slow');
                    }
                });
                
                r.search({
                    val: $search.val()
                });
            }
        });
        
        $search_btn.click(function(e) {
            
            //close menu
            $.each($mc, function(k, s) {
                //if div is shown kill him!!!
                if(s.is(":visible")) {
                    st = true; 
                    //yeah , I found it and killing!
                    s.slideUp('slow');
                }
            });
            
            e.preventDefault();
            r.search({
                val: $search.val()
            });
        });
        
        $search.autocomplete({
            minLength: 2,
            open: function(){
                $(this).autocomplete('widget').css('z-index', 10000000);
                return false;
            },
            
            source: function(request, response) {
                jQuery.ajax({
                    url: "/api/resources/search",
                    type: "post",
                    dataType: "json",
                    data: {
                        q: $search.val()
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.title,
                                resource_id: item.resource_id,
                                resource: item.resource
                            }
                        }));
                    }
                    
                })
            },
            
            select: function(event, ui) {
                window.location.href = '/' + ui.item.resource + '/' + ui.item.resource_id;
            }
        })
    },
    
    rgetall: function(url, params, callback) {
        //loadin effect
        $left_content.css({
            opacity: 0.55
        });
        $right_content.css({
            opacity: 0.55
        });
        
        $.post(url, params, function(data){
            //close all shown divs
            $.each($mc, function(k, s) {
                //if div is shown kill him!!!
                if(s.is(":visible")) {
                    st = true; 
                    //yeah , I found it and killing!
                    s.slideUp('slow');
                }
            });
            
            //fill left, right contents 
            $left_content.html(data.left);
            $right_content.html(data.right);
                
            //paging init
            eval(callback + "(" + data.page + ", " + data.total + ",'" + data.container + "', '" + params.artist_id + "')"); 
                
            $left_content.css({
                opacity: 1
            });
            $right_content.css({
                opacity: 1
            });
        }, 'json');
    },
    
    apinit: function(p, t, c, params) {
        if(params == "undefined") params = false;
       
        var side;
        var type;
        
        if(c == 'video') {
            side = 'right';
            type=1;
        }
        
        if(c == 'audio') {
            side = 'left';
            type=0;
        }
        
        if(c == 'video_cat') {
            c = 'video';
            side = 'right';
            type=2;
        }
        
        if(c == 'audio_cat') {
            c = 'audio';
            side = 'left';
            type=2;
        }
        $('div.conten-middle-center-' + side + '-cont',$right_content).append('<div align="center" style="width: 100%; margin-top: 15px;" id="' + c + '_paging"></div>');
        
        var $pagerItems = $('#' + c + '_paging');
        if(p != undefined && t != undefined) {
            var total = Math.ceil(t/20);
            
            if(total <= 1) return;
            
            var start = p >= 2 ? parseInt(p)-2 : 0;
            var pdraw = (total > 5 + start) ? 5 + parseInt(start) : total;
            
            $pagerItems.html('');
            
            if(start >= 1) {
                $pagerItems.append('<a href="#" click="return false" class="normal" alt="' + (parseInt(p) - 1) + '">' + '<' + '</a>');
            }
            
            for(var iterator = start; iterator < pdraw; iterator++) {
                if(iterator == p) {
                    $pagerItems.append('<a href="#" click="return false" alt="' + iterator + '" class="selected">' + (parseInt(iterator) + 1) + '</a>');
                }else {
                    $pagerItems.append('<a href="#" click="return false" class="normal" alt="' + iterator + '">' + (parseInt(iterator) + 1) + '</a>');
                }
            }
            
            if(pdraw < total) {
                $pagerItems.append('<a href="#" click="return false" class="normal" alt="' + (parseInt(p) + 1) + '">' + '>' + '</a>');
            }
        }
        
        $('a', $pagerItems).each(function(i, e) {
            $(e).unbind('click');
            $(e).click(function(event) {
                event.preventDefault();
                
                r.rgetall($uall[type], {
                    page: $(this).attr('alt'),
                    artist_id: params
                }, 'r.apinit');
            });
        });
        
    },
    
    load_top10: function() {
        $.post('/api/resources/top10', function(data) {
            
            //close menu
            $.each($mc, function(k, s) {
                //if div is shown kill him!!!
                if(s.is(":visible")) {
                    st = true; 
                    //yeah , I found it and killing!
                    s.slideUp('slow');
                }
            });
            
            //fill data
            $left_content.html(data.left);
            $right_content.html(data.right);
            
            $left_content.css({
                opacity: 1
            });
            $right_content.css({
                opacity: 1
            });
            
            //renew objects
            $all_aud = $('a.all_audios');
            $all_vid = $('a.all_videos');
            
            $all_aud.click(function(event) {
                event.preventDefault();
                r.rgetall($uall[0], {
                    artist_id:false
                }, "r.apinit");
            });
            
            $all_vid.click(function(event) {
                event.preventDefault();
                r.rgetall($uall[1], {
                    artist_id:false
                }, "r.apinit");
            });
        }, 'json');
    },
    
    //artists request 
    arequest: function(u, d, o, k, s){
        var $left = $('div.conten-middle-center-alfavit-ru-menu ul', o);
        var $right = $('div.conten-middle-center-alfavit-eng-menu ul', o);
        var $count = $('div.conten-middle-center-alfavit-page-count span', o);
        
        //if it's in requst do nothing!
        if(r.bstatus[k]) return;
        
        //set busy status
        r.bstatus[k] = true;
                
        //loading data effect
        $left.css({
            opacity: 0.5
        });
        $right.css({
            opacity: 0.5
        });
        
        
        $.post(u, d, function(data) {
            //data loaded
            $left.css({
                opacity: 1
            });
            $right.css({
                opacity: 1
            });
                    
            //set artists to their places
            $left.html(data.artists_left);
            $right.html(data.artists_right);
                    
            //set total found 
            $count.html(data.total);
            
            //it's already free
            r.bstatus[k] = false;
            //evel callback
            
            //init artists link click event
            r.ainit(o, k);
            
            //init paging
            r.pinit(o, k, data.page, data.total);
        }, 'json');
    },
    
    init : function(o, k) {
        //alphabet letters click
        $('a.alpabet', o).each(function(i, e) {         
            $(e).click(function(event) {
                event.preventDefault();                
                if(r.bstatus[k]) return;
                
                r.sletter[k] = $(this).attr('alt');
                                
                //uncheck old selected letter
                $('a.alpabet.selected', o).removeClass('selected');
                $(this).addClass('selected');
                
                r.arequest($au[k], {
                    letter: $(this).attr('alt')
                }, o, k, $(this));
            });
        });
    },
    
    pinit: function(o, k, p, t) {
        var $pagerItems = $('span.paging', o);
        if(p != undefined && t != undefined) {
            $pagerItems.html('');
            var total = Math.ceil(t/10);
            
            var start = p >= 2 ? parseInt(p)-2 : 0;
            var pdraw = (total > 5 + start) ? 5 + parseInt(start) : total;
            
            $pagerItems.html('');
            
            if(start >= 1) {
                $pagerItems.append('<a href="#" click="return false" alt="' + (parseInt(p) - 1) + '">' + '<' + '</a>');
            }
            
            for(var iterator = start; iterator < pdraw; iterator++) {
                if(iterator == p) {
                    $pagerItems.append('<a href="#" click="return false" alt="' + iterator + '" class="selected">' + (parseInt(iterator) + 1) + '</a>');
                }else {
                    $pagerItems.append('<a href="#" click="return false" alt="' + iterator + '">' + (parseInt(iterator) + 1) + '</a>');
                }
            }
            
            if(pdraw < total) {
                $pagerItems.append('<a href="#" click="return false" alt="' + (parseInt(p) + 1) + '">' + '>' + '</a>');
            }
        }
        
        $('a', $pagerItems).each(function(i, e) {
            $(e).unbind('click');
            $(e).click(function(event) {
                event.preventDefault();
                
                r.arequest($au[k], {
                    letter: r.sletter[k],
                    page: $(this).attr('alt')
                }, o, k);
            });
        });
    },
    
    ainit: function(o, k) {
        var class_name = "artists";
        if(k == 2) class_name = "res_cat";
        
        $('a.' + class_name, o).each(function(i, e) {
            $(e).click(function(event) {
                event.preventDefault();
                var params = {
                    artist_id: $(this).attr('src')
                };
                
                r.rgetall($uall[k], params, 'r.apinit');
            });
        });
    } ,
    
    search: function(param) {
        $('ul.ui-autocomplete').slideUp();
        
        if(param.val.replace(/\s+/g, '-') == '') return;
        
        //loadin effect        
        if(typeof param.type == undefined) {
            $left_content.css({
                opacity: 0.55
            });

            $right_content.css({
                opacity: 0.55
            });
        }
        else {
            if(param.type == 'audio') {
                $left_content.css({
                    opacity: 0.55
                });
            }
            if(param.type == 'video') {
                $right_content.css({
                    opacity: 0.55
                });
            }
        }
        
        $.post('/api/resources/search-by', param , function(data) {
            $('ul.ui-autocomplete').slideUp();
            
            
            //loadin effect
            $left_content.html(data.left);
            $right_content.html(data.right);
            
            $left_content.css({
                opacity: 1
            });
            $right_content.css({
                opacity: 1
            });
            
            //rearch result paging
            
            if(data.left_count != 0 && data.left_count!=undefined) {
                r.sinit($left_content, data.left_count, data.left_page, 'left', param.val);
            }
            
            if(data.right_count != 0 && data.right_count!=undefined) {
                r.sinit($right_content, data.right_count, data.right_page, 'right', param.val);
            }
        }, 'json');
    },
    
    sinit: function(o, t, p, s, v) {
        //resource by side
        var rc;
        if(s == 'left') rc = 'audio';
        if(s == 'right') rc = 'video';
        
        $('div.conten-middle-center-' + s + '-cont').append('<div align="center" style="width: 100%; margin-top: 15px;" id="' + rc + '_paging"></div>');
        var $pagerItems = $("#" + rc + "_paging");
        
        if(p != undefined && t != undefined) {
            var total = Math.ceil(t/10);
            if(total <= 1) return;
            
            var start = p >= 2 ? parseInt(p)-2 : 0;
            var pdraw = (total > 5 + start) ? 5 + parseInt(start) : total;
            
            $pagerItems.html('');
            
            if(start >= 1) {
                $pagerItems.append('<a href="#" click="return false" class="normal" alt="' + (parseInt(p) - 1) + '">' + '<' + '</a>');
            }
            
            for(var iterator = start; iterator < pdraw; iterator++) {
                if(iterator == p) {
                    $pagerItems.append('<a href="#" click="return false" alt="' + iterator + '" class="selected">' + (parseInt(iterator) + 1) + '</a>');
                }else {
                    $pagerItems.append('<a href="#" click="return false" class="normal" alt="' + iterator + '">' + (parseInt(iterator) + 1) + '</a>');
                }
            }
            
            if(pdraw < total) {
                $pagerItems.append('<a href="#" click="return false" class="normal" alt="' + (parseInt(p) + 1) + '">' + '>' + '</a>');
            }
        }
        
        $('a', $pagerItems).each(function(i, e) {
            $(e).unbind('click');
            $(e).click(function(event) {
                
                event.preventDefault();
                var params = {
                    page: $(this).attr('alt'),
                    count: t,
                    val: v,
                    type: rc
                };
                
                r.search(params);
            });
        });
    }
}