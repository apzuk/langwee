var d = {
    render: function(message, action, title) {
        $mask = $("#mask");
        $dialog = $("#message");
        
        $mask.css({
            left:0, 
            top:0, 
            width: $(document).width()+'px', 
            height: $(document).height()+"px"
        });
        
        $dialog.find('.body').css({
            height: (200-24)+'px'
        })
        $dialog.css({
            height: 200+'px'
        });
        
        var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
        var hPos = ($(document).width()-$dialog.width())/2;
        
        $dialog.css({
            left: hPos, 
            top: vPos
        });
        if(title != undefined) {
            $('div.header p.title').html(title);
        }
        
        message+="<br /><br /><a href='/payment' class='button'>Получить Premium!</a>";
        message+="<a href='' class='button'>Добыть звезды!</a>";
        
        $('p.text', $dialog).html(message);
            
        $mask.show();
        $dialog.fadeIn();
        
        $(document).unbind('scroll');
        $(document).scroll(function() {
            var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
            var hPos = ($(document).width()-$dialog.width())/2;
        
            $dialog.css({
                left: hPos, 
                top: vPos
            });
        });
        
        $('a.close', $dialog).unbind('click');
        $('a.close', $dialog).click(function(e) {
            e.preventDefault();
            $mask.fadeOut();
            $dialog.fadeOut();
            
            if(action != undefined) {
                $.post('/video/page/close-message', {
                    action_name: action
                });
            }
        });
    },
    
    show: function(height) {
        $mask = $("#mask");
        $dialog = $("#message");
        
        $mask.css({
            left:0, 
            top:0, 
            width: $(document).width()+'px', 
            height: $(document).height()+"px"
        });
        
        $dialog.find('.body').css({
            height: height+'px'
        });
        
        $dialog.css({
            height: height+'px'
        });
        
        var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
        var hPos = ($(document).width()-$dialog.width())/2;
        
        $dialog.css({
            left: hPos, 
            top: vPos
        });
        
        $(document).unbind('scroll');
        $(document).scroll(function() {
            var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
            var hPos = ($(document).width()-$dialog.width())/2;
        
            $dialog.css({
                left: hPos, 
                top: vPos
            });
        });
        
        $mask.show();
        $dialog.fadeIn();
        
        $('a.close', $dialog).unbind('click');
        $('a.close', $dialog).click(function(e) {
            e.preventDefault();
            $mask.fadeOut();
            $dialog.fadeOut();
            
            if(typeof action != 'undefined') {
                $.post('/video/page/close-message', {
                    action_name: action
                });
            }
        });
    }
}