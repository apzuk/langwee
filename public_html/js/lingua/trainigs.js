var $lang = $('div.conten-middle-lang');

var TRAININGS = {
    lingua_id: -1,
    lang: 'en',
    key: -'',
    trainings: new Array(),
    
    run: function(lingua_id, lang, key) {
        TRAININGS.key = key;
        TRAININGS.lingua_id = lingua_id;
        TRAININGS.lang = lang;
        TRAININGS.trainings = new Array();
        TRAININGS.trainings['word-translate'] = 'WT';
        TRAININGS.trainings['translate-word'] = 'TW';
        TRAININGS.trainings['constructor'] = 'C';       
        TRAININGS.trainings['listening'] = 'A';       
        TRAININGS.trainings['phrase-translate'] = 'PT';   
        TRAININGS.trainings['translate-phrase'] = 'TP'; 
        TRAININGS.trainings['spelling'] = 'S'; 
        TRAININGS.trainings['listening-translating'] = 'AT';  
         
        TRAININGS.initLang(true);
        
        $('div.conten-middle-center div.conten-middle-center-cont a.tr_item').each(function(index, element){
            $(element).click(function(e){  
                if(this.id != "resources") e.preventDefault();
                
                if($(element).attr('href') == "translate-phrase-favourite" || $(element).attr('href') == "phrase-translate-favourite" || $(element).attr('href') == "crossword") {
                    var message="<span style='font-size: 15px; font-family: Helvetica; text-align:center'>В данный момент тренинг модернизируется, как только закончатся работы, Вам придет оповещение на Ваш e-mail.</span>";
        
                    TRAININGS.showMessage(message, "Информация", true);
                    return;
                }
                
                $("#classContainer").html('<link rel="stylesheet" href="/css/training/all/' + $(element).attr('href') + '.css" type="text/css" media="screen, projection" />');
                
                var loading = $(this).find('img.loading');
                loading.css('top' , '-50px');
                $('div.conten-middle-center-back-bottom').css('margin-top', '0');
                TRAININGS.load($(element).attr('href'), loading); 
            });
        });
        
        $("#scroll1").jScrollPane();
    },
     
    initLang: function(status) {
        $(".conten-middle-lang a").each(function(index, element){
            $(element).unbind('click');
            if(!status) {
                return; 
            }
            
            //languages switcher init
            $('a', $lang).each(function(i, e) {
                $(e).click(function(event) {
                    event.preventDefault();
                
                    $.post('/api/global/global-change-language', {
                        short_name: this.id
                    }, function(data) {
                        if(data.result == 'success') {
                            window.location.href = '/trainings';
                        }
                        else if(data.result == 'fail') {
                        //show somehow returned reason of the fail
                        //data.reason
                        }
                    }, 'json');
                });
            });
        });  
    },
    
    load : function(name, obj) {
        $.post('/' + name, {
            clear: true,
            lang: TRAININGS.lang  
        } , function(data) { 
            var training = TRAININGS.trainings[name];
            obj.css('top', '10000px');
            $('div.jspTrack').hide();
            $('#container').animate({ 
                left: '-1000px'
            }, 800, function(){
                TRAININGS.initLang(false);
                eval(training + ".run(data)");
                
                $('div.jspTrack').fadeIn();
            }); 
        });
    },
    
    
    error: function() {
        var $main = $("#training div.conten-middle-center");
        var $error = $('#error');
        var $resources = $('#resources');
        var $container = $('#container');
        $resources.unbind('click');
        $resources.click(function(e) {
            $container.animate({
                left: '0'
            }, 1200, function(){
                
                }); 
                
            $error.animate({
                opacity: 0, 
                left: -2000
            }, 'slow', function(){
                });
        });
        
        
        var posX = $main.width()/2 - $error.width()/2;
        var posY = $main.height()/2 -  $error.height()/2 - 150;
        
        $error.css({
            left: posX, 
            top: posY
        });
        
        //var interval = setInterval(WT.position, 1);
        $error.animate({
            // width: 250, 
            // height: 80,
            opacity: 1
        }, 'slow', function(){
            //     clearInterval(interval);
            });
    },
    
    
    callExternalInterface: function(path) {
        var flashObj = TRAININGS.getFlashMovieObject("myFlash");
        flashObj.play_sound(path);
    }, 
            
    getFlashMovieObject:function(movieName){
        if (window.document[movieName]){
            return window.document[movieName];
        }
        if (navigator.appName.indexOf("Microsoft Internet")==-1){
            if (document.embeds && document.embeds[movieName])
                return document.embeds[movieName];
        } 
        else{
            return document.getElementById(movieName);
        }
    },
    
    voiceHandler: function(id) {
        TRAININGS.callExternalInterface(id);
    },
    
    report: function(type) {
        $('div.conten-middle-center-back-bottom').css('margin-top', '30px');
        
        var history = eval(type+'.history');
        var $trainings = $("#training");
        var $main = $("div.conten-middle-center", $trainings);
        
        var html = '<div class="conten-middle-center-tren1">'+
        '<center><h4>Слово перевод: результат тренировки</h4></center>'   +
        '<div class="conten-middle-center-tren1-tblock">'+
        '<div class="conten-middle-center-tren-report">'+
        '<center>'+
        '<table>';
        
        var count = 0;
        $.each(history, function(key, value) {
            if(value.status == true) count++;
            
            html += '<tr>';
            if(value.status == true) {
                html += '<td width="5%"><img src="/images/training/report/true-res-tren.png" /></td>';
            }
            else {
                html += '<td width="5%"><img src="/images/training/report/false-res-tren.png" /></td>';
            }
            
            html += '<td width="70%"><a href="#">' + value.word_src + '</a><span>';
            
            if(value.word_trg != undefined) {
                html += ' - ' + value.word_trg;
            }
            
            html += '</span></td>' + '<td width="5%">';
            
            if(value.word_trg != undefined) {
                html += '<img src="/images/training/report/result-tren-auido.png" class="audirovanie" title="Прослушать" id="img_' + value.word_id + '" />';

            }
        
            html += '</td>'+'<td width="5%" class="choose"><input type="checkbox" value="" id="' + value.word_id + '" class="choose" '+ 
            ((value.status == true) ? 'checked' : "")
            +' style="margin-top: -2px;" /></td>' +
            '</tr>';   
        });
        
        html += '</table>'+
        '</center>'+
        '</div>'+    
        '<div class="conten-middle-center-tren1-tblock-bottom">'+
        '<center>'+
        '<p>Верных ответов: ' + count + ' из 10</p>'+
        '<div class="conten-middle-center-tren1-tblock-bottom-continue">'+
        '<a href="#">Продолжить тренировку</a>'+ 
        '</div>'+
        '</center>'+
        '</div>'+ 
        '</div>'+ 
        '</div>';
    
        $main.html('');
        $main.append(html);
            
        $trainings.fadeIn('slow');
        eval(type + ".history = new Array();");
        eval(type + ".answer = false;");
        
        TRAININGS.initReport();
    },
    
    initReport: function() {
        var busy = false;
        
        $('.audirovanie').each(function(i, e) {
            $(e).click(function(event) {
                event.preventDefault();
                
                var idstr = e.id.split('_');
                TRAININGS.voiceHandler(idstr[1]);
            });
        });
        
        $('input.choose').each(function(index, element) {
            $(element).unbind('mouseover');
            $(element).unbind('mouseout');
            
            $(element).mouseover(function() {
                if(busy) return;
                
                busy = true;
                var p = $(this).position();
                $('#popup').css({
                    left: (p.left + 5 + $(this).width())
                });
                $('#popup').css({
                    top: p.top - $(this).height()/2
                });
                $('#popup').fadeIn('slow', function(){
                    busy = false;
                });
            });
            
            $(element).mouseout(function() {       
                busy = false;
                $('#popup').fadeOut(0, function() {
                    });
            });
        });
        
        $('div.conten-middle-center-tren1-tblock-bottom-continue a').click(function(e){
            e.preventDefault();
            
            var array = new Array();
            $("input.choose:checked").each(function(index, element) {
                array.push(new Array(element.id));
            });
            
            $.post('/trainings/training/store', {
                result: array
            }, function(data) {
        
                $("#training").fadeOut();
                $('#container').animate({
                    left: '0'
                }, 1200, function(){
                    }); 
            });
        });
    },
    
    next: function(type) {
        
        $('div.conten-middle-center-tren1-tblock-bottom a.next').unbind('click');
        $('div.conten-middle-center-tren1-tblock-bottom a.next').click(function(e) {
            e.preventDefault();
            var obj = new Array('result');
            
            var history = eval(type+".history");
            var right = eval(type+".right");
            var status = eval(type+".status");
            var word = eval(type+".word");
            var word_src = eval(type+".options[" + type + ".right]");
            var tries = eval(type+".tries");
            var answer = eval(type+".answer"); 
            
            //if nothing answered do nothing
            if(!answer) {
                return;
            }
                        
            obj['result'] = new Array(new Array(eval(type + ".right"), eval(type+".status")));
            
            history.push({
                word_src: word, 
                word_trg: word_src, 
                status: status,
                word_id: right
            });
                        
            if(tries >= 10) {
                TRAININGS.report(type);
                return;
            } 
            
            else if(answer) {
                $('#training').fadeOut('slow', function() {
                    eval(type+".load(obj)");
                    eval(type+".answer = false;");
                });
            }
        });
    },
    
    initMe: function(type) {
        var back_button = $('div.conten-middle-center-back-bottom a');
        back_button.unbind('click');
        back_button.click(function(e){
            e.preventDefault();
            
            if(!confirm("Вы действительно хотите покинуть тренинг?")) {
                return;
            }
            
            $("#training").fadeOut();
            $('#container').animate({
                left: '0'
            }, 1200, function(){
                TRAININGS.initLang(true);
            });
                
        // eval(type+".clean()");
        });
    },
    
    showMessage: function(message, title, textOnly) {
        $mask = $("#mask");
        $dialog = $("#message");
        
        $mask.css({
            left:0, 
            top:0, 
            width: $(document).width()+'px', 
            height: $(document).height()+"px"
        });
        
        $dialog.find('.body').css({
            height: (200-24)+'px'
        })
        $dialog.css({
            height: 200+'px'
        });
        
        var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
        var hPos = ($(document).width()-$dialog.width())/2;
        
        $dialog.css({
            left: hPos, 
            top: vPos
        });
        
        if(!textOnly) {
            message+="<br /><br /><a href='/payment' class='button'>Получить Premium!</a>";
            message+="<a href='' class='button'>Добыть звезды!</a>";
        }
        $('p.text', $dialog).html(message);
        $('p.title', $dialog).html(title);
        $mask.show();
        $dialog.fadeIn();
        
        $(document).unbind('scroll');
        $(document).scroll(function() {
            var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
            var hPos = ($(document).width()-$dialog.width())/2;
        
            $dialog.css({
                left: hPos, 
                top: vPos
            });
        });
        
        $('a.close', $dialog).unbind('click'); 
        $('a.close', $dialog).click(function(e) {
            e.preventDefault();
            $mask.fadeOut();
            $dialog.fadeOut(); 
        });
    }
}

//-----------------word translation-----------------
var WT = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'word-translate',
    status: false,
    history: new Array(),
    capture: 'Слово перевод',
    helps: 0,
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRAININGS.error(10);
            return;
        }
        WT.word = json.data.word;
        WT.right = json.data.right;
        WT.options = json.data.options;
        WT.tries = json.data.tries;
        WT.draw();
    },
    
    draw: function() {
        var $main = $("#training div.conten-middle-center-tren1-tblock");
        var $title = $("#training_title");
        $title.html('Выберите верный перевод по оригинальному слову');
        
        var html = '<div class="conten-middle-center-tren1-tblock-cont">'+
        '<div class="conten-middle-center-tren1-tblock-cont-lf">'+
        '<h1 style="padding-bottom: 8px;">' + WT.word + '</h1>'+
        '<p></p>'+'<div class="conten-middle-center-tren1-tblock-cont-lf-proizn">'+
        '<img src="/images/training/all/slovari-praiznesti-img.png" />'+'<a href="#">произнести</a>'+'</div></div>'+
        '<div class="conten-middle-center-tren1-tblock-cont-rt">'+
        '<ul>';
        $.each(WT.options, function(key, value) {
            html += '<li>'+
            '<a href="' + key + '" onclick="return false;"><nobr>' + 
            value + '</nobr></a></li>';
        });
        
        html+='<li style="margin-top:15px;"><a href="-1" onclick="return false;"><nobr>Не знаю</nobr></a></li>'+
        '</ul>'+'</div>'+'</div>'+'<div class="conten-middle-center-tren1-tblock-bottom">'+
        '<a href="#" onclick="return false;" class="help"><img src="/images/training/all/trenings/podskazka-button.png" /></a>'+
        '<a href="#" onclick="return false;" class="next"><img src="/images/training/all/trenings/next-button.png" /></a>'+
        '<center><p>' + WT.tries + ' из 10</p></center>';                 
    
        $main.html('');
        $main.prepend(html);
        var training = $('#training');
        training.css({
            left: 0,
            display: 'none'
        });
        
        training.fadeIn(800, function() {
            TRAININGS.voiceHandler(WT.right);
            WT.init();
        });
        
        TRAININGS.initMe('WT');
    },
     
    init: function() { 
        WT.helps = 0;
        var $main = $('div.conten-middle-center-tren1-tblock-cont-rt');
        var ids = new Array();
        $.each(WT.options, function(i,e) {
            if(i!=WT.right) {
                ids.push(i);
            }
        });
        
        TRAININGS.next('WT');
        $('ul li a', $main).each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {
                e.preventDefault();
                
                if(WT.answer) return;
                if($(this).attr('href') == WT.right) {
                    $(this).parent().removeClass('true');
                    $(this).parent().removeClass('false');
                    $(this).parent().addClass('true');
                    
                    WT.status = true;
                }
                else {
                    $("ul li a[href='" + WT.right + "']", $main).parent().addClass('true');
                    $(this).parent().addClass('false');
                    WT.status = false;
                }
                WT.answer = true;
            });
        });
        
        var listening = $('div.conten-middle-center-tren1-tblock-cont-lf-proizn a');
        
        listening.unbind('click');
        listening.click(function(e){
            e.preventDefault();
            TRAININGS.voiceHandler(WT.right);
        });
        
        $('a.help').click(function(e) {
            e.preventDefault();
            WT.helps++;    
            
            var item = ids[Math.floor(Math.random()*ids.length)];
            $("ul li a[href='" + item + "']", $main).parent().fadeOut();
            ids.remove(item);
                        
            if(WT.helps >= 2) {
                $(this).attr('disabled', 'disabled');
                $(this).css('opacity','0.5');
                $(this).unbind('click');
                return;
            }
            
        });
    },
    
    load : function(store) { 
        $.post('/' + WT.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            WT.run(data);
        });
    }
}

/*
 *  
 */
 
//-----------------word translation-----------------
var TW = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'translate-word',
    status: false,
    history: new Array(),
    capture: 'Обратный Слово перевод',
    helps: 0,
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRAININGS.error(10);
            return;
        }
        
        TW.word = json.data.word;
        TW.right = json.data.right;
        TW.options = json.data.options;
        TW.tries = json.data.tries;
        TW.draw();
    },
        
    draw: function() {
        var $main = $("#training div.conten-middle-center-tren1-tblock");
        var $title = $("#training_title");
        $title.html('Выберите верный перевод по оригинальному слову');
        
        var html = '<div class="conten-middle-center-tren1-tblock-cont">'+
        '<div class="conten-middle-center-tren1-tblock-cont-lf">'+
        '<h1 style="padding-bottom: 9px; line-height: 26px">' + TW.word + '</h1>'+
        '<p></p>'+'<div class="conten-middle-center-tren1-tblock-cont-lf-proizn">'+
        ''+''+'</div></div>'+
        '<div class="conten-middle-center-tren1-tblock-cont-rt">'+
        '<ul>';
        $.each(TW.options, function(key, value) {
            html += '<li>'+
            '<a href="' + key + '" onclick="return false;"><nobr>' + 
            value + '</nobr></a></li>';
        });
        html+='<li style="margin-top:15px;"><a href="-1"><nobr>Не знаю</nobr></a></li>'+
        '</ul>'+'</div>'+'</div>'+'<div class="conten-middle-center-tren1-tblock-bottom">'+
        '<a href="#" onclick="return false" class="help"><img src="/images/training/all/trenings/podskazka-button.png" /></a>'+
        '<a href="#" onclick="return false" class="next"><img src="/images/training/all/trenings/next-button.png" /></a>'+
        '<center><p>' + TW.tries + ' из 10</p></center>';
    
        $main.html('');
        $main.append(html);
        var $training = $('#training');
        if(TW.tries == 1) {
            $training.fadeIn();
            $training.animate({
                left: '0' 
            }, 1200, function() {
                //TRAININGS.voiceHandler(TW.right);
                TW.init();
            });
        }
        else {
            $training.fadeIn('slow', function() {
                // 
                TW.init();
            });
        }
        
        TRAININGS.initMe('TW');
    },
        
    init: function() {
        var $main = $("#training div.conten-middle-center");
        TW.helps = 0;
        TRAININGS.next('TW');
        
        var ids = new Array();
        $.each(TW.options, function(i,e) {
            if(i != TW.right) {
                ids.push(i);
            }
        });
        
        $('div.conten-middle-center-tren1-tblock-cont-rt ul li a').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {
                e.preventDefault();
                 
                if(TW.answer) return;
                if($(this).attr('href') == TW.right) {
                    $(this).parent().removeClass('true');
                    $(this).parent().removeClass('false');
                    $(this).parent().addClass('true');
                    
                    TW.status = true;
                }
                else {
                    $("div.conten-middle-center-tren1-tblock-cont-rt ul li a[href='" + TW.right + "']").parent().addClass('true');
                    $(this).parent().addClass('false');
                    TW.status = false;
                }
                TW.answer = true;
                
                TRAININGS.voiceHandler(TW.right);
            });
        });
        
        $('div.conten-middle-center-tren1-tblock-cont-lf-proizn a').click(function(e){
            e.preventDefault();
            $(this).unbind('click');
            TRAININGS.voiceHandler(TW.right);
        }); 
        
        $('a.help').click(function(e) {
            e.preventDefault();
            TW.helps++;    
            
            var item = ids[Math.floor(Math.random()*ids.length)];
            $("ul li a[href='" + item + "']", $main).parent().fadeOut();
            ids.remove(item);
                        
            if(TW.helps >= 2) {
                $(this).attr('disabled', 'disabled');
                $(this).css('opacity','0.5');
                $(this).unbind('click');
                return;
            }
        });
    },
    
    load : function(store) { 
        $.post('/' + TW.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            TW.run(data);
        });
    }
}

/*
 *  
 */
 
var PT = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'phrase-translate',
    status: false,
    history: new Array(),
    capture: 'Перевод фразы',
    helps: 0,
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRAININGS.error(10);
            return;
        }
                
        PT.word = json.data.word;
        PT.right = json.data.right;
        PT.options = json.data.options;
        PT.tries = json.data.tries;
        PT.draw();
    },
        
    draw: function() { 
        var $main = $("#training div.conten-middle-center");
        
        var html = '<center><h4>Выберите верный перевод по оригинальному слову</h4></center>'+
        '<div class="conten-middle-center-tren1-tblock">'+
        '<div class="conten-middle-center-tren1-tblock-cont">'+
        '<div class="conten-middle-center-tren1-tblock-cont-lf">'+
        '<h1 style="padding-bottom: 9px; line-height: 26px">' + PT.word + '</h1>'+
        '<p></p>'+'<div class="conten-middle-center-tren1-tblock-cont-lf-proizn">'+
        ''+''+'</div></div>'+
        '<div class="conten-middle-center-tren1-tblock-cont-rt">'+
        '<ul>'; 
    
        $.each(PT.options, function(key, value) {    
            var k = key.split('_'); 
            html += '<li>'+ 
            '<a href="' + k[1] + '" onclick="return false;"><nobr>' + 
            value + '</nobr></a></li>';
        }); 
        html+='<li style="margin-top:15px;"><a href="-1" onclick="return false;"><nobr>Не знаю</nobr></a></li>'+
        '</ul>'+'</div>'+'</div>'+'<div class="conten-middle-center-tren1-tblock-bottom">'+
        '<a href="#" onclick="return false;" class="help"><img src="/images/training/all/trenings/podskazka-button.png" /></a>'+
        '<a href="#" onclick="return false;" class="next"><img src="/images/training/all/trenings/next-button.png" /></a>'+
        '<center><p>' + PT.tries + ' из 10</p></center>'+
        '</div>';     
         
        $main.html('');
        $main.prepend(html);
        
        var training = $('#training');
        training.css({
            left: 0,
            display: 'none'
        });
        
        training.fadeIn(800, function() {  
            PT.init();
        });
        
        TRAININGS.initMe('WT');
    },
        
    init: function() {
        PT.helps++;
        
        var $main = $("#training div.conten-middle-center-tren1-tblock");
        TRAININGS.next('PT');
        
        var ids = new Array();
        $.each(PT.options, function(i, e) {
            var d = i.split('_');
            if(i != PT.right) {
                ids.push(d[1]);
            }
        });
        
        
        $('div.conten-middle-center-tren1-tblock-cont-rt ul li a').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {
                e.preventDefault();
                 
                if(PT.answer) return;
                if($(this).attr('href') == PT.right) {
                    $(this).parent().removeClass('true');
                    $(this).parent().removeClass('false');
                    $(this).parent().addClass('true');
                    
                    PT.status = true;
                }
                else {
                    $("div.conten-middle-center-tren1-tblock-cont-rt ul li a[href='" + PT.right + "']").parent().addClass('true');
                    $(this).parent().addClass('false');
                    PT.status = false;
                }
                PT.answer = true;
                
                TRAININGS.voiceHandler(PT.right);
            });
        });
        
        $('div.conten-middle-center-tren1-tblock-cont-lf-proizn a').click(function(e){
            e.preventDefault();
            $(this).unbind('click');
            TRAININGS.voiceHandler(PT.right);
        }); 
        
        $('a.help').click(function(e) {
            e.preventDefault();
            PT.helps++;    
            
            var item = ids[Math.floor(Math.random()*ids.length)];
            $("ul li a[href='" + item + "']", $main).parent().fadeOut();
            ids.remove(item);
                        
            if(TW.helps >= 2) {
                $(this).attr('disabled', 'disabled');
                $(this).css('opacity','0.5');
                $(this).unbind('click');
                return;
            }
        });
    },
    
    load : function(store) { 
        $.post('/' + PT.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            PT.run(data);
        });
    }
}

var TP = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'translate-phrase',
    status: false,
    history: new Array(),
    capture: 'Перевод фразы',
    helps: 0,
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRAININGS.error(10);
            return;
        }
                
        TP.word = json.data.word;
        TP.right = json.data.right;
        TP.options = json.data.options;
        TP.tries = json.data.tries;
        TP.draw();
    },
        
    draw: function() {
        var $main = $("#training div.conten-middle-center");
        
        var html = '<center><h4>Выберите верный перевод по оригинальному слову</h4></center>'+
        '<div class="conten-middle-center-tren1-tblock">'+
        '<div class="conten-middle-center-tren1-tblock-cont">'+
        '<div class="conten-middle-center-tren1-tblock-cont-lf">'+
        '<h1 style="padding-bottom: 9px; line-height: 26px">' + TP.word + '</h1>'+
        '<p></p>'+'<div class="conten-middle-center-tren1-tblock-cont-lf-proizn">'+
        ''+''+'</div></div>'+
        '<div class="conten-middle-center-tren1-tblock-cont-rt">'+
        '<ul>';
        $.each(TP.options, function(key, value) {  
            var k = key.split('_'); 
            html += '<li>'+
            '<a href="' + k[1] + '" onclick="return false;"><nobr>' + 
            value + '</nobr></a></li>';
        });
        html+='<li style="margin-top:15px;"><a href="-1" onclick="return false;"><nobr>Не знаю</nobr></a></li>'+
        '</ul>'+'</div>'+'</div>'+'<div class="conten-middle-center-tren1-tblock-bottom">'+
        '<a href="#" class="help" onclick="return false;"><img src="/images/training/all/trenings/podskazka-button.png" /></a>'+
        '<a href="#" class="next" onclick="return false;"><img src="/images/training/all/trenings/next-button.png" /></a>'+
        '<center><p>' + TP.tries + ' из 10</p></center>'+
        '</div>';     
         
        $main.html('');
        $main.prepend(html);
        var training = $('#training');
        training.css({
            left: 0,
            display: 'none'
        });
        
        training.fadeIn(800, function() {  
            TP.init();
        });
        
        TRAININGS.initMe('WT');
    },
        
    init: function() {
        var $main = $("#training div.conten-middle-center-tren1-tblock");
        TP.helps = 0;
        TRAININGS.next('TP');
        
        var ids = new Array();
        $.each(TP.options, function(i, e) {
            var d = i.split('_');
            
            if(i != TP.right) {
                ids.push(d[1]);
            }
        });
        
        $('div.conten-middle-center-tren1-tblock-cont-rt ul li a').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {
                e.preventDefault();
                 
                if(TP.answer) return;
                if($(this).attr('href') == TP.right) {
                    $(this).parent().removeClass('true');
                    $(this).parent().removeClass('false');
                    $(this).parent().addClass('true');
                    
                    TP.status = true;
                }
                else {
                    $("div.conten-middle-center-tren1-tblock-cont-rt ul li a[href='" + TP.right + "']").parent().addClass('true');
                    $(this).parent().addClass('false');
                    TP.status = false;
                }
                TP.answer = true;
                
                TRAININGS.voiceHandler(TP.right);
            });
        });
        
        $('div.conten-middle-center-tren1-tblock-cont-lf-proizn a').click(function(e){
            e.preventDefault();
            $(this).unbind('click');
            TRAININGS.voiceHandler(TP.right);
        }); 
        
        
        $('a.help').click(function(e) {
            e.preventDefault();
            TP.helps++;    
            var item = ids[Math.floor(Math.random()*ids.length)];
            $("ul li a[href='" + item + "']", $main).parent().fadeOut();
            ids.remove(item);
                        
            if(TP.helps >= 2) {
                $(this).attr('disabled', 'disabled');
                $(this).css('opacity','0.5');
                $(this).unbind('click');
                return;
            }
        });
    },
    
    load : function(store) { 
        $.post('/' + TP.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            TP.run(data);
        });
    }
}
 
//-----------------constructor-----------------
var C = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'constructor',
    status: false,
    history: new Array(),
    capture: 'Конструктор',
    helps: 0,
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRAININGS.error();
            return;
        }
        C.word = json.data.word;
        C.right = json.data.right;
        C.options = json.data.options;
        C.tries = json.data.tries;
        C.draw();
        
        $('div.conten-middle-center-tren1-tblock-cont-quest').animate({
            top: '-=20px',
            opacity: 1
        },  300);
        
        C.helps = 0;
    },
            
    draw: function() {
        var $main = $("#training div.conten-middle-center");
        var $error = $('#error');
        var word = eval("C.options['"+C.right+"']");  

        var wordRand = C.word.shuffle();
        wordRand = wordRand.shuffle();
        wordRand = wordRand.shuffle();
        
        var html = '<center><h4>Соберите оригинальное слово по его переводу</h4></center>' +
        '<div class="conten-middle-center-tren1-tblock">' + 
        '<div class="conten-middle-center-tren1-tblock-cont">' + 
        '<div class="conten-middle-center-tren1-tblock-cont-answer-wd" align="center">' + 
        '<ul style="width: '+wordRand.length*46+'px">';
        for(var i = 0; i< wordRand.length; i++) {
            html += '<li><p>?</p></li>';
        }
        html += '</ul>' + 
        '</div>' +  
        '<div class="conten-middle-center-tren1-tblock-cont-question-wd" align="center">' + 
        '<ul style="width: '+wordRand.length*46+'px">' ;
        for(i = 0; i< wordRand.length; i++) {
            html += '<li><p>' + wordRand[i] + '</p></li>';
        }
        html += '</ul>' + 
        '</div>' + 
        '<div class="conten-middle-center-tren1-tblock-cont-quest">' + 
        '<p>' + word + '</p>' + 
        '</div>' +  
        '</div>' +  
        '<div class="conten-middle-center-tren1-tblock-bottom">' +  
        '<a href="#" onclick="return false;" class="help"><img src="/images/training/all/trenings/podskazka-button.png" /></a>' + 
        '<a href="#" onclick="return false;" class="next"><img src="/images/training/all/trenings/next-button.png" /></a>' + 
        '<center><p>' + C.tries + ' из 10</p></center>' + 
        '</div>';
    
        $main.html('');
        $main.prepend(html);
        var training = $('#training');
        training.css({
            left: 0,
            display: 'none'
        });
        
        training.fadeIn(800, function() {  
            C.init(wordRand);
        });
        
        TRAININGS.initMe('C');
    },
    
    init: function(wrand) {
        TRAININGS.next('C');
        
        //the position of the word which will be replaced
        var currentActualPos = 0;
        
        //create an array with lenght of word 
        var status = new Array();
        for(var i = 0 ; i < C.word.length; i++) {
            status[i] = false;
        }
        
        //wrong answers count
        var wrongAnswer = 0;
        
        //handle click 
        $('div.conten-middle-center-tren1-tblock-cont-question-wd ul li p').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {
                //get clicked word
                var txt = $(this).text();
                
                //check if selected letter and letter of the word in actual position is same
                if(txt == C.word[currentActualPos]) {
                    //clear selected word in question cicle
                    $(this).text('');
                    
                    //fill it to answer
                    var t = $('div.conten-middle-center-tren1-tblock-cont-answer-wd ul li').get(currentActualPos);
                    $(t).find('p').text(txt);
                    
                    //the placae "currentActualPos" is busy
                    status[currentActualPos] = true;
                    currentActualPos++;
                    
                    while(status[currentActualPos]) {
                        currentActualPos++;
                        
                        if(currentActualPos > C.word.length) {
                            break;
                        }
                    }
                }
                else {
                    wrongAnswer++;
                    
                    //I don't know what it's!!!
                    if(wrongAnswer == 3) {
                        C.setRightAnswer();
                    } 
                }
                
                if(currentActualPos >= C.word.length) {
                    C.answer = true;
                    C.status = true;
                    TRAININGS.voiceHandler(C.right);
                    s
                }
            });
        });
        
        $('div.conten-middle-center-tren1-tblock-cont-lf-proizn a').click(function(e){
            e.preventDefault();
            $(this).unbind('click');
            TRAININGS.voiceHandler(C.right);
        }); 
        
        $('a.help').click(function(e) {
            e.preventDefault();
            C.helps++;    
            
            var letter = C.word[currentActualPos];            
            var s = $('div.conten-middle-center-tren1-tblock-cont-question-wd ul li');
            
            var b = false;
            $(s).find('p').each(function(i, e) {
                if($(e).text() == letter) {
                    if(b) return;
                    
                    $(e).text('');
                    b = true;
                    return;
                }
            });      
                        
            var t = $('div.conten-middle-center-tren1-tblock-cont-answer-wd ul li').get(currentActualPos);
            $(t).find('p').text(letter);
            
            status[currentActualPos] = true;
            currentActualPos++;
                        
            if(C.helps >= 2) {
                $(this).attr('disabled', 'disabled');
                $(this).css('opacity','0.5');
                $(this).unbind('click');
                return;
            }
        });
        
        TRAININGS.next('C');
    },
    
    setRightAnswer: function() {
        $('div.conten-middle-center-tren1-tblock-cont-answer-wd li').each(function(index, element) {
            $(this).find('p').text(C.word[index]);
        });
        
        C.answer = true;
        C.status = false;
        
        TRAININGS.voiceHandler(C.right);
    },
    
    load : function(store) { 
        $.post('/' + C.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            C.run(data);
        });
    }
}


//-----------------audirovanie-----------------
var A = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'listening',
    status: false,
    history: new Array(),
    capture: 'Аудирование',
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRAININGS.error();
            return;
        } 
        A.word = json.data.word;
        A.right = json.data.right;
        A.options = json.data.options;
        A.tries = json.data.tries;
        A.draw();
         
        $('div.conten-middle-center-tren1-tblock-cont-quest').animate({
            top: '-=20px',
            opacity: 1
        },  300);
    },
        
    draw: function() {
        var $main = $("#training div.conten-middle-center");
        var $error = $('#error');
        var word = eval("A.options['"+A.right+"']");  

        var wordRand = A.word.shuffle();
        wordRand = wordRand.shuffle(); 
        wordRand = wordRand.shuffle();            	
         
        var html = '<div class="">' +  
        '<center><h4>Введите оригинальное слово по произношению</h4></center>' +
        '<div class="conten-middle-center-tren1-tblock">' +   
        '<div class="conten-middle-center-tren1-tblock-cont">' +   
        '<center><a href="#"><img src="/images/training/all/listening-button.png" /></a></center>'+
        '<center>' + 
        '<div class="conten-middle-center-tren1-tblock-cont-text">' +  
        '<input type="text" size="40" name="text" id="answer" />' + 
        '</div>' + 
        '</center>'+
        '<center>'+ 
        '<div class="conten-middle-center-tren1-tblock-cont-title"><p>Прослушайте и напишите<span>(нажмите enter для проверки слово)</span></p>' + 
        '</div>' + 
        '<p id="right_answer">' + '' + '</p>' + 
        '<b id="wrong_answer">' + '' + '</b>' + 
        '</center>' + 
        '</div>' + 
        '<div class="conten-middle-center-tren1-tblock-bottom">' + 
        '<a href="#" class="help disabled" onclick="return false"><img src="/images/training/all/podskazka-button.png" /></a>'+
        '<a href="#" class="next" onclick="return false;"><img src="/images/training/all/next-button.png" /></a>'+
        '<center><p>' + A.tries + ' из 10</p></center>'+
        '</div>'+                 
        '</div>';
        
        $main.html('');
        $main.append(html);
        
        if(A.tries == 1) {
            $('#training').fadeIn();
            $('#training').animate({
                left: '0' 
            }, 1200, function() {
                TRAININGS.voiceHandler(A.right);
                A.init();
            }); 
        }
        else {
            $('#training').fadeIn('slow', function() {
                TRAININGS.voiceHandler(A.right);
                A.init();
            }); 
        }
        
        $("#answer").keydown(function(event) {
            if(event.which == 13) {
                var answer = $(this).val();
                if(answer.replace(/\s+/g, '') == "") {
                    $(this).val('');
                    $(this).focus();
                    return;
                }
                
                $("#right_answer").text(A.word + ' - ' + A.options[A.right]);
                if($(this).val() != A.word) {
                    $("#wrong_answer").text($(this).val());
                    A.status = false;
                } 
                else {
                    A.status = true;
                }
                 
                A.answer = true;
            }
        });
        
        
        TRAININGS.initMe('А'); 
    },
    
    init: function() {
        TRAININGS.next('A');
        
        var currentPos = 0;
        $('div.conten-middle-center-tren1-tblock-cont-question-wd ul li p').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {                
                var txt = $(this).text();
                if(txt == A.word[currentPos]) {
                    $(this).text('');
                    var t = $('div.conten-middle-center-tren1-tblock-cont-answer-wd ul li').get(currentPos);
                    $(t).find('p').text(txt);
                    currentPos++;
                }
                
                if(currentPos >= A.word.length) {
                    A.answer = true;
                    A.status = true;
                }
            });
        });
        
        
        $('div.conten-middle-center-tren1-tblock-cont img').click(function(e){
            e.preventDefault();
            TRAININGS.voiceHandler(A.right);
        }); 
        
    },
    
    load : function(store) { 
        $.post('/' + A.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            A.run(data);
        });
    }
}

//-----------------audirovanie perevod-----------------
var AT = {
    word: '', 
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'listening',
    status: false,
    history: new Array(),
    capture: 'Аудирование',
    
    run: function(data) {
        var json = $.parseJSON(data);
        if(json.empty == true) {
            TRAININGS.error();
            return;
        } 
        AT.word = json.data.word;
        AT.right = json.data.right;
        AT.options = json.data.options;
        AT.tries = json.data.tries;
        AT.draw();
         
        $('div.conten-middle-center-tren1-tblock-cont-quest').animate({
            top: '-=20px',
            opacity: 1
        },  300);
    },
        
    draw: function() {
        var $main = $("#training div.conten-middle-center");
        var $error = $('#error');
        var word = eval("AT.options['"+AT.right+"']");  

        var wordRand = AT.word.shuffle();
        wordRand = wordRand.shuffle(); 
        wordRand = wordRand.shuffle();            	
         
        var html = '<div class="">' +  
        '<center><h4>Введите перевод слова, которое воспроизводится</h4></center>' +
        '<div class="conten-middle-center-tren1-tblock">' +   
        '<div class="conten-middle-center-tren1-tblock-cont">' +   
        '<center><a href="#"><img src="/images/training/all/listening-button.png" /></a></center>'+
        '<center>' + 
        '<div class="conten-middle-center-tren1-tblock-cont-text">' +   
        '<input type="text" size="40" name="text" id="answer" />' + 
        '</div>' + 
        '</center>'+
        '<center>'+ 
        '<div class="conten-middle-center-tren1-tblock-cont-title"><p>Прослушайте и напишите<span>(нажмите enter для проверки слово)</span></p>' + 
        '</div>' + 
        '<p id="right_answer">' + '' + '</p>' + 
        '<b id="wrong_answer">' + '' + '</b>' + 
        '</center>' + 
        '</div>' + 
        '<div class="conten-middle-center-tren1-tblock-bottom">' + 
        '<a href="#" onclick="return false;" class="help disabled"><img src="/images/training/all/podskazka-button.png" /></a>'+
        '<a href="#" class="next"  onclick="return false;"><img src="/images/training/all/next-button.png" /></a>'+
        '<center><p>' + AT.tries + ' из 10</p></center>'+
        '</div>'+                 
        '</div>';
        
        $main.html('');
        $main.append(html);
        
        if(AT.tries == 1) {
            $('#training').fadeIn();
            $('#training').animate({
                left: '0' 
            }, 1200, function() {
                TRAININGS.voiceHandler(AT.right);
                AT.init();
            }); 
        }
        else {
            $('#training').fadeIn('slow', function() {
                TRAININGS.voiceHandler(AT.right);
                AT.init();
            }); 
        }
        
        $("#answer").keydown(function(event) {
            if(event.which == 13) {
                var answer = $(this).val();
                if(answer.replace(/\s+/g, '') == "") {
                    $(this).val('');
                    $(this).focus();
                    return;
                }
                
                $("#right_answer").text(AT.word + ' - ' + AT.options[AT.right]);
                if($(this).val() != AT.options[AT.right]) {
                    $("#wrong_answer").text($(this).val());
                    AT.status = false;
                } 
                else {
                    AT.status = true;
                }
                 
                AT.answer = true;
            }
        });
        
        
        TRAININGS.initMe('АТ'); 
    },
    
    init: function() {
        TRAININGS.next('AT');
        
        var currentPos = 0;
        $('div.conten-middle-center-tren1-tblock-cont-question-wd ul li p').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {                
                var txt = $(this).text();
                if(txt == AT.word[currentPos]) {
                    $(this).text('');
                    var t = $('div.conten-middle-center-tren1-tblock-cont-answer-wd ul li').get(currentPos);
                    $(t).find('p').text(txt);
                    currentPos++;
                }
                
                if(currentPos >= AT.word.length) {
                    AT.answer = true;
                    AT.status = true;
                }
            });
        });
        
        
        $('div.conten-middle-center-tren1-tblock-cont img').click(function(e){
            e.preventDefault();
            TRAININGS.voiceHandler(AT.right);
        }); 
        
    },
    
    load : function(store) { 
        $.post('/' + AT.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            AT.run(data);
        });
    }
}

var S = {
    word: '',
    right: -1,
    options: null,
    tries: 0,
    answer: false,
    name: 'spelling',
    status: false,
    history: new Array(),
    capture: 'Правописание',
    
    run: function(data) {
        var json = $.parseJSON(data);
        
        if(json.empty == true) {
            TRSININGS.error();
            return;
        } 
        S.word = json.data.word;
        S.right = json.data.right;
        S.options = json.data.options;
        S.tries = json.data.tries;
        S.draw();
         
        $('div.conten-middle-center-tren1-tblock-cont-quest').animate({
            top: '-=20px',
            opacity: 1
        },  300);
        
    /*var l = '';
        for(var i = 0 ; i < S.word.length ; i++) {  
            l+="*";
        }
        $('div.conten-middle-center-tren1-tblock-cont-text input').val(l);*/
    }, 
        
    draw: function() {
        var $main = $("#training div.conten-middle-center");
        var $error = $('#error');
        var word = eval("S.options['"+S.right+"']");  

        var wordRand = S.word.shuffle();
        wordRand = wordRand.shuffle(); 
        wordRand = wordRand.shuffle();            	

        var html = '<center><h4>Введите перевод слова в форму ниже</h4></center>' +
        '<div class="conten-middle-center-tren1-tblock">' +   
        '<div class="conten-middle-center-tren1-tblock-cont">' +   
        '<center>' + 
        '<div class="conten-middle-center-tren1-tblock-cont-quest">' +  
        '<p id="true">' + S.options[S.right] + '</p>' + 
        '</div>' + 
        '<p id="right" style="display: block;"></p>'+
        '<b id="wrong" style="display: block;"></b>'+ 
        '</center>'+ 
        '<center>'+
        '<div class="conten-middle-center-tren1-tblock-cont-text">' +
        '<input type="text" size="37" name="text" />' + 
        '</div>'+ 
        '</center>'+
        '<center>'+   
        '<div class="conten-middle-center-tren1-tblock-cont-title"><p></p></div>'+
        '</center>'+
        '</div>'+
        '<div class="conten-middle-center-tren1-tblock-bottom">' + 
        '<a href="#" onclick="return false;" class="help disabled"><img src="/images/training/all/podskazka-button.png" /></a>'+
        '<a href="#" class="next"  onclick="return false;"><img src="/images/training/all/next-button.png" /></a>'+
        '<center><p>' + S.tries + ' из 10</p></center>'+
        '</div>'+                 
        '</div>';
         
        $main.html('');
        $main.prepend(html);
        var training = $('#training');
        training.css({
            left: 0,
            display: 'none'
        });
        
        training.fadeIn(800, function() {  
            S.init();
        });
        
        TRAININGS.initMe('S'); 
    },
     
    init: function() { 
        TRAININGS.next('S');
        
        $('div.conten-middle-center-tren1-tblock-cont-text input').keyup(function(e){ 
            if(e.which == 13) {
                if($(this).val() == S.word) {
                    $('#right').text(S.word);
                    S.status = true;
                } 
                else {
                    $('#right').text(S.word);
                    $('#wrong').text($(this).val());
                    S.status = false;
                }
                
                S.answer = true; 
                TRAININGS.voiceHandler(S.right);
            }
        });
    },
    
    load : function(store) { 
        $.post('/' + S.name,{
            lang: TRAININGS.lang, 
            store: store['result'] 
        }, function(data) {
            S.run(data);
        });
    }
}

String.prototype.shuffle = function () {
    var a = this.split(""),
    n = a.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}

Array.prototype.shuffle = function() {
    var len = this.length;
    var i = len;
    while (i--) {
        var p = parseInt(Math.random()*len);
        var t = this[i];
        this[i] = this[p];
        this[p] = t;
    }
};

Array.prototype.remove= function(){
    var what, a= arguments, L= a.length, ax;
    while(L && this.length){
        what= a[--L];
        while((ax= this.indexOf(what))!= -1){
            this.splice(ax, 1);
        }
    }
    return this;
}

Array.prototype.find = function(value) {
    var ctr = "";
    for (var i=0; i < this.length; i++) {
        // use === to check for Matches. ie., identical (===), ;
        if (this[i] == value) {
            return i;
        }
    }
    return ctr;
}