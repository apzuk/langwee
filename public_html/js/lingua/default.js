//login popup windows
var $login_win = $("#content-login-popup");
var $remember_win = $('#content-remember-popup');

//registration form login and password fields selector
var $email_reg = $("#email");
var $reg_pass = $("#pass");
var $reg_new_password_field = null;
var $reg_pass_cont = $("#reg_pass_container");

//login and send to login buttons
var $login_link = $("#login_link"); //кнопка вход 
var $send_login = $("#send_login"); //кнопка войти

//login form login passwords
var $email_login = $("#login_email");
var $pass_login = $("#login_pass");
var $login_error = $("span.login_error");
var $login_new_passwd_field = null;
var $pass_login_cont = $("span.login_pass_container", $login_win);

//registration button
var $reg_btn = $("#reg_button");  
var $reg = $("#reg");

var langwee = {
    run: function() {
        /* Yandex.Metrika counter */
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter14186416 = new Ya.Metrika({
                        id:14186416, 
                        enableAll: true, 
                        webvisor:true
                    });
                } catch(e) {}
            });
            
            var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
            
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
        /*Yandex.Metrika counter*/
        
        $('#reg_button').click(function() {
            var bottom = $(window).height();
            window.scrollBy(0, bottom);
            $email_reg.focus();
        });
        
        //community menu drop down handling
        $('#li_m4').toggle(function(){
            $('#div_drob_menu').slideDown();
        }, function(){
            $('#div_drob_menu').slideUp();
        });
        
        //password remember link
        $('.passwd-remember').click(function(event) {
            event.preventDefault();
            $login_win.slideUp('normal', function() {
                //login button positions
                
                $('#remember_email').val('');
                $('#remember_email').focus();
                if($remember_win.height() == 0) {
                
                    $remember_win.css({
                        height: 200, 
                        display: 'none'
                    }).slideToggle();
                }else {
                    $remember_win.slideToggle('normal');
                } 
            });
        });
        
        $('#send_remember_login').click(function() {
            $.post('/default/index/remember', {
                email : $('#remember_email').val()
            }, function(data) {
                var json = $.parseJSON(data);
                if(json.result == 'fail') {
                    if(json.reason == 'alreadysent') {
                        $("#content-remember-popup div span.login_error").html('На Ваш E-mail уже отправлено письмо с вашим паролем.');
                    }
                  
                    if(json.reason == 'notfound') {
                        $("#content-remember-popup div span.login_error").html('Уважаемый посетитель, будьте внимательны! Пользователь с указанным E-mail не зарегистрирован. Попробуйте еще раз!');
                    }
                }
            
                if(json.result == 'success') {
                    $("#content-remember-popup div").html('<span style="color: green;">На Ваш E-mail отправлено письмо с вашим паролем.</span><br /><a href="#" onclick="return false" id="close_1">закрыть</a>');
                    $("#close_1").click(function() {
                        $("#content-remember-popup").slideUp();
                    });
                }
            });
        });
        
        $('a[name="login_again"]').click(function() {
            $remember_win.slideUp('normal', function() {
                $login_win.slideDown('normal');
            });
        });
                
        //events init
        langwee.einit();
    },
    
    //init static events
    einit: function() {
        //handling email fields possible events 
        //from registration form
        $email_reg.click(function(e) {
            if($(this).val() == "введите e-mail") {
                //empty field
                $(this).val('');
                
                //remove class field_error from email field
                //add new bg class
                $(this).removeClass('field_error').addClass('bg');
            }
        }).blur(function() {
            //if email field is empty 
            //restore default text :)
            if($(this).val() == '') {
                $(this).val('введите e-mail');
            }
        }).focus(function() {
            //empty field
            $(this).val('');
            
            //remove class field_error from email field
            //add new bg class
            $(this).removeClass('field_error').
            addClass('bg');
        });
        
        //handling email fields possible events 
        //from login form
        $email_login.click(function(e) {
            if($(this).val() == "Email") {
                //empty field
                $(this).val('');
            }
        }).focus(function() {
            //empty field
            if($(this).val() == "Email") {
                //empty field
                $(this).val('');
            }
        }).blur(function() {
            //if email field is empty 
            //restore default text :)
            if($(this).val() == "") {
                //empty field
                $(this).val('Email');
            }
        });
        
        $login_link.click(function(e) {
            
            //login button positions
            if($login_win.height() == 0) {
                $login_win.css({
                    height: 200, 
                    display: 'none'
                }).slideToggle();
            }else {
                $login_win.slideToggle('normal');
            }
        });
        
        //ajax call to log in
        $send_login.click(function(e) {
            $.post('/login', 
            {
                login_email: $email_login.val(), 
                login_pass: $login_new_passwd_field.val()            
            } ,function(data) {
                if(data.result == 'fail') {
                    $login_error.animate({
                        opacity: 0
                    }, 'slow', function() {
                        $login_error.html(data.message);
                        $login_error.animate({
                            opacity: 100
                        }, 'slow');
                    });
                }
                else if(data.result == 'success') {
                    $login_error.animate({
                        opacity: 0
                    }, 'slow', function() {
                        $login_error.css({
                            color: 'green'
                        });
                        $login_error.html(data.message);
                        $login_error.animate({
                            opacity: 100
                        }, 'slow');
                    });
                    
                    window.location.href = data.redirect;
                }
            }, "json");
        });
        
        //make login password field type "password"
        //when it focused
        langwee.lgn_pass_init();
        
        //make reg password field type "password"
        //when it focused
        langwee.reg_pass_init();
    },
    
    /**
 *
 * init reg form password field
 *
 **/
    reg_pass_init: function() {
        $reg_pass = $("#pass");
        $reg_pass.focus(function(e) {
            $reg_pass_cont.html('<input type="password" name="pass" size="25" value="" id="reg_passw" class="bg" />');
            
            $reg_new_passwd_field = $('#reg_passw');
            $reg_new_passwd_field.focus();
            
            $reg_new_passwd_field.blur(function() {
                if($(this).val() == "") {
                    $reg_pass_cont.html('<input type="text" name="pass" size="25" value="придумайте пароль" id="pass" class="bg" />');
                    langwee.reg_pass_init();
                }
            });
            
        })
    },
    
    /**
 *
 * init login form password field 
 *
 **/
    lgn_pass_init: function() {
        $pass_login = $("#login_pass");
        
        $pass_login.focus(function(e) {
            $pass_login_cont.html('<input type="password" name="pass" size="25" value="" id="passw" class="bg" />');
            
            $login_new_passwd_field = $('#passw');
            $login_new_passwd_field.focus();
            
            $($login_new_passwd_field.selector).keydown(function(e){
                if(e.which == 13) {
                    $.post('/login', 
                    {
                        login_email: $email_login.val(), 
                        login_pass: $login_new_passwd_field.val()            
                    } ,function(data) {
                        if(data.result == 'fail') {
                            $login_error.animate({
                                opacity: 0
                            }, 'slow', function() {
                                $login_error.html(data.message);
                                $login_error.animate({
                                    opacity: 100 
                                }, 'slow');
                            });
                        }
                        else if(data.result == 'success') {
                            $login_error.animate({
                                opacity: 0
                            }, 'slow', function() {
                                $login_error.css({
                                    color: 'green'
                                });
                                $login_error.html(data.message);
                                $login_error.animate({
                                    opacity: 100
                                }, 'slow');
                            });
                    
                            window.location.href = data.redirect;
                        }
                    }, "json");
                }
                
            });
            
            $login_new_passwd_field.blur(function() {
                if($(this).val() == "") {
                    $pass_login_cont.html('<input type="text" name="pass" size="25" value="Пароль" id="login_pass" class="bg" />');
                    langwee.lgn_pass_init();
                }
            });
        })
    }
}

