var NOTIFICATION = {
    lingua_id: -1,
    timeout: null,
    type: false,
    paid: false,
    shown: false,
    credit: 0,
    
    run: function(lingua_id) {
        NOTIFICATION.lingua_id = lingua_id;
        NOTIFICATION.timeout = setTimeout(NOTIFICATION.get, 3000);
    },
    
    get: function() {
        $.post('/community/community/notification-get', NOTIFICATION.processing);
    },
    
    processing: function(data) {
        var json = $.parseJSON(data);
                        
        var content = json.data;
        
        var $credit = $('div.header_menu2_credit h1');
        
        var credit = parseInt($credit.text());
        
        if(credit != parseInt(json.credit) && parseInt(NOTIFICATION.paid) == 0) {
            $credit.animate({
                top: "-" + 70 + 'px'
            }, 800, function(){
                $credit.text(json.credit);
                $credit.css('top', 50); 
                
                $credit.animate({
                    top: 0
                }, 800); 
            });
        }
         
        if(content.empty!=undefined) {
            NOTIFICATION.hide();
            return;
        }          
         
        NOTIFICATION.paid = json.paid;
        NOTIFICATION.credit = json.credit;
        NOTIFICATION.fill(content, json.type);
    },
    
    stop: function() {
        clearTimeout(NOTIFICATION.timeout);
    },
   
    fill: function(data, type) { 
        if(NOTIFICATION.shown) return;
        
        $('#popup').html(data);
        
        NOTIFICATION.type = type;
        $('li.' + type + ' a').removeClass('notifications');
        $('li.' + type + ' a').addClass('shown');
        
        $('#popup').fadeIn('slow');
        NOTIFICATION.init();
        NOTIFICATION.shown = true;
    },
    
    init: function() {
        $('a.approve').click(function() {
            NOTIFICATION.hide();
        });
        
        $('a.reject').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('href');
            
            $.post('/community/community/notification-reject/id/' + id, function(data) {
                NOTIFICATION.hide();
            });
        });
        
        $('a.close').click(function(e){
            e.preventDefault();
            NOTIFICATION.close($(this).attr('href'));
        }); 
    }, 
    
    close: function(id) { 
        $.post('/community/community/notification-got/id/' + id, function(data) {
            var $r = $.parseJSON(data);
            if($r.result) {
                NOTIFICATION.hide();
            } 
        });
    },
    
    hide: function() {        
        NOTIFICATION.shown = false;
        NOTIFICATION.stop();
        
        $('li.' + NOTIFICATION.type + ' a').addClass('notifications');
        $('li.' + NOTIFICATION.type + ' a').removeClass('shown');
                    
        $('#popup').fadeOut('slow', function() {
            NOTIFICATION.timeout = setTimeout(NOTIFICATION.get, 5000);
        });
        
        NOTIFICATION.shown = false;
    }
}

