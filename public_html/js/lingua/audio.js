var $words_block;
var $hide = $('div.conten-middle-right-side-transl a.hide');
var $translate = $('div.conten-middle-right-side-transl-block2-txt p');
var $translate_div = $('div.conten-middle-right-side-transl-block2');
var $original_div = $('div.conten-middle-right-side-transl-block1');

var AUDIO = {
    audio_id: -1, 
    audio_lang: 'en',
    key: '',
    lingua_id: -1, 
    speech_status: false,
    words: null,

    shown: -1, 
    
    run: function(audio_id, key, lingua_id, audio_lang) {
        DISCUSS.stop = true;
        DISCUSS.shown = false;
        $words_block = $("#scrollcontent-goog");
        AUDIO.audio_id = audio_id;
        AUDIO.key = key;
        AUDIO.lingua_id = lingua_id;
        AUDIO.audio_lang = audio_lang;
        
        $words_block.html("загрузка...");
        
        AUDIO.request('/audio/page/get-user-unknow-words', {
            audio_id: AUDIO.audio_id
        }, AUDIO.userUnknowWordsLoaded);
        
        //init words block scroll
        $("#scroll-goog").jScrollPane({
            animateTo:true, 
            autoReinitialise: true
        });
        
        //init text block scroll
        $("#scroll1").jScrollPane();
        
        var flashvars = {};
        var params = {
            bgcolor: '#4EB1D9',
            scale: 'noScale',
            allowFullScreen: 'false',
            allowScriptAccess: 'always',
            allowNetworking: 'true',
            salign: 'lt'
        };
        var attributes = {
            id : "myFlash"
        };    
        swfobject.embedSWF("/js/mp3player/script.swf", "jpId", "0", "0", "9.0.0", false, flashvars, params, attributes);
        
        //text block rows
        $('div.conten-middle-right-side-transl-block1-txt span.row').each(function(index, element) {
            $(element).mouseover(function(e) {
                $(this).css({
                    'background-color': '#ffde7c', 
                    'border-radius': '3px'
                });
                
                var trg = $('div.conten-middle-right-side-transl-block2-txt span[row="' + $(this).attr('row') + '"]');
                trg.css({
                    'background-color': '#ffde7c', 
                    'border-radius': '3px'
                });
            });
            $(element).mouseout(function(e) {
                $(this).css({
                    'background-color': ''
                });
                
                var trg = $('div.conten-middle-right-side-transl-block2-txt span[row="' + $(this).attr('row') + '"]');
                trg.css({
                    'background-color': ''
                });
            });
        });
        
        //text click init
        $('span.row a').each(function(index, element) {
            $(element).unbind('click');
            $(element).click(function(e) {
                e.preventDefault();
                
                if(DISCUSS.shown) {
                    AUDIO.run(AUDIO.audio_id, AUDIO.key, AUDIO.lingua_id, AUDIO.audio_lang);
                    DISCUSS.shown = false;
                    return;
                }
                
                var txt = $(this).text().toLowerCase();
                var b = false;
                $.each(AUDIO.words, function(key, value) {
                    //alert(value.word_src+ '|' + $(this).text().toLowerCase());
                    if(value.word_src.toLowerCase() == txt) {
                        b = true;
                    }
                });
                
                if(b) return false;
                
                var parent = $(this).parent().get(0);
                var context = $(parent).text();
                
                AUDIO.request('/audio/page/word-add', {
                    word: $(this).text(), 
                    context: context, 
                    audio_id: AUDIO.audio_id, 
                    audio_lang: AUDIO.audio_lang,
                    key: AUDIO.key
                },
                AUDIO.wordAdded);
            });
        });
        
        $("#examine").click(function(e) {
            e.preventDefault();
            
            AUDIO.examine();
        });
        
        $('div.conten-middle-right-side-transl-block2-txt a.showAll').click(function(){
            $.post('/audio/page/audio-get-translation', {
                audio_id: AUDIO.audio_id
            }, function(data) {
                var json = $.parseJSON(data);
                
                if(json.result == 'fail') {
                    if(json.reason == 'showMessage') {
                        AUDIO.showMessage(json.message, json.action);
                        return;
                    }
                }
                
                var words = json.data;
                var rowhtml = '';
                var row = 0;
                $.each(words, function(key, value) {
                    rowhtml += "<span row='" + row + "' class='row' style='padding: 1px; width: 100%; display: block;'>";
                    $.each(value, function(k, v) {
                        if(v.fchar!=undefined) {
                            rowhtml+=v.fchar;
                        }
                        rowhtml += v.word;
                        if(v.lchar!=undefined) {
                            rowhtml+=v.lchar;
                        }else {
                            rowhtml += ' ';
                        }
                    });
                    rowhtml += "</span>"
                    row++;
                });
                $('div.conten-middle-right-side-transl-block2-txt p').html(rowhtml);
            });
        });
        
        $hide.click(function(e) {
            e.preventDefault();
            
            if($translate.is(':visible')) {
                $translate.fadeOut();
                $translate_div.height($original_div.height());
                $(this).text('раскрыть перевод');
            }
            else {
                $translate_div.height($original_div.height());
                $translate.fadeIn('1200');
                $(this).text('скрыть перевод');
            }
        });
    },
    
    examine: function() {
        var $win = window.open('/audio/page/audio-examine/audio_id/' + AUDIO.audio_id, 'Проверь себя','width=800,height=350')
    },
    
    wordAdded: function(data) {
        var result = $.parseJSON(data);
        if(result.result == 'fail') {
            if(result.reason == 'freeCount') {
                // alert("На бесплантом аккаунте доступны только 30 слов");
                return;
            }
            
            if(result.reason == 'showMessage') {
                AUDIO.showMessage(result.message, result.action);
                return;
            }
            
            
        }
        if(result.result == 'success') {
            $('a.words').each(function(index, element) {
                if($(element).text().toLowerCase() == result.word_src.toLowerCase()) {
                    $(element).addClass("selected");
                }
            });
            
            var new_word = {
                user_word_id : result.user_word_id, 
                word_src: result.word_src, 
                word_trg: result.word_trg
            };
            
            AUDIO.words[AUDIO.words.length] = new_word;
            
            var html = AUDIO.drawWords(result);
            $('div.conten-middle-right-side-google-block-txt').append(html);
            
            html = AUDIO.appendWordDetails(result);
            $('#word_' + result.user_word_id + ' div.conten-middle-right-side-google-block-scr-cont').remove();
            $('#word_' + result.user_word_id).append(html);
            $('#word_' + result.user_word_id).append('<div style="clear: both"></div>');
            
            html = AUDIO.appendWordDetails(result);
            
            var detail_block = $('#word_' + result.user_word_id + ' div.conten-middle-right-side-google-block-scr-cont');
                
            
            $('#word_' + AUDIO.shown + ' div.conten-middle-right-side-google-block-scr-cont').toggle('slow', function(){
                $("#scroll-goog").data('jsp').reinitialise();
                $("#scroll-goog").data('jsp').scrollTo(0, 500000, {
                    animateTo:true
                });
            });
            detail_block.fadeIn('slow', function(){
                AUDIO.init();
                $("#scroll-goog").data('jsp').reinitialise();
                $("#scroll-goog").data('jsp').scrollTo(0, 500000, {
                    animateTo:true
                });
            });
            
            detail_block.find('a.google_translate').click(function(e){
                e.preventDefault();
                    
                var $win = window.open('http://translate.google.com/#' + AUDIO.audio_lang + '|ru|'+result.word_src,'Google','width=800,height=350');
            });
                
            detail_block.find('a.lingvo_translate').click(function(e){
                e.preventDefault();
                var $win = window.open('http://lingvopro.abbyyonline.com/ru/Search/' + AUDIO.audio_lang + '-ru/'+result.word_src,'Lingvo','width=800,height=600');
            });
                
            detail_block.find('a.speak').click(function(e){
                e.preventDefault();
                AUDIO.speak(result.user_word_id);
            });
                
            AUDIO.shown = result.user_word_id;
        }
    },
     
    showMessage: function(message, action) {
        $mask = $("#mask");
        $dialog = $("#message");
        
        $mask.css({
            left:0, 
            top:0, 
            width: $(document).width()+'px', 
            height: $(document).height()+"px"
        });
        
        $dialog.find('.body').css({
            height: (200-24)+'px'
        })
        $dialog.css({
            height: 200+'px'
        });
        
        var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
        var hPos = ($(document).width()-$dialog.width())/2;
        
        $dialog.css({
            left: hPos, 
            top: vPos
        });
        
        message+="<br /><br /><a href='/payment' class='button'>Получить Premium!</a>";
        message+="<a href='' class='button'>Добыть звезды!</a>";
        
        $('p.text', $dialog).html(message);
            
        $mask.show();
        $dialog.fadeIn();
        
        $(document).unbind('scroll');
        $(document).scroll(function() {
            var vPos = ($(window).height()-$dialog.height())/2 + $(document).scrollTop();
            var hPos = ($(document).width()-$dialog.width())/2;
        
            $dialog.css({
                left: hPos, 
                top: vPos
            });
        });
        
        $('a.close', $dialog).unbind('click');
        $('a.close', $dialog).click(function(e) {
            e.preventDefault();
            $mask.fadeOut();
            $dialog.fadeOut();
            $.post('/audio/page/close-message', {
                action_name: action
            });
        });
    },
    
    request: function(url, data, callback) {
        $.post(url, data, callback);
    },
    
    speak: function(id) {
        AUDIO.callExternalInterface(id);
    },
    
    init: function() {
        $('div.conten-middle-right-side-google-block-txt img').each(function(index, element){
            $(element).unbind('click');
            
            $(element).click(function(e) {
                if(AUDIO.speech_status) return;
                
                AUDIO.speech_status = true;
                var data = this.id.split('_');
                var id = (data[1] != undefined) ? data[1] : -1;
                
                AUDIO.speak(id);
            });
        }); 
        
        $('b.translated_word').each(function(index, element) {
            $(element).css('cursor', 'pointer');
            
            $(element).unbind('click');
            $(element).click(function() {
                var str_id = element.id.split('_');
                var id = str_id[1] != undefined ? str_id[1] : -1;
                
                var word = AUDIO.wordFindById(id);
                var html = '';
                if(word != null) html = AUDIO.appendWordDetails(word);
                
                if(id == AUDIO.shown) return;
                
                $('#word_' + id + ' div.conten-middle-right-side-google-block-scr-cont').remove();
                $('#word_'+id).append(html);
                $('#word_'+id).append('<div style="clear: both"></div>');
                
                var detail_block = $('#word_' + id + ' div.conten-middle-right-side-google-block-scr-cont');
                
                if(AUDIO.shown > id) {
                    $('#word_' + AUDIO.shown + ' div.conten-middle-right-side-google-block-scr-cont').toggle('slow');
                    detail_block.fadeIn('slow');
                }
                else {
                    detail_block.fadeIn('slow');
                    $('#word_' + AUDIO.shown + ' div.conten-middle-right-side-google-block-scr-cont').toggle('slow');
                }
                
                detail_block.find('a.google_translate').click(function(e){
                    e.preventDefault();
                    
                    var $win = window.open('http://translate.google.com/#' + AUDIO.audio_lang + '|ru|'+word.word_src,'Google','width=800,height=350');
                });
                
                detail_block.find('a.lingvo_translate').click(function(e){
                    e.preventDefault();
                    var $win = window.open('http://lingvopro.abbyyonline.com/ru/Search/' + AUDIO.audio_lang + '-ru/'+word.word_src,'Lingvo','width=800,height=600');
                });
                
                detail_block.find('a.speak').click(function(e){
                    e.preventDefault();
                    AUDIO.speak(id);
                });
                
                AUDIO.shown = id;
            });
        });
    },   
    
    //--------------------------------------------------------------------------
    wordFindById: function(id) {
        var word = null;
        $.each(AUDIO.words, function(key, value) {
            if(id == value.user_word_id) word = value;
        });  
        
        return word;
    },
    
    //--------------------------------------------------------------------------
    appendWordDetails: function(word) {        
        var html = '<div class="conten-middle-right-side-google-block-scr-cont">' +
        '<div class="conten-middle-right-side-google-block-scr-cont-txt-trans-word">'+
        '<p><span id="text_src" class="text_src">' + word.word_src + '</span><nobr><span id="text_trg" class="text_trg">-' + word.word_trg + '</span></nobr></p>'+
        '</div>'+
        '<div class="conten-middle-right-side-google-block-scr-cont-txt" style="height: 10px; display: block;"></div>' + 
        '<div class="conten-middle-right-side-transl-bottom">' + 
        '<div class="conten-middle-right-side-transl-bottom1">' +
        '<p>Словари:</p>' + 
        '<a href="javascript:void(0)" class="google_translate" id="google_translate"><img src="/images/video/page/slovari-goog-ico.png" alt="учись со звездами" /></a>' + 
        '<a href="javascript:void(0)" class="lingvo_translate" id="lingvo_translate"><img src="/images/video/page/slovari-abby-ico.png" alt="учись со звездами" /></a>' + 
        '</div>'+
        '<div class="conten-middle-right-side-transl-bottom2"></div>'+
        '<div class="conten-middle-right-side-transl-bottom3">' + 
        '<img src="/images/video/page/slovari-izmen-perev-img.png" alt="Изменить перевод" />' +                                							
        '<span>' + /*<a href="#">Изменить перевод</a></span>*/  
        '</div>' + 
        '<div class="conten-middle-right-side-transl-bottom4">'+
        '<img src="/images/video/page/slovari-praiznesti-img.png" alt="Произнести" />'+
        '<span><a href="#" class="speak">Произнести</a></span>'+                                         
        '</div>' + 
        '</div></div>';
        
        return html;
    },
    
    //--------------------------------------------------------------------------
    userUnknowWordsLoaded: function(data) {
        var json = $.parseJSON(data);
        
        AUDIO.words = json.data;
        
        var html =  '<div class="conten-middle-right-side-google-block-txt">';
        $.each(AUDIO.words, function(key, value) {
            html += AUDIO.drawWords(value);
        });
        html += '</div>';
        
        $words_block.fadeOut('slow', function() {
            $words_block.html(html);
            
            $words_block.fadeIn('slow', function() {
                AUDIO.init();
            });
        });
            
    },
    
    drawWords: function(word) {
        var html = '<div id="word_' + word.user_word_id + '"><img src="/images/video/page/1330288171_audio-volume-high.png" id="ozvuchka_' + word.user_word_id + '" />'+
        '<p id="p_' + word.user_word_id + '">' + 
        '<b class="translated_word" id="trwordsrc_' + word.user_word_id + '">' + word.word_src + '</b> <span> - </span>' + 
        '<span id="trwordtrg_' + word.user_word_id + '">' + word.word_trg + '</span></p></div>';
        
        return html;
    },
    
    callExternalInterface: function(path) 
    {
        var flashObj = AUDIO.getFlashMovieObject("myFlash");
        flashObj.play_sound(path);
        AUDIO.speech_status = false;
    },
            
    getFlashMovieObject: function(movieName){
        if (window.document[movieName]){
            return window.document[movieName];
        }
        if (navigator.appName.indexOf("Microsoft Internet")==-1){
            if (document.embeds && document.embeds[movieName])
                return document.embeds[movieName];
        }
        else{
            return document.getElementById(movieName);
        }
    }
}


var DISCUSS = {
    audio_id: -1,
    discuss_last_id: 0,
    stop: false,
    shown: false,
    requested: false,
    timeout: null,
     
    run: function(audio_id) {
        if(DISCUSS.shown) return;
        
        DISCUSS.audio_id = audio_id;
        DISCUSS.stop = false;
        DISCUSS.shown = true;
        $('#scrollcontent-goog').html("загрузка...");
        
        DISCUSS.request('/audio/page/discuss', {
            audio_id: DISCUSS.audio_id
        }, DISCUSS.discussLoaded);
        
    },
    
    request: function(url, data, callback) {
        $.post(url, data, callback);
    },
    
    init: function() {
        $('textarea.discuss_text').keyup(function(e){
            if(e.which == 13) {
                var val = $(this).val();
                $(this).val('');
                DISCUSS.request('/audio/page/discuss-add', {
                    text: val, 
                    audio_id: DISCUSS.audio_id
                }, DISCUSS.disscussUpdate);
            }
        });
        
        $('#post').click(function(e){
            e.preventDefault();
            var val = $('textarea.discuss_text').val();
            $('textarea.discuss_text').val('');
            
            DISCUSS.request('/audio/page/discuss-add', {
                text: val, 
                audio_id: DISCUSS.audio_id
            }, DISCUSS.disscussUpdate);
        });
        
        DISCUSS.discussRemoveInit();
    },
    
    disscussStopUpdate: function() {
        DISCUSS.stop = true;
    },
    
    disscussUpdate: function() {
        if(DISCUSS.stop || DISCUSS.requested) return;
        
        clearTimeout(DISCUSS.timeout);
        DISCUSS.requested = true;
        DISCUSS.request('/audio/page/discuss-updates', {
            id: DISCUSS.discuss_last_id, 
            audio_id: DISCUSS.audio_id
        }, DISCUSS.disscussUpdateDraw);
    },
    
    discussLoaded: function(data) {
        var json = $.parseJSON(data);
        
        var html = '';
        html += DISCUSS.discussDrawForm(json.data.own_photo);
        html += DISCUSS.discussDraw(json.data);
        
        $words_block.fadeOut('slow', function() {
            $words_block.html(html);
            
            $words_block.fadeIn('slow', function() {
                DISCUSS.init();
                DISCUSS.timeout = setTimeout(DISCUSS.disscussUpdate, 5000);
            });
        });
    },
    
    disscussUpdateDraw: function(data) {
        if(DISCUSS.stop) return;
        var json = $.parseJSON(data);
        var html = '';
        html += DISCUSS.discussDraw(json.data);
        
        DISCUSS.requested = false;
        
        $('div#discussions').prepend(html);
        DISCUSS.discussRemoveInit();
        DISCUSS.timeout = setTimeout(DISCUSS.disscussUpdate, 5000);
    },
    
    discussRemoveInit: function(){
        $('a.remove_discuss').each(function(index, element) {

            $(element).unbind('click');
            $(element).click(function(e){
                e.preventDefault();
                
                if(!confirm('Вы уверены?')) {
                    return;
                }
                var str_id = element.id.split('_');
                var id = str_id[1] != undefined ? str_id[1] : -1;
                DISCUSS.discussRemove(id);
            });
        });
    },
    
    discussRemove: function(id) {
        DISCUSS.request('/audio/page/discuss-remove', {
            discuss_id: id
        }, DISCUSS.discussRemoved);
    },
    
    discussRemoved: function(data) {
        var result = $.parseJSON(data);
        $("#discuss_" + result.discuss_id).slideUp();
    },
    
    discussDrawForm: function(own_photo) {
        var form_html = '<table style="border-bottom: 1px dashed #e6a94e;margin-bottom:-10px;">'+
        '<tr>'+
        '<td valign="top">'+
        '<img src="' + own_photo + '" title="Учись со звездами" alt="Учись со звездами"  width="60" />'+
        '</td>'+
        '<td>'+
        '<textarea class="discuss_text"></textarea><br />'+
        '<a id="post" href="#" style="float: right;">'+
        '<img alt="учись со звездами" src="/images/video/page/video-page-post.png">'+
        '</a>'+
        '</td>'+
        '</tr>'+
        '</table><br />'+'<div id="discussions"></div>';
        
        return form_html;
    },
    
    discussDraw: function(data) {
        var html = '';
        var max = 0;
        $.each(data, function(key, value) {
            if(value.audio_comment_id == undefined) return;
            if(key == 0) {
                max = value.audio_comment_id;
            }
            
            html += '<div class="discuss" id="discuss_' + value.audio_comment_id + '">' + 
            '<table width="100%">' + 
            '<tr valign="top">'+
            '<td valign="top" style="height:auto; width: 65px">' + 
            '<img src="' + value.photo + '" width="60" title="Учись со звездами" alt="Учись со звездами" />' + 
            '</td>' + 
            '<td valign="top" style="border-left: 1px solid #e6a94e;padding-left:10px;min-width:300px;">' + 
            '<span style="font-size: 10px;min-width:295px;height:auto;">' + 
            '<a href="/user/' + value.lingua_id + '" target="_blank" style="font-size:12px;color:#15669a;margin-right:10px;float:left;" >' + value.nickname + '</a>';

            if(value.owner == true) {
                html += '<a href="#" id="a_' + value.audio_comment_id + '" class="remove_discuss"><img src="/images/video/page/remove.png" style="float: right" /></a>';
            }
           
            html += '<p style="color:#725432;font-size: 9px;">' + value.create_ts + '</p>' + 
            '</span>' + 
            '<p style="color:#725432;margin-top:-17px;border-top: 1px dashed #e6a94e;padding-top:5px;height:auto;font-size:11px;">' + value.comment + '<p>' + 
            '</td>' + 
            '</tr>' + 
            '</table>' + 
            '</div>';
        
            if(parseInt(max) <= parseInt(value.audio_comment_id)) {
                max = value.audio_comment_id;
                DISCUSS.discuss_last_id = max;
                
            }
        });
        
        return html;
    }
}