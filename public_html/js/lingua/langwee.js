var LANGWEE = {
    run: function() {
        /* Yandex.Metrika counter */
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter14186416 = new Ya.Metrika({
                        id:14186416, 
                        enableAll: true, 
                        webvisor:true
                    });
                } catch(e) {}
            });

            var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
        /*Yandex.Metrika counter*/
        
        var $tdd = $('#training_drob_menu'); //training drop down
        var $cdd = $('#div_drob_menu'); //community drop down
        
        
        //community menu drop downing
        $('#li_m4').click(function(){
            if(!$cdd.is(':visible')) {
                $tdd.slideUp();
                $cdd.slideDown();
            }else {
                $cdd.slideUp();
            }
        });
        
        //trainings menu drop downing
        $('#li_m2').click(function(){
            if(!$tdd.is(':visible')) {
                $cdd.slideUp();
                $tdd.slideDown();
            }else {
                $tdd.slideUp();
            }
        });
        
        
        
    }
}