var mdiv = $('#scrollcontent');
var trackbar = $('.trackbar');
var line = $('.line');

(function($){ 
    $.fn.extend({  
        limit: function(limit,element) {
			
            var interval, f;
            var self = $(this);
					
            $(this).focus(function(){
                interval = window.setInterval(substring,100);
            });
			
            $(this).blur(function(){
                clearInterval(interval);
                substring();
            });
			
            substringFunction = "function substring(){ var val = $(self).val();var length = val.length;if(length > limit){$(self).val($(self).val().substring(0,limit));}";
            if(typeof element != 'undefined')
                substringFunction += "if($(element).html() != limit-length){$(element).html((limit-length<=0)?'0':limit-length);}"
				
            substringFunction += "}";
			
            eval(substringFunction);
            substring();
			
        } 
    }); 
})(jQuery);

var CHAT_ROOM = {
    id: -1,
    sound: true,
    tout: null,
    msg_id: 0,
    status: 'online',
    istatus: true,
    sa_timeout: null,
    lingua_id: -1,
    
    run: function(id, key, lingua_id) {
        CHAT_ROOM.id = id;
        // CHAT_ROOM.msg_id = msg_id;
        CHAT_ROOM.lingua_id = lingua_id;
        $("select").uniform();
        
        //init T, Smiles buttons drop downs
        $('#Т_img').click(function(){
            $('#div_drob-smil').slideUp('normal', function() {
                $('#div_drobТ').slideToggle('slow');
            });
        });
                
        $('#smilik_img').click(function(){
            $('#div_drobТ').slideUp('normal', function(){
                $('#div_drob-smil').slideToggle('slow');
            });
        });
        
        //init right phrases div drop down
        $('#li_m4').toggle(function(){
            $('#div_drob_menu').slideDown();
        }, function(){
            $('#div_drob_menu').slideUp();
        });
        
        $('#scen_img').toggle(function(){
            $('#div_drob').slideDown();
        }, function(){
            $('#div_drob').slideUp();
        });
        
        //init messages block scroll
        $("#scroll1").jScrollPane({
            animateTo:true, 
            autoReinitialise: true
        });
        
        //init smiles
        $(".smile").each(function(index, element) {
            $(element).click(function(e){
                e.preventDefault();
                
                var $msg = $('#msg').val();
                $msg += "::" + $(this).attr('title') + ":: ";
                $('#msg').val($msg);
            });
        });
        
        //init send button, key up event in the input
        $('input[name="send"]').click(function(e) {
            CHAT_ROOM.say();
        });
        
        $('#msg').keyup(function(e) {
            if(e.which == 13) {
                CHAT_ROOM.say();
            }
        });
        
        $('.online-state').click(CHAT_ROOM.change_status);
        $('.offline-state').click(CHAT_ROOM.change_status);
        
        var p = line.position();
        trackbar.css('left', p.left + 24 + trackbar.width() / 2);
        CHAT_ROOM.start();
                 
        TRANSLATE.run(key);
        $('.src_word').limit('70', '#charLeft'); 
        
        var flashvars = {};
        var params = {
            bgcolor: '#4EB1D9',
            scale: 'noScale',
            allowFullScreen: 'false',
            allowScriptAccess: 'always',
            allowNetworking: 'true',
            salign: 'lt'
        };
        var attributes = {
            id : "myFlash"
        };    
        swfobject.embedSWF("/js/mp3player/script.swf", "jpId", "0", "0", "9.0.0", false, flashvars, params, attributes);
    },
    
    change_status: function(e) {
        e.preventDefault();
        
        var className = $(this).attr('class');
        var status;
        if(className == "online-state") {
            status = 'online';
        }else {
            status = 'offline';
        }
        
        $.post('/community/community/chat-change-status', {
            status: status,
            chat_id: CHAT_ROOM.id
        }, function(data){
            var json = $.parseJSON(data);
            if(json.status == CHAT_ROOM.status) return;
            
            CHAT_ROOM.status = json.status;
            var line_pos = trackbar.position();
            if(CHAT_ROOM.status == "offline") trackbar.css({
                'left' : line_pos.left - line.width() + trackbar.width()/2 ,
                'position': 'absolute'
            });
            if(CHAT_ROOM.status == "online") trackbar.css({
                'left' : line_pos.left + line.width() - trackbar.width()/2 ,
                'position': 'absolute'
            });
        });
    },
    
    start: function() {
        CHAT_ROOM.tout = setTimeout(CHAT_ROOM.get, 4000);
    },
    
    stop: function() {
        clearTimeout(CHAT_ROOM.tout);
    },
    
    switchSound: function() {
        CHAT_ROOM.sound = CHAT_ROOM.sound ? false : true;
        
        if(CHAT_ROOM.sound) {
            $("#ITA").css('background-image', 'url(/images/community/chatting-txt-sound-onn-but-icon.png)');
        }
        else {
            $("#ITA").css('background-image', 'url(/images/community/chatting-txt-sound-off-but-icon.png)');
        }
    },
    
    say: function() {
        if($('#msg').val() == "") return;
        
        $.post('/community/community/say/chat/' + CHAT_ROOM.id, {
            message: $('#msg').val()
        },function() {
            $('#msg').val('');
            CHAT_ROOM.stop();
            CHAT_ROOM.get();
        });
    },
    
    get: function() {
        $.post('/community/community/messages-get/chat/' + CHAT_ROOM.id, {
            msg_id: CHAT_ROOM.msg_id
        } , CHAT_ROOM.show);
    },
    
    show: function(data) {
        var json = $.parseJSON(data);
        
        if(json.data.status != CHAT_ROOM.istatus) {
            CHAT_ROOM.istatus = json.data.status;
            var status;
            
            if(!CHAT_ROOM.istatus) {
                status = 'n/a';
            }
            else {
                status = 'a/v';
            }
            
            clearInterval(CHAT_ROOM.sa_timeout);
            
            $('p.online_state').fadeOut('slow', function(){
                $('p.online_state').html(status);
                $('p.online_state').fadeIn('normal', function() {
                    if(!CHAT_ROOM.istatus) {
                        CHAT_ROOM.sa_timeout = setInterval(CHAT_ROOM.status_animate, 1000);
                    }
                    else {
                        if(CHAT_ROOM.sa_timeout) {
                            clearInterval(CHAT_ROOM.sa_timeout);
                        }
                    }
                });    
            });
        }
        
        if(json.data.empty != undefined) {
            CHAT_ROOM.start();
            return;
        } 
        
        var html = '';
        var isMy = true; 
        $.each(json.data.messages, function(key, value){
            html += '<table style="width: 700px"><tr valign="top"><td style="width: 60px"><a href="/user/' + value.lingua_id + '" target="_blank">'+
            value.nickname + '</a>:&nbsp;</td><td style="width: 525px"><p>'+
            value.message + '</p></td>' + 
            '<td style="width: 115px"><i style="font-size: 10px; float: right;">' + value.create_ts + '</i></td></tr></table>';
            
            CHAT_ROOM.msg_id = value.message_id;
            if(value.lingua_id != CHAT_ROOM.lingua_id && isMy) {
                isMy = false;
            }
        });
        
        if(CHAT_ROOM.sound && !isMy) {
            var media = '/data/default/sound.mp3';
            CHAT_ROOM.callExternalInterface(media); 
        }
        
        var h = mdiv.html();
        mdiv.html(h + html); 
        CHAT_ROOM.start();
        
        $("#scroll1").data('jsp').reinitialise();
        $("#scroll1").data('jsp').scrollTo(0, 500000, {
            animateTo:true
        });
    },
    
    status_animate: function() {
        $('div.conten-middle-center-chat-left-1block-rgt-foto-av').animate({
            opacity: 0.25
        }, 400, function(){
            $(this).animate({
                opacity: 1
            }, 400);
        });
    },
    
    callExternalInterface: function(path) 
    {
        var flashObj = CHAT_ROOM.getFlashMovieObject("myFlash");
        flashObj.play_url(path);
        CHAT_ROOM.speech_status = false;
    },
    
    getFlashMovieObject: function(movieName){
        if (window.document[movieName]){
            return window.document[movieName];
        }
        if (navigator.appName.indexOf("Microsoft Internet")==-1){
            if (document.embeds && document.embeds[movieName])
                return document.embeds[movieName];
        }
        else{
            return document.getElementById(movieName);
        }
    }
} 

var TRANSLATE = {
    src: '', 
    trg: '',
    type: '',
    key: '',
    
    run: function(key) {
        TRANSLATE.key = key;
        
        $("#src").click(TRANSLATE.sel_lang);
        $("#trg").click(TRANSLATE.sel_lang);
         
        $("#languages img").each(function(index, element) {
            $(element).css('cursor', 'pointer');
            $(element).unbind('click');
             
            $(element).click(function(e) {
                e.preventDefault();
                 
                $("a#" + TRANSLATE.type).text($(this).attr('title'));
                
                if(TRANSLATE.type == 'src') {
                    TRANSLATE.src = $(this).attr('short');
                }
                
                if(TRANSLATE.type == 'trg') {
                    TRANSLATE.trg = $(this).attr('short');
                }
                
                $("#languages").slideUp('slow', function() {
                    $('#trg').css('background-color', '');
                    $('#src').css('background-color', '');
                   
                    if($("#src").text() != "выберите язык" && $("#trg").text() != "выберите язык")
                        $("#translate").fadeIn();
                });
                 
            });
        })
        
        $("#translate").click(function(e) {
            e.preventDefault();
            
            $(this).val('Идет перевод...');
            $(this).attr('disabled', '');
            
            $.post('/community/community/translate/key/' + TRANSLATE.key, {
                text: $(".src_word").val(),
                src: TRANSLATE.src,
                trg: TRANSLATE.trg
            }, function(data){
                $("#translate").removeAttr('disabled');
                $("#translate").val('перевести');
                
                $('#trg_text').fadeOut('slow', function(){
                    $('#trg_text').html(data);
                    $('#trg_text').fadeIn('slow');
                });
            });
        });
    },
    
    sel_lang: function(e) {
        e.preventDefault();
        var t = this.id;
        $("#translate").fadeOut();
        $("#languages").slideUp('normal', function(){
            var p =  $('.lang').position();
            $("#languages").css('top', p.top + $('.lang').height());
            
            if(TRANSLATE.type != t) {
                $('#trg').css('background-color', '');
                $('#src').css('background-color', '');
            }
            
            TRANSLATE.type = t;
                
            if(TRANSLATE.type == 'src') 
            { 
                $("#languages").css('left', 11);
                $('#src').css('background-color', '#EF8B26');
                $('#trg').css('background-color', 'none');
            }
            if(TRANSLATE.type == 'trg') {
                $("#languages").css('left', 102);
                $('#trg').css('background-color', '#EF8B26');
                $('#src').css('background-color', 'none');
            } 
            $("#languages").slideDown('slow');
        });
        
        
    }
}
