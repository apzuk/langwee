var TRAININGS = {
    lingua_id: -1,
    lang: 'en',
    
    run: function(lingua_id, lang) {
        TRAININGS.lingua_id = lingua_id;
        TRAININGS.lang = lang;
        
        $(".conten-middle-lang a").each(function(index, element){
            $(element).unbind('click');
            
            $(element).click(function(e){
                e.preventDefault();
                
                $("a#"+TRAININGS.lang).removeClass('select');
                $("a#"+this.id).addClass('select');
                TRAININGS.lang = this.id;
            });
        });
    }
}