var flashvars = {};
var params = {
    bgcolor: '#4EB1D9',
    scale: 'noScale',
    allowFullScreen: 'false',
    allowScriptAccess: 'always',
    allowNetworking: 'true',
    salign: 'lt'
};
var attributes = {
    id : "myFlash"
};    
swfobject.embedSWF("/js/mp3player/script.swf", "jpId", "0", "0", "9.0.0", false, flashvars, params, attributes);
       
function callExternalInterface(path) 
{
    var flashObj = getFlashMovieObject("myFlash");
    flashObj.play_sound(path);
}
            
function getFlashMovieObject(movieName){
    if (window.document[movieName]){
        return window.document[movieName];
    }
    if (navigator.appName.indexOf("Microsoft Internet")==-1){
        if (document.embeds && document.embeds[movieName])
            return document.embeds[movieName];
    }
    else{
        return document.getElementById(movieName);
    }
    return null;
}