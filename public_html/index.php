<?php

function denis($var, $with_exit = true)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    
    if ($with_exit)
        exit();
}

error_reporting(E_ERROR);
ini_set('display_errors', 'on');

define('APPLICATION_PATH', realpath(dirname(__FILE__)) . '/../application');
define('PUBLIC_PATH', realpath(dirname(__FILE__)));
define('PRIVATE_PATH', realpath(dirname(__FILE__)) . '/../private');
define('APPLICATION_ENVIOREMNT', 'development');
define('CONFIGS', realpath(dirname(__FILE__)) . '/../private/configs');

define('PROD_INI', CONFIGS . '/../private/prod.ini');
define('APPLICATION_INI', CONFIGS . '/application.ini');

set_include_path(
        APPLICATION_PATH . '/../library'
        . PATH_SEPARATOR . get_include_path()
);

require_once 'Zend/Loader.php';
Zend_Loader::registerAutoload();
//echo 11;
try {
    require '../application/bootstramp.php';
    
} catch (Exception $e) {

    echo "<html><body>An exception occured while bootstrapping the application";

    //if (defined(APPLICATION_ENVIOREMNT) && APPLICATION_ENVIOREMNT != 'production') {
    echo $e->getMessage();
    echo '<br /><br />' . $e->getMessage() . '<br />' .
    '<div align="left">Stack Trace: ' .
    '<pre>' . $e->getTraceAsString() . '</pre></div>';
    //}

    echo '</body></html>';
    exit(1);
}